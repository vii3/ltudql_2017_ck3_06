/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP (1000)
       [MaDoiBong]
	  ,[Hang]
	  ,[TongDiem]
	  ,[TongHieuSo]
	  ,[TongSoBanThang]
	  ,[SoBanThang]
      ,[SoBanThua]
	  ,[TongSoBanThua]
      ,[SoTranDaThiDau]
      ,[TongSoTranThang]
      ,[TongSoTranThua]
      ,[TongSoTranHoa]
      ,[Diem]
      ,[HieuSo]
  FROM [QuanLyGiaiVoDichQuocGiaDB].[dbo].[SoLieuThongKeDoiBong]
  where MaVongDau = 'VD003' and SoTranDaThiDau != '10'
  order by Hang asc

  SELECT TOP (1000) [MaMuaGiai]
      ,[MaTranDau]
      ,[MaVongDau]
      ,[NgayThiDau]
      ,[GioThiDau]
      ,[ChuNha]
      ,[Khach]
      ,[DaThiDau]
      ,[SoBanThang_ChuNha]
      ,[SoBanThang_Khach]
  FROM [QuanLyGiaiVoDichQuocGiaDB].[dbo].[ChiTietTranDau]

  SELECT TOP (10) [MaMuaGiai]
      ,[MaVongDau]
      ,[MaCauThu]
      ,[MaDoiBong]
      ,[SoBanThang]
      ,[TongSoBanThang]
      ,[Hang]
  FROM [QuanLyGiaiVoDichQuocGiaDB].[dbo].[SoLieuThongKeCauThu]
  where TongSoBanThang != 0
  order by Hang asc, MaVongDau

  SELECT * FROM MuaGiai

  SELECT * FROM LoaiCauThu