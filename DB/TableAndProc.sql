
CREATE TABLE [dbo].[CauThu](
	[MaCauThu] [varchar](10) NOT NULL,
	[HoTenCauThu] [nvarchar](50) NULL,
	[NgaySinh] [date] NULL,
	[MaLoaiCauThu] [int] NULL,
	[MaDoiBong] [varchar](10) NULL,
	[TongSoBanThang] [int] NULL,
	[DaXoa] [bit] NULL,
 CONSTRAINT [PK_CauThu] PRIMARY KEY CLUSTERED 
(
	[MaCauThu] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CauThuThuocDoiBongThamGiaGiaiDau]    Script Date: 1/8/2020 7:47:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CauThuThuocDoiBongThamGiaGiaiDau](
	[MaGiaiDau] [varchar](10) NOT NULL,
	[MaDoiBong] [varchar](10) NULL,
	[TenDoiBong] [nvarchar](50) NULL,
	[MaCauThu] [varchar](10) NOT NULL,
	[TenCauThu] [nvarchar](50) NULL,
	[MaLoaiCauThu] [int] NULL,
	[TongSoBanThang] [int] NULL,
 CONSTRAINT [PK_CauThuThuocDoiBongThamGiaGiaiDau] PRIMARY KEY CLUSTERED 
(
	[MaGiaiDau] ASC,
	[MaCauThu] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ChiTietBanThang]    Script Date: 1/8/2020 7:47:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChiTietBanThang](
	[MaMuaGiai] [varchar](10) NOT NULL,
	[MaVongDau] [varchar](10) NOT NULL,
	[MaTranDau] [varchar](10) NOT NULL,
	[MaCauThu] [varchar](10) NOT NULL,
	[MaDoiBong] [varchar](10) NULL,
	[LoaiBanThang] [int] NOT NULL,
	[ThoiDiem] [time](7) NOT NULL,
 CONSTRAINT [PK_KetQua] PRIMARY KEY CLUSTERED 
(
	[MaMuaGiai] ASC,
	[MaVongDau] ASC,
	[MaTranDau] ASC,
	[MaCauThu] ASC,
	[LoaiBanThang] ASC,
	[ThoiDiem] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ChiTietTranDau]    Script Date: 1/8/2020 7:47:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChiTietTranDau](
	[MaMuaGiai] [varchar](10) NOT NULL,
	[MaTranDau] [varchar](10) NOT NULL,
	[MaVongDau] [varchar](10) NOT NULL,
	[NgayThiDau] [date] NULL,
	[GioThiDau] [time](7) NULL,
	[ChuNha] [varchar](10) NULL,
	[Khach] [varchar](10) NULL,
	[DaThiDau] [int] NULL,
	[SoBanThang_ChuNha] [int] NULL,
	[SoBanThang_Khach] [int] NULL,
 CONSTRAINT [PK_LichThiDau] PRIMARY KEY CLUSTERED 
(
	[MaMuaGiai] ASC,
	[MaVongDau] ASC,
	[MaTranDau] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DoiBong]    Script Date: 1/8/2020 7:47:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DoiBong](
	[MaDoiBong] [varchar](10) NOT NULL,
	[TenDoiBong] [nvarchar](50) NULL,
	[SanNha] [nvarchar](50) NULL,
	[GhiChu] [bit] NULL,
	[DaXoa] [bit] NULL,
 CONSTRAINT [PK_DoiBong] PRIMARY KEY CLUSTERED 
(
	[MaDoiBong] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DoiBongThamGiaGiaiDau]    Script Date: 1/8/2020 7:47:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DoiBongThamGiaGiaiDau](
	[MaGiaiDau] [varchar](10) NOT NULL,
	[MaDoiBong] [varchar](10) NOT NULL,
	[TenDoiBong] [nvarchar](50) NULL,
	[SoLuongCauThuDangKy] [int] NULL,
	[SoLuongCauThuNuocNgoaiDangKy] [int] NULL,
	[HoanTatDangKy] [bit] NULL,
 CONSTRAINT [PK_DoiBongThamGiaGiaiDau] PRIMARY KEY CLUSTERED 
(
	[MaGiaiDau] ASC,
	[MaDoiBong] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LoaiCauThu]    Script Date: 1/8/2020 7:47:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoaiCauThu](
	[MaLoaiCauThu] [int] NOT NULL,
	[TenLoaiCauThu] [nvarchar](50) NULL,
 CONSTRAINT [PK_LoaiCauThu] PRIMARY KEY CLUSTERED 
(
	[MaLoaiCauThu] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MuaGiai]    Script Date: 1/8/2020 7:47:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MuaGiai](
	[MaMuaGiai] [varchar](10) NOT NULL,
	[TenMuaGiai] [nvarchar](50) NULL,
	[SoLuongDoiThamDuToiDa] [int] NULL,
	[SoLuongDoiDaDangKy] [int] NULL,
	[HoanTatDangKy] [bit] NULL,
 CONSTRAINT [PK_MuaGiai] PRIMARY KEY CLUSTERED 
(
	[MaMuaGiai] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SoLieuThongKeCauThu]    Script Date: 1/8/2020 7:47:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SoLieuThongKeCauThu](
	[MaMuaGiai] [varchar](10) NOT NULL,
	[MaVongDau] [varchar](10) NOT NULL,
	[MaCauThu] [varchar](10) NOT NULL,
	[MaDoiBong] [varchar](10) NULL,
	[SoBanThang] [int] NULL,
	[TongSoBanThang] [int] NULL,
	[Hang] [int] NULL,
 CONSTRAINT [PK_BangXepHangCauThu] PRIMARY KEY CLUSTERED 
(
	[MaMuaGiai] ASC,
	[MaVongDau] ASC,
	[MaCauThu] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SoLieuThongKeDoiBong]    Script Date: 1/8/2020 7:47:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SoLieuThongKeDoiBong](
	[MaMuaGiai] [varchar](10) NOT NULL,
	[MaVongDau] [varchar](10) NOT NULL,
	[MaDoiBong] [varchar](10) NOT NULL,
	[SoTranDaThiDau] [int] NULL,
	[TongSoTranThang] [int] NULL,
	[TongSoTranThua] [int] NULL,
	[TongSoTranHoa] [int] NULL,
	[SoBanThang] [int] NULL,
	[TongSoBanThang] [int] NULL,
	[SoBanThua] [int] NULL,
	[TongSoBanThua] [int] NULL,
	[Diem] [int] NULL,
	[TongDiem] [int] NULL,
	[HieuSo] [int] NULL,
	[TongHieuSo] [int] NULL,
	[Hang] [int] NULL,
 CONSTRAINT [PK_ThongKeDoiBong] PRIMARY KEY CLUSTERED 
(
	[MaMuaGiai] ASC,
	[MaVongDau] ASC,
	[MaDoiBong] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TaiKhoan]    Script Date: 1/8/2020 7:47:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TaiKhoan](
	[TenDangNhap] [varchar](20) NOT NULL,
	[MatKhau] [varchar](20) NULL,
	[MaLoaiTaiKhoan] [int] NULL,
 CONSTRAINT [PK_TaiKhoan] PRIMARY KEY CLUSTERED 
(
	[TenDangNhap] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[VP_TinhBangXepHang]    Script Date: 1/8/2020 7:47:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[VP_TinhBangXepHang]
@sovongdau int,
@madb varchar(10),
@mamg varchar(10),
@mavongdautruoc varchar(10) output,
@tongdiemvongtruoc int output,
@hieusovongdautruoc int output,
@tongbanthangvongdautruoc int output,
@tongbanthuavongdautruoc int output,
@tongtranthangvongdautruoc int output,
@tongtranhoavongdautruoc int output,
@tongtranthuavongdautruoc int output
as
begin
	set @mavongdautruoc = concat('VD0', RIGHT ('00' + ltrim(str(@sovongdau - 1)),2))
	set @tongdiemvongtruoc = (select TongDiem
							  from SoLieuThongKeDoiBong
							  where MaVongDau = @mavongdautruoc and MaDoiBong = @madb and MaMuaGiai = @mamg)
	set @hieusovongdautruoc = (select HieuSo
							   from SoLieuThongKeDoiBong
							   where MaVongDau = @mavongdautruoc and MaDoiBong = @madb and MaMuaGiai = @mamg)
	set @tongbanthangvongdautruoc = (select TongSoBanThang
									 from SoLieuThongKeDoiBong 
									 where MaVongDau = @mavongdautruoc and MaDoiBong = @madb and MaMuaGiai = @mamg)
	set @tongbanthuavongdautruoc = (select TongSoBanThua
									from SoLieuThongKeDoiBong
									where MaMuaGiai = @mamg and MaVongDau = @mavongdautruoc and MaDoiBong = @madb)
	set @tongtranthangvongdautruoc = (select TongSoTranThang
									  from SoLieuThongKeDoiBong 
									  where MaVongDau = @mavongdautruoc and MaDoiBong = @madb and MaMuaGiai = @mamg)							
	set @tongtranhoavongdautruoc = (select TongSoTranHoa
									from SoLieuThongKeDoiBong 
									where MaVongDau = @mavongdautruoc and MaDoiBong = @madb and MaMuaGiai = @mamg)
	set @tongtranthuavongdautruoc = (select TongSoTranThua
								   	 from SoLieuThongKeDoiBong 
									 where MaVongDau = @mavongdautruoc and MaDoiBong = @madb and MaMuaGiai = @mamg)
end
GO
/****** Object:  StoredProcedure [dbo].[VP_XoaDanhSachCauThuThuocDoiBongThamGiaGiaiDau]    Script Date: 1/8/2020 7:47:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[VP_XoaDanhSachCauThuThuocDoiBongThamGiaGiaiDau]
as
begin
	declare @mact varchar(10)
	declare @mamg varchar(10)
	declare cur cursor for
	select MaGiaiDau, MaCauThu
	from CauThuThuocDoiBongThamGiaGiaiDau
	
	open cur
	fetch next from cur
	into @mamg, @mact

	while @@fetch_status = 0
	begin
		delete from CauThuThuocDoiBongThamGiaGiaiDau
		where MaGiaiDau = @mamg
		and MaCauThu = @mact

		fetch next from cur
		into @mamg, @mact
	end
	close cur
	deallocate cur
end

exec VP_XoaDanhSachCauThuThuocDoiBongThamGiaGiaiDau

GO
/****** Object:  StoredProcedure [dbo].[VP_XoaDanhSachDoiBongThamGiaGiaiDau]    Script Date: 1/8/2020 7:47:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[VP_XoaDanhSachDoiBongThamGiaGiaiDau]
as
begin
	declare @madb varchar(10)
	declare @mamg varchar(10)
	declare cur cursor for
	select MaGiaiDau, MaDoiBong
	from DoiBongThamGiaGiaiDau
	open cur
	fetch next from cur
	into @mamg, @madb

	while @@fetch_status = 0
	begin
		delete from DoiBongThamGiaGiaiDau
		where MaGiaiDau = @mamg
		and MaDoiBong = @madb

		fetch next from cur
		into @mamg, @madb
	end
	close cur
	deallocate cur
end
GO
USE [master]
GO
ALTER DATABASE [QuanLyGiaiVoDichQuocGiaDB] SET  READ_WRITE 
GO
