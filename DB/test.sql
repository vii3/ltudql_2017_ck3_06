delete from ChiTietBanThang
delete from ChiTietTranDau
delete from SoLieuThongKeCauThu
delete from SoLieuThongKeDoiBong


insert into ChiTietTranDau(MaMuaGiai, MaVongDau, MaTranDau, ChuNha, Khach)
values('2019-2020', 'VD001', 'TD001', 'DB01', 'DB02')
insert into ChiTietTranDau(MaMuaGiai, MaVongDau, MaTranDau, ChuNha, Khach)
values('2019-2020', 'VD001', 'TD002', 'DB03', 'DB04')
insert into ChiTietTranDau(MaMuaGiai, MaVongDau, MaTranDau, ChuNha, Khach)
values('2019-2020', 'VD002', 'TD001', 'DB01', 'DB03')
insert into ChiTietTranDau(MaMuaGiai, MaVongDau, MaTranDau, ChuNha, Khach)
values('2019-2020', 'VD002', 'TD002', 'DB02', 'DB04')
insert into ChiTietTranDau(MaMuaGiai, MaVongDau, MaTranDau, ChuNha, Khach)
values('2019-2020', 'VD003', 'TD001', 'DB03', 'DB01')
insert into ChiTietTranDau(MaMuaGiai, MaVongDau, MaTranDau, ChuNha, Khach)
values('2019-2020', 'VD003', 'TD002', 'DB04', 'DB02')


update ChiTietTranDau
set DaThiDau = 1
where (MaVongDau = 'VD001' and MaTranDau = 'TD001')
update ChiTietTranDau
set DaThiDau = 1
where (MaVongDau = 'VD001' and MaTranDau = 'TD002')
update ChiTietTranDau
set DaThiDau = 1
where (MaVongDau = 'VD002' and MaTranDau = 'TD001')
update ChiTietTranDau
set DaThiDau = 1
where (MaVongDau = 'VD002' and MaTranDau = 'TD002')
update ChiTietTranDau
set DaThiDau = 1
where (MaVongDau = 'VD003' and MaTranDau = 'TD001')
update ChiTietTranDau
set DaThiDau = 1
where (MaVongDau = 'VD003' and MaTranDau = 'TD002')


update ChiTietBanThang
set LoaiBanThang = 1
where MaVongDau = 'VD001' and MaTranDau = 'TD001' and MaCauThu = 'CT05'
insert into ChiTietBanThang(MaMuaGiai, MaVongDau, MaTranDau, MaCauThu, LoaiBanThang, ThoiDiem)
values ('2019-2020', 'VD001', 'TD001', 'CT05', '1', GETDATE())
update ChiTietBanThang
set LoaiBanThang = 1
where MaVongDau = 'VD001' and MaTranDau = 'TD001' and MaCauThu = 'CT11'
update ChiTietBanThang
set LoaiBanThang = 1
where MaVongDau = 'VD001' and MaTranDau = 'TD001' and MaCauThu = 'CT12'
update ChiTietBanThang
set LoaiBanThang = 1
where MaVongDau = 'VD001' and MaTranDau = 'TD001' and MaCauThu = 'CT24'
update ChiTietBanThang
set LoaiBanThang = 1
where MaVongDau = 'VD001' and MaTranDau = 'TD001' and MaCauThu = 'CT35'
update ChiTietBanThang
set LoaiBanThang = 1
where MaVongDau = 'VD001' and MaTranDau = 'TD001' and MaCauThu = 'CT36'
update ChiTietBanThang
set LoaiBanThang = 1
where MaVongDau = 'VD001' and MaTranDau = 'TD001' and MaCauThu = 'CT37'
update ChiTietBanThang
set LoaiBanThang = 1
where MaVongDau = 'VD001' and MaTranDau = 'TD001' and MaCauThu = 'CT26'
update ChiTietBanThang
set LoaiBanThang = 1
where MaVongDau = 'VD001' and MaTranDau = 'TD002' and MaCauThu = 'CT71'
update ChiTietBanThang
set LoaiBanThang = 1
where MaVongDau = 'VD001' and MaTranDau = 'TD002' and MaCauThu = 'CT70'
update ChiTietBanThang
set LoaiBanThang = 1
where MaVongDau = 'VD001' and MaTranDau = 'TD002' and MaCauThu = 'CT48'

-- DB1 vs DB3 -- VD002 TD001
update ChiTietBanThang
set LoaiBanThang = 1
where MaVongDau = 'VD002' and MaTranDau = 'TD001' and MaCauThu = 'CT05'
update ChiTietBanThang
set LoaiBanThang = 1
where MaVongDau = 'VD002' and MaTranDau = 'TD001' and MaCauThu = 'CT45'
update ChiTietBanThang
set LoaiBanThang = 1
where MaVongDau = 'VD002' and MaTranDau = 'TD001' and MaCauThu = 'CT46'

-- DB2 vs DB4 -- VD002 TD002
update ChiTietBanThang
set LoaiBanThang = 1
where MaVongDau = 'VD002' and MaTranDau = 'TD002' and MaCauThu = 'CT21'
update ChiTietBanThang
set LoaiBanThang = 1
where MaVongDau = 'VD002' and MaTranDau = 'TD002' and MaCauThu = 'CT23'
update ChiTietBanThang
set LoaiBanThang = 1
where MaVongDau = 'VD002' and MaTranDau = 'TD002' and MaCauThu = 'CT25'
update ChiTietBanThang
set LoaiBanThang = 1
where MaVongDau = 'VD002' and MaTranDau = 'TD002' and MaCauThu = 'CT75'
update ChiTietBanThang
set LoaiBanThang = 1
where MaVongDau = 'VD002' and MaTranDau = 'TD002' and MaCauThu = 'CT76'

-- DB3 vs DB1 -- VD003 TD001
update ChiTietBanThang
set LoaiBanThang = 1
where MaVongDau = 'VD003' and MaTranDau = 'TD001' and MaCauThu = 'CT01'
update ChiTietBanThang
set LoaiBanThang = 1
where MaVongDau = 'VD003' and MaTranDau = 'TD001' and MaCauThu = 'CT05'
update ChiTietBanThang
set LoaiBanThang = 1
where MaVongDau = 'VD003' and MaTranDau = 'TD001' and MaCauThu = 'CT06'
update ChiTietBanThang
set LoaiBanThang = 1
where MaVongDau = 'VD003' and MaTranDau = 'TD001' and MaCauThu = 'CT45'


-- DB4 vs DB2 -- VD003 TD002
update ChiTietBanThang
set LoaiBanThang = 1
where MaVongDau = 'VD003' and MaTranDau = 'TD002' and MaCauThu = 'CT78'
update ChiTietBanThang
set LoaiBanThang = 1
where MaVongDau = 'VD003' and MaTranDau = 'TD002' and MaCauThu = 'CT79'
update ChiTietBanThang
set LoaiBanThang = 1
where MaVongDau = 'VD003' and MaTranDau = 'TD002' and MaCauThu = 'CT25'
update ChiTietBanThang
set LoaiBanThang = 1
where MaVongDau = 'VD003' and MaTranDau = 'TD002' and MaCauThu = 'CT26'