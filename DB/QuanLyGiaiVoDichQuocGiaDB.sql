﻿

use master
go
if db_id('QuanLyGiaiVoDichQuocGiaDB') is not null
drop database QuanLyGiaiVoDichQuocGiaDB

go
create database QuanLyGiaiVoDichQuocGiaDB

go
use QuanLyGiaiVoDichQuocGiaDB

go
create table CauThu
(
	MaCauThu varchar(10),
	HoTenCauThu nvarchar(50),
	NgaySinh date,
	MaLoaiCauThu int,
	MaDoiBong varchar(10),
	TongSoBanThang int,
	DaXoa bit

	Constraint PK_CauThu
	Primary Key(MaCauThu)
)

create table DoiBong
(
	MaDoiBong varchar(10),
	TenDoiBong nvarchar(50),
	SanNha nvarchar(50),
	GhiChu bit,
	DaXoa bit

	Constraint PK_DoiBong
	Primary Key(MaDoiBong)
)

create table MuaGiai
(
	MaMuaGiai varchar(10),
	TenMuaGiai nvarchar(50),
	SoLuongDoiThamDuToiDa int,
	SoLuongDoiDaDangKy int,
	HoanTatDangKy bit

	Constraint PK_MuaGiai
	Primary Key(MaMuaGiai)
)


create table DoiBongThamGiaGiaiDau
(
	MaGiaiDau varchar(10),
	MaDoiBong varchar(10),
	TenDoiBong nvarchar(50),
	SoLuongCauThuDangKy int,
	SoLuongCauThuNuocNgoaiDangKy int,
	HoanTatDangKy bit

	Constraint PK_DoiBongThamGiaGiaiDau
	Primary Key(MaGiaiDau, MaDoiBong)
)

create table CauThuThuocDoiBongThamGiaGiaiDau
(
	MaGiaiDau varchar(10),
	MaDoiBong varchar(10),
	TenDoiBong nvarchar(50),
	MaCauThu varchar(10),
	TenCauThu nvarchar(50),
	MaLoaiCauThu int,
	TongSoBanThang int

	Constraint PK_CauThuThuocDoiBongThamGiaGiaiDau
	Primary key(MaGiaiDau, MaCauThu)
)

create table LoaiCauThu
(
	MaLoaiCauThu int,
	TenLoaiCauThu nvarchar(50)

	Constraint PK_LoaiCauThu
	Primary Key(MaLoaiCauThu)
)

create table ChiTietTranDau
(
	MaMuaGiai varchar(10),
	MaTranDau varchar(10),
	MaVongDau varchar(10),
	NgayThiDau date,
	GioThiDau time,
	ChuNha varchar(10),
	Khach varchar(10),
	DaThiDau int,
	SoBanThang_ChuNha int,
	SoBanThang_Khach int

	Constraint PK_LichThiDau
	Primary Key(MaMuaGiai, MaVongDau, MaTranDau)
)

Create table ChiTietBanThang
(
	MaMuaGiai varchar(10),
	MaVongDau varchar(10),
	MaTranDau varchar(10),
	MaCauThu varchar(10),
	MaDoiBong varchar(10),
	LoaiBanThang int,
	ThoiDiem time

	Constraint PK_KetQua
	Primary Key(MaMuaGiai, MaVongDau, MaTranDau, MaCauThu, LoaiBanThang, ThoiDiem)
)

Create table SoLieuThongKeDoiBong
(
	MaMuaGiai varchar(10),
	MaVongDau varchar(10),
	MaDoiBong varchar(10),
	SoTranDaThiDau int,
	TongSoTranThang int,
	TongSoTranThua int,
	TongSoTranHoa int,
	SoBanThang int,
	TongSoBanThang int,
	SoBanThua int,
	TongSoBanThua int,
	Diem int,
	TongDiem int,
	HieuSo int,
	TongHieuSo int,
	Hang int
	

	Constraint PK_ThongKeDoiBong
	Primary Key(MaMuaGiai, MaVongDau, MaDoiBong)
)


Create table SoLieuThongKeCauThu
(
	MaMuaGiai varchar(10),
	MaVongDau varchar(10),
	MaCauThu varchar(10),
	MaDoiBong varchar(10),
	SoBanThang int,
	TongSoBanThang int, 
	Hang int

	Constraint PK_BangXepHangCauThu
	Primary Key(MaMuaGiai, MaVongDau, MaCauThu)
)
