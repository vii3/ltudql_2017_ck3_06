﻿
if object_id('VP_XoaDanhSachCauThuThuocDoiBongThamGiaGiaiDau', 'p') is not null
drop procedure VP_XoaDanhSachCauThuThuocDoiBongThamGiaGiaiDau
go
create procedure VP_XoaDanhSachCauThuThuocDoiBongThamGiaGiaiDau
as
begin
	declare @mact varchar(10)
	declare @mamg varchar(10)
	declare cur cursor for
	select MaGiaiDau, MaCauThu
	from CauThuThuocDoiBongThamGiaGiaiDau
	
	open cur
	fetch next from cur
	into @mamg, @mact

	while @@fetch_status = 0
	begin
		delete from CauThuThuocDoiBongThamGiaGiaiDau
		where MaGiaiDau = @mamg
		and MaCauThu = @mact

		fetch next from cur
		into @mamg, @mact
	end
	close cur
	deallocate cur
end

exec VP_XoaDanhSachCauThuThuocDoiBongThamGiaGiaiDau

go 
if object_id('VP_XoaDanhSachDoiBongThamGiaGiaiDau', 'p') is not null
drop procedure VP_XoaDanhSachDoiBongThamGiaGiaiDau
go
create procedure VP_XoaDanhSachDoiBongThamGiaGiaiDau
as
begin
	declare @madb varchar(10)
	declare @mamg varchar(10)
	declare cur cursor for
	select MaGiaiDau, MaDoiBong
	from DoiBongThamGiaGiaiDau
	open cur
	fetch next from cur
	into @mamg, @madb

	while @@fetch_status = 0
	begin
		delete from DoiBongThamGiaGiaiDau
		where MaGiaiDau = @mamg
		and MaDoiBong = @madb

		fetch next from cur
		into @mamg, @madb
	end
	close cur
	deallocate cur
end

delete from MuaGiai
exec VP_XoaDanhSachDoiBongThamGiaGiaiDau
exec VP_XoaDanhSachCauThuThuocDoiBongThamGiaGiaiDau
delete from ChiTietBanThang
delete from ChiTietTranDau
delete from SoLieuThongKeCauThu
delete from SoLieuThongKeDoiBong

update DoiBong
set TenDoiBong = N'Đăk Lăk'
where MaDoiBong = 'DB13'