﻿-------------------------------------------------- STORE PROCEDURED --------------------------------------------------
if object_id('VP_TinhBangXepHang', 'p') is not null
drop procedure VP_TinhBangXepHang
go
create procedure VP_TinhBangXepHang
@sovongdau int,
@madb varchar(10),
@mamg varchar(10),
@mavongdautruoc varchar(10) output,
@tongdiemvongtruoc int output,
@hieusovongdautruoc int output,
@tongbanthangvongdautruoc int output,
@tongbanthuavongdautruoc int output,
@tongtranthangvongdautruoc int output,
@tongtranhoavongdautruoc int output,
@tongtranthuavongdautruoc int output
as
begin
	set @mavongdautruoc = concat('VD0', RIGHT ('00' + ltrim(str(@sovongdau - 1)),2))
	set @tongdiemvongtruoc = (select TongDiem
							  from SoLieuThongKeDoiBong
							  where MaVongDau = @mavongdautruoc and MaDoiBong = @madb and MaMuaGiai = @mamg)
	set @hieusovongdautruoc = (select HieuSo
							   from SoLieuThongKeDoiBong
							   where MaVongDau = @mavongdautruoc and MaDoiBong = @madb and MaMuaGiai = @mamg)
	set @tongbanthangvongdautruoc = (select TongSoBanThang
									 from SoLieuThongKeDoiBong 
									 where MaVongDau = @mavongdautruoc and MaDoiBong = @madb and MaMuaGiai = @mamg)
	set @tongbanthuavongdautruoc = (select TongSoBanThua
									from SoLieuThongKeDoiBong
									where MaMuaGiai = @mamg and MaVongDau = @mavongdautruoc and MaDoiBong = @madb)
	set @tongtranthangvongdautruoc = (select TongSoTranThang
									  from SoLieuThongKeDoiBong 
									  where MaVongDau = @mavongdautruoc and MaDoiBong = @madb and MaMuaGiai = @mamg)							
	set @tongtranhoavongdautruoc = (select TongSoTranHoa
									from SoLieuThongKeDoiBong 
									where MaVongDau = @mavongdautruoc and MaDoiBong = @madb and MaMuaGiai = @mamg)
	set @tongtranthuavongdautruoc = (select TongSoTranThua
								   	 from SoLieuThongKeDoiBong 
									 where MaVongDau = @mavongdautruoc and MaDoiBong = @madb and MaMuaGiai = @mamg)
end
go
--------------------------------------------------	--------------------------------------------------
--------------------------------------------------	--------------------------------------------------







--------------------------------------------------	--------------------------------------------------
-- Từ bảng Lịch thi -> Khởi tạo bảng ChiTietBanThang
-- Bảng kết quả chứa các thông tin Mã vòng đấu, Mã trận đấu, Mã cầu thủ, Loại bàn thắng, Thời điểm ghi bàn
-- Mã đội bóng CHƯA CẬP NHẬT
--------------------------------------------------
--	BẢNG TẦM ẢNH HƯỞNG
--					INSERT		UPDATE		DELETE		
--	ChiTietTranDau		  +			  +			  +
--------------------------------------------------	
if object_id('VTG_KhoiTaoChiTietBanThang', 'tr') is not null
drop trigger VTG_KhoiTaoChiTietBanThang
go
create trigger VTG_KhoiTaoChiTietBanThang on ChiTietTranDau
for insert, update
as
begin
	set nocount on
	if exists (select * from inserted)
	begin
		declare @mamg varchar(10)
		declare @mavd varchar(10)
		declare @matd varchar(10)
		declare @mact varchar(10)
		declare @madb1 varchar(10) -- Chủ nhà
		declare @madb2 varchar(10) -- Khách
		set @mamg = (select MaMuaGiai from inserted)
		set @mavd = (select MaVongDau from inserted)
		set @matd = (select MaTranDau from inserted)
		set @madb1 = (select ChuNha from inserted)
		set @madb2 = (select Khach from inserted)
		if (select i.DaThiDau from inserted i) = -1
		begin		
			if (select COUNT(*)
				from  MuaGiai mg
				where mg.MaMuaGiai = @mamg) = 0
			begin
				insert into MuaGiai(MaMuaGiai, TenMuaGiai) values(@mamg , CONCAT(N'Mùa giải', @mamg));
			end

			-- Insert cau thu doi bong 1
			declare curKhoiTaoChiTietBanThang cursor for
			select ct.MaCauThu
			from CauThuThuocDoiBongThamGiaGiaiDau ct
			where (ct.MaDoiBong = @madb1 and ct.MaGiaiDau = @mamg)

			open curKhoiTaoChiTietBanThang
			fetch next from curKhoiTaoChiTietBanThang
			into @mact
			while @@fetch_status = 0
			begin
				
				insert into ChiTietBanThang(MaMuaGiai, MaVongDau, MaTranDau, MaCauThu, MaDoiBong, LoaiBanThang, ThoiDiem)
				values (@mamg, @mavd, @matd, @mact, @madb1, '-1', GETDATE())
				fetch next from curKhoiTaoChiTietBanThang
				into @mact
			end
			close curKhoiTaoChiTietBanThang
			deallocate curKhoiTaoChiTietBanThang	
			
			-- Insert cau thu doi bong 2
			declare curKhoiTaoChiTietBanThang cursor for
			select ct.MaCauThu
			from CauThuThuocDoiBongThamGiaGiaiDau ct
			where (ct.MaDoiBong = @madb2 and ct.MaGiaiDau = @mamg)

			open curKhoiTaoChiTietBanThang
			fetch next from curKhoiTaoChiTietBanThang
			into @mact
			while @@fetch_status = 0
			begin
				
				insert into ChiTietBanThang(MaMuaGiai, MaVongDau, MaTranDau, MaCauThu, MaDoiBong, LoaiBanThang, ThoiDiem)
				values (@mamg, @mavd, @matd, @mact, @madb2, '-1', GETDATE())
				fetch next from curKhoiTaoChiTietBanThang
				into @mact
			end
			close curKhoiTaoChiTietBanThang
			deallocate curKhoiTaoChiTietBanThang
		end
		else if (select i.DaThiDau from inserted i) = 1 and (select i.SoBanThang_ChuNha from inserted i) is null and (select i.SoBanThang_Khach from inserted i) is null
		begin
			-- Update Số bàn thắng chủ nhà và số bàn thắng đội khách = 0 khi trận đấu bắt đầu diễn ra
			declare @kt int
			set @kt = (select SoBanThang_ChuNha
					   from ChiTietTranDau
					   where MaMuaGiai = @mamg
					   and MaVongDau = @mavd
					   and MaTranDau = @matd)
			if @kt is null
			begin
				update ChiTietTranDau
				set SoBanThang_ChuNha = 0
				from ChiTietTranDau
				where MaMuaGiai = @mamg
				and MaVongDau = @mavd
				and MaTranDau = @matd
			end
			set @kt = (select SoBanThang_Khach
					   from ChiTietTranDau
					   where MaMuaGiai = @mamg
					   and MaVongDau = @mavd
					   and MaTranDau = @matd)
			if @kt is null
			begin
				update ChiTietTranDau
				set SoBanThang_Khach = 0
				from ChiTietTranDau
				where MaMuaGiai = @mamg
				and MaVongDau = @mavd
				and MaTranDau = @matd
			end
		end
	end
end
go
--------------------------------------------------	--------------------------------------------------
--------------------------------------------------	--------------------------------------------------




-- Khi cập nhật kết quả ở bảng Kết quả -> Update Số bàn thắng chủ nhà, Số bàn thắng đội khách ở bảng Lịch thi đấu
--------------------------------------------------
--	BẢNG TẦM ẢNH HƯỞNG
--					INSERT		UPDATE		DELETE		
--	ChiTietBanThang			  +			  +			  +
--------------------------------------------------
if object_id('VTG_Update2ChiTietTranDau', 'tr') is not null
drop trigger VTG_Update2ChiTietTranDau
go
create trigger VTG_Update2ChiTietTranDau on ChiTietBanThang
after insert, update
as
begin
	set nocount on
	if exists (select * from inserted)
	begin
		declare @mavd varchar(10)
		declare @mamg varchar(10)
		declare @matd varchar(10)
		declare @madb varchar(10)
		declare @mact varchar(10)
		declare @sovongdau int
		declare @mavongdautruoc varchar(10)
		declare @vongdautruoc varchar(10)
		declare @xbanthang int -- Số lượng bàn thắng mà một cầu thủ ghi được trong vòng đấu đang xét
		declare @tongbanthang int -- Tổng số bàn thắng một cầu thủ ghi được trong vòng đấu đang xét
		declare @tongbanthangvongdautruoc int -- Tống số bàn thắng cầu thủ ghi được từ vòng đấu đầu tiên tới vòng đấu đang xét
		declare @banthang int -- Số bàn thắng mà một đội ghi được trong vòng đấu đang xét
		declare @phanluoi int -- Số bàn phản lưới mà một đội trong vòng đấu đang xét
		declare @kt int -- Kiểm tra ? Chủ nhà : Khách
		declare @loaibt int -- Loại bàn thắng
		if (select ct.DaThiDau
			from ChiTietTranDau ct, inserted i
			where ct.MaMuaGiai = i.MaMuaGiai
			and ct.MaVongDau = i.MaVongDau
			and ct.MaTranDau = i.MaTranDau) != -1
		begin
			set @mamg = (select MaMuaGiai from inserted)
			set @mavd = (select MaVongDau from inserted)
			set @matd = (select MaTranDau from inserted)
			set @madb = (select MaDoiBong from inserted)
			set @mact = (select MaCauThu from inserted)

-------------------------------------------------- Update Chi tiết trận đấu --------------------------------------------------
			set @banthang = (select count(*)
							from ChiTietBanThang
							where MaTranDau = @matd
							and MaVongDau = @mavd
							and MaMuaGiai = @mamg
							and MaDoiBong = @madb
							and LoaiBanThang != -1
							and LoaiBanThang != -11
							group by MaDoiBong)
			set @phanluoi = (select count(*)
							from ChiTietBanThang 
							where MaTranDau = @matd
							and MaVongDau = @mavd
							and MaMuaGiai = @mamg
							and MaDoiBong = @madb
							and LoaiBanThang = -11
							group by MaDoiBong)
			if @banthang is null
			set @banthang = 0
			if @phanluoi is null
			set @phanluoi = 0

			if (select count(*) from ChiTietTranDau where MaMuaGiai = @mamg and MaVongDau = @mavd and MaTranDau = @matd and ChuNha = @madb) = 1
			begin
				update ChiTietTranDau
				set SoBanThang_ChuNha = @banthang, SoBanThang_Khach = SoBanThang_Khach + @phanluoi
				where MaTranDau = @matd
				and MaVongDau = @mavd
				and ChuNha = @madb
				and MaMuaGiai = @mamg
			end
			else
			begin
				update ChiTietTranDau
				set SoBanThang_Khach =  @banthang, SoBanThang_ChuNha = SoBanThang_ChuNha + @phanluoi
				where MaTranDau = @matd
				and MaVongDau = @mavd
				and Khach = @madb
				and MaMuaGiai = @mamg
			end
-------------------------------------------------- Hoàn tất update Chi tiết trận đấu --------------------------------------------------
			--------------------------------------------------	--------------------------------------------------
-------------------------------------------------- Update Số liệu thống kê cầu thủ --------------------------------------------------
			set @xbanthang = (select count(*)
							  from ChiTietBanThang kq
							  where kq.MaMuaGiai = @mamg
							  and kq.MaVongDau = @mavd
							  and kq.MaTranDau = @matd
							  and kq.MaCauThu = @mact
							  and kq.LoaiBanThang != -1
							  and kq.LoaiBanThang != -11
							  and kq.LoaiBanThang != -100)

			set @sovongdau = cast(substring((select i.MaVongDau from inserted i), 4, 5) as int)
			set @mavongdautruoc = concat('VD0', RIGHT ('00' + ltrim(str(@sovongdau - 1)),2))
			if (select i.MaVongDau from inserted i) = 'VD001'
			begin
				update SoLieuThongKeCauThu
				set SoBanThang = @xbanthang, TongSoBanThang = @xbanthang
				where MaMuaGiai = @mamg and MaVongDau = @mavd and MaCauThu = @mact
			end
			else
			begin
				set @mavongdautruoc = concat('VD0', RIGHT ('00' + ltrim(str(@sovongdau - 1)),2))			
				set @tongbanthangvongdautruoc = (select sltk.TongSoBanThang
												 from SoLieuThongKeCauThu sltk
												 where MaMuaGiai = @mamg
												 and MaVongDau = @mavongdautruoc
												 and MaCauThu = @mact)
				update SoLieuThongKeCauThu
				set SoBanThang = @xbanthang, TongSoBanThang = @tongbanthangvongdautruoc + @xbanthang
				where MaMuaGiai = @mamg and MaVongDau = @mavd and MaCauThu = @mact				
			end

			declare @xtongbanthang int -- Số bàn thắng của cầu thủ ở vị trí trước đó
			declare @i int
			set @i = 1
			declare curXepHangCauThu cursor for

			select MaCauThu, TongSoBanThang
			from SoLieuThongKeCauThu
			where MaMuaGiai = @mamg
			and MaVongDau = @mavd
			order by TongSoBanThang desc

			open curXepHangCauThu
			fetch next from curXepHangCauThu
			into @mact, @tongbanthang
			set @xtongbanthang = @tongbanthang
			while @@fetch_status = 0
			begin
				if @tongbanthang = @xtongbanthang
				begin
					update SoLieuThongKeCauThu
					set Hang = @i
					where MaMuaGiai = @mamg
					and MaVongDau = @mavd
					and MaCauThu = @mact
				end
				else
				begin
					set @i = @i + 1
					update SoLieuThongKeCauThu
					set Hang = @i
					where MaMuaGiai = @mamg
					and MaVongDau = @mavd
					and MaCauThu = @mact
				end	
				set @xtongbanthang = @tongbanthang
				fetch next from curXepHangCauThu
				into @mact, @tongbanthang
			end
			close curXepHangCauThu
			deallocate curXepHangCauThu
-------------------------------------------------- Hoàn tất update Số liệu thống kê cầu thủ --------------------------------------------------
		end
	end
end
go
--------------------------------------------------	--------------------------------------------------
--------------------------------------------------	--------------------------------------------------



--------------------------------------------------	--------------------------------------------------
--------------------------------------------------	--------------------------------------------------
-- Cập nhật bảng Thống kê từ bảng Lịch thi đấu
-- Từ bảng Lịch thi đấu -> Khởi tạo bảng thống kê đội bóng tại mỗi vòng đấu
-- Bảng Thống kê lưu thông tin với mỗi vòng sẽ có danh sách tất cả các đội bóng
-- Mỗi đội bóng có thông tin Số bàn thắng, Số bàn thua
-- Từ Số bàn thắng, Số bàn thua -> Số điểm -> Tổng điểm (điểm tính từ vòng đấu đầu tới vòng đấu đang xét)
-- Từ bảng Lịch thi đấu -> Khởi tạo Bảng xếp hạng độ bóng
-- Bảng xếp hạng đội bóng khởi tạo các cột Mã vòng đấu, Mã đội bóng
-- Các cột số trận thắng, hòa, thua, điểm, hiệu số, hạng để trống và sẽ được cập nhật từ bảng thống kê đội bóng
--------------------------------------------------
--	BẢNG TẦM ẢNH HƯỞNG
--					INSERT		UPDATE		DELETE		
--	ChiTietTranDau		   +		   +		   +
--------------------------------------------------
if object_id('VTG_Update1ThongKe', 'tr') is not null
drop trigger VTG_Update1ThongKe
go
create trigger  VTG_Update1ThongKe on ChiTietTranDau
for insert, update
as
begin
	set nocount on
	if exists (select * from inserted)
	begin
		declare @mamg varchar(10) -- Mã mùa giải
		declare @mavd varchar(10) -- Mã vòng đấu
		declare @matd varchar(10) -- Mã trận đấu
		declare @madb varchar(10) -- Mã đội bóng dùng để quét (không phân biệt chủ nhà hay đội khách)
		declare @madb1 varchar(10) -- Mã chủ nhà
		declare @madb2 varchar(10) -- Mã đội khách
		declare @mact varchar(10) -- Mã cầu thủ (dùng con trỏ để quét danh sách cầu thủ)
		declare @btchunha int -- Bàn thắng của chủ nhà
		declare @btkhach int -- Bàn thắng đội khách
		declare @sovongdau int -- Số vòng đấu
		declare @mavongdautruoc varchar(10) -- Mã vòng đấu trước
		declare @tongdiemvongtruoc int -- Tổng điểm tính đến vòng đấu trước
		declare @hieuso int -- Hiệu số ghi bàn tại tính từ vòng đấu đầu tiên tới vòng đấu đang xét
		declare @hieusovongdautruoc int -- Hiệu số bàn thắng tính đến vòng đấu trước
		declare @tongbanthangvongdautruoc int -- Tổng bàn thắng tính đến vòng đấu trước
		declare @tongbanthuavongdautruoc int -- Tổng bàn thua tính đến vòng đấu trước
		declare @tongtranthangvongdautruoc int -- Tổng số trận thắng tính tới vòng đấu trước
		declare @tongtranhoavongdautruoc int -- Tổng số trận hòa tính đến vòng đấu trước
		declare @tongtranthuavongdautruoc int -- Tổng số trận thua tính đến vòng đấu trước
		set @mamg = (select MaMuaGiai from inserted)
		set @mavd = (select MaVongDau from inserted)
		set @matd = (select MaTranDau from inserted)
		set @madb1 = (select ChuNha from inserted)
		set @madb2 = (select Khach from inserted)
		if (select i.DaThiDau from inserted i) = -1
		begin
-------------------------------------------------- Khởi tạo Số liệu thống kê đội bóng --------------------------------------------------
			if not exists (select *
						   from SoLieuThongKeDoiBong
						   where MaMuaGiai = @mamg 
						   and MaVongDau = @mavd)
			begin
				declare curKhoiTaoThongKeDoiBong cursor for
				select MaDoiBong
				from DoiBongThamGiaGiaiDau
				where MaGiaiDau = @mamg
				open curKhoiTaoThongKeDoiBong
				fetch next from curKhoiTaoThongKeDoiBong
				into @madb
				while @@fetch_status = 0
				begin
					-- Thêm Mã vòng đấu, Mã đội bóng, set tổng điểm của tất cả đội bóng ban đầu = 0
					--insert into SoLieuThongKeDoiBong
					--values (@mamg, @mavd, @madb, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
					insert into SoLieuThongKeDoiBong(MaMuaGiai, MaVongDau, MaDoiBong)
					values (@mamg, @mavd, @madb)
					fetch next from curKhoiTaoThongKeDoiBong
					into @madb
				end
				close curKhoiTaoThongKeDoiBong
				deallocate curKhoiTaoThongKeDoiBong
			end
-------------------------------------------------- Kết thúc khởi tạo Số liệu Thống kê đội bóng --------------------------------------------------


-------------------------------------------------- Khởi tạo Số liệu thống kê cầu thủ --------------------------------------------------
			if not exists (select *
						   from SoLieuThongKeCauThu sltk, inserted i
						   where sltk.MaVongDau = i.MaVongDau
						   and sltk.MaMuaGiai = i.MaMuaGiai)
			begin
				declare curKhoiTaoThongKeCauThu cursor for -- Con trỏ quét toàn bộ mã cầu thủ
				select MaCauThu
				from CauThuThuocDoiBongThamGiaGiaiDau
				where MaGiaiDau = @mamg
				open curKhoiTaoThongKeCauThu
				fetch next from curKhoiTaoThongKeCauThu
				into @mact
				while @@fetch_status = 0
				begin
					set @madb = (select MaDoiBong
								 from CauThuThuocDoiBongThamGiaGiaiDau
								 where MaGiaiDau = @mamg 
								 and MaCauThu = @mact)
					insert into SoLieuThongKeCauThu(MaMuaGiai, MaVongDau, MaDoiBong, MaCauThu, SoBanThang, TongSoBanThang, Hang)
					values (@mamg, @mavd, @madb, @mact, 0, 0, 0)
					fetch next from curKhoiTaoThongKeCauThu
					into @mact
				end
				close curKhoiTaoThongKeCauThu
				deallocate curKhoiTaoThongKeCauThu
			end
-------------------------------------------------- Kết thúc khởi tạo Bảng xếp hạng cầu thủ --------------------------------------------------
		end

		else if (select DaThiDau from inserted) = 1 and (select SoBanThang_ChuNha from inserted ) is not null or (select SoBanThang_Khach from inserted) is not null
		begin
			set @madb1 = (select ChuNha from inserted)
			set @madb2 = (select Khach from inserted)
			set @btchunha = (select SoBanThang_ChuNha from inserted)
			set @btkhach = (select SoBanThang_Khach from inserted)
			if @btchunha is null
			set @btchunha = 0
			if @btkhach is null
			set @btkhach = 0
			if (select MaVongDau from inserted) = 'VD001'
			begin
				if @btchunha > @btkhach
				begin
					update SoLieuThongKeDoiBong
					set SoTranDaThiDau = 1,
					TongSoTranThang = 1,
					TongSoTranThua = 0,
					TongSoTranHoa = 0,
					SoBanThang = @btchunha,
					TongSoBanThang = @btchunha,
					SoBanThua = @btkhach,
					TongSoBanThua = @btkhach,
					Diem = 3,
					TongDiem = 3,
					HieuSo = @btchunha - @btkhach,
					TongHieuSo = @btchunha - @btkhach
					where MaMuaGiai = @mamg and MaVongDau = @mavd and MaDoiBong = @madb1

					update SoLieuThongKeDoiBong
					set SoTranDaThiDau = 1,
					TongSoTranThang = 0,
					TongSoTranThua = 1,
					TongSoTranHoa = 0,
					SoBanThang = @btkhach,
					TongSoBanThang = @btkhach,
					SoBanThua = @btchunha,
					TongSoBanThua = @btchunha,
					Diem = 0,
					TongDiem = 0,
					HieuSo = @btkhach - @btchunha,
					TongHieuSo = @btkhach - @btchunha
					where MaMuaGiai = @mamg and MaVongDau = @mavd and MaDoiBong = @madb2
				end
				else if @btchunha = @btkhach
				begin
					update SoLieuThongKeDoiBong
					set SoTranDaThiDau = 1,
					TongSoTranThang = 0,
					TongSoTranThua = 0,
					TongSoTranHoa = 1,
					SoBanThang = @btkhach,
					TongSoBanThang = @btkhach,
					SoBanThua = @btchunha,
					TongSoBanThua = @btchunha,
					Diem = 1,
					TongDiem = 1,
					HieuSo = @btkhach - @btchunha,
					TongHieuSo = @btkhach - @btchunha
					where MaMuaGiai = @mamg and MaVongDau = @mavd and (MaDoiBong = @madb2 or MaDoiBong = @madb1)
				end
				else
				begin
					update SoLieuThongKeDoiBong
					set SoTranDaThiDau = 1,
					TongSoTranThang = 0,
					TongSoTranThua = 1,
					TongSoTranHoa = 0,
					SoBanThang = @btchunha,
					TongSoBanThang = @btchunha,
					SoBanThua = @btkhach,
					TongSoBanThua = @btkhach,
					Diem = 0,
					TongDiem = 0,
					HieuSo = @btchunha - @btkhach,
					TongHieuSo = @btchunha - @btkhach
					where MaMuaGiai = @mamg and MaVongDau = @mavd and MaDoiBong = @madb1

					update SoLieuThongKeDoiBong
					set SoTranDaThiDau = 1,
					TongSoTranThang = 1,
					TongSoTranThua = 0,
					TongSoTranHoa = 0,
					SoBanThang = @btkhach,
					TongSoBanThang = @btkhach,
					SoBanThua = @btchunha,
					TongSoBanThua = @btchunha,
					Diem = 3,
					TongDiem = 3,
					HieuSo = @btkhach - @btchunha,
					TongHieuSo = @btkhach - @btchunha
					where MaMuaGiai = @mamg and MaVongDau = @mavd and MaDoiBong = @madb2
				end
			end
			else
			begin
				-- Update các cột ngoại trừ cột xếp hạng
				set @sovongdau = cast(substring((select i.MaVongDau from inserted i), 4, 5) as int)
				exec VP_TinhBangXepHang 
				@sovongdau, 
				@madb1, 
				@mamg, 
				@mavongdautruoc output,
				@tongdiemvongtruoc output,
				@hieusovongdautruoc output,
				@tongbanthangvongdautruoc output,
				@tongbanthuavongdautruoc output,
				@tongtranthangvongdautruoc output,
				@tongtranhoavongdautruoc output,
				@tongtranthuavongdautruoc output
				if @btchunha > @btkhach
				begin
					update SoLieuThongKeDoiBong
					set SoTranDaThiDau = @tongtranthangvongdautruoc + @tongtranhoavongdautruoc + @tongtranthuavongdautruoc + 1,
					TongSoTranThang = @tongtranthangvongdautruoc + 1,
					TongSoTranHoa = @tongtranhoavongdautruoc,
					TongSoTranThua = @tongtranthuavongdautruoc,
					SoBanThang = @btchunha,
					TongSoBanThang = @tongbanthangvongdautruoc + @btchunha,
					SoBanThua = @btkhach,
					TongSoBanThua = @tongbanthuavongdautruoc + @btkhach,
					Diem = 3,
					TongDiem = @tongdiemvongtruoc + 3,
					HieuSo = @btchunha - @btkhach,
					TongHieuSo = @hieusovongdautruoc + @btchunha - @btkhach
					where MaVongDau = @mavd and MaDoiBong = @madb1 and MaMuaGiai = @mamg
				end
				else if @btchunha = @btkhach
				begin
					update SoLieuThongKeDoiBong
					set SoTranDaThiDau = @tongtranthangvongdautruoc + @tongtranhoavongdautruoc + @tongtranthuavongdautruoc + 1,
					TongSoTranThang = @tongtranthangvongdautruoc,
					TongSoTranHoa = @tongtranhoavongdautruoc + 1,
					TongSoTranThua = @tongtranthuavongdautruoc,
					SoBanThang = @btchunha,
					TongSoBanThang = @tongbanthangvongdautruoc + @btchunha,
					SoBanThua = @btkhach,
					TongSoBanThua = @tongbanthuavongdautruoc + @btkhach,
					Diem = 1,
					TongDiem = @tongdiemvongtruoc + 1,
					HieuSo = @btchunha - @btkhach,
					TongHieuSo = @hieusovongdautruoc + @btchunha - @btkhach
					where MaVongDau = @mavd and MaDoiBong = @madb1 and MaMuaGiai = @mamg
				end
				else
				begin
					update SoLieuThongKeDoiBong
					set SoTranDaThiDau = @tongtranthangvongdautruoc + @tongtranhoavongdautruoc + @tongtranthuavongdautruoc + 1,
					TongSoTranThang = @tongtranthangvongdautruoc,
					TongSoTranHoa = @tongtranhoavongdautruoc,
					TongSoTranThua = @tongtranthuavongdautruoc + 1,
					SoBanThang = @btchunha,
					TongSoBanThang = @tongbanthangvongdautruoc + @btchunha,
					SoBanThua = @btkhach,
					TongSoBanThua = @tongbanthuavongdautruoc + @btkhach,
					Diem = 0,
					TongDiem = @tongdiemvongtruoc,
					HieuSo = @btchunha - @btkhach,
					TongHieuSo = @hieusovongdautruoc + @btchunha - @btkhach
					where MaVongDau = @mavd and MaDoiBong = @madb1 and MaMuaGiai = @mamg
				end
				
				exec VP_TinhBangXepHang 
				@sovongdau, 
				@madb2, 
				@mamg, 
				@mavongdautruoc output,
				@tongdiemvongtruoc output,
				@hieusovongdautruoc output,
				@tongbanthangvongdautruoc output,
				@tongbanthuavongdautruoc output,
				@tongtranthangvongdautruoc output,
				@tongtranhoavongdautruoc output,
				@tongtranthuavongdautruoc output
				if @btchunha > @btkhach
				begin
					update SoLieuThongKeDoiBong
					set SoTranDaThiDau = @tongtranthangvongdautruoc + @tongtranhoavongdautruoc + @tongtranthuavongdautruoc + 1,
					TongSoTranThang = @tongtranthangvongdautruoc,
					TongSoTranHoa = @tongtranhoavongdautruoc,
					TongSoTranThua = @tongtranthuavongdautruoc + 1,
					SoBanThang = @btkhach,
					TongSoBanThang = @tongbanthangvongdautruoc + @btkhach,
					SoBanThua = @btchunha,
					TongSoBanThua = @tongbanthuavongdautruoc + @btchunha,
					Diem = 0,
					TongDiem = @tongdiemvongtruoc + 0,
					HieuSo = @btchunha - @btkhach,
					TongHieuSo = @hieusovongdautruoc + @btchunha - @btkhach
					where MaVongDau = @mavd and MaDoiBong = @madb2 and MaMuaGiai = @mamg
				end
				else if @btchunha = @btkhach
				begin
					update SoLieuThongKeDoiBong
					set SoTranDaThiDau = @tongtranthangvongdautruoc + @tongtranhoavongdautruoc + @tongtranthuavongdautruoc + 1,
					TongSoTranThang = @tongtranthangvongdautruoc,
					TongSoTranHoa = @tongtranhoavongdautruoc + 1,
					TongSoTranThua = @tongtranthuavongdautruoc,
					SoBanThang = @btkhach,
					TongSoBanThang = @tongbanthangvongdautruoc + @btkhach,
					SoBanThua = @btchunha,
					TongSoBanThua = @tongbanthuavongdautruoc + @btchunha,
					Diem = 1,
					TongDiem = @tongdiemvongtruoc + 1,
					HieuSo = @btchunha - @btkhach,
					TongHieuSo = @hieusovongdautruoc + @btchunha - @btkhach
					where MaVongDau = @mavd and MaDoiBong = @madb2 and MaMuaGiai = @mamg
				end
				else
				begin
					update SoLieuThongKeDoiBong
					set SoTranDaThiDau = @tongtranthangvongdautruoc + @tongtranhoavongdautruoc + @tongtranthuavongdautruoc + 1,
					TongSoTranThang = @tongtranthangvongdautruoc + 1,
					TongSoTranHoa = @tongtranhoavongdautruoc,
					TongSoTranThua = @tongtranthuavongdautruoc,
					SoBanThang = @btkhach,
					TongSoBanThang = @tongbanthangvongdautruoc + @btkhach,
					SoBanThua = @btchunha,
					TongSoBanThua = @tongbanthuavongdautruoc + @btchunha,
					Diem = 3,
					TongDiem = @tongdiemvongtruoc + 3,
					HieuSo = @btchunha - @btkhach,
					TongHieuSo = @hieusovongdautruoc + @btchunha - @btkhach
					where MaVongDau = @mavd and MaDoiBong = @madb2 and MaMuaGiai = @mamg
				end
			end
		end

-------------------------------------------------- Cài đặt bảng xếp hạng --------------------------------------------------
		declare @i int
		set @i = 1
		declare curCaiDatXepHangDoiBong cursor for
		select MaDoiBong
		from SoLieuThongKeDoiBong
		where MaMuaGiai = @mamg
		and MaVongDau = @mavd
		order by TongDiem desc, TongHieuSo desc, TongSoBanThang desc

		open curCaiDatXepHangDoiBong
		fetch next from curCaiDatXepHangDoiBong
		into @madb

		while @@fetch_status = 0
		begin
			update SoLieuThongKeDoiBong
			set Hang = @i
			where MaMuaGiai = @mamg
			and MaVongDau = @mavd
			and MaDoiBong = @madb

			set @i = @i + 1
			fetch next from curCaiDatXepHangDoiBong
			into @madb
		end
		close curCaiDatXepHangDoiBong
		deallocate curCaiDatXepHangDoiBong
-------------------------------------------------- Hoàn tất cài đặt bảng xếp hạng --------------------------------------------------
	end
end
go
--------------------------------------------------	--------------------------------------------------
--------------------------------------------------	--------------------------------------------------



-- Từ Bảng Cầu thủ thuộc đội bóng tham gia giải đấu -> Cập nhật số lượng đội tuyển đã đăng ký của Bảng Mùa giải
--------------------------------------------------
--	BẢNG TẦM ẢNH HƯỞNG
--											INSERT		UPDATE		DELETE		
--	CauThuThuocDoiBongThamGiaGiaiDau		   +		   +		   +
--------------------------------------------------
--if object_id('VTG_UpdateMuaGiai', 'tr') is not null
--drop trigger VTG_UpdateMuaGiai
--go
--create trigger VTG_UpdateMuaGiai on CauThuThuocDoiBongThamGiaGiaiDau
--for insert, update, delete
--as
--begin
--	if exists (select * from inserted)
--	begin
--		declare @mamg varchar(10)
--		set @mamg = (select MaGiaiDau from inserted)
		
--		declare @sldadangky int -- Số lượng đội tuyển đã đăng ký
--		set @sldadangky = (select count(distinct c.MaDoiBong)
--						   from CauThuThuocDoiBongThamGiaGiaiDau c
--						   where c.MaGiaiDau = @mamg)

--		update MuaGiai
--		set SoLuongDoiDaDangKy = @sldadangky
--		where MaMuaGiai = @mamg
--	end
--	if exists (select * from deleted)
--	begin
--		declare @mamg1 varchar(10)
--		set @mamg1 = (select MaGiaiDau from deleted)
		
--		declare @sldadangky1 int -- Số lượng đội tuyển đã đăng ký
--		set @sldadangky1 = (select count(distinct c.MaDoiBong)
--						   from CauThuThuocDoiBongThamGiaGiaiDau c
--						   where c.MaGiaiDau = @mamg)

--		update MuaGiai
--		set SoLuongDoiDaDangKy = @sldadangky1
--		where MaMuaGiai = @mamg1
--	end
--end
--go


if OBJECT_ID('VTG_CapNhatDoiBongThamGiaGiaiDau', 'tr') is not null
drop trigger VTG_CapNhatDoiBongThamGiaGiaiDau
go
create trigger VTG_CapNhatDoiBongThamGiaGiaiDau on CauThuThuocDoiBongThamGiaGiaiDau
for insert, update, delete
as
begin
	if exists (select * from inserted)
	begin
		declare @mamg varchar(10)
		declare @madb varchar(10)
		declare @soluongcauthu int
		declare @soluongcauthunuocngoai int

		set @mamg = (select MaGiaiDau from inserted)
		set @madb = (select MaDoiBong from inserted)
		set @soluongcauthu = (select count(distinct MaCauThu)
							  from CauThuThuocDoiBongThamGiaGiaiDau
							  where MaGiaiDau = @mamg
							  and MaDoiBong = @madb)
		set @soluongcauthunuocngoai = (select count(distinct MaCauThu)
									   from CauThuThuocDoiBongThamGiaGiaiDau
									   where MaGiaiDau = @mamg
									   and MaDoiBong = @madb
									   and MaLoaiCauThu = 2)

		update DoiBongThamGiaGiaiDau
		set SoLuongCauThuDangKy = @soluongcauthu, SoLuongCauThuNuocNgoaiDangKy = @soluongcauthunuocngoai
		where MaGiaiDau = @mamg
		and MaDoiBong = @madb
	end
	if exists (select * from deleted)
	begin
		declare @mamg1 varchar(10)
		declare @madb1 varchar(10)
		declare @soluongcauthu1 int
		declare @soluongcauthunuocngoai1 int

		set @mamg1 = (select MaGiaiDau from deleted)
		set @madb1 = (select MaDoiBong from deleted)
		set @soluongcauthu1 = (select count(distinct MaCauThu)
							  from CauThuThuocDoiBongThamGiaGiaiDau
							  where MaGiaiDau = @mamg1
							  and MaDoiBong = @madb1)
		set @soluongcauthunuocngoai = (select count(distinct MaCauThu)
									   from CauThuThuocDoiBongThamGiaGiaiDau
									   where MaGiaiDau = @mamg1
									   and MaDoiBong = @madb1
									   and MaLoaiCauThu = 2)

		update DoiBongThamGiaGiaiDau
		set SoLuongCauThuDangKy = @soluongcauthu1, SoLuongCauThuNuocNgoaiDangKy = @soluongcauthunuocngoai1
		where MaGiaiDau = @mamg1
		and MaDoiBong = @madb1
	end
end
go


if object_id('vtg_SoLuongDoiHoanTatDangKyGiaiDau', 'tr') is not null
drop trigger vtg_SoLuongDoiHoanTatDangKyGiaiDau
go
create trigger vtg_SoLuongDoiHoanTatDangKyGiaiDau on DoiBongThamGiaGiaiDau
for insert, update, delete
as
begin
	declare @madb varchar(10)
	declare @mamg varchar(10)
	if exists (select * from inserted) or update(HoanTatDangKy)
	begin
		set @madb = (select MaDoiBong from inserted)
		set @mamg = (select MaGiaiDau from inserted)
		update MuaGiai
		set SoLuongDoiDaDangKy = (select count(*)
									from DoiBongThamGiaGiaiDau db
									where db.MaGiaiDau = @mamg
									and db.HoanTatDangKy = 1)
		where MaMuaGiai = @mamg
	end
	if exists (select * from deleted)
	begin
		set @madb = (select MaDoiBong from deleted)
		set @mamg = (select MaGiaiDau from deleted)
		update MuaGiai
		set SoLuongDoiDaDangKy = (select count(*)
									from DoiBongThamGiaGiaiDau db
									where db.MaGiaiDau = @mamg
									and db.HoanTatDangKy = 1)
		where MaMuaGiai = @mamg
	end
end
go


if object_id('vtg_UpdateSoLuongBanThang', 'tr') is not null
drop trigger vtg_UpdateSoLuongBanThang
go
create trigger vtg_UpdateSoLuongBanThang on SoLieuThongKeCauThu
for insert, update, delete
as
begin
	declare @tongsobanthang int
	declare @mamg varchar(10)
	declare @mact varchar(10)
	if exists (select * from inserted)
	begin
		set @tongsobanthang = (select TongSoBanThang from inserted)
		set @mamg = (select MaMuaGiai from inserted)
		set @mact = (select MaCauThu from inserted)

		update CauThuThuocDoiBongThamGiaGiaiDau
		set TongSoBanThang = @tongsobanthang
		where MaCauThu = @mact
		and MaGiaiDau = @mamg
	end
	if exists (select * from deleted)
	begin
		set @tongsobanthang = (select TongSoBanThang from deleted)
		set @mamg = (select MaMuaGiai from deleted)
		set @mact = (select MaCauThu from deleted)

		update CauThuThuocDoiBongThamGiaGiaiDau
		set TongSoBanThang = @tongsobanthang
		where MaCauThu = @mact
		and MaGiaiDau = @mamg
	end
end