﻿USE [master]
GO
/****** Object:  Database [QuanLyGiaiVoDichQuocGiaDB]    Script Date: 1/3/2020 11:24:52 PM ******/
CREATE DATABASE [QuanLyGiaiVoDichQuocGiaDB]
 CONTAINMENT = NONE

GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [QuanLyGiaiVoDichQuocGiaDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [QuanLyGiaiVoDichQuocGiaDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [QuanLyGiaiVoDichQuocGiaDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [QuanLyGiaiVoDichQuocGiaDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [QuanLyGiaiVoDichQuocGiaDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [QuanLyGiaiVoDichQuocGiaDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [QuanLyGiaiVoDichQuocGiaDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [QuanLyGiaiVoDichQuocGiaDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [QuanLyGiaiVoDichQuocGiaDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [QuanLyGiaiVoDichQuocGiaDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [QuanLyGiaiVoDichQuocGiaDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [QuanLyGiaiVoDichQuocGiaDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [QuanLyGiaiVoDichQuocGiaDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [QuanLyGiaiVoDichQuocGiaDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [QuanLyGiaiVoDichQuocGiaDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [QuanLyGiaiVoDichQuocGiaDB] SET  ENABLE_BROKER 
GO
ALTER DATABASE [QuanLyGiaiVoDichQuocGiaDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [QuanLyGiaiVoDichQuocGiaDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [QuanLyGiaiVoDichQuocGiaDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [QuanLyGiaiVoDichQuocGiaDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [QuanLyGiaiVoDichQuocGiaDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [QuanLyGiaiVoDichQuocGiaDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [QuanLyGiaiVoDichQuocGiaDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [QuanLyGiaiVoDichQuocGiaDB] SET RECOVERY FULL 
GO
ALTER DATABASE [QuanLyGiaiVoDichQuocGiaDB] SET  MULTI_USER 
GO
ALTER DATABASE [QuanLyGiaiVoDichQuocGiaDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [QuanLyGiaiVoDichQuocGiaDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [QuanLyGiaiVoDichQuocGiaDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [QuanLyGiaiVoDichQuocGiaDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [QuanLyGiaiVoDichQuocGiaDB] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'QuanLyGiaiVoDichQuocGiaDB', N'ON'
GO
USE [QuanLyGiaiVoDichQuocGiaDB]
GO
/****** Object:  Table [dbo].[CauThu]    Script Date: 1/3/2020 11:24:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CauThu](
	[MaCauThu] [varchar](10) NOT NULL,
	[HoTenCauThu] [nvarchar](50) NULL,
	[NgaySinh] [date] NULL,
	[MaLoaiCauThu] [int] NULL,
	[MaDoiBong] [varchar](10) NULL,
	[TongSoBanThang] [int] NULL,
	[DaXoa] [bit] NULL,
 CONSTRAINT [PK_CauThu] PRIMARY KEY CLUSTERED 
(
	[MaCauThu] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CauThuThuocDoiBongThamGiaGiaiDau]    Script Date: 1/3/2020 11:24:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CauThuThuocDoiBongThamGiaGiaiDau](
	[MaGiaiDau] [varchar](10) NOT NULL,
	[MaDoiBong] [varchar](10) NULL,
	[TenDoiBong] [nvarchar](50) NULL,
	[MaCauThu] [varchar](10) NOT NULL,
	[TenCauThu] [nvarchar](50) NULL,
	[MaLoaiCauThu] [int] NULL,
	[TongSoBanThang] [int] NULL,
 CONSTRAINT [PK_CauThuThuocDoiBongThamGiaGiaiDau] PRIMARY KEY CLUSTERED 
(
	[MaGiaiDau] ASC,
	[MaCauThu] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ChiTietBanThang]    Script Date: 1/3/2020 11:24:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ChiTietBanThang](
	[MaMuaGiai] [varchar](10) NOT NULL,
	[MaVongDau] [varchar](10) NOT NULL,
	[MaTranDau] [varchar](10) NOT NULL,
	[MaCauThu] [varchar](10) NOT NULL,
	[MaDoiBong] [varchar](10) NULL,
	[LoaiBanThang] [int] NOT NULL,
	[ThoiDiem] [time](7) NOT NULL,
 CONSTRAINT [PK_KetQua] PRIMARY KEY CLUSTERED 
(
	[MaMuaGiai] ASC,
	[MaVongDau] ASC,
	[MaTranDau] ASC,
	[MaCauThu] ASC,
	[LoaiBanThang] ASC,
	[ThoiDiem] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ChiTietTranDau]    Script Date: 1/3/2020 11:24:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ChiTietTranDau](
	[MaMuaGiai] [varchar](10) NOT NULL,
	[MaTranDau] [varchar](10) NOT NULL,
	[MaVongDau] [varchar](10) NOT NULL,
	[NgayThiDau] [date] NULL,
	[GioThiDau] [time](7) NULL,
	[ChuNha] [varchar](10) NULL,
	[Khach] [varchar](10) NULL,
	[DaThiDau] [int] NULL,
	[SoBanThang_ChuNha] [int] NULL,
	[SoBanThang_Khach] [int] NULL,
 CONSTRAINT [PK_LichThiDau] PRIMARY KEY CLUSTERED 
(
	[MaMuaGiai] ASC,
	[MaVongDau] ASC,
	[MaTranDau] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DoiBong]    Script Date: 1/3/2020 11:24:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DoiBong](
	[MaDoiBong] [varchar](10) NOT NULL,
	[TenDoiBong] [nvarchar](50) NULL,
	[SanNha] [nvarchar](50) NULL,
	[GhiChu] [bit] NULL,
	[DaXoa] [bit] NULL,
 CONSTRAINT [PK_DoiBong] PRIMARY KEY CLUSTERED 
(
	[MaDoiBong] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DoiBongThamGiaGiaiDau]    Script Date: 1/3/2020 11:24:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DoiBongThamGiaGiaiDau](
	[MaGiaiDau] [varchar](10) NOT NULL,
	[MaDoiBong] [varchar](10) NOT NULL,
	[TenDoiBong] [nvarchar](50) NULL,
	[SoLuongCauThuDangKy] [int] NULL,
	[SoLuongCauThuNuocNgoaiDangKy] [int] NULL,
	[HoanTatDangKy] [bit] NULL,
 CONSTRAINT [PK_DoiBongThamGiaGiaiDau] PRIMARY KEY CLUSTERED 
(
	[MaGiaiDau] ASC,
	[MaDoiBong] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LoaiCauThu]    Script Date: 1/3/2020 11:24:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoaiCauThu](
	[MaLoaiCauThu] [int] NOT NULL,
	[TenLoaiCauThu] [nvarchar](50) NULL,
 CONSTRAINT [PK_LoaiCauThu] PRIMARY KEY CLUSTERED 
(
	[MaLoaiCauThu] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MuaGiai]    Script Date: 1/3/2020 11:24:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MuaGiai](
	[MaMuaGiai] [varchar](10) NOT NULL,
	[TenMuaGiai] [nvarchar](50) NULL,
	[SoLuongDoiThamDuToiDa] [int] NULL,
	[SoLuongDoiDaDangKy] [int] NULL,
	[HoanTatDangKy] [bit] NULL,
 CONSTRAINT [PK_MuaGiai] PRIMARY KEY CLUSTERED 
(
	[MaMuaGiai] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SoLieuThongKeCauThu]    Script Date: 1/3/2020 11:24:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SoLieuThongKeCauThu](
	[MaMuaGiai] [varchar](10) NOT NULL,
	[MaVongDau] [varchar](10) NOT NULL,
	[MaCauThu] [varchar](10) NOT NULL,
	[MaDoiBong] [varchar](10) NULL,
	[SoBanThang] [int] NULL,
	[TongSoBanThang] [int] NULL,
	[Hang] [int] NULL,
 CONSTRAINT [PK_BangXepHangCauThu] PRIMARY KEY CLUSTERED 
(
	[MaMuaGiai] ASC,
	[MaVongDau] ASC,
	[MaCauThu] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SoLieuThongKeDoiBong]    Script Date: 1/3/2020 11:24:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SoLieuThongKeDoiBong](
	[MaMuaGiai] [varchar](10) NOT NULL,
	[MaVongDau] [varchar](10) NOT NULL,
	[MaDoiBong] [varchar](10) NOT NULL,
	[SoTranDaThiDau] [int] NULL,
	[TongSoTranThang] [int] NULL,
	[TongSoTranThua] [int] NULL,
	[TongSoTranHoa] [int] NULL,
	[SoBanThang] [int] NULL,
	[TongSoBanThang] [int] NULL,
	[SoBanThua] [int] NULL,
	[TongSoBanThua] [int] NULL,
	[Diem] [int] NULL,
	[TongDiem] [int] NULL,
	[HieuSo] [int] NULL,
	[TongHieuSo] [int] NULL,
	[Hang] [int] NULL,
 CONSTRAINT [PK_ThongKeDoiBong] PRIMARY KEY CLUSTERED 
(
	[MaMuaGiai] ASC,
	[MaVongDau] ASC,
	[MaDoiBong] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'', NULL, NULL, NULL, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT001', N'Lê Văn Sơn', CAST(N'1993-04-05' AS Date), 1, N'DB01', 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT002', N'Lê Văn Quyên', CAST(N'1990-03-08' AS Date), 1, N'DB01', 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT003', N'Nguyễn Trường Sơn', CAST(N'1991-04-06' AS Date), 1, N'DB01', 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT004', N'Lương Xuân Trường', CAST(N'1990-12-03' AS Date), 1, N'DB01', 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT005', N'Nguyễn Phong Hồng Duy', CAST(N'1989-03-04' AS Date), 1, N'DB01', 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT006', N'Trần Minh Vương', CAST(N'1990-12-07' AS Date), 1, N'DB01', 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT007', N'Nguyễn Văn Toàn', CAST(N'1993-08-03' AS Date), 1, N'DB01', 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT008', N'Nguyễn Công Phượng', CAST(N'1990-07-07' AS Date), 1, N'DB01', 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT009', N'Nguyễn Tuấn Anh', CAST(N'1991-12-12' AS Date), 1, N'DB01', 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT010', N'Vũ Văn Thanh', CAST(N'1992-10-22' AS Date), 1, N'DB01', 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT011', N'Vũ Vương Hồng', CAST(N'1993-04-05' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT012', N'Trần Thanh Sơn', CAST(N'1993-05-17' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT013', N'Đặng Văn Ni', CAST(N'1990-04-30' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT014', N'Nguyễn Kiên Quyết', CAST(N'1990-07-07' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT015', N'Tạ Thái Học ', CAST(N'1993-09-18' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT016', N'Lương Hoàng Nam', CAST(N'1996-12-13' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT017', N'A Hoàng', CAST(N'1988-04-09' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT018', N'Triệu Việt Hưng', CAST(N'1994-07-18' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT019', N'Dụng Quang Nho', CAST(N'1994-07-18' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT020', N'Phạm Nguyên Sa', CAST(N'1997-04-10' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT021', N'Cao Thái Hoàng', CAST(N'1996-12-10' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT022', N'Nguyễn Minh Minh ', CAST(N'1995-09-16' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT023', N'Lê Hùng Thắng', CAST(N'1994-11-14' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT024', N'Lỹ Ái Chân', CAST(N'1988-04-09' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT025', N'Cao Thế Lĩnh ', CAST(N'1988-12-23' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT026', N'Lê Căn Đình', CAST(N'1995-02-27' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT027', N'Vũ Khác Trinh', CAST(N'1992-04-05' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT028', N'Lê Bảo Ngọc Phát', CAST(N'1991-09-22' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT029', N'Lê Quốc Tuấn', CAST(N'1994-05-16' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT030', N'Trần Quốc Cao', CAST(N'1989-10-26' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT031', N'Đoàn Văn Hậu', CAST(N'1995-04-10' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT032', N'Nguyễn Cường Đại', CAST(N'1997-03-08' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT033', N'Nguyễn Thanh Phong', CAST(N'1998-10-12' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT034', N'Cát Đại Tường', CAST(N'1998-08-19' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT035', N'Đặng Thái Hiển', CAST(N'1995-01-11' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT036', N'Đỗ Khanh Lê', CAST(N'1990-10-20' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT037', N'Jichardo', CAST(N'1996-06-09' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT039', N'Jayson Star', CAST(N'1990-10-20' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT040', N'Rick DiViick', CAST(N'1990-10-20' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT041', N'Jiha Jackma', CAST(N'1995-02-20' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT042', N'DiChione', CAST(N'1996-06-30' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT043', N'Domacali Tak', CAST(N'1992-01-25' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT044', N'Sarit Cal', CAST(N'1996-06-30' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT045', N'Diana John', CAST(N'1996-08-02' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT046', N'DonPichen Luu', CAST(N'1995-02-20' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT047', N'Lucian Dimine', CAST(N'1995-05-21' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT048', N'Nguyễn Bảo Quốc', CAST(N'1989-09-13' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT049', N'Cao Minh Nhọc', CAST(N'1989-09-13' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT050', N'Nguyễn Thái Lê', CAST(N'1995-02-20' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT051', N'Đỗ Văn Maxlen', CAST(N'1989-09-13' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT052', N'Cao Văn Tri Hoàn', CAST(N'1989-09-13' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT053', N'Lý Thế Mĩ', CAST(N'1992-07-05' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT054', N'Nguyễn Hà Mai', CAST(N'1995-02-20' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT055', N'Lê Anh ', CAST(N'1992-01-25' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT056', N'Chu Văn Mi', CAST(N'1992-01-25' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT057', N'Nguyễn Hoàng Văn Lê', CAST(N'1994-01-19' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT058', N'Nguyễn Phi Hùng', CAST(N'1992-07-05' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT059', N'Đỗ Ngọc Vinh', CAST(N'1995-05-21' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT060', N'Vĩnh Xuân Quyền', CAST(N'1995-05-21' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT061', N'Hà Tôn Sách', CAST(N'1990-06-08' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT062', N'Cao Chí Tài', CAST(N'1994-01-19' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT063', N'Nguyễn Hoài Linh', CAST(N'1990-08-30' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT064', N'Đinh Sơn Tùng', CAST(N'1992-07-05' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT065', N'Lê Bảo Bảo', CAST(N'1990-08-30' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT066', N'Cao Văn Giót', CAST(N'1994-01-19' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT067', N'Phạm Đình Khôi Minh', CAST(N'1992-01-25' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT068', N'Chí Càng Khôn', CAST(N'1994-01-19' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT069', N'Nguyễn Chinh Thám', CAST(N'1990-08-30' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT070', N'Đỗ Xuân Trinh Hoàng', CAST(N'1992-07-05' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT071', N'Thái Văn Mỹ', CAST(N'1991-03-12' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT072', N'Lê Hoàng Tuyên', CAST(N'1992-04-17' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT073', N'Đá Văn Quách', CAST(N'1992-04-17' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT074', N'Sơn Thạch Lê', CAST(N'1991-03-12' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT075', N'Nguyễn Quốc', CAST(N'1991-03-12' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT076', N'Hàn Quốc Hùng', CAST(N'1992-07-05' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT077', N'Đỗ Tài', CAST(N'1993-11-30' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT078', N'Hoàng Khuyển', CAST(N'1992-01-25' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT079', N'Nguyễn Xuân Chí ', CAST(N'1993-11-30' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT080', N'Nguyễn Xuân Mới', CAST(N'1992-01-25' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT081', N'Cao Bá Quát', CAST(N'1990-08-30' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT082', N'Lý Thái Vũ', CAST(N'1991-03-12' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT083', N'Lê Hoàng Tôn', CAST(N'1990-08-30' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT084', N'Vinh Cao Đế', CAST(N'1992-07-05' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT085', N'Nguyễn Chu Sơn', CAST(N'1992-04-17' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT086', N'Đỗ Minh Hoàng Quách', CAST(N'1993-11-30' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT087', N'Lê Quách Minh Hoàng', CAST(N'1993-11-30' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT088', N'Đỗ Văn Huỳnh', CAST(N'1991-02-12' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT089', N'Mai Chí Tôn', CAST(N'1991-02-12' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT090', N'Hà Văn Phụ', CAST(N'1991-02-12' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT091', N'Ca Phi', CAST(N'1992-07-05' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT092', N'Đặng Hoàng Đăng', CAST(N'1992-04-17' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT093', N'Mai A tí', CAST(N'1993-11-30' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT094', N'Trí Hoàng Tôn', CAST(N'1994-02-28' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT095', N'Văn Minh Đỗ', CAST(N'1994-02-28' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT096', N'Trí Minh Tài Văn', CAST(N'1993-11-30' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT097', N'Nguyễn Hoàng Sang', CAST(N'1992-07-05' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT098', N'Nguyễn Hoàng Nguyên', CAST(N'1994-02-28' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT099', N'Tô Văn Lịch', CAST(N'1992-07-05' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT100', N'Phạm Văn Phong', CAST(N'1990-04-05' AS Date), 1, NULL, 0, NULL)
GO
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT101', N'Trần Văn Chiến', CAST(N'1991-03-03' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT102', N'Trần Văn Học', CAST(N'1992-06-09' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT103', N'Huỳnh Tấn Sinh', CAST(N'1994-10-10' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT104', N'Trần Văn Tâm', CAST(N'1992-12-25' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT105', N'Đặng Hữu Phước', CAST(N'1993-04-05' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT106', N'Hà Minh Tuấn', CAST(N'1993-04-05' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT107', N'Phan Đình Thắng', CAST(N'1993-04-05' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT108', N'Trần Hoàng Hưng', CAST(N'1993-04-05' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT109', N'Nguyễn Hoàng Quốc Chí', CAST(N'1993-04-05' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT110', N'Ngô Đức Thắng', CAST(N'1993-04-05' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT111', N'Đào Duy Khánh', CAST(N'1988-04-09' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT112', N'Ngô Quang Huy', CAST(N'1988-04-09' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT113', N'Nguyễn Huy Tân', CAST(N'1988-04-09' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT114', N'Trần Mạnh Toàn', CAST(N'1988-04-09' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT115', N'Trần Văn Hòa', CAST(N'1992-05-05' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT116', N'Nguyễn Trần Mila', CAST(N'1990-04-30' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT117', N'Phạm Văn Tuấn Tú', CAST(N'1991-04-20' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT118', N'Cao Thanh Vinh', CAST(N'1991-04-20' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT119', N'Nguyễn Chí Nguyên', CAST(N'1993-03-03' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT120', N'Phan Tài Lộc', CAST(N'1991-04-20' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT121', N'Đỗ Minh Thuận', CAST(N'1988-04-05' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT122', N'Cao Lê Đỗ', CAST(N'1988-04-09' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT123', N'Đỗ Duy Khải', CAST(N'1992-03-29' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT124', N'Minh Vương Hoàng', CAST(N'1988-04-09' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT125', N'Đào Bá Đức', CAST(N'1993-05-17' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT126', N'Huỳnh Xuân Minh', CAST(N'1993-05-17' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT127', N'Dương Văn', CAST(N'1990-07-07' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT128', N'Nguyễn Minh Tinh', CAST(N'1990-07-07' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT129', N'Lê Anh Tài', CAST(N'1988-04-05' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT130', N'Nguyễn Trọng Đại', CAST(N'1992-03-29' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT131', N'Võ Ngọc Đức', CAST(N'1990-07-07' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT132', N'Phi Xuân', CAST(N'1992-03-29' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT133', N'Cao Thái Minh', CAST(N'1990-04-30' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT134', N'Trần Đình Hoàng', CAST(N'1992-03-29' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT135', N'Hồ Sĩ Sâm', CAST(N'1990-07-07' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT136', N'Hồ Tuấn Tài', CAST(N'1996-02-17' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT137', N'Phạm Xuân Mạnh', CAST(N'1996-02-17' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT138', N'Dương Văn Khoa', CAST(N'1990-07-07' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT139', N'Phan Văn Đức', CAST(N'1995-07-12' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT140', N'Trịnh Mai Anh', CAST(N'1995-07-12' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT141', N'Mai Tiến Đạt', CAST(N'1995-07-12' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT142', N'Lê Công Ơn', CAST(N'1988-04-09' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT143', N'Miêng Xuân Cái', CAST(N'1992-12-12' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT144', N'Lê Lộc Phát', CAST(N'1995-07-12' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT145', N'Công Minh', CAST(N'1996-02-17' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT146', N'Nguyễn Hoàng Tiến Sang', CAST(N'1993-09-18' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT147', N'Phan Thanh Tuấn', CAST(N'1992-12-12' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT148', N'Lâm Tế Đàn', CAST(N'1997-08-13' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT149', N'Trương Minh Hiển', CAST(N'1996-02-17' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT150', N'Huỳnh Khương Ninh', CAST(N'1993-09-18' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT151', N'Phạm Công Danh', CAST(N'1996-02-17' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT152', N'Đào Hữu Duy', CAST(N'1997-08-13' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT153', N'Đào Hữu Nghĩa', CAST(N'1992-12-12' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT154', N'Quàng Thế Tài', CAST(N'1997-08-13' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT155', N'Hồ Sỹ Giáp', CAST(N'1988-04-09' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT156', N'Tô Minh Hải', CAST(N'1997-08-13' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT157', N'Lý Gia Bảo', CAST(N'1992-12-12' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT158', N'Đỗ Việt Sơn', CAST(N'1996-04-08' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT159', N'Minh An', CAST(N'1988-04-09' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT160', N'Trương Thế Vinh', CAST(N'1996-04-08' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT161', N'Nguyễn Thái Ngọc Hoàng', CAST(N'1994-02-19' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT162', N'Trần Trà Sữa', CAST(N'1996-02-08' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT163', N'Lê Luân Vũ', CAST(N'1990-03-02' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT164', N'Thái Hùng Minh', CAST(N'1988-04-09' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT165', N'Nguyễn Xuân Mới', CAST(N'1994-07-18' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT166', N'Nguyễn Triều Vạn', CAST(N'1996-04-08' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT167', N'Cao Xuân Lĩnh', CAST(N'1996-04-08' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT168', N'Nghinh Mai Lý', CAST(N'1988-04-09' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT169', N'Phạm Đức Anh', CAST(N'1996-04-08' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT170', N'Phạm Hoàng Chí', CAST(N'1990-04-26' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT171', N'Mai Văn Thị', CAST(N'1990-04-26' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT172', N'Huỳnh Hoàng Việt', CAST(N'1994-10-14' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT173', N'Nguyễn Ngọc Long', CAST(N'1988-04-09' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT174', N'Đào Duy Khương', NULL, 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT175', N'Lý Khả Ái Quốc', CAST(N'1990-04-26' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT176', N'Đặng Văn Lâm', CAST(N'1993-09-18' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT177', N'Đặng Mi ', CAST(N'1990-04-26' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT178', N'Trương Thái Tú', CAST(N'1994-07-18' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT179', N'Nguyễn Lam', CAST(N'1990-04-30' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT180', N'Lâm Quý', CAST(N'1990-04-30' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT181', N'Đình Tài ', CAST(N'1994-10-14' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT182', N'Đinh Bảo Linh', CAST(N'1993-09-18' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT183', N'Cao Bá Sơn Đài', CAST(N'1993-09-18' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT184', N'Lê Hùng Dũng', CAST(N'1993-09-18' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT185', N'Trần Văn Chiến', CAST(N'1994-12-17' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT186', N'Trần Văn Học', CAST(N'1990-04-30' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT187', N'Trần Dần', CAST(N'1994-07-18' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT188', N'Nguyễn Bá Nam', CAST(N'1993-02-11' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT189', N'Huỳnh Văn Tình', CAST(N'1993-09-18' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT190', N'Đỗ Văn Khiêm', CAST(N'1993-02-11' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT191', N'Mai Lý Ka', CAST(N'1990-04-30' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT192', N'Hồ Phúc Thịnh', CAST(N'1993-02-11' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT193', N'Cao Xuân Thắng ', CAST(N'1994-10-14' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT194', N'Châu Kiệt ', CAST(N'1994-02-19' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT195', N'Luân Thái Hoài', CAST(N'1994-02-18' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT196', N'Nguyễn Trung Hiếu', CAST(N'1997-04-10' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT197', N'Nguyễn Trung Hậu', CAST(N'1994-02-18' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT198', N'Nguyễn Ngọc Anh', CAST(N'1996-12-10' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT199', N'Nguyễn Như Quân', CAST(N'1990-04-30' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT200', N'Vũ Đình Phong', CAST(N'1997-04-10' AS Date), 1, NULL, 0, NULL)
GO
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT201', N'Lê Văn Quyết', CAST(N'1994-10-14' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT202', N'Thái Dũng', CAST(N'1996-12-10' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT203', N'Trần Công Dụng', CAST(N'1996-12-10' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT204', N'Chiến Minh Công', CAST(N'1994-10-14' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT205', N'Phan Thái Vinh', CAST(N'1991-09-09' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT206', N'Đinh Tiến Ninh', CAST(N'1991-09-09' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT207', N'Lưu Công Sơn', CAST(N'1988-04-09' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT208', N'Đặng Văn Trâm', CAST(N'1996-12-10' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT209', N'Hoàng Xuân Vinh', CAST(N'1996-12-10' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT210', N'Hà Đức Chinh', CAST(N'1995-09-16' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT211', N'Hà Tiều Phu', CAST(N'1994-02-19' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT212', N'Minh Hiểu Tài', CAST(N'1995-09-16' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT213', N'Hà Hiểu Minh', CAST(N'1994-02-18' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT214', N'Trương Thái Minh ', CAST(N'1991-09-09' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT215', N'Trương Minh Hiền', CAST(N'1988-12-23' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT216', N'Hoàng Văn Khánh', CAST(N'1991-09-09' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT217', N'Nhâm Mạnh Dũng', CAST(N'1995-09-16' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT218', N'Phạm Thế Nhật', CAST(N'1995-09-16' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT219', N'Nguyễn Trung Bình', CAST(N'1991-09-09' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT220', N'Trương Văn Kỳ', CAST(N'1990-02-14' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT221', N'Nguyễn Thái Sang', CAST(N'1994-02-19' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT222', N'Quế Ngọc Hải', CAST(N'1990-02-14' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT223', N'Trương Văn Thiết', CAST(N'1995-09-16' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT224', N'Hồ Hùng Cường', CAST(N'1990-02-14' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT225', N'Phạm Văn Quyết', CAST(N'1990-02-14' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT226', N'Giang Trần Quách', CAST(N'1988-04-09' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT227', N'Trần Minh Tân', CAST(N'1994-11-14' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT228', N'Lê Văn Nhật', CAST(N'1994-02-18' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT229', N'Đoàn Quốc Quý', CAST(N'1990-02-01' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT230', N'Phạm Lê Long', CAST(N'1990-12-23' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT231', N'Đinh Bảo Phúc', CAST(N'1990-02-01' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT232', N'Nguyễn Trung Trí', CAST(N'1994-11-14' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT233', N'Hà Huy Giáp', CAST(N'1990-12-23' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT234', N'Lê Vương Quốc', CAST(N'1990-12-23' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT235', N'Nguyễn Thị Quyến', CAST(N'1990-02-01' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT236', N'Trịnh Công Tình', CAST(N'1988-12-23' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT237', N'Mai Công Nam', CAST(N'1996-05-01' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT238', N'Nguyễn Công Trình', CAST(N'1990-02-01' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT239', N'Nguyễn Quốc Chí', CAST(N'1996-05-01' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT240', N'Trần Đình Đồng', CAST(N'1996-05-01' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT241', N'Trịnh Hoa Hồng', CAST(N'1994-02-18' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT242', N'Ngô Bá Khá', CAST(N'1996-05-01' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT243', N'Đỗ Minh Mai', CAST(N'1996-05-01' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT244', N'Lê Công Thái', CAST(N'1994-02-18' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT245', N'Huỳnh Văn Duy', CAST(N'1997-04-15' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT246', N'Lý Văn Hoàng', CAST(N'1998-05-04' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT247', N'Nguyễn Hiền', CAST(N'1993-01-01' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT248', N'Lề Hồng Huy', CAST(N'1991-09-09' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT249', N'Nguyễn Ngọc Duy', CAST(N'1993-01-01' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT250', N'Trịnh Thái Bình', CAST(N'1997-04-15' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT251', N'Cao Thái Bình', CAST(N'1996-05-01' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT252', N'Nguyễn Sơn Tùng', CAST(N'1997-04-15' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT253', N'Lý Minh Quốc', CAST(N'1996-05-01' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT254', N'Phan Thị Lệ Bình', CAST(N'1998-05-04' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT255', N'Cao Xuân Nghị', CAST(N'1998-05-04' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT256', N'Nguyễn Tuấn Vi', CAST(N'1988-12-23' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT257', N'Chinh Lê', CAST(N'1995-02-27' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT258', N'Châu Bùi', CAST(N'1995-02-27' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT259', N'Chiến Minh Công', CAST(N'1990-02-01' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT260', N'Đinh Mai Văn', CAST(N'1993-01-01' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT261', N'Vũ Vy Vượng', CAST(N'1995-02-27' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT262', N'Trương Minh Hiển', CAST(N'1990-02-01' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT263', N'Nguyễn Anh Khoa', CAST(N'1991-09-09' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT264', N'Lê Ái Hoàng Minh', CAST(N'1998-05-04' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT265', N'Trịnh Quốc Ca', CAST(N'1990-02-01' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT266', N'Thái Văn Toàn', CAST(N'1991-09-09' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT267', N'Minh Lê Quốc', CAST(N'1996-05-01' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT268', N'Nguyễn Huy Hoàng Hải', CAST(N'1992-04-05' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT269', N'Trương Minh Tuyền', CAST(N'1992-04-05' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT270', N'Nguyễn Hoài nam', CAST(N'1991-09-22' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT271', N'Phương Mỹ Lính', CAST(N'1991-09-22' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT272', N'Lê Bảo Khánh', CAST(N'1993-01-01' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT273', N'Lý Mộc Châu', CAST(N'1991-09-09' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT274', N'Nguyễn Huy Nam', CAST(N'1996-05-01' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT275', N'Đinh Gia Bảo', CAST(N'1997-09-05' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT276', N'Huỳnh Minh Chu', CAST(N'1988-12-23' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT277', N'Đỗ Si Tình', CAST(N'1997-09-05' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT278', N'Minh Quốc Đại', CAST(N'1996-05-01' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT279', N'Tạ Chấn Phong', CAST(N'1996-09-17' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT280', N'Đỗ Thế Tài', CAST(N'1997-09-05' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT281', N'La Anh Trí', CAST(N'1996-05-01' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT282', N'Đỗ Hoàng Huy', CAST(N'1997-09-05' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT283', N'Hoàng Thái Vĩnh', CAST(N'1996-05-01' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT284', N'Chu Kiều Phong', CAST(N'1996-09-17' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT285', N'Nguyễn Đoàn Dự', CAST(N'1996-09-17' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT286', N'Bá Hùng Cường', CAST(N'1991-09-22' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT287', N'Tiến Thanh Liêm', CAST(N'1996-09-17' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT288', N'Huỳnh Thanh Sang', CAST(N'1996-09-17' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT289', N'La Anh Thanh', CAST(N'1996-05-01' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT290', N'Nguyễn Văn Quế', CAST(N'1994-05-16' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT291', N'Nguyễn Văn Kỳ', CAST(N'1988-12-23' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT292', N'Nguyễn Văn Quân', CAST(N'1994-05-16' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT293', N'Nguyễn Cao Đài', CAST(N'1991-09-09' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT294', N'Nguyễn Thiện', CAST(N'1994-05-16' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT295', N'Nhân Anh ', CAST(N'1991-09-22' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT296', N'Lê Dương Bảo', CAST(N'1994-05-16' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT297', N'Đặng Trọng Tín', CAST(N'1990-10-19' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT298', N'Nguyễn Minh Sơn', CAST(N'1994-05-16' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT299', N'Lê Hải Triều', CAST(N'1991-09-22' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT300', N'Nguyễn Quốc Thanh', CAST(N'1991-09-09' AS Date), 1, NULL, 0, NULL)
GO
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT301', N'Nguyễn Quốc Bình', CAST(N'1990-10-19' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT302', N'La Hoàng Thảo', CAST(N'1995-08-22' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT303', N'Đỗ Ngọc Thuận', CAST(N'1990-10-19' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT304', N'Đinh Chinh Tài', CAST(N'1995-08-22' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT305', N'Thạch Sơn Kiện', CAST(N'1990-10-19' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT306', N'Văn Thủy Tinh', CAST(N'1990-10-19' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT307', N'Chế Lan Viên', CAST(N'1989-10-26' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT308', N'Huỳnh Văn Hạ', CAST(N'1991-09-22' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT309', N'Nguyễn Văn Đạt', CAST(N'1995-04-10' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT310', N'Lê Đông Quốc', CAST(N'1995-04-10' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT311', N'Cao Khả Chính', CAST(N'1997-03-08' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT312', N'Chu Thái Bảo', CAST(N'1995-04-10' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT313', N'Lê Hoàng Khôi', CAST(N'1997-03-08' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT314', N'Nguyễn Xuân Bách', CAST(N'1991-09-22' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT315', N'Chu Thái An', CAST(N'1995-08-22' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT316', N'Nguyễn Anh Khương', CAST(N'1997-03-08' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT317', N'Nguyễn Thái Học', CAST(N'1995-01-11' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT318', N'Minh Lê Đáo', CAST(N'1995-01-11' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT319', N'Nguyễn Hoàng Công', CAST(N'1995-01-11' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT320', N'Lê Kim Quý', CAST(N'1988-12-23' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT321', N'Châu Hải Đăng', CAST(N'1997-03-08' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT322', N'Lê Minh Toàn', CAST(N'1997-03-08' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT323', N'Khổng Văn Tước', CAST(N'1998-12-10' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT324', N'Nguyễn Hiền Minh', CAST(N'1995-08-22' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT325', N'Huỳnh Văn Đô', CAST(N'1998-12-10' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT326', N'Đỗ Can', CAST(N'1998-10-12' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT327', N'Lê Công Thành', CAST(N'1995-02-20' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT328', N'La Chí Thành', CAST(N'1995-02-20' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT329', N'Lê Văn Nhật Tân', CAST(N'1995-02-20' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT330', N'Hà Duy Thiên', CAST(N'1998-12-10' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT331', N'Chí Mai', CAST(N'1991-09-22' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT332', N'Đỗ Quốc Nội', CAST(N'1998-10-12' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT333', N'Nguyễn Thanh Nguyên', CAST(N'1998-10-12' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT334', N'Hoàng Chu Du', CAST(N'1995-01-11' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT335', N'Cao Thiện Đức', CAST(N'1995-08-22' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT336', N'Đào Công Danh', CAST(N'1998-08-19' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT337', N'Nguyễn Hùng Dũng', CAST(N'1995-01-11' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT338', N'Văn Thanh', CAST(N'1998-08-19' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT339', N'Chu Vi Cá', CAST(N'1991-09-22' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT340', N'Lê Bảo Hà', CAST(N'1998-08-19' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT341', N'Cao Minh Nhựt Vinh', CAST(N'1998-08-19' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT342', N'Chu Cao Lãnh', CAST(N'1996-06-09' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT343', N'Đặng Hoàng Hải', CAST(N'1998-08-19' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT344', N'Minh Hoàng Huỳnh', CAST(N'1990-10-20' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT345', N'Huỳnh Minh Tinh', CAST(N'1995-08-22' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT346', N'Đổ Văn Siêu', CAST(N'1993-12-24' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT347', N'Thái Hoàng Hùng', CAST(N'1990-10-20' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT348', N'Chu Văn Lý', CAST(N'1993-12-24' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT349', N'Nguyễn Thành Hiệp', CAST(N'1993-12-24' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT350', N'Lê Công Vinh', CAST(N'1993-12-24' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT351', N'Lê Công Tài', CAST(N'1996-06-09' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT352', N'Đỗ Công Trí', CAST(N'1993-12-24' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT353', N'Nguyễn Khắc Hoan', CAST(N'1995-01-11' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT354', N'Hà Đình Trọng', CAST(N'1993-12-24' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT355', N'Trần Tiểu Hoàng', CAST(N'1996-06-09' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT356', N'Minh Văn Sĩ', CAST(N'1997-07-18' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT357', N'Nguyễn Sỉ Minh', CAST(N'1996-10-02' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT358', N'Ngọc Hùng', CAST(N'1997-07-18' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT359', N'Thế Anh ', CAST(N'1996-10-02' AS Date), 1, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT360', N'Patrick MiLa', CAST(N'1997-07-18' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT361', N'DonKi Hote', CAST(N'1996-10-02' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT362', N'LandRick Simar', CAST(N'1996-10-02' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT363', N'Sipachok', CAST(N'1990-10-20' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT364', N'NamiCaly', CAST(N'1996-10-02' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT365', N'Jack Sparrow', CAST(N'1997-07-18' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT366', N'Nick Fury', CAST(N'1998-09-09' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT367', N'Alex Domison', CAST(N'1995-02-20' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT368', N'RonalDiCa', CAST(N'1997-07-18' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT369', N'Pajic Man', CAST(N'1998-09-09' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT370', N'ArecBoo', CAST(N'1995-02-20' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT371', N'Calia Patch', CAST(N'1996-06-09' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT372', N'RakiTik', CAST(N'1998-09-09' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT373', N'SaRu Pache', CAST(N'1996-06-09' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT374', N'CaNaPek Meni', CAST(N'1998-09-09' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT375', N'Melio', CAST(N'1990-10-20' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT376', N'Ban Jackson', CAST(N'1997-12-01' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT377', N'Chichel Ricardo', CAST(N'1996-06-19' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT378', N'Sigeor', CAST(N'1990-10-20' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT379', N'Minh Bao Chong', CAST(N'1997-12-01' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT380', N'Seheung Min', CAST(N'1996-06-19' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT381', N'DiCalogMan', CAST(N'1997-12-01' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT382', N'Arialocal Mijack', CAST(N'1995-02-20' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT383', N'John Cena', CAST(N'1997-12-01' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT384', N'Calot terihan', CAST(N'1997-07-18' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT385', N'Jisa Bok', CAST(N'1997-12-01' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT386', N'Carido', CAST(N'1996-06-19' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT387', N'Huỳnh Kesley Alves', CAST(N'1996-06-09' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT388', N'Đoàn Marcelo', CAST(N'1995-02-20' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT389', N'Rianevie', CAST(N'1994-12-15' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT390', N'CadiNevi', CAST(N'1994-12-15' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT391', N'Anica Topiloi', CAST(N'1994-12-15' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT392', N'Supachik', CAST(N'1990-10-20' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT393', N'ToteRang', CAST(N'1994-12-15' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT394', N'Chon Qick', CAST(N'1996-06-30' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT395', N'Carew Nijack', CAST(N'1996-06-30' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT396', N'BuffYoeng', CAST(N'1995-02-20' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT397', N'Swarjack', CAST(N'1996-06-30' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT398', N'DomiHoCali', CAST(N'1994-12-15' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT399', N'SonHo Hea', CAST(N'1996-06-30' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT400', N'AleBanRoan', CAST(N'1993-09-02' AS Date), 2, NULL, 0, NULL)
GO
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT401', N'Aronoa', CAST(N'1992-01-25' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[CauThu] ([MaCauThu], [HoTenCauThu], [NgaySinh], [MaLoaiCauThu], [MaDoiBong], [TongSoBanThang], [DaXoa]) VALUES (N'CT402', N'JiJinpark', CAST(N'1992-01-25' AS Date), 2, NULL, 0, NULL)
INSERT [dbo].[DoiBong] ([MaDoiBong], [TenDoiBong], [SanNha], [GhiChu], [DaXoa]) VALUES (N'DB01', N'Hoàng Anh Gia Lai', N'Sân Nhà 1', 1, 0)
INSERT [dbo].[DoiBong] ([MaDoiBong], [TenDoiBong], [SanNha], [GhiChu], [DaXoa]) VALUES (N'DB02', N'Becamex Bình Dương', N'Sân Nhà 2', 1, 0)
INSERT [dbo].[DoiBong] ([MaDoiBong], [TenDoiBong], [SanNha], [GhiChu], [DaXoa]) VALUES (N'DB03', N'Nam Định', N'Sân Nhà 3', 1, 0)
INSERT [dbo].[DoiBong] ([MaDoiBong], [TenDoiBong], [SanNha], [GhiChu], [DaXoa]) VALUES (N'DB04', N'Hải Phòng', N'Sân Nhà 4', 1, 0)
INSERT [dbo].[DoiBong] ([MaDoiBong], [TenDoiBong], [SanNha], [GhiChu], [DaXoa]) VALUES (N'DB05', N'Sài Gòn', N'Sân Nhà 5', 1, 0)
INSERT [dbo].[DoiBong] ([MaDoiBong], [TenDoiBong], [SanNha], [GhiChu], [DaXoa]) VALUES (N'DB06', N'Quảng Nam', N'Sân Nhà 6', 1, 0)
INSERT [dbo].[DoiBong] ([MaDoiBong], [TenDoiBong], [SanNha], [GhiChu], [DaXoa]) VALUES (N'DB07', N'Thanh Hóa', N'Sân Nhà 7', 1, 0)
INSERT [dbo].[DoiBong] ([MaDoiBong], [TenDoiBong], [SanNha], [GhiChu], [DaXoa]) VALUES (N'DB08', N'Viettel', N'Sân Nhà 8', 1, 0)
INSERT [dbo].[DoiBong] ([MaDoiBong], [TenDoiBong], [SanNha], [GhiChu], [DaXoa]) VALUES (N'DB09', N'Sông Lam Nghệ An', N'Sân Nhà 9', 1, 0)
INSERT [dbo].[DoiBong] ([MaDoiBong], [TenDoiBong], [SanNha], [GhiChu], [DaXoa]) VALUES (N'DB10', N'Than Quảng Ninh', N'Sân Nhà 10', 1, 0)
INSERT [dbo].[DoiBong] ([MaDoiBong], [TenDoiBong], [SanNha], [GhiChu], [DaXoa]) VALUES (N'DB11', N'Hà Nội', N'Sân Nhà 11', 1, 0)
INSERT [dbo].[DoiBong] ([MaDoiBong], [TenDoiBong], [SanNha], [GhiChu], [DaXoa]) VALUES (N'DB12', N'SHB Đà Nẵng', N'Sân Nhà 12', 1, 0)
INSERT [dbo].[DoiBong] ([MaDoiBong], [TenDoiBong], [SanNha], [GhiChu], [DaXoa]) VALUES (N'DB13', NULL, N'Sân Nhà 13', 1, 0)
INSERT [dbo].[DoiBong] ([MaDoiBong], [TenDoiBong], [SanNha], [GhiChu], [DaXoa]) VALUES (N'DB14', N'TP.HCM', N'Sân Nhà 14', 1, 0)
INSERT [dbo].[DoiBong] ([MaDoiBong], [TenDoiBong], [SanNha], [GhiChu], [DaXoa]) VALUES (N'DB15', N'Fico Tây Ninh', N'Sân Nhà 15', 1, 0)
INSERT [dbo].[DoiBong] ([MaDoiBong], [TenDoiBong], [SanNha], [GhiChu], [DaXoa]) VALUES (N'DB16', N'Long An', N'Sân Nhà 16', 1, 0)
INSERT [dbo].[DoiBong] ([MaDoiBong], [TenDoiBong], [SanNha], [GhiChu], [DaXoa]) VALUES (N'DB17', N'Phố Hiến', N'Sân Nhà 17', 1, 0)
INSERT [dbo].[DoiBong] ([MaDoiBong], [TenDoiBong], [SanNha], [GhiChu], [DaXoa]) VALUES (N'DB18', N'Cần Thơ', N'Sân Nhà 18', 1, 0)
INSERT [dbo].[DoiBong] ([MaDoiBong], [TenDoiBong], [SanNha], [GhiChu], [DaXoa]) VALUES (N'DB19', N'Công An Nhân dân', N'Sân Nhà 19', 1, 0)
INSERT [dbo].[DoiBong] ([MaDoiBong], [TenDoiBong], [SanNha], [GhiChu], [DaXoa]) VALUES (N'DB20', N'Đồng Tháp', N'Sân Nhà 20', 1, 0)
INSERT [dbo].[DoiBong] ([MaDoiBong], [TenDoiBong], [SanNha], [GhiChu], [DaXoa]) VALUES (N'DB21', N'No', N'No', 0, 0)
USE [master]
GO
ALTER DATABASE [QuanLyGiaiVoDichQuocGiaDB] SET  READ_WRITE 
GO
