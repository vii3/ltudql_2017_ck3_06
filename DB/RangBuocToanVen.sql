﻿--------------------------------------------------	--------------------------------------------------
-- Từ bảng Lịch thi -> Khởi tạo bảng KetQua
-- Bảng kết quả chứa các thông tin Mã vòng đấu, Mã trận đấu, Mã cầu thủ, Loại bàn thắng, Thời điểm ghi bàn
-- Mã đội bóng CHƯA CẬP NHẬT
--------------------------------------------------
--	BẢNG TẦM ẢNH HƯỞNG
--					INSERT		UPDATE		DELETE		
--	LICHTHIDAU		  +			  +			  +
--------------------------------------------------	
if object_id('VTG_KhoiTaoKetQua', 'tr') is not null
drop trigger VTG_KhoiTaoKetQua
go
create trigger VTG_KhoiTaoKetQua on LichThiDau
for insert, update, delete
as
begin
	set nocount on
	if exists (select * from inserted)
	begin
		if (select i.DaThiDau from inserted i) is null
		begin
			declare @matd varchar(10)
			declare @mavd varchar(10)
			declare @mact varchar(10)
			declare @mamg varchar(10)

			declare curKhoiTaoKetQua cursor for
			select ct.MaCT, i.MaTranDau, i.MaVongDau, i.MaMuaGiai
			from inserted i, CauThu ct
			where ct.MaDB = i.ChuNha or ct.MaDB = i.Khach

			open curKhoiTaoKetQua
			fetch next from curKhoiTaoKetQua
			into @mact, @matd, @mavd, @mamg

			while @@fetch_status = 0
			begin
				insert into KetQua(MaMuaGiai, MaVongDau, MaTranDau, MaCT, LoaiBanThang, ThoiDiem)
				values (@mamg, @mavd, @matd, @mact, '-1', GETDATE())

				fetch next from curKhoiTaoKetQua
				into @mact, @matd, @mavd, @mamg
			end
			close curKhoiTaoKetQua
			deallocate curKhoiTaoKetQua		
		end
		else if (select i.DaThiDau from inserted i) = 1 and (select i.SoBanThang_ChuNha from inserted i) is null and (select i.SoBanThang_Khach from inserted i) is null
		begin
			-- Update Số bàn thắng chủ nhà và số bàn thắng đội khách = 0 khi trận đấu bắt đầu diễn ra
			declare @kt int
			set @kt = (select l.SoBanThang_ChuNha
					   from inserted i, LichThiDau l
					   where i.MaVongDau = l.MaVongDau
					   and i.MaTranDau = l.MaTranDau
					   and i.MaMuaGiai = l.MaMuaGiai)
			if @kt is null
			begin
				update LichThiDau
				set SoBanThang_ChuNha = 0
				from inserted, LichThiDau
				where inserted.MaVongDau = LichThiDau.MaVongDau
				and inserted.MaTranDau = LichThiDau.MaTranDau
				and inserted.MaMuaGiai = LichThiDau.MaMuaGiai
			end
			set @kt = (select l.SoBanThang_Khach
					   from inserted i, LichThiDau l
					   where i.MaVongDau = l.MaVongDau
					   and i.MaTranDau = l.MaTranDau
					   and i.MaMuaGiai = l.MaMuaGiai)
			if @kt is null
			begin
				update LichThiDau
				set SoBanThang_Khach = 0
				from inserted, LichThiDau
				where inserted.MaVongDau = LichThiDau.MaVongDau
				and inserted.MaTranDau = LichThiDau.MaTranDau
			end
		end
	end
end
go
--------------------------------------------------	--------------------------------------------------
--------------------------------------------------	--------------------------------------------------



--------------------------------------------------	--------------------------------------------------
-- Từ bảng Lịch thi đấu -> Update bảng kết quả (Tiếp nối trigger VTG_KhoiTaoKetQua)
-- Update xảy ra khi Lịch thi đấu được khởi tạo hoặc xóa, sửa
-- Trigger VTG_KhoiTaoKetQua kích hoạt sự kiện INSERT, UPDATE ở bảng kết quả
-- -> Dựa vào Mã cầu thủ -> Update Mã đội bóng tương ứng cho bảng Kết quả
--------------------------------------------------
--	BẢNG TẦM ẢNH HƯỞNG
--					INSERT		UPDATE		DELETE		
--	LICHTHIDAU		  +			  +			  +
--	KETQUA			  +			  +			  -
--------------------------------------------------
if object_id('VTG_Update1MDB', 'tr') is not null
drop trigger VTG_Update1MDB
go
create trigger VTG_Update1MDB on KetQua
after insert, update
as
begin
	set nocount on
	if exists (select count(*)
				from KetQua kq, inserted i
				where kq.MaCT = i.MaCT
				and kq.MaTranDau = i.MaTranDau
				and kq.MaVongDau = i.MaVongDau
				and kq.MaMuaGiai = i.MaMuaGiai)
	begin
		update KetQua
		set MaDB = ct.MaDB
		from inserted i, CauThu ct, KetQua kq
		where i.MaCT = ct.MaCT and i.MaCT = kq.MaCT
	end
end
go
--------------------------------------------------	--------------------------------------------------
--------------------------------------------------	--------------------------------------------------



-- Khi cập nhật kết quả ở bảng Kết quả -> Update Số bàn thắng chủ nhà, Số bàn thắng đội khách ở bảng Lịch thi đấu
--------------------------------------------------
--	BẢNG TẦM ẢNH HƯỞNG
--					INSERT		UPDATE		DELETE		
--	KETQUA			  +			  +			  +
--------------------------------------------------
if object_id('VTG_Update2LichThiDau', 'tr') is not null
drop trigger VTG_Update2LichThiDau
go
create trigger VTG_Update2LichThiDau on KetQua
after insert, update, delete
as
begin
	set nocount on
	if exists (select * from inserted)
	begin
		declare @tongbanthang int -- Tổng số bàn thắng một cầu thủ ghi được trong vòng đấu đang xét
		declare @tongbanthangvongdautruoc int -- Tống số bàn thắng cầu thủ ghi được từ vòng đấu đầu tiên tới vòng đấu đang xét
		declare @banthang int -- Số bàn thắng mà một đội ghi được trong vòng đấu đang xét
		declare @phanluoi int -- Số bàn phản lưới mà một đội trong vòng đấu đang xét
		declare @kt int -- Kiểm tra ? Chủ nhà : Khách
		declare @loaibt int -- Loại bàn thắng
		--set @loaibt = (select i.LoaiBanThang from inserted i)
		--if @loaibt != -1
		if update(LoaiBanThang)
		begin
			-- Kiểm tra đội đang cập nhật : 1 chủ nhà	!= 1 khách
			set @kt = (select count(*)
						from inserted i, LichThiDau l
						where i.MaTranDau = l.MaTranDau
						and i.MaVongDau = l.MaVongDau
						and i.MaMuaGiai = l.MaMuaGiai
						and i.MaDB = l.ChuNha) 
			-- Update Lịch thi đấu
			if @kt = 1
			begin
				set @banthang = (select count(*)
								from KetQua kq, inserted i
								where kq.MaTranDau = i.MaTranDau
								and kq.MaVongDau = i.MaVongDau
								and kq.MaMuaGiai = i.MaMuaGiai
								and kq.MaDB = i.MaDB
								and kq.LoaiBanThang != -1
								and kq.LoaiBanThang != -11
								group by kq.MaDB)
				set @phanluoi = (select count(*)
								from KetQua kq, inserted i
								where kq.MaTranDau = i.MaTranDau
								and kq.MaVongDau = i.MaVongDau
								and kq.MaMuaGiai = i.MaMuaGiai
								and kq.MaDB = i.MaDB
								and kq.LoaiBanThang = -11
								group by kq.MaDB)
				if @banthang is null
				set @banthang = 0
				if @phanluoi is null
				set @phanluoi = 0
				update LichThiDau
				set SoBanThang_ChuNha = @banthang, SoBanThang_Khach = SoBanThang_Khach + @phanluoi
				from inserted
				where LichThiDau.MaTranDau = inserted.MaTranDau 
				and LichThiDau.MaVongDau = inserted.MaVongDau
				and LichThiDau.ChuNha = inserted.MaDB
			end
			else
			begin
				set @banthang = (select count(*)
								from KetQua kq, inserted i
								where kq.MaTranDau = i.MaTranDau
								and kq.MaVongDau = i.MaVongDau
								and kq.MaMuaGiai = i.MaMuaGiai
								and kq.MaDB = i.MaDB
								and kq.LoaiBanThang != -1
								and kq.LoaiBanThang != -11
								group by kq.MaDB)
				set @phanluoi = (select count(*)
								from KetQua kq, inserted i
								where kq.MaTranDau = i.MaTranDau
								and kq.MaVongDau = i.MaVongDau
								and kq.MaMuaGiai = i.MaMuaGiai
								and kq.MaDB = i.MaDB
								and kq.LoaiBanThang = -11
								group by kq.MaDB)
				if @banthang is null
				set @banthang = 0
				if @phanluoi is null
				set @phanluoi = 0
				update LichThiDau
				set SoBanThang_Khach =  @banthang, SoBanThang_ChuNha = SoBanThang_ChuNha + @phanluoi
				from inserted, LichThiDau
				where LichThiDau.MaTranDau = inserted.MaTranDau 
				and LichThiDau.MaVongDau = inserted.MaVongDau
				and LichThiDau.Khach = inserted.MaDB
			end
			-- Hoàn tất update Lịch thi đấu

		end
	end
end
go
--------------------------------------------------	--------------------------------------------------
--------------------------------------------------	--------------------------------------------------



--------------------------------------------------	--------------------------------------------------
--------------------------------------------------	--------------------------------------------------
-- Cập nhật bảng Thống kê từ bảng Lịch thi đấu
-- Từ bảng Lịch thi đấu -> Khởi tạo bảng thống kê đội bóng tại mỗi vòng đấu
-- Bảng Thống kê lưu thông tin với mỗi vòng sẽ có danh sách tất cả các đội bóng
-- Mỗi đội bóng có thông tin Số bàn thắng, Số bàn thua
-- Từ Số bàn thắng, Số bàn thua -> Số điểm -> Tổng điểm (điểm tính từ vòng đấu đầu tới vòng đấu đang xét)
-- Từ bảng Lịch thi đấu -> Khởi tạo Bảng xếp hạng độ bóng
-- Bảng xếp hạng đội bóng khởi tạo các cột Mã vòng đấu, Mã đội bóng
-- Các cột số trận thắng, hòa, thua, điểm, hiệu số, hạng để trống và sẽ được cập nhật từ bảng thống kê đội bóng
--------------------------------------------------
--	BẢNG TẦM ẢNH HƯỞNG
--					INSERT		UPDATE		DELETE		
--	LICHTHIDAU		   +		   +		   +
--------------------------------------------------
if object_id('VTG_UpdateThongKe', 'tr') is not null
drop trigger VTG_UpdateThongKe
go
create trigger  VTG_UpdateThongKe on LichThiDau
for insert, update, delete
as
begin
	set nocount on
	if exists (select * from inserted)
	begin
		if (select i.DaThiDau from inserted i) is null
		begin
			declare @mamg varchar(10) -- Mùa giải
			declare @mavd varchar(10) -- Vòng đấu
			declare @madb varchar(10) -- Đội bóng
			declare @mact varchar(10) -- Cầu thủ
			set @mamg = (select i.MaMuaGiai from inserted i)
			set @mavd = (select i.MaVongDau from inserted i)

			-- Khởi tạo Thống kê đội bóng 
			if not exists (select *
						   from ThongKe_DoiBong tk, inserted i
						   where tk.MaVongDau = i.MaVongDau
						   and tk.MaMuaGiai = i.MaMuaGiai)
			begin
				declare curKhoiTaoThongKe cursor for
				select db.MaDB
				from DoiBong db
				open curKhoiTaoThongKe
				fetch next from curKhoiTaoThongKe
				into @madb
				while @@fetch_status = 0
				begin
					-- Thêm Mã vòng đấu, Mã đội bóng, set tổng điểm của tất cả đội bóng ban đầu = 0
					insert into ThongKe_DoiBong(MaMuaGiai, MaVongDau, MaDoiBong, TongDiem)
					values (@mamg, @mavd, @madb, 0)
					fetch next from curKhoiTaoThongKe
					into @madb
				end
				close curKhoiTaoThongKe
				deallocate curKhoiTaoThongKe
			end
			-- Kết thúc khởi tạo Thống kê đội bóng

			-- Khởi tạo Bảng xếp hạng đội bóng
			if not exists (select *
						   from BangXepHang_DoiBong bxh, inserted i
						   where bxh.MaVongDau = i.MaVongDau
						   and bxh.MaMuaGiai = i.MaMuaGiai)
			begin
				declare @i int -- Biến tạm thế Mã đội bóng (Mã đội bóng được cập nhật tại Bảng Thống kê đội bóng)
				set @i = 1
				declare curQuetDoiBong cursor for
				select db.MaDB
				from DoiBong db
				open curQuetDoiBong
				fetch next from curQuetDoiBong
				into @madb
				while @@fetch_status = 0
				begin
					insert into BangXepHang_DoiBong(MaMuaGiai, MaVongDau, MaDB, SoTranThang, SoTranThua, SoTranHoa, HieuSo, Hang)
					values (@mamg, @mavd, @i, 0, 0, 0, 0, @i)
					set @i = @i + 1
					fetch next from curQuetDoiBong
					into @madb
				end
				close curQuetDoiBong
				deallocate curQuetDoiBong
			end
			-- Kết thúc khởi tạo Bảng xếp hạng đội bóng

			-- Khởi tạo bảng Xếp hạng cầu thủ
			if not exists (select *
						   from BangXepHang_CauThu bxh, inserted i
						   where bxh.MaVongDau = i.MaVongDau
						   and bxh.MaMuaGiai = i.MaMuaGiai)
			begin
				declare curMaCauThu cursor for
				select ct.MaCT
				from CauThu ct
				open curMaCauThu
				fetch next from curMaCauThu
				into @mact
				while @@fetch_status = 0
				begin
					set @madb = (select ct.MaDB
								 from CauThu ct
								 where ct.MaCT = @mact)
					insert into BangXepHang_CauThu(MaMuaGiai, MaVongDau, MaDB, MaCT, SoBanThang, TongSoBanThang, Hang)
					values (@mamg, @mavd, @madb, @mact, 0, 0, 0)
					fetch next from curMaCauThu
					into @mact
				end
				close curMaCauThu
				deallocate curMaCauThu
			end
			-- Kết thúc khởi tạo Bảng xếp hạng cầu thủ
		end

		else if (select i.DaThiDau from inserted i) = 1 and (select i.SoBanThang_ChuNha from inserted i) is not null or (select i.SoBanThang_Khach from inserted i) is not null
		begin
			declare @mamg1 varchar(10)
			declare @mavd1 varchar(10)
			declare @madb1 varchar(10)
			declare @btchunha int -- Bàn thắng của chủ nhà
			declare @btkhach int -- Bàn thắng đội khách
			declare @mavongdautruoc varchar(10) -- Mã vòng đấu trước
			declare @tongdiemvongtruoc int -- Tổng điểm tính đến vòng đấu trước
			declare @hieuso int -- Hiệu số ghi bàn tại tính từ vòng đấu đầu tiên tới vòng đấu đang xét
			declare @hieusovongdautruoc int -- Hiệu số bàn thắng tính đến vòng đấu trước
			declare @tongbanthangvongdautruoc int -- Tổng bàn thắng tính đến vòng đấu trước
			declare @tongtranthangvongdautruoc int -- Tổng số trận thắng tính tới vòng đấu trước
			declare @tongtranhoavongdautruoc int -- Tổng số trận hòa tính đến vòng đấu trước
			declare @tongtranthuavongdautruoc int -- Tổng số trận thua tính đến vòng đấu trước
			declare curQuetDoiBong cursor for
			select tk.MaVongDau, tk.MaDoiBong, tk.MaMuaGiai
			from ThongKe_DoiBong tk
			open curQuetDoiBong
			fetch next from curQuetDoiBong
			into @mavd1, @madb1, @mamg1
			while @@fetch_status = 0
			begin
				if (select i.MaVongDau from inserted i) = @mavd1 and (select i.ChuNha from inserted i) = @madb1 and (select i.MaMuaGiai from inserted i) = @mamg1
				begin
					declare @sovongdau int
					set @btchunha = (select i.SoBanThang_ChuNha from inserted i)
					set @btkhach = (select i.SoBanThang_Khach from inserted i)
					if @btchunha is null
					set @btchunha = 0
					if @btkhach is null
					set @btkhach = 0
					if @btchunha > @btkhach
					begin
						update ThongKe_DoiBong
						set SoBanThang = @btchunha, SoBanThua = @btkhach, Diem = 3
						where MaVongDau = @mavd1 and MaDoiBong = @madb1 and MaMuaGiai = @mamg1

						if (select i.MaVongDau from inserted i) = 'VD001'
						begin
							update ThongKe_DoiBong
							set TongDiem = Diem, HieuSo = @btchunha - @btkhach, TongBanThang = @btchunha, TongSoTranThang = 1, TongSoTranThua = 0, TongSoTranHoa = 0
							where MaVongDau = @mavd1 and MaDoiBong = @madb1 and MaMuaGiai = @mamg1
						end
						else
						begin
							set @sovongdau = cast(substring((select i.MaVongDau from inserted i), 4, 5) as int)
							set @mavongdautruoc = concat('VD0', RIGHT ('00' + ltrim(str(@sovongdau - 1)),2))
							
							--set @mavongdautruoc = 'VD0' + cast((@sovongdau - 1) as char)
							set @tongdiemvongtruoc = (select tk.TongDiem
													from ThongKe_DoiBong tk
													where MaVongDau = @mavongdautruoc and MaDoiBong = @madb1 and MaMuaGiai = @mamg1)
							set @hieusovongdautruoc = (select tk.HieuSo
													   from ThongKe_DoiBong tk
													   where MaVongDau = @mavongdautruoc and MaDoiBong = @madb1 and MaMuaGiai = @mamg1)
							set @tongbanthangvongdautruoc = (select tk.TongBanThang
															 from ThongKe_DoiBong tk
															 where MaVongDau = @mavongdautruoc and MaDoiBong = @madb1 and MaMuaGiai = @mamg1)
							set @tongtranthangvongdautruoc = (select tk.TongSoTranThang
															 from ThongKe_DoiBong tk
															 where MaVongDau = @mavongdautruoc and MaDoiBong = @madb1 and MaMuaGiai = @mamg1)							
							set @tongtranhoavongdautruoc = (select tk.TongSoTranHoa
															 from ThongKe_DoiBong tk
															 where MaVongDau = @mavongdautruoc and MaDoiBong = @madb1 and MaMuaGiai = @mamg1)
							set @tongtranthuavongdautruoc = (select tk.TongSoTranThua
															 from ThongKe_DoiBong tk
															 where MaVongDau = @mavongdautruoc and MaDoiBong = @madb1 and MaMuaGiai = @mamg1) 
							update ThongKe_DoiBong
							set TongDiem = @tongdiemvongtruoc + Diem, HieuSo = @hieusovongdautruoc + @btchunha - @btkhach, TongBanThang = @tongbanthangvongdautruoc + @btchunha,
							TongSoTranThang = @tongtranthangvongdautruoc + 1,
							TongSoTranHoa = @tongtranhoavongdautruoc,
							TongSoTranThua = @tongtranthuavongdautruoc
							where MaVongDau = @mavd1 and MaDoiBong = @madb1 and MaMuaGiai = @mamg1
						end
					end
					else if @btchunha = @btkhach
					begin
						update ThongKe_DoiBong
						set SoBanThang = @btchunha, SoBanThua = @btkhach, Diem = 1
						where MaVongDau = @mavd1 and MaDoiBong = @madb1 and MaMuaGiai = @mamg1

						if (select i.MaVongDau from inserted i) = 'VD001'
						begin
							update ThongKe_DoiBong
							set TongDiem = Diem, HieuSo = @btchunha - @btkhach, TongBanThang = @btchunha, TongSoTranThang = 0, TongSoTranThua = 0, TongSoTranHoa = 1
							where MaVongDau = @mavd1 and MaDoiBong = @madb1 and MaMuaGiai = @mamg1
						end
						else
						begin
							set @sovongdau = cast(substring((select i.MaVongDau from inserted i), 4, 5) as int)
							set @mavongdautruoc = concat('VD0', RIGHT ('00' + ltrim(str(@sovongdau - 1)),2))
							set @tongdiemvongtruoc = (select tk.TongDiem
													  from ThongKe_DoiBong tk
													  where MaVongDau = @mavongdautruoc and MaDoiBong = @madb1 and MaMuaGiai = @mamg1)
							set @hieusovongdautruoc = (select tk.HieuSo
													   from ThongKe_DoiBong tk
													   where MaVongDau = @mavongdautruoc and MaDoiBong = @madb1 and MaMuaGiai = @mamg1)
							set @tongbanthangvongdautruoc = (select tk.TongBanThang
															 from ThongKe_DoiBong tk
															 where MaVongDau = @mavongdautruoc and MaDoiBong = @madb1 and MaMuaGiai = @mamg1)
							set @tongtranthangvongdautruoc = (select tk.TongSoTranThang
															 from ThongKe_DoiBong tk
															 where MaVongDau = @mavongdautruoc and MaDoiBong = @madb1 and MaMuaGiai = @mamg1)							
							set @tongtranhoavongdautruoc = (select tk.TongSoTranHoa
															 from ThongKe_DoiBong tk
															 where MaVongDau = @mavongdautruoc and MaDoiBong = @madb1 and MaMuaGiai = @mamg1)
							set @tongtranthuavongdautruoc = (select tk.TongSoTranThua
															 from ThongKe_DoiBong tk
															 where MaVongDau = @mavongdautruoc and MaDoiBong = @madb1 and MaMuaGiai = @mamg1)
							update ThongKe_DoiBong
							set TongDiem = @tongdiemvongtruoc + Diem, HieuSo = @hieusovongdautruoc + @btchunha - @btkhach, TongBanThang = @tongbanthangvongdautruoc + @btchunha,
							TongSoTranThang = @tongtranthangvongdautruoc,
							TongSoTranHoa = @tongtranhoavongdautruoc + 1,
							TongSoTranThua = @tongtranthuavongdautruoc
							where MaVongDau = @mavd1 and MaDoiBong = @madb1 and MaMuaGiai = @mamg1
						end
					end
					else
					begin
						update ThongKe_DoiBong
						set SoBanThang = @btchunha, SoBanThua = @btkhach, Diem = 0
						where MaVongDau = @mavd1 and MaDoiBong = @madb1 and MaMuaGiai = @mamg1

						if (select i.MaVongDau from inserted i) = 'VD001'
						begin
							update ThongKe_DoiBong
							set TongDiem = Diem, HieuSo = @btchunha - @btkhach, TongBanThang = @btchunha, TongSoTranThang = 0, TongSoTranThua = 1, TongSoTranHoa = 0
							where MaVongDau = @mavd1 and MaDoiBong = @madb1 and MaMuaGiai = @mamg1
						end
						else
						begin
							set @sovongdau = cast(substring((select i.MaVongDau from inserted i), 4, 5) as int)
							set @mavongdautruoc = concat('VD0', RIGHT ('00' + ltrim(str(@sovongdau - 1)),2))
							set @tongdiemvongtruoc = (select tk.TongDiem
													from ThongKe_DoiBong tk
													where MaVongDau = @mavongdautruoc and MaDoiBong = @madb1 and MaMuaGiai = @mamg1)
							set @hieusovongdautruoc = (select tk.HieuSo
													   from ThongKe_DoiBong tk
													   where MaVongDau = @mavongdautruoc and MaDoiBong = @madb1 and MaMuaGiai = @mamg1)
							set @tongbanthangvongdautruoc = (select tk.TongBanThang
															 from ThongKe_DoiBong tk
															 where MaVongDau = @mavongdautruoc and MaDoiBong = @madb1 and MaMuaGiai = @mamg1)
							set @tongtranthangvongdautruoc = (select tk.TongSoTranThang
															 from ThongKe_DoiBong tk
															 where MaVongDau = @mavongdautruoc and MaDoiBong = @madb1 and MaMuaGiai = @mamg1)							
							set @tongtranhoavongdautruoc = (select tk.TongSoTranHoa
															 from ThongKe_DoiBong tk
															 where MaVongDau = @mavongdautruoc and MaDoiBong = @madb1 and MaMuaGiai = @mamg1)
							set @tongtranthuavongdautruoc = (select tk.TongSoTranThua
															 from ThongKe_DoiBong tk
															 where MaVongDau = @mavongdautruoc and MaDoiBong = @madb1 and MaMuaGiai = @mamg1)
							update ThongKe_DoiBong
							set TongDiem = @tongdiemvongtruoc + Diem, HieuSo = @hieusovongdautruoc + @btchunha - @btkhach, TongBanThang = @tongbanthangvongdautruoc + @btchunha,
							TongSoTranThang = @tongtranthangvongdautruoc,
							TongSoTranHoa = @tongtranhoavongdautruoc,
							TongSoTranThua = @tongtranthuavongdautruoc + 1
							where MaVongDau = @mavd1 and MaDoiBong = @madb1 and MaMuaGiai = @mamg1
						end
					end
				end
				---------------------------------------
				else if (select i.MaVongDau from inserted i) = @mavd1 and (select i.Khach from inserted i) = @madb1 and (select i.MaMuaGiai from inserted i) = @mamg1
				begin
					set @btchunha = (select i.SoBanThang_ChuNha from inserted i)
					set @btkhach = (select i.SoBanThang_Khach from inserted i)
					if @btchunha is null
					set @btchunha = 0
					if @btkhach is null
					set @btkhach = 0
					if @btchunha > @btkhach
					begin
						update ThongKe_DoiBong
						set SoBanThang = @btkhach, SoBanThua = @btchunha, Diem = 0
						where MaVongDau = @mavd1 and MaDoiBong = @madb1 and MaMuaGiai = @mamg1

						if (select i.MaVongDau from inserted i) = 'VD001'
						begin
							update ThongKe_DoiBong
							set TongDiem = Diem, HieuSo = @btkhach - @btchunha, TongBanThang = @btkhach, TongSoTranThang = 0, TongSoTranThua = 1, TongSoTranHoa = 0
							where MaVongDau = @mavd1 and MaDoiBong = @madb1 and MaMuaGiai = @mamg1
						end
						else
						begin
							set @sovongdau = cast(substring((select i.MaVongDau from inserted i), 4, 5) as int)
							set @mavongdautruoc = concat('VD0', RIGHT ('00' + ltrim(str(@sovongdau - 1)),2))
							set @tongdiemvongtruoc = (select tk.TongDiem
													from ThongKe_DoiBong tk
													where MaVongDau = @mavongdautruoc and MaDoiBong = @madb1 and MaMuaGiai = @mamg1)
							set @hieusovongdautruoc = (select tk.HieuSo
													   from ThongKe_DoiBong tk
													   where MaVongDau = @mavongdautruoc and MaDoiBong = @madb1 and MaMuaGiai = @mamg1)
							set @tongbanthangvongdautruoc = (select tk.TongBanThang
															 from ThongKe_DoiBong tk
															 where MaVongDau = @mavongdautruoc and MaDoiBong = @madb1 and MaMuaGiai = @mamg1)
							set @tongtranthangvongdautruoc = (select tk.TongSoTranThang
															 from ThongKe_DoiBong tk
															 where MaVongDau = @mavongdautruoc and MaDoiBong = @madb1 and MaMuaGiai = @mamg1)							
							set @tongtranhoavongdautruoc = (select tk.TongSoTranHoa
															 from ThongKe_DoiBong tk
															 where MaVongDau = @mavongdautruoc and MaDoiBong = @madb1 and MaMuaGiai = @mamg1)
							set @tongtranthuavongdautruoc = (select tk.TongSoTranThua
															 from ThongKe_DoiBong tk
															 where MaVongDau = @mavongdautruoc and MaDoiBong = @madb1 and MaMuaGiai = @mamg1)
							update ThongKe_DoiBong
							set TongDiem = @tongdiemvongtruoc + Diem, HieuSo = @hieusovongdautruoc + @btkhach - @btchunha, TongBanThang = @tongbanthangvongdautruoc + @btkhach,
							TongSoTranThang = @tongtranthangvongdautruoc,
							TongSoTranHoa = @tongtranhoavongdautruoc,
							TongSoTranThua = @tongtranthuavongdautruoc + 1
							where MaVongDau = @mavd1 and MaDoiBong = @madb1 and MaMuaGiai = @mamg1
						end
					end
					else if @btchunha = @btkhach
					begin
						update ThongKe_DoiBong
						set SoBanThang = @btkhach, SoBanThua = @btchunha, Diem = 1
						where MaVongDau = @mavd1 and MaDoiBong = @madb1 and MaMuaGiai = @mamg1

						if (select i.MaVongDau from inserted i) = 'VD001'
						begin
							update ThongKe_DoiBong
							set TongDiem = Diem, HieuSo = @btkhach - @btchunha, TongBanThang = @btkhach, TongSoTranThang = 0, TongSoTranThua = 0, TongSoTranHoa = 1
							where MaVongDau = @mavd1 and MaDoiBong = @madb1 and MaMuaGiai = @mamg1
						end
						else
						begin
							set @sovongdau = cast(substring((select i.MaVongDau from inserted i), 4, 5) as int)
							set @mavongdautruoc = concat('VD0', RIGHT ('00' + ltrim(str(@sovongdau - 1)),2))
							set @tongdiemvongtruoc = (select tk.TongDiem
													from ThongKe_DoiBong tk
													where MaVongDau = @mavongdautruoc and MaDoiBong = @madb1 and MaMuaGiai = @mamg1)
							set @hieusovongdautruoc = (select tk.HieuSo
													   from ThongKe_DoiBong tk
													   where MaVongDau = @mavongdautruoc and MaDoiBong = @madb1 and MaMuaGiai = @mamg1)
							set @tongbanthangvongdautruoc = (select tk.TongBanThang
															 from ThongKe_DoiBong tk
															 where MaVongDau = @mavongdautruoc and MaDoiBong = @madb1 and MaMuaGiai = @mamg1)
							set @tongtranthangvongdautruoc = (select tk.TongSoTranThang
															 from ThongKe_DoiBong tk
															 where MaVongDau = @mavongdautruoc and MaDoiBong = @madb1 and MaMuaGiai = @mamg1)							
							set @tongtranhoavongdautruoc = (select tk.TongSoTranHoa
															 from ThongKe_DoiBong tk
															 where MaVongDau = @mavongdautruoc and MaDoiBong = @madb1 and MaMuaGiai = @mamg1)
							set @tongtranthuavongdautruoc = (select tk.TongSoTranThua
															 from ThongKe_DoiBong tk
															 where MaVongDau = @mavongdautruoc and MaDoiBong = @madb1 and MaMuaGiai = @mamg1)
							update ThongKe_DoiBong
							set TongDiem = @tongdiemvongtruoc + Diem, HieuSo = @hieusovongdautruoc + @btkhach - @btchunha, TongBanThang = @tongbanthangvongdautruoc + @btkhach,
							TongSoTranThang = @tongtranthangvongdautruoc,
							TongSoTranHoa = @tongtranhoavongdautruoc + 1,
							TongSoTranThua = @tongtranthuavongdautruoc
							where MaVongDau = @mavd1 and MaDoiBong = @madb1 and MaMuaGiai = @mamg1
						end
					end
					else
					begin
						update ThongKe_DoiBong
						set SoBanThang = @btkhach, SoBanThua = @btchunha, Diem = 3
						where MaVongDau = @mavd1 and MaDoiBong = @madb1 and MaMuaGiai = @mamg1

						if (select i.MaVongDau from inserted i) = 'VD001'
						begin
							update ThongKe_DoiBong
							set TongDiem = Diem, HieuSo = @btkhach - @btchunha, TongBanThang = @btkhach, TongSoTranThang = 1, TongSoTranThua = 0, TongSoTranHoa = 0
							where MaVongDau = @mavd1 and MaDoiBong = @madb1 and MaMuaGiai = @mamg1
						end
						else
						begin
							set @sovongdau = cast(substring((select i.MaVongDau from inserted i), 4, 5) as int)
							set @mavongdautruoc = concat('VD0', RIGHT ('00' + ltrim(str(@sovongdau - 1)),2))
							set @tongdiemvongtruoc = (select tk.TongDiem
													from ThongKe_DoiBong tk
													where MaVongDau = @mavongdautruoc and MaDoiBong = @madb1 and MaMuaGiai = @mamg1)
							set @hieusovongdautruoc = (select tk.HieuSo
													   from ThongKe_DoiBong tk
													   where MaVongDau = @mavongdautruoc and MaDoiBong = @madb1 and MaMuaGiai = @mamg1)
							set @tongbanthangvongdautruoc = (select tk.TongBanThang
															 from ThongKe_DoiBong tk
															 where MaVongDau = @mavongdautruoc and MaDoiBong = @madb1 and MaMuaGiai = @mamg1)
							set @tongtranthangvongdautruoc = (select tk.TongSoTranThang
															 from ThongKe_DoiBong tk
															 where MaVongDau = @mavongdautruoc and MaDoiBong = @madb1 and MaMuaGiai = @mamg1)							
							set @tongtranhoavongdautruoc = (select tk.TongSoTranHoa
															 from ThongKe_DoiBong tk
															 where MaVongDau = @mavongdautruoc and MaDoiBong = @madb1 and MaMuaGiai = @mamg1)
							set @tongtranthuavongdautruoc = (select tk.TongSoTranThua
															 from ThongKe_DoiBong tk
															 where MaVongDau = @mavongdautruoc and MaDoiBong = @madb1 and MaMuaGiai = @mamg1)
							update ThongKe_DoiBong
							set TongDiem = @tongdiemvongtruoc + Diem, HieuSo = @hieusovongdautruoc + @btkhach - @btchunha, TongBanThang = @tongbanthangvongdautruoc + @btkhach,
							TongSoTranThang = @tongtranthangvongdautruoc + 1,
							TongSoTranHoa = @tongtranhoavongdautruoc,
							TongSoTranThua = @tongtranthuavongdautruoc
							where MaVongDau = @mavd1 and MaDoiBong = @madb1 and MaMuaGiai = @mamg1
						end
					end
				end
				fetch next from curQuetDoiBong
				into @mavd1, @madb1, @mamg1
			end
			close curQuetDoiBong
			deallocate curQuetDoiBong
		end
	end
	-- can sua...
end
go
--------------------------------------------------	--------------------------------------------------
--------------------------------------------------	--------------------------------------------------



-- Từ Bảng thống kê -> cập nhật bảng xếp hạng đội bóng
-- (Khởi tạo Bảng xếp hạng đội bóng bắt đầu từ Tạo Lịch thi đấu)
--------------------------------------------------
--	BẢNG TẦM ẢNH HƯỞNG
--						INSERT		UPDATE		DELETE		
--	THONGKE_DOIBONG		   +		   +		   +
--------------------------------------------------
if object_id('VTG_UpdateBangXepHangDoiBong2', 'tr') is not null
drop trigger VTG_UpdateBangXepHangDoiBong2
go
create trigger VTG_UpdateBangXepHangDoiBong2 on ThongKe_DoiBong
for insert, update, delete
as
begin
	if exists (select * from inserted)
	begin
		set nocount on
		-----------------------------------------------------------------------------------------------------
		---------------------------------------- RESET BẢNG XẾP HẠNG ----------------------------------------
		-- Reset lại bảng xếp hạng mỗi lần có thay đổi kết quả để việc cập nhật Bảng xếp hạng không bị trùng lặp PK
		declare @iReset int
		declare curReset cursor for
		select bxh.Hang
		from BangXepHang_DoiBong bxh, inserted i
		where bxh.MaMuaGiai = i.MaMuaGiai
		and bxh.MaVongDau = i.MaVongDau
		open curReset
		fetch next from curReset
		into @iReset
		while @@fetch_status = 0
		begin
			update BangXepHang_DoiBong
			set MaDB = @iReset
			from BangXepHang_DoiBong bxh, inserted i
			where bxh.MaMuaGiai = i.MaMuaGiai
			and bxh.MaVongDau = i.MaVongDau
			and bxh.Hang = @iReset

			fetch next from curReset
			into @iReset
		end
		close curReset
		deallocate curReset 
		---------------------------------------- HOÀN TẤT RESET BẢNG XẾP HẠNG ----------------------------------------

		---------------------------------------- CẬP NHẬT BẢNG XẾP HẠNG ----------------------------------------
		declare @i int
		set @i = 1
		declare @madb varchar(10)
		declare @mamg varchar(10)
		declare @mavd varchar(10)
		declare @tongdiem int
		declare @hieuso int
		declare @tongtranthang int
		declare @tongtranthua int
		declare @tongtranhoa int

		declare curBangXepHang cursor for
		select tk.MaDoiBong, tk.MaMuaGiai, tk.MaVongDau, tk.TongDiem, tk.TongSoTranThang, tk.TongSoTranHoa, tk.TongSoTranThua, tk.TongDiem, tk.HieuSo
		from ThongKe_DoiBong tk, inserted i
		where tk.MaMuaGiai = i.MaMuaGiai and tk.MaVongDau = i.MaVongDau
		order by tk.TongDiem desc, tk.HieuSo desc, tk.TongBanThang desc			
					
		open curBangXepHang
		fetch next from curBangXepHang
		into @madb, @mamg, @mavd, @tongdiem, @tongtranthang, @tongtranhoa, @tongtranthua, @tongdiem, @hieuso
		
		while @@fetch_status = 0
		begin
			update BangXepHang_DoiBong
			set MaDB = @madb, SoTranThang = @tongtranthang, SoTranHoa = @tongtranhoa, SoTranThua = @tongtranthua, Diem = @tongdiem, HieuSo = @hieuso
			from BangXepHang_DoiBong bxh, inserted i
			where bxh.Hang = @i
			and bxh.MaMuaGiai = i.MaMuaGiai
			and bxh.MaVongDau = i.MaVongDau

			set @i = @i + 1
			fetch next from curBangXepHang
		into @madb, @mamg, @mavd, @tongdiem, @tongtranthang, @tongtranhoa, @tongtranthua, @tongdiem, @hieuso
		end
		close curBangXepHang
		deallocate curBangXepHang
		---------------------------------------- HOÀN TẤT CẬP NHẬT BẢNG XẾP HẠNG ----------------------------------------
	end
end
go

-- Từ Bảng Kết quả -> Cập nhật bảng xếp hạng cầu thủ

------ test
--select *
--from ThongKe_DoiBong tk
--where tk.MaVongDau = 'VD002'
--order by TongDiem desc, HieuSo desc, TongBanThang desc
--select * from BangXepHang_DoiBong