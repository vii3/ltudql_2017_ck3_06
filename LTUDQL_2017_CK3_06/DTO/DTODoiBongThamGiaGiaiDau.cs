﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTODoiBongThamGiaGiaiDau
    {
        string _maMuaGiai;
        string _maDoiBong;
        string _tenDoiBong;
        int _soLuongCauThuDangKy;
        int _soLuongCauThuNuocNgoai;
        bool _hoanTatDangKy;

        public string MaMuaGiai
        {
            get { return _maMuaGiai; }
            set { _maMuaGiai = value; }
        }
        public string MaDoiBong
        {
            get { return _maDoiBong; }
            set { _maDoiBong = value; }
        }
        public string TenDoiBong
        {
            get { return _tenDoiBong; }
            set { _tenDoiBong = value; }
        }
        public int SoLuongCauThuDangKy
        {
            get { return _soLuongCauThuDangKy; }
            set { _soLuongCauThuDangKy = value; }
        }
        public int SoLuongCauThuNuocNgoai
        {
            get { return _soLuongCauThuNuocNgoai; }
            set { _soLuongCauThuNuocNgoai = value; }
        }
        public bool HoanTatDangKy
        {
            get { return _hoanTatDangKy; }
            set { _hoanTatDangKy = value; }
        }
    }
}
