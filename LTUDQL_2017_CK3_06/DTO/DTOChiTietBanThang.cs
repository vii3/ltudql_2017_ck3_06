﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTOChiTietBanThang
    {
        string _maMuaGiai;
        string _maVongDau;
        string _maTranDau;
        string _maCT;
        string _maDB;
        int _loaiBanThang;
        TimeSpan _thoiDiem;

        public string MaMuaGiai
        {
            get { return _maMuaGiai; }
            set { _maMuaGiai = value; }
        }
        public string MaVongDau
        {
            get { return _maVongDau; }
            set { _maVongDau = value; }
        }
        public string MaTranDau
        {
            get { return _maTranDau; }
            set { _maTranDau = value; }
        }
        public string MaCauThu
        {
            get { return _maCT; }
            set { _maCT = value; }
        }
        public string MaDoiBong
        {
            get { return _maDB; }
            set { _maDB = value; }
        }
        public int LoaiBanThang
        {
            get { return _loaiBanThang; }
            set { _loaiBanThang = value; }
        }
        public TimeSpan ThoiDiem
        {
            get { return _thoiDiem; }
            set { _thoiDiem = value; }
        }
    }
}
