﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTOCauThuThuocDoiBongThamGiaGiaiDau
    {
        string _maMuaGiai;
        string _maCauThu;
        string _tenCauThu;
        string _maDoiBong;
        string _tenDoiBong;
        int _maLoaiCauThu;
        int _tongSoBanThang;
        public string MaMuaGiai
        {
            get { return _maMuaGiai; }
            set { _maMuaGiai = value; }
        }
        public string MaCauThu
        {
            get { return _maCauThu; }
            set { _maCauThu = value; }
        }
        public string TenCauThu
        {
            get { return _tenCauThu; }
            set { _tenCauThu = value; }
        }
        public string MaDoiBong
        {
            get { return _maDoiBong; }
            set { _maDoiBong = value; }
        }
        public string TenDoiBong
        {
            get { return _tenDoiBong; }
            set { _tenDoiBong = value; }
        }
        public int MaLoaiCauThu
        {
            get { return _maLoaiCauThu; }
            set { _maLoaiCauThu = value; }
        }
        public int TongSoBanThang
        {
            get { return _tongSoBanThang; }
            set { _tongSoBanThang = value; }
        }
    }
}
