﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTODoiBong
    {
        string _maDB;
        string _tenDB;
        string _sanNha;
        bool _ghiChu;
        bool _daXoa;
        public string MaDoiBong
        {
            get { return _maDB; }
            set { _maDB = value; }
        }
        public string TenDoiBong
        {
            get { return _tenDB; }
            set { _tenDB = value; }
        }
        public string SanNha
        {
            get { return _sanNha; }
            set { _sanNha = value; }
        }
        public bool GhiChu
        {
            get { return _ghiChu; }
            set { _ghiChu = value; }
        }
        public bool DaXoa
        {
            get { return _daXoa; }
            set { _daXoa = value; }
        }
    }
}
