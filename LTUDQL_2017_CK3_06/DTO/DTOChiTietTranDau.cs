﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTOChiTietTranDau
    {
        string _muaGiai;
        string _maVongDau;
        string _maTranDau;
        DateTime _ngayThiDau;
        TimeSpan _gioThiDau;
        string _maChuNha;
        string _maKhach;
        int _daThiDau; // Chua thi dau -1   --------- Da thi dau 1
        int _soBanThangChuNha;
        int _soBanThangKhach;

        public string MaMuaGiai
        {
            get { return _muaGiai; }
            set { _muaGiai = value; }
        }
        public string MaVongDau
        {
            get { return _maVongDau; }
            set { _maVongDau = value; }
        }
        public string MaTranDau
        {
            get { return _maTranDau; }
            set { _maTranDau = value; }
        }
        public DateTime NgayThiDau
        {
            get { return _ngayThiDau; }
            set { _ngayThiDau = value; }
        }
        public TimeSpan GioThiDau
        {
            get { return _gioThiDau; }
            set { _gioThiDau = value; }
        }
        public string MaChuNha
        {
            get { return _maChuNha; }
            set { _maChuNha = value; }
        }
        public string MaKhach
        {
            get { return _maKhach; }
            set { _maKhach = value; }
        }
        public int DaThiDau
        {
            get { return _daThiDau; }
            set { _daThiDau = value; }
        }
        public int SoBanThangChuNha
        {
            get { return _soBanThangChuNha; }
            set { _soBanThangChuNha = value; }
        }
        public int SoBanThangKhach
        {
            get { return _soBanThangKhach; }
            set { _soBanThangKhach = value; }
        }
    }
}
