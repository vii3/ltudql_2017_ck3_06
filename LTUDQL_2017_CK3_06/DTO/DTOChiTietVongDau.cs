﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTOChiTietVongDau
    {
        string _muaGiai;
        string _tenMuaGiai;
        string _maVongDau;
        string _maTranDau;
        DateTime _ngayThiDau;
        TimeSpan _gioThiDau;
        string _tenChuNha;
        string _tenKhach;
        public string MaMuaGiai
        {
            get { return _muaGiai; }
            set { _muaGiai = value; }
        }
        public string MaVongDau
        {
            get { return _maVongDau; }
            set { _maVongDau = value; }
        }
        public string MaTranDau
        {
            get { return _maTranDau; }
            set { _maTranDau = value; }
        }
        public DateTime NgayThiDau
        {
            get { return _ngayThiDau; }
            set { _ngayThiDau = value; }
        }
        public TimeSpan GioThiDau
        {
            get { return _gioThiDau; }
            set { _gioThiDau = value; }
        }
        public string TenChuNha
        {
            get { return _tenChuNha; }
            set { _tenChuNha = value; }
        }
        public string TenKhach
        {
            get { return _tenKhach; }
            set { _tenKhach = value; }
        }
        public string TenMuaGiai
        {
            get { return _tenMuaGiai; }
            set { _tenMuaGiai = value; }
        }
    }
}
