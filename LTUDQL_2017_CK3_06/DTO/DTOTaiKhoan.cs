﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTOTaiKhoan
    {
        string _tenDangNhap;
        string _matKhau;
        int _maLoaiTaiKhoan;
        public  string TenDangNhap
        {
            get { return _tenDangNhap; }
            set { _tenDangNhap = value; }
        }
        public string MatKhau
        {
            get { return _matKhau; }
            set { _matKhau = value; }
        }
        public int MaLoaiTaiKhoan
        {
            get { return _maLoaiTaiKhoan; }
            set { _maLoaiTaiKhoan = value; }
        }
    }
}
