﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTOMuaGiai
    {
        string _maMuaGiai;
        string _tenMuaGiai;
        int _soLuongDoiBongToiDa;
        int _soLuongDoiBongDaDangKy;
        bool _hoanTatDangKy;
        public string MaMuaGiai
        {
            get { return _maMuaGiai; }
            set { _maMuaGiai = value; }
        }
        public string TenMuaGiai
        {
            get { return _tenMuaGiai; }
            set
            {
                _tenMuaGiai = value;
            }
        }
        public int SoLuongDoiThamDuToiDa
        {
            get { return _soLuongDoiBongToiDa; }
            set { _soLuongDoiBongToiDa = value; }
        }
        public int SoLuongDoiDaDangKy
        {
            get { return _soLuongDoiBongDaDangKy; }
            set
            {
                _soLuongDoiBongDaDangKy = value;
            }
        }
        public bool HoanTatDangKy
        {
            get { return _hoanTatDangKy; }
            set { _hoanTatDangKy = value; }
        }
    }
}
