﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTOSoLieuThongKeCauThu
    {
        string _maMuaGiai;
        string _maVongDau;
        string _maCauThu;
        string _maDoiBong;
        int _soBanThang;
        int _tongSoBanThang;
        int _hang;

        public string MaMuaGiai
        {
            get { return _maMuaGiai; }
            set { _maMuaGiai = value; }
        }
        public string MaVongDau
        {
            get { return _maVongDau; }
            set { _maVongDau = value; }
        }
        public string MaCauThu
        {
            get { return _maCauThu; }
            set { _maCauThu = value; }
        }
        public string MaDoiBong
        {
            get { return _maDoiBong; }
            set { _maDoiBong = value; }
        }
        public int SoBanThang
        {
            get { return _soBanThang; }
            set { _soBanThang = value; }
        }
        public int TongSoBanThang
        {
            get { return _tongSoBanThang; }
            set { _tongSoBanThang = value; }
        }
        public int Hang
        {
            get { return _hang; }
            set { _hang = value; }
        }
    }
}
