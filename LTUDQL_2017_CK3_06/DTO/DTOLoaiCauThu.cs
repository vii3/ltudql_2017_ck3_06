﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTOLoaiCauThu
    {
        int _maLoaiCauThu;
        string _tenLoaiCauThu;

        public int MaLoaiCauThu
        {
            get { return _maLoaiCauThu; }
            set { _maLoaiCauThu = value; }
        }
        public string TenLoaiCauThu
        {
            get { return _tenLoaiCauThu; }
            set { _tenLoaiCauThu = value; }
        }
    }
}
