﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTOThongTinCauThu
    {
        string maCauThu;
        string maDoiBong;
        string tenCauThu;
        string tenDoiBong;
        public string TenCauThu
        {
            get { return tenCauThu; }
            set { tenCauThu = value; }
        }
        public string TenDoiBong
        {
            get { return tenDoiBong; }
            set { tenDoiBong = value; }
        }
        public string MaCauThu
        {
            get { return maCauThu; }
            set { maCauThu = value; }
        }
        public string MaDoiBong
        {
            get { return maDoiBong; }
            set { maDoiBong = value; }
        }
    }
}
