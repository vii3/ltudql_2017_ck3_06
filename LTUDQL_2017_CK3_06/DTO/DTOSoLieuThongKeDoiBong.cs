﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTOSoLieuThongKeDoiBong
    {
        string _maMuaGiai;
        string _maVongDau;
        string _maDoiBong;
        int _soTranDaThiDau;
        int _tongSoTranThang;
        int _tongSoTranHoa;
        int _tongtSoTranThua;
        int _soBanThang;
        int _tongSoBanThang;
        int _soBanThua;
        int _tongSoBanThua;
        int _diem; // Điểm thu được từ vòng đấu đang xét
        int _tongDiem;
        int _hieuSo;
        int _tongHieuSo;
        int _hang;

        public string MaMuaGiai
        {
            get { return _maMuaGiai; }
            set { _maMuaGiai = value; }
        }
        public string MaVongDau
        {
            get { return _maVongDau; }
            set { _maVongDau = value; }
        }
        public string MaDoiBong
        {
            get { return _maDoiBong; }
            set { _maDoiBong = value; }
        }
        public int SoTranDaThiDau
        {
            get { return _soTranDaThiDau; }
            set { _soTranDaThiDau = value; }
        }
        public int TongSoTranThang
        {
            get { return _tongSoTranThang; }
            set { _tongSoTranThang = value; }
        }
        public int TongSoTranHoa
        {
            get { return _tongSoTranHoa; }
            set { _tongSoTranHoa = value; }
        }
        public int TongSoTranThua
        {
            get { return _tongtSoTranThua; }
            set { _tongtSoTranThua = value; }
        }
        public int SoBanThang
        {
            get { return _soBanThang; }
            set { _soBanThang = value; }
        }
        public int TongSoBanThang
        {
            get { return _tongSoBanThang; }
            set { _tongSoBanThang = value; }
        }
        public int SoBanThua
        {
            get { return _soBanThua; }
            set { _soBanThua = value; }
        }
        public int TongSoBanThua
        {
            get { return _tongSoBanThua; }
            set { _tongSoBanThua = value; }
        }
        public int Diem
        {
            get { return _diem; }
            set { _diem = value; }
        }
        public int TongDiem
        {
            get { return _tongDiem; }
            set { _tongDiem = value; }
        }
        public int HieuSo
        {
            get { return _hieuSo; }
            set { _hieuSo = value; }
        }
        public int TongHieuSo
        {
            get { return _tongHieuSo; }
            set { _tongHieuSo = value; }
        }
        public int Hang
        {
            get { return _hang; }
            set { _hang = value; }
        }
    }
}
