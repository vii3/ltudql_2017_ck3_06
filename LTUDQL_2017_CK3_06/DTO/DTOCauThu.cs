﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTOCauThu
    {
        string _maCT;
        string _hoTenCT;
        DateTime _ngaySinh;
        int _maloaiCT;
        string _maDB;
        int _tongSoBanThang;
        bool _daXoa;

        public string MaCauThu
        {
            get { return _maCT; }
            set { _maCT = value; }
        }
        public string HoTenCauThu
        {
            get { return _hoTenCT; }
            set { _hoTenCT = value; }
        }
        public DateTime NgaySinh
        {
            get { return _ngaySinh; }
            set { _ngaySinh = value; }
        }
        public int MaLoaiCauThu
        {
            get { return _maloaiCT; }
            set { _maloaiCT = value; }
        }
        public string MaDoiBong
        {
            get { return _maDB; }
            set { _maDB = value; }
        }
        public int TongSoBanThang
        {
            get { return _tongSoBanThang; }
            set { _tongSoBanThang = value; }
        }
        public bool DaXoa
        {
            get { return _daXoa; }
            set { _daXoa = value; }
        }
    }
}
