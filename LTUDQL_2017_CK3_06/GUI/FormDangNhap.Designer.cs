﻿namespace GUI
{
    partial class FormDangNhap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbDangNhap_TenTaiKhoan = new System.Windows.Forms.TextBox();
            this.tbDangNhap_MatKhau = new System.Windows.Forms.TextBox();
            this.btDangNhap_DangNhap = new System.Windows.Forms.Button();
            this.btDangNhap_Thoat = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Location = new System.Drawing.Point(88, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(176, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Đăng nhập tài khoản";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(30, 90);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Mật khẩu";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(30, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Tên tài khoản ";
            // 
            // tbDangNhap_TenTaiKhoan
            // 
            this.tbDangNhap_TenTaiKhoan.BackColor = System.Drawing.Color.Azure;
            this.tbDangNhap_TenTaiKhoan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDangNhap_TenTaiKhoan.ForeColor = System.Drawing.Color.Teal;
            this.tbDangNhap_TenTaiKhoan.Location = new System.Drawing.Point(150, 50);
            this.tbDangNhap_TenTaiKhoan.Name = "tbDangNhap_TenTaiKhoan";
            this.tbDangNhap_TenTaiKhoan.Size = new System.Drawing.Size(134, 20);
            this.tbDangNhap_TenTaiKhoan.TabIndex = 1;
            // 
            // tbDangNhap_MatKhau
            // 
            this.tbDangNhap_MatKhau.BackColor = System.Drawing.Color.Azure;
            this.tbDangNhap_MatKhau.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDangNhap_MatKhau.ForeColor = System.Drawing.Color.Teal;
            this.tbDangNhap_MatKhau.Location = new System.Drawing.Point(150, 90);
            this.tbDangNhap_MatKhau.Name = "tbDangNhap_MatKhau";
            this.tbDangNhap_MatKhau.PasswordChar = '*';
            this.tbDangNhap_MatKhau.Size = new System.Drawing.Size(134, 20);
            this.tbDangNhap_MatKhau.TabIndex = 2;
            this.tbDangNhap_MatKhau.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormDangNhap_KeyDown);
            // 
            // btDangNhap_DangNhap
            // 
            this.btDangNhap_DangNhap.BackColor = System.Drawing.Color.Azure;
            this.btDangNhap_DangNhap.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btDangNhap_DangNhap.Location = new System.Drawing.Point(33, 133);
            this.btDangNhap_DangNhap.Name = "btDangNhap_DangNhap";
            this.btDangNhap_DangNhap.Size = new System.Drawing.Size(100, 25);
            this.btDangNhap_DangNhap.TabIndex = 3;
            this.btDangNhap_DangNhap.Text = "ĐĂNG NHẬP";
            this.btDangNhap_DangNhap.UseVisualStyleBackColor = false;
            this.btDangNhap_DangNhap.Click += new System.EventHandler(this.btDangNhap_DangNhap_Click);
            // 
            // btDangNhap_Thoat
            // 
            this.btDangNhap_Thoat.BackColor = System.Drawing.Color.Azure;
            this.btDangNhap_Thoat.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btDangNhap_Thoat.Location = new System.Drawing.Point(150, 133);
            this.btDangNhap_Thoat.Name = "btDangNhap_Thoat";
            this.btDangNhap_Thoat.Size = new System.Drawing.Size(100, 25);
            this.btDangNhap_Thoat.TabIndex = 4;
            this.btDangNhap_Thoat.Text = "THOÁT";
            this.btDangNhap_Thoat.UseVisualStyleBackColor = false;
            this.btDangNhap_Thoat.Click += new System.EventHandler(this.button1_Click);
            // 
            // FormDangNhap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Snow;
            this.ClientSize = new System.Drawing.Size(334, 211);
            this.Controls.Add(this.btDangNhap_Thoat);
            this.Controls.Add(this.btDangNhap_DangNhap);
            this.Controls.Add(this.tbDangNhap_MatKhau);
            this.Controls.Add(this.tbDangNhap_TenTaiKhoan);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FormDangNhap";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Đăng nhập";
            this.Load += new System.EventHandler(this.FormDangNhap_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormDangNhap_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbDangNhap_TenTaiKhoan;
        private System.Windows.Forms.TextBox tbDangNhap_MatKhau;
        private System.Windows.Forms.Button btDangNhap_DangNhap;
        private System.Windows.Forms.Button btDangNhap_Thoat;
    }
}