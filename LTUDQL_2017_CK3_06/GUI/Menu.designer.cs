﻿namespace GUI
{
    partial class Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Menu));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tcMenu = new System.Windows.Forms.TabControl();
            this.tpHome = new System.Windows.Forms.TabPage();
            this.tcHOME = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tpHOME_DangKyGiaiDau = new System.Windows.Forms.TabPage();
            this.splitContainer12 = new System.Windows.Forms.SplitContainer();
            this.gb_HOME_DangKyGiaiDau_QuyDinh = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.btHOME_DangKyGiaiDau_DangKy = new System.Windows.Forms.Button();
            this.cbbHOME_DangKyGiaiDau_TenGiaiDau = new System.Windows.Forms.ComboBox();
            this.tbHOME_DangKyGiaiDau_MaGiaiDau = new System.Windows.Forms.TextBox();
            this.nudHOME_DangKyGiaiDau_SoLuongDoi = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.lbMaGiai = new System.Windows.Forms.Label();
            this.gb_HOME_DangKyGiaiDau_DanhSachGiai = new System.Windows.Forms.GroupBox();
            this.dgvDanhSachMuaGiai = new System.Windows.Forms.DataGridView();
            this.MaMuaGiaiDSMG = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TenMuaGiaiDSMG = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SoLuongDoiTuyenDSMG = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpHOME_TienDangKyDoiBong = new System.Windows.Forms.TabPage();
            this.splitContainer15 = new System.Windows.Forms.SplitContainer();
            this.splitContainer16 = new System.Windows.Forms.SplitContainer();
            this.tbHOME_DangKyDanhSachDoiBong_TieuDe = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btHOME_TienDangKyHOSoCauThu_XacNhanDangKy = new System.Windows.Forms.Button();
            this.rbtHOME_TienDangKyHOSoCauThu_LoaiCauThu_NuocNgoai = new System.Windows.Forms.RadioButton();
            this.rbtHOME_TienDangKyHOSoCauThu_LoaiCauThu_VietNam = new System.Windows.Forms.RadioButton();
            this.dtpHOME_TienDangKyHOSoCauThu_NgaySinh = new System.Windows.Forms.DateTimePicker();
            this.label33 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.tbHOME_TienDangKyHOSoCauThu_TenCauThu = new System.Windows.Forms.TextBox();
            this.tbHOME_TienDangKyHOSoCauThu_MaCauThu = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btDangKyHoSoDoiBong_XacNhanDangKy_DangKy = new System.Windows.Forms.Button();
            this.btDangKyHoSoDoiBong_ThongTinDangKy_DangKy = new System.Windows.Forms.Button();
            this.label30 = new System.Windows.Forms.Label();
            this.tbDangKyHoSoDoiBong_ThongTinDangKy_SanNha = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.tbDangKyHoSoDoiBong_ThongTinDangKy_TenDoiBong = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.tbDangKyHoSoDoiBong_ThongTinDangKy_MaDoiBong = new System.Windows.Forms.TextBox();
            this.tpHome_HoSoGiaiDau = new System.Windows.Forms.TabPage();
            this.tcHoSoGiaiDau = new System.Windows.Forms.TabControl();
            this.tpHoSoGiaiDau_HoSoDoiBong = new System.Windows.Forms.TabPage();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.dgvDanhSachDoiBongTongHop = new System.Windows.Forms.DataGridView();
            this.MaDoiBongHS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TenDoiBongHS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SanNhaHS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpHoSoGiaiDau_HoSoCauThu = new System.Windows.Forms.TabPage();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.dgvDanhSachCauThuTongHop = new System.Windows.Forms.DataGridView();
            this.MaCauThuHS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TenCauThuHS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NgaySinhHS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LoaiCauThuHS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TongSoBanThangHS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpTiepNhanHoSo = new System.Windows.Forms.TabPage();
            this.tcTiepNhanHoSo = new System.Windows.Forms.TabControl();
            this.tpTiepNhanHoSo_QuyDinh_DangKyGiaiDau = new System.Windows.Forms.TabPage();
            this.splitContainer21 = new System.Windows.Forms.SplitContainer();
            this.splitContainer22 = new System.Windows.Forms.SplitContainer();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this.dgvTiepNhanHoSo_DangKyGiaiDau = new System.Windows.Forms.DataGridView();
            this.MaGiaiDau = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TenGiaiDau = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SoLuongDoiToiDa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SoDoiDADangKy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TrangThaiDK = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CapNhatGiaiDau = new System.Windows.Forms.DataGridViewButtonColumn();
            this.HoanTatGiaiDau = new System.Windows.Forms.DataGridViewButtonColumn();
            this.tpTiepNhanHoSo_DangKyTTDoiBong = new System.Windows.Forms.TabPage();
            this.tcTiepNhanHoSoDangKyThongTinDoiBong = new System.Windows.Forms.TabControl();
            this.tpTiepNhanHoSo_DangKyThongTinDoiBOng = new System.Windows.Forms.TabPage();
            this.splitContainer18 = new System.Windows.Forms.SplitContainer();
            this.gbTiepNhanHoSo_DanhSachDoiBongCoTheDangKy = new System.Windows.Forms.GroupBox();
            this.dgvChiTietThongTinDoiBOng = new System.Windows.Forms.DataGridView();
            this.MaDoiBongTC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TenDoiBOngTC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AddDB = new System.Windows.Forms.DataGridViewButtonColumn();
            this.gpTiepNhanHOSo_DangKyThongTinDoiBong = new System.Windows.Forms.GroupBox();
            this.gbTiepNhanHoSo_DanhSachDoiBongDaDangKy = new System.Windows.Forms.GroupBox();
            this.dgvTiepNhanHoSo_DanhSachDoiBOngDaDangKy = new System.Windows.Forms.DataGridView();
            this.MaDB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SLCT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Update = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridViewButtonColumn1 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.XacNhan = new System.Windows.Forms.DataGridViewButtonColumn();
            this.cbbTNHS_DKDB_DKDB_MaDoiBong = new System.Windows.Forms.ComboBox();
            this.btTiepNhanHoSo_DangKyDoiBong_DangKyDoiBong = new System.Windows.Forms.Button();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.tbTNHS_DKDB_DKDB_SoLuongCauThu = new System.Windows.Forms.TextBox();
            this.tbTNHS_DKDB_DKDB_SanNha = new System.Windows.Forms.TextBox();
            this.tbTNHS_DKDB_DKDB_TenDoiBong = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.tpTiepNhanHoSoDangKyThongTinCauThu = new System.Windows.Forms.TabPage();
            this.splitContainer19 = new System.Windows.Forms.SplitContainer();
            this.gbTiepNhanHoSo_DangKyThongTinDoiBong_DanhSachCauThuCoTheDangKy = new System.Windows.Forms.GroupBox();
            this.dgvChiTietTTCT = new System.Windows.Forms.DataGridView();
            this.MaCTTC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TenCTTC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ThemCauThu = new System.Windows.Forms.DataGridViewButtonColumn();
            this.gbTiepNhanHOSo_DangKYThongTinCauThu = new System.Windows.Forms.GroupBox();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.dgvTNHS_DKDB_DKCT_DanhSachCauThu = new System.Windows.Forms.DataGridView();
            this.MaCT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.XoaCT = new System.Windows.Forms.DataGridViewButtonColumn();
            this.tbTNHS_DKDB_DKCT_NgaySinh = new System.Windows.Forms.TextBox();
            this.cbbTNHS_DKDB_DKCT_MaCauThu = new System.Windows.Forms.ComboBox();
            this.tbTNHS_DKDB_DKCT_LoaiCauThu = new System.Windows.Forms.TextBox();
            this.tbTNHS_DKDB_DKCT_TenCauThu = new System.Windows.Forms.TextBox();
            this.btTiepNhanHoSo_DangKyDoiBong_XacNhanHoanTatDangKyDoiBong = new System.Windows.Forms.Button();
            this.btTiepNhanHoSo_DangKyDoiBong_DangKyCauThu = new System.Windows.Forms.Button();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.tpTaoLichThiDau = new System.Windows.Forms.TabPage();
            this.tcTaoLichThiDau = new System.Windows.Forms.TabControl();
            this.tpTaoLichThiDau_QuyDinh = new System.Windows.Forms.TabPage();
            this.splitContainer11 = new System.Windows.Forms.SplitContainer();
            this.splitContainer23 = new System.Windows.Forms.SplitContainer();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.label37 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this.dgvTaoLichThiDau_DanhSachMuaGiai = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewButtonColumn2 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.tpTaoLichThiDau_TaoLichThiDau = new System.Windows.Forms.TabPage();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.gbTaoLichThiDau_TaoLichThiDau_QuyDinh = new System.Windows.Forms.GroupBox();
            this.btTaoLichThiDau_TaoLichThiDau = new System.Windows.Forms.Button();
            this.btTaoLichThiDau_XacNhanLichThiDau = new System.Windows.Forms.Button();
            this.nudstep = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.nudfirstmatch = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.nudlastmatch = new System.Windows.Forms.NumericUpDown();
            this.nudThoiGianNghiGiuaHaiLuot = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.nudKhoangCachVongDau = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpNgayBatDau = new System.Windows.Forms.DateTimePicker();
            this.gbTaoLichThiDau_TaoLichThiDau_TenMuaGiai = new System.Windows.Forms.GroupBox();
            this.dgvListXepHang = new System.Windows.Forms.DataGridView();
            this.MaVongDau = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MaTranDau = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NgayThiDau = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GioThiDau = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ChuNha = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Khach = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpCapNhatKetQua = new System.Windows.Forms.TabPage();
            this.tcCapNhatKetQua = new System.Windows.Forms.TabControl();
            this.tpCapNhatKetQua_QuyDinh = new System.Windows.Forms.TabPage();
            this.splitContainer17 = new System.Windows.Forms.SplitContainer();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.label22 = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.dgvCapNhatKetQua_DanhSachTranDau = new System.Windows.Forms.DataGridView();
            this.MaTranDauCNKQ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NgayThiDauCNKQ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GioThiDauCNKQ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TrangThai = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ChuNhaCNKQ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KhachCNKQ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CapNhatCNKQ = new System.Windows.Forms.DataGridViewButtonColumn();
            this.HoanTatKNKQ = new System.Windows.Forms.DataGridViewButtonColumn();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.tsmiCapNhatKetQua_MuaGiai = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCapNhatKetQua_VongDau = new System.Windows.Forms.ToolStripMenuItem();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.tpCapNhatKetQua_CapNhatKetQuaTranDau = new System.Windows.Forms.TabPage();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.splitContainer10 = new System.Windows.Forms.SplitContainer();
            this.gbCapNhatKetQua_ChuNha = new System.Windows.Forms.GroupBox();
            this.dgvCapNhatKetQua_ChuNha = new System.Windows.Forms.DataGridView();
            this.MaCauThuCN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TenCauThuCN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CapNhatCN = new System.Windows.Forms.DataGridViewButtonColumn();
            this.gbCapNhatKetQua_Khach = new System.Windows.Forms.GroupBox();
            this.dgvCapNhatKetQua_Khach = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewButtonColumn3 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.splitContainer24 = new System.Windows.Forms.SplitContainer();
            this.groupBox23 = new System.Windows.Forms.GroupBox();
            this.tbCapNhatKetQuaTranDau_CapNhat_TranDau = new System.Windows.Forms.TextBox();
            this.tbCapNhatKetQuaTranDau_CapNhat_VongDau = new System.Windows.Forms.TextBox();
            this.tbCapNhatKetQuaTranDau_CapNhat_MuaGiai = new System.Windows.Forms.TextBox();
            this.tbkhach = new System.Windows.Forms.TextBox();
            this.tbbanthangkhach = new System.Windows.Forms.TextBox();
            this.tbbanthangchunha = new System.Windows.Forms.TextBox();
            this.tbtrangthai = new System.Windows.Forms.TextBox();
            this.tbgiothidau = new System.Windows.Forms.TextBox();
            this.tbngaythidau = new System.Windows.Forms.TextBox();
            this.tbchunha = new System.Windows.Forms.TextBox();
            this.btCapNhatKetQua_CapNhatThongTin_CapNhat = new System.Windows.Forms.Button();
            this.tbCapNhatKetQua_MaDoiBong_hidden = new System.Windows.Forms.TextBox();
            this.tbTenDoiBong = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.rbtbanthangtructiep = new System.Windows.Forms.RadioButton();
            this.tbTenCauThu = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.rbtpenalty = new System.Windows.Forms.RadioButton();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.nudGiay = new System.Windows.Forms.NumericUpDown();
            this.label21 = new System.Windows.Forms.Label();
            this.rbtphanluoi = new System.Windows.Forms.RadioButton();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.nudPhut = new System.Windows.Forms.NumericUpDown();
            this.tpMenuLichThiDau = new System.Windows.Forms.TabPage();
            this.dgvLichThiDau = new System.Windows.Forms.DataGridView();
            this.MaVongDauLTD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MaTranDauLTD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TinhTrangLTD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NgayThiDauLTD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GioThiDauLTD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ChuNhaLTD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiemChuNha = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiemKhachLTD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KhachLTD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.menuStrip4 = new System.Windows.Forms.MenuStrip();
            this.tsmiLichThiDau_MaMuaGiai = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiLichThiDau_MaVongDau = new System.Windows.Forms.ToolStripMenuItem();
            this.tpBangXepHang = new System.Windows.Forms.TabPage();
            this.dgvBangXepHangBangXepHang = new System.Windows.Forms.DataGridView();
            this.MaDoiBong = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SoTranDaThiDau = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TongSoTranThang = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TongSoTranHoa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TongSoTranThua = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TongSoBanThang = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TongSoBanThua = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TongHieuSo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TongDiem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Hang = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.tsmiBangXepHang_MuaGiai = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiBangXepHang_VongDau = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiBangXepHang_NgayBatDau = new System.Windows.Forms.ToolStripTextBox();
            this.tpSoLieuThongKe = new System.Windows.Forms.TabPage();
            this.dgvSoLieuThongKeCauThu = new System.Windows.Forms.DataGridView();
            this.MaCauThu = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HoTenCauThu = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TongSoBanThangCT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SoBanThang = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HangSLCT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.menuStrip3 = new System.Windows.Forms.MenuStrip();
            this.tsmiBangXepHang_CauThu_MuaGiai = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiBangXepHang_CauThu_VongDau = new System.Windows.Forms.ToolStripMenuItem();
            this.tpTraCuuCauThu = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.splitContainer20 = new System.Windows.Forms.SplitContainer();
            this.label54 = new System.Windows.Forms.Label();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.cbbTraCuu_DoiBong = new System.Windows.Forms.ComboBox();
            this.label52 = new System.Windows.Forms.Label();
            this.btTraCuu_LocTimKiem = new System.Windows.Forms.Button();
            this.rbtTraCuu_LoaiCauThu_NuocNgoai = new System.Windows.Forms.RadioButton();
            this.label50 = new System.Windows.Forms.Label();
            this.cbbTraCuu_MuaGiai = new System.Windows.Forms.ComboBox();
            this.rbt_TraCuu_LoaiCauThu_VietNam = new System.Windows.Forms.RadioButton();
            this.label49 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.nudTraCuu_TuoiToiDa = new System.Windows.Forms.NumericUpDown();
            this.nudTraCuu_TuoiToiThieu = new System.Windows.Forms.NumericUpDown();
            this.label53 = new System.Windows.Forms.Label();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.rbtTraCuu_TenCauThu = new System.Windows.Forms.RadioButton();
            this.rbtTraCuu_TenDoiBong = new System.Windows.Forms.RadioButton();
            this.btTraCuu_TimKiem = new System.Windows.Forms.Button();
            this.label51 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.tbTraCuuCauThu_ThongTin = new System.Windows.Forms.TextBox();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.dgvTraCuu_DanhSachTraCuu = new System.Windows.Forms.DataGridView();
            this.ma = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Loai = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.So = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpThayDoiQuyDinh = new System.Windows.Forms.TabPage();
            this.tcQuyDinh = new System.Windows.Forms.TabControl();
            this.tpQuyDinh_TongHopQuyDinh = new System.Windows.Forms.TabPage();
            this.tcQuyDinh_DanhSachQuyDinh = new System.Windows.Forms.TabControl();
            this.tpQuyDinh_QuyDinhChung = new System.Windows.Forms.TabPage();
            this.groupBox24 = new System.Windows.Forms.GroupBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.tp_QuyDinh_ChiTiet = new System.Windows.Forms.TabPage();
            this.groupBox27 = new System.Windows.Forms.GroupBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.groupBox26 = new System.Windows.Forms.GroupBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.groupBox25 = new System.Windows.Forms.GroupBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.tpQuyDinh_ChiTietQuyDinh = new System.Windows.Forms.TabPage();
            this.groupBox29 = new System.Windows.Forms.GroupBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.groupBox28 = new System.Windows.Forms.GroupBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.tpQuyDinh_SuaQuyDinh = new System.Windows.Forms.TabPage();
            this.groupBox31 = new System.Windows.Forms.GroupBox();
            this.tbQuyDinh_ThoiDiemGhiBanToiDa = new System.Windows.Forms.TextBox();
            this.tbQuyDinh_TuoiToiThieu = new System.Windows.Forms.TextBox();
            this.tbQuyDinh_TuoiToiDa = new System.Windows.Forms.TextBox();
            this.tbQuyDinh_SoLuongCauThuNuocNgoai = new System.Windows.Forms.TextBox();
            this.tbQuyDinh_SoLuongCauThuToiThieu = new System.Windows.Forms.TextBox();
            this.tbQuyDinh_SoLuongCauThuToiDa = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.groupBox30 = new System.Windows.Forms.GroupBox();
            this.label23 = new System.Windows.Forms.Label();
            this.nudQuyDinh_ThoiDiemGhiBanToiDa = new System.Windows.Forms.NumericUpDown();
            this.btQuyDinh_XacNhan = new System.Windows.Forms.Button();
            this.nudQuyDinh_TuoiToiThieu = new System.Windows.Forms.NumericUpDown();
            this.label24 = new System.Windows.Forms.Label();
            this.nudQuyDinh_TuoiToiDa = new System.Windows.Forms.NumericUpDown();
            this.nudQuyDinh_SoLuongCauThuNuocNgoai = new System.Windows.Forms.NumericUpDown();
            this.label27 = new System.Windows.Forms.Label();
            this.nudQuyDinh_SoLuongCauThuToiThieu = new System.Windows.Forms.NumericUpDown();
            this.label25 = new System.Windows.Forms.Label();
            this.nudQuyDinh_SoLuongCauThuToiDa = new System.Windows.Forms.NumericUpDown();
            this.label26 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.cbbTiepNhanHoSo_DanhSachGiaiDauCoTheThamDu = new System.Windows.Forms.ComboBox();
            this.tbTiepNhanHoSo_DanhSachGiaiDauCoTheThamDu = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.tbTiepNhanHoSo_SoLuongDoiToiDa = new System.Windows.Forms.TextBox();
            this.tbTiepNhanHoSo_SoLuongDoiDaDangKy = new System.Windows.Forms.TextBox();
            this.tbTiepNhanHoSo_SoLuongDoiDaHoanTatDangKy = new System.Windows.Forms.TextBox();
            this.btTiepNhanHoSo_DangKy = new System.Windows.Forms.Button();
            this.splitContainer9 = new System.Windows.Forms.SplitContainer();
            this.gb_QuyDinhDangKy = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.tsmiBangXepHang_CauThu_ThoiGian = new System.Windows.Forms.ToolStripTextBox();
            this.tcMenu.SuspendLayout();
            this.tpHome.SuspendLayout();
            this.tcHOME.SuspendLayout();
            this.tpHOME_DangKyGiaiDau.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer12)).BeginInit();
            this.splitContainer12.Panel1.SuspendLayout();
            this.splitContainer12.Panel2.SuspendLayout();
            this.splitContainer12.SuspendLayout();
            this.gb_HOME_DangKyGiaiDau_QuyDinh.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudHOME_DangKyGiaiDau_SoLuongDoi)).BeginInit();
            this.gb_HOME_DangKyGiaiDau_DanhSachGiai.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDanhSachMuaGiai)).BeginInit();
            this.tpHOME_TienDangKyDoiBong.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer15)).BeginInit();
            this.splitContainer15.Panel1.SuspendLayout();
            this.splitContainer15.Panel2.SuspendLayout();
            this.splitContainer15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer16)).BeginInit();
            this.splitContainer16.Panel1.SuspendLayout();
            this.splitContainer16.Panel2.SuspendLayout();
            this.splitContainer16.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tpHome_HoSoGiaiDau.SuspendLayout();
            this.tcHoSoGiaiDau.SuspendLayout();
            this.tpHoSoGiaiDau_HoSoDoiBong.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDanhSachDoiBongTongHop)).BeginInit();
            this.tpHoSoGiaiDau_HoSoCauThu.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDanhSachCauThuTongHop)).BeginInit();
            this.tpTiepNhanHoSo.SuspendLayout();
            this.tcTiepNhanHoSo.SuspendLayout();
            this.tpTiepNhanHoSo_QuyDinh_DangKyGiaiDau.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer21)).BeginInit();
            this.splitContainer21.Panel1.SuspendLayout();
            this.splitContainer21.Panel2.SuspendLayout();
            this.splitContainer21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer22)).BeginInit();
            this.splitContainer22.Panel1.SuspendLayout();
            this.splitContainer22.Panel2.SuspendLayout();
            this.splitContainer22.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTiepNhanHoSo_DangKyGiaiDau)).BeginInit();
            this.tpTiepNhanHoSo_DangKyTTDoiBong.SuspendLayout();
            this.tcTiepNhanHoSoDangKyThongTinDoiBong.SuspendLayout();
            this.tpTiepNhanHoSo_DangKyThongTinDoiBOng.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer18)).BeginInit();
            this.splitContainer18.Panel1.SuspendLayout();
            this.splitContainer18.Panel2.SuspendLayout();
            this.splitContainer18.SuspendLayout();
            this.gbTiepNhanHoSo_DanhSachDoiBongCoTheDangKy.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvChiTietThongTinDoiBOng)).BeginInit();
            this.gpTiepNhanHOSo_DangKyThongTinDoiBong.SuspendLayout();
            this.gbTiepNhanHoSo_DanhSachDoiBongDaDangKy.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTiepNhanHoSo_DanhSachDoiBOngDaDangKy)).BeginInit();
            this.tpTiepNhanHoSoDangKyThongTinCauThu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer19)).BeginInit();
            this.splitContainer19.Panel1.SuspendLayout();
            this.splitContainer19.Panel2.SuspendLayout();
            this.splitContainer19.SuspendLayout();
            this.gbTiepNhanHoSo_DangKyThongTinDoiBong_DanhSachCauThuCoTheDangKy.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvChiTietTTCT)).BeginInit();
            this.gbTiepNhanHOSo_DangKYThongTinCauThu.SuspendLayout();
            this.groupBox18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTNHS_DKDB_DKCT_DanhSachCauThu)).BeginInit();
            this.tpTaoLichThiDau.SuspendLayout();
            this.tcTaoLichThiDau.SuspendLayout();
            this.tpTaoLichThiDau_QuyDinh.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer11)).BeginInit();
            this.splitContainer11.Panel1.SuspendLayout();
            this.splitContainer11.Panel2.SuspendLayout();
            this.splitContainer11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer23)).BeginInit();
            this.splitContainer23.Panel1.SuspendLayout();
            this.splitContainer23.Panel2.SuspendLayout();
            this.splitContainer23.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTaoLichThiDau_DanhSachMuaGiai)).BeginInit();
            this.tpTaoLichThiDau_TaoLichThiDau.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.gbTaoLichThiDau_TaoLichThiDau_QuyDinh.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudstep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudfirstmatch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudlastmatch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudThoiGianNghiGiuaHaiLuot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudKhoangCachVongDau)).BeginInit();
            this.gbTaoLichThiDau_TaoLichThiDau_TenMuaGiai.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListXepHang)).BeginInit();
            this.tpCapNhatKetQua.SuspendLayout();
            this.tcCapNhatKetQua.SuspendLayout();
            this.tpCapNhatKetQua_QuyDinh.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer17)).BeginInit();
            this.splitContainer17.Panel1.SuspendLayout();
            this.splitContainer17.Panel2.SuspendLayout();
            this.splitContainer17.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCapNhatKetQua_DanhSachTranDau)).BeginInit();
            this.menuStrip2.SuspendLayout();
            this.tpCapNhatKetQua_CapNhatKetQuaTranDau.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer10)).BeginInit();
            this.splitContainer10.Panel1.SuspendLayout();
            this.splitContainer10.Panel2.SuspendLayout();
            this.splitContainer10.SuspendLayout();
            this.gbCapNhatKetQua_ChuNha.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCapNhatKetQua_ChuNha)).BeginInit();
            this.gbCapNhatKetQua_Khach.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCapNhatKetQua_Khach)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer24)).BeginInit();
            this.splitContainer24.Panel1.SuspendLayout();
            this.splitContainer24.Panel2.SuspendLayout();
            this.splitContainer24.SuspendLayout();
            this.groupBox23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudGiay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPhut)).BeginInit();
            this.tpMenuLichThiDau.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLichThiDau)).BeginInit();
            this.menuStrip4.SuspendLayout();
            this.tpBangXepHang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBangXepHangBangXepHang)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.tpSoLieuThongKe.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSoLieuThongKeCauThu)).BeginInit();
            this.menuStrip3.SuspendLayout();
            this.tpTraCuuCauThu.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer20)).BeginInit();
            this.splitContainer20.Panel1.SuspendLayout();
            this.splitContainer20.Panel2.SuspendLayout();
            this.splitContainer20.SuspendLayout();
            this.groupBox20.SuspendLayout();
            this.groupBox17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudTraCuu_TuoiToiDa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTraCuu_TuoiToiThieu)).BeginInit();
            this.groupBox19.SuspendLayout();
            this.groupBox14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTraCuu_DanhSachTraCuu)).BeginInit();
            this.tpThayDoiQuyDinh.SuspendLayout();
            this.tcQuyDinh.SuspendLayout();
            this.tpQuyDinh_TongHopQuyDinh.SuspendLayout();
            this.tcQuyDinh_DanhSachQuyDinh.SuspendLayout();
            this.tpQuyDinh_QuyDinhChung.SuspendLayout();
            this.groupBox24.SuspendLayout();
            this.tp_QuyDinh_ChiTiet.SuspendLayout();
            this.groupBox27.SuspendLayout();
            this.groupBox26.SuspendLayout();
            this.groupBox25.SuspendLayout();
            this.tpQuyDinh_ChiTietQuyDinh.SuspendLayout();
            this.groupBox29.SuspendLayout();
            this.groupBox28.SuspendLayout();
            this.tpQuyDinh_SuaQuyDinh.SuspendLayout();
            this.groupBox31.SuspendLayout();
            this.groupBox30.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudQuyDinh_ThoiDiemGhiBanToiDa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudQuyDinh_TuoiToiThieu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudQuyDinh_TuoiToiDa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudQuyDinh_SoLuongCauThuNuocNgoai)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudQuyDinh_SoLuongCauThuToiThieu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudQuyDinh_SoLuongCauThuToiDa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer9)).BeginInit();
            this.splitContainer9.SuspendLayout();
            this.SuspendLayout();
            // 
            // tcMenu
            // 
            this.tcMenu.Controls.Add(this.tpHome);
            this.tcMenu.Controls.Add(this.tpTiepNhanHoSo);
            this.tcMenu.Controls.Add(this.tpTaoLichThiDau);
            this.tcMenu.Controls.Add(this.tpCapNhatKetQua);
            this.tcMenu.Controls.Add(this.tpMenuLichThiDau);
            this.tcMenu.Controls.Add(this.tpBangXepHang);
            this.tcMenu.Controls.Add(this.tpSoLieuThongKe);
            this.tcMenu.Controls.Add(this.tpTraCuuCauThu);
            this.tcMenu.Controls.Add(this.tpThayDoiQuyDinh);
            this.tcMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tcMenu.Location = new System.Drawing.Point(0, 0);
            this.tcMenu.Name = "tcMenu";
            this.tcMenu.SelectedIndex = 0;
            this.tcMenu.Size = new System.Drawing.Size(984, 610);
            this.tcMenu.TabIndex = 7;
            this.tcMenu.SelectedIndexChanged += new System.EventHandler(this.tcMenu_SelectedIndexChanged);
            // 
            // tpHome
            // 
            this.tpHome.BackColor = System.Drawing.Color.Snow;
            this.tpHome.Controls.Add(this.tcHOME);
            this.tpHome.Location = new System.Drawing.Point(4, 25);
            this.tpHome.Name = "tpHome";
            this.tpHome.Padding = new System.Windows.Forms.Padding(3);
            this.tpHome.Size = new System.Drawing.Size(976, 581);
            this.tpHome.TabIndex = 0;
            this.tpHome.Text = "HOME";
            // 
            // tcHOME
            // 
            this.tcHOME.Controls.Add(this.tabPage2);
            this.tcHOME.Controls.Add(this.tpHOME_DangKyGiaiDau);
            this.tcHOME.Controls.Add(this.tpHOME_TienDangKyDoiBong);
            this.tcHOME.Controls.Add(this.tpHome_HoSoGiaiDau);
            this.tcHOME.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcHOME.Location = new System.Drawing.Point(3, 3);
            this.tcHOME.Name = "tcHOME";
            this.tcHOME.SelectedIndex = 0;
            this.tcHOME.Size = new System.Drawing.Size(970, 575);
            this.tcHOME.TabIndex = 4;
            this.tcHOME.SelectedIndexChanged += new System.EventHandler(this.tcHOME_SelectedIndexChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.Transparent;
            this.tabPage2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tabPage2.BackgroundImage")));
            this.tabPage2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabPage2.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(962, 546);
            this.tabPage2.TabIndex = 0;
            this.tabPage2.Text = "Trang chủ";
            // 
            // tpHOME_DangKyGiaiDau
            // 
            this.tpHOME_DangKyGiaiDau.Controls.Add(this.splitContainer12);
            this.tpHOME_DangKyGiaiDau.Location = new System.Drawing.Point(4, 25);
            this.tpHOME_DangKyGiaiDau.Name = "tpHOME_DangKyGiaiDau";
            this.tpHOME_DangKyGiaiDau.Padding = new System.Windows.Forms.Padding(3);
            this.tpHOME_DangKyGiaiDau.Size = new System.Drawing.Size(962, 546);
            this.tpHOME_DangKyGiaiDau.TabIndex = 1;
            this.tpHOME_DangKyGiaiDau.Text = "Đăng ký giải đấu";
            this.tpHOME_DangKyGiaiDau.UseVisualStyleBackColor = true;
            // 
            // splitContainer12
            // 
            this.splitContainer12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer12.Location = new System.Drawing.Point(3, 3);
            this.splitContainer12.Name = "splitContainer12";
            // 
            // splitContainer12.Panel1
            // 
            this.splitContainer12.Panel1.Controls.Add(this.gb_HOME_DangKyGiaiDau_QuyDinh);
            // 
            // splitContainer12.Panel2
            // 
            this.splitContainer12.Panel2.Controls.Add(this.gb_HOME_DangKyGiaiDau_DanhSachGiai);
            this.splitContainer12.Size = new System.Drawing.Size(956, 540);
            this.splitContainer12.SplitterDistance = 502;
            this.splitContainer12.TabIndex = 0;
            // 
            // gb_HOME_DangKyGiaiDau_QuyDinh
            // 
            this.gb_HOME_DangKyGiaiDau_QuyDinh.BackColor = System.Drawing.Color.Azure;
            this.gb_HOME_DangKyGiaiDau_QuyDinh.Controls.Add(this.groupBox5);
            this.gb_HOME_DangKyGiaiDau_QuyDinh.Controls.Add(this.btHOME_DangKyGiaiDau_DangKy);
            this.gb_HOME_DangKyGiaiDau_QuyDinh.Controls.Add(this.cbbHOME_DangKyGiaiDau_TenGiaiDau);
            this.gb_HOME_DangKyGiaiDau_QuyDinh.Controls.Add(this.tbHOME_DangKyGiaiDau_MaGiaiDau);
            this.gb_HOME_DangKyGiaiDau_QuyDinh.Controls.Add(this.nudHOME_DangKyGiaiDau_SoLuongDoi);
            this.gb_HOME_DangKyGiaiDau_QuyDinh.Controls.Add(this.label16);
            this.gb_HOME_DangKyGiaiDau_QuyDinh.Controls.Add(this.label15);
            this.gb_HOME_DangKyGiaiDau_QuyDinh.Controls.Add(this.lbMaGiai);
            this.gb_HOME_DangKyGiaiDau_QuyDinh.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gb_HOME_DangKyGiaiDau_QuyDinh.ForeColor = System.Drawing.Color.DarkGreen;
            this.gb_HOME_DangKyGiaiDau_QuyDinh.Location = new System.Drawing.Point(0, 0);
            this.gb_HOME_DangKyGiaiDau_QuyDinh.Name = "gb_HOME_DangKyGiaiDau_QuyDinh";
            this.gb_HOME_DangKyGiaiDau_QuyDinh.Size = new System.Drawing.Size(502, 540);
            this.gb_HOME_DangKyGiaiDau_QuyDinh.TabIndex = 0;
            this.gb_HOME_DangKyGiaiDau_QuyDinh.TabStop = false;
            this.gb_HOME_DangKyGiaiDau_QuyDinh.Text = "Quy định đăng ký";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.textBox2);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox5.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox5.Location = new System.Drawing.Point(3, 291);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(496, 246);
            this.groupBox5.TabIndex = 8;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Quy định đăng ký";
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.textBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.ForeColor = System.Drawing.Color.Teal;
            this.textBox2.Location = new System.Drawing.Point(3, 18);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(490, 225);
            this.textBox2.TabIndex = 7;
            this.textBox2.Text = resources.GetString("textBox2.Text");
            // 
            // btHOME_DangKyGiaiDau_DangKy
            // 
            this.btHOME_DangKyGiaiDau_DangKy.BackColor = System.Drawing.Color.LightCyan;
            this.btHOME_DangKyGiaiDau_DangKy.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btHOME_DangKyGiaiDau_DangKy.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btHOME_DangKyGiaiDau_DangKy.ForeColor = System.Drawing.Color.Teal;
            this.btHOME_DangKyGiaiDau_DangKy.Location = new System.Drawing.Point(25, 180);
            this.btHOME_DangKyGiaiDau_DangKy.Name = "btHOME_DangKyGiaiDau_DangKy";
            this.btHOME_DangKyGiaiDau_DangKy.Size = new System.Drawing.Size(121, 30);
            this.btHOME_DangKyGiaiDau_DangKy.TabIndex = 3;
            this.btHOME_DangKyGiaiDau_DangKy.Text = "Đăng ký giải";
            this.btHOME_DangKyGiaiDau_DangKy.UseVisualStyleBackColor = false;
            this.btHOME_DangKyGiaiDau_DangKy.Click += new System.EventHandler(this.btHOME_DangKyGiaiDau_DangKy_Click);
            // 
            // cbbHOME_DangKyGiaiDau_TenGiaiDau
            // 
            this.cbbHOME_DangKyGiaiDau_TenGiaiDau.FormattingEnabled = true;
            this.cbbHOME_DangKyGiaiDau_TenGiaiDau.Items.AddRange(new object[] {
            "Giải vô địch bóng đá Quốc gia",
            "GVĐBDQG",
            "Cup Quốc Gia"});
            this.cbbHOME_DangKyGiaiDau_TenGiaiDau.Location = new System.Drawing.Point(215, 90);
            this.cbbHOME_DangKyGiaiDau_TenGiaiDau.Name = "cbbHOME_DangKyGiaiDau_TenGiaiDau";
            this.cbbHOME_DangKyGiaiDau_TenGiaiDau.Size = new System.Drawing.Size(205, 24);
            this.cbbHOME_DangKyGiaiDau_TenGiaiDau.TabIndex = 1;
            this.cbbHOME_DangKyGiaiDau_TenGiaiDau.Text = "Giải vô địch bóng đá Quốc gia";
            // 
            // tbHOME_DangKyGiaiDau_MaGiaiDau
            // 
            this.tbHOME_DangKyGiaiDau_MaGiaiDau.Location = new System.Drawing.Point(215, 50);
            this.tbHOME_DangKyGiaiDau_MaGiaiDau.Name = "tbHOME_DangKyGiaiDau_MaGiaiDau";
            this.tbHOME_DangKyGiaiDau_MaGiaiDau.ReadOnly = true;
            this.tbHOME_DangKyGiaiDau_MaGiaiDau.Size = new System.Drawing.Size(205, 22);
            this.tbHOME_DangKyGiaiDau_MaGiaiDau.TabIndex = 4;
            // 
            // nudHOME_DangKyGiaiDau_SoLuongDoi
            // 
            this.nudHOME_DangKyGiaiDau_SoLuongDoi.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudHOME_DangKyGiaiDau_SoLuongDoi.Location = new System.Drawing.Point(370, 130);
            this.nudHOME_DangKyGiaiDau_SoLuongDoi.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.nudHOME_DangKyGiaiDau_SoLuongDoi.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudHOME_DangKyGiaiDau_SoLuongDoi.Name = "nudHOME_DangKyGiaiDau_SoLuongDoi";
            this.nudHOME_DangKyGiaiDau_SoLuongDoi.Size = new System.Drawing.Size(50, 22);
            this.nudHOME_DangKyGiaiDau_SoLuongDoi.TabIndex = 2;
            this.nudHOME_DangKyGiaiDau_SoLuongDoi.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(25, 130);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(171, 16);
            this.label16.TabIndex = 2;
            this.label16.Text = "Số lượng đội bóng tham gia";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(25, 90);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(83, 16);
            this.label15.TabIndex = 1;
            this.label15.Text = "Tên giải đấu";
            // 
            // lbMaGiai
            // 
            this.lbMaGiai.AutoSize = true;
            this.lbMaGiai.Location = new System.Drawing.Point(25, 50);
            this.lbMaGiai.Name = "lbMaGiai";
            this.lbMaGiai.Size = new System.Drawing.Size(80, 16);
            this.lbMaGiai.TabIndex = 0;
            this.lbMaGiai.Text = "Mã Giải đấu";
            // 
            // gb_HOME_DangKyGiaiDau_DanhSachGiai
            // 
            this.gb_HOME_DangKyGiaiDau_DanhSachGiai.Controls.Add(this.dgvDanhSachMuaGiai);
            this.gb_HOME_DangKyGiaiDau_DanhSachGiai.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gb_HOME_DangKyGiaiDau_DanhSachGiai.ForeColor = System.Drawing.Color.DarkGreen;
            this.gb_HOME_DangKyGiaiDau_DanhSachGiai.Location = new System.Drawing.Point(0, 0);
            this.gb_HOME_DangKyGiaiDau_DanhSachGiai.Name = "gb_HOME_DangKyGiaiDau_DanhSachGiai";
            this.gb_HOME_DangKyGiaiDau_DanhSachGiai.Size = new System.Drawing.Size(450, 540);
            this.gb_HOME_DangKyGiaiDau_DanhSachGiai.TabIndex = 0;
            this.gb_HOME_DangKyGiaiDau_DanhSachGiai.TabStop = false;
            this.gb_HOME_DangKyGiaiDau_DanhSachGiai.Text = "Danh sách giải đã đăng ký";
            // 
            // dgvDanhSachMuaGiai
            // 
            this.dgvDanhSachMuaGiai.AllowUserToAddRows = false;
            this.dgvDanhSachMuaGiai.AllowUserToDeleteRows = false;
            this.dgvDanhSachMuaGiai.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvDanhSachMuaGiai.BackgroundColor = System.Drawing.Color.Snow;
            this.dgvDanhSachMuaGiai.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDanhSachMuaGiai.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MaMuaGiaiDSMG,
            this.TenMuaGiaiDSMG,
            this.SoLuongDoiTuyenDSMG});
            this.dgvDanhSachMuaGiai.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDanhSachMuaGiai.Location = new System.Drawing.Point(3, 18);
            this.dgvDanhSachMuaGiai.Name = "dgvDanhSachMuaGiai";
            this.dgvDanhSachMuaGiai.ReadOnly = true;
            this.dgvDanhSachMuaGiai.Size = new System.Drawing.Size(444, 519);
            this.dgvDanhSachMuaGiai.TabIndex = 0;
            // 
            // MaMuaGiaiDSMG
            // 
            this.MaMuaGiaiDSMG.DataPropertyName = "MaMuaGiai";
            this.MaMuaGiaiDSMG.HeaderText = "Mã mùa giải";
            this.MaMuaGiaiDSMG.Name = "MaMuaGiaiDSMG";
            this.MaMuaGiaiDSMG.ReadOnly = true;
            // 
            // TenMuaGiaiDSMG
            // 
            this.TenMuaGiaiDSMG.DataPropertyName = "TenMuaGiai";
            this.TenMuaGiaiDSMG.HeaderText = "Tên mùa giải";
            this.TenMuaGiaiDSMG.Name = "TenMuaGiaiDSMG";
            this.TenMuaGiaiDSMG.ReadOnly = true;
            // 
            // SoLuongDoiTuyenDSMG
            // 
            this.SoLuongDoiTuyenDSMG.DataPropertyName = "SoLuongDoiThamDuToiDa";
            this.SoLuongDoiTuyenDSMG.HeaderText = "Số đội";
            this.SoLuongDoiTuyenDSMG.Name = "SoLuongDoiTuyenDSMG";
            this.SoLuongDoiTuyenDSMG.ReadOnly = true;
            // 
            // tpHOME_TienDangKyDoiBong
            // 
            this.tpHOME_TienDangKyDoiBong.Controls.Add(this.splitContainer15);
            this.tpHOME_TienDangKyDoiBong.Location = new System.Drawing.Point(4, 25);
            this.tpHOME_TienDangKyDoiBong.Name = "tpHOME_TienDangKyDoiBong";
            this.tpHOME_TienDangKyDoiBong.Padding = new System.Windows.Forms.Padding(3);
            this.tpHOME_TienDangKyDoiBong.Size = new System.Drawing.Size(962, 546);
            this.tpHOME_TienDangKyDoiBong.TabIndex = 2;
            this.tpHOME_TienDangKyDoiBong.Text = "Tiền đăng ký hồ sơ";
            this.tpHOME_TienDangKyDoiBong.UseVisualStyleBackColor = true;
            // 
            // splitContainer15
            // 
            this.splitContainer15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer15.Location = new System.Drawing.Point(3, 3);
            this.splitContainer15.Name = "splitContainer15";
            // 
            // splitContainer15.Panel1
            // 
            this.splitContainer15.Panel1.Controls.Add(this.splitContainer16);
            // 
            // splitContainer15.Panel2
            // 
            this.splitContainer15.Panel2.BackColor = System.Drawing.Color.Azure;
            this.splitContainer15.Panel2.Controls.Add(this.groupBox3);
            this.splitContainer15.Panel2.Controls.Add(this.groupBox1);
            this.splitContainer15.Size = new System.Drawing.Size(956, 540);
            this.splitContainer15.SplitterDistance = 300;
            this.splitContainer15.TabIndex = 0;
            // 
            // splitContainer16
            // 
            this.splitContainer16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer16.Location = new System.Drawing.Point(0, 0);
            this.splitContainer16.Name = "splitContainer16";
            this.splitContainer16.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer16.Panel1
            // 
            this.splitContainer16.Panel1.Controls.Add(this.tbHOME_DangKyDanhSachDoiBong_TieuDe);
            // 
            // splitContainer16.Panel2
            // 
            this.splitContainer16.Panel2.Controls.Add(this.groupBox6);
            this.splitContainer16.Size = new System.Drawing.Size(300, 540);
            this.splitContainer16.SplitterDistance = 144;
            this.splitContainer16.TabIndex = 0;
            // 
            // tbHOME_DangKyDanhSachDoiBong_TieuDe
            // 
            this.tbHOME_DangKyDanhSachDoiBong_TieuDe.BackColor = System.Drawing.Color.Azure;
            this.tbHOME_DangKyDanhSachDoiBong_TieuDe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbHOME_DangKyDanhSachDoiBong_TieuDe.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbHOME_DangKyDanhSachDoiBong_TieuDe.ForeColor = System.Drawing.Color.Teal;
            this.tbHOME_DangKyDanhSachDoiBong_TieuDe.Location = new System.Drawing.Point(0, 0);
            this.tbHOME_DangKyDanhSachDoiBong_TieuDe.Multiline = true;
            this.tbHOME_DangKyDanhSachDoiBong_TieuDe.Name = "tbHOME_DangKyDanhSachDoiBong_TieuDe";
            this.tbHOME_DangKyDanhSachDoiBong_TieuDe.Size = new System.Drawing.Size(300, 144);
            this.tbHOME_DangKyDanhSachDoiBong_TieuDe.TabIndex = 0;
            this.tbHOME_DangKyDanhSachDoiBong_TieuDe.Text = "\r\n\r\nQUY ĐỊNH ĐĂNG KÝ \r\nTHÔNG TIN ĐỘI BÓNG";
            this.tbHOME_DangKyDanhSachDoiBong_TieuDe.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.textBox3);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox6.Location = new System.Drawing.Point(0, 0);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(300, 392);
            this.groupBox6.TabIndex = 1;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Danh sách quy định đăng ký";
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.Color.Snow;
            this.textBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox3.ForeColor = System.Drawing.Color.Teal;
            this.textBox3.Location = new System.Drawing.Point(3, 18);
            this.textBox3.Multiline = true;
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(294, 371);
            this.textBox3.TabIndex = 0;
            this.textBox3.Text = resources.GetString("textBox3.Text");
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btHOME_TienDangKyHOSoCauThu_XacNhanDangKy);
            this.groupBox3.Controls.Add(this.rbtHOME_TienDangKyHOSoCauThu_LoaiCauThu_NuocNgoai);
            this.groupBox3.Controls.Add(this.rbtHOME_TienDangKyHOSoCauThu_LoaiCauThu_VietNam);
            this.groupBox3.Controls.Add(this.dtpHOME_TienDangKyHOSoCauThu_NgaySinh);
            this.groupBox3.Controls.Add(this.label33);
            this.groupBox3.Controls.Add(this.label35);
            this.groupBox3.Controls.Add(this.label34);
            this.groupBox3.Controls.Add(this.label29);
            this.groupBox3.Controls.Add(this.tbHOME_TienDangKyHOSoCauThu_TenCauThu);
            this.groupBox3.Controls.Add(this.tbHOME_TienDangKyHOSoCauThu_MaCauThu);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupBox3.Location = new System.Drawing.Point(337, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(315, 540);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Đăng ký cầu thủ";
            // 
            // btHOME_TienDangKyHOSoCauThu_XacNhanDangKy
            // 
            this.btHOME_TienDangKyHOSoCauThu_XacNhanDangKy.BackColor = System.Drawing.Color.LightCyan;
            this.btHOME_TienDangKyHOSoCauThu_XacNhanDangKy.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btHOME_TienDangKyHOSoCauThu_XacNhanDangKy.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btHOME_TienDangKyHOSoCauThu_XacNhanDangKy.ForeColor = System.Drawing.Color.Teal;
            this.btHOME_TienDangKyHOSoCauThu_XacNhanDangKy.Location = new System.Drawing.Point(30, 250);
            this.btHOME_TienDangKyHOSoCauThu_XacNhanDangKy.Name = "btHOME_TienDangKyHOSoCauThu_XacNhanDangKy";
            this.btHOME_TienDangKyHOSoCauThu_XacNhanDangKy.Size = new System.Drawing.Size(150, 30);
            this.btHOME_TienDangKyHOSoCauThu_XacNhanDangKy.TabIndex = 14;
            this.btHOME_TienDangKyHOSoCauThu_XacNhanDangKy.Text = "Xác nhận đăng ký";
            this.btHOME_TienDangKyHOSoCauThu_XacNhanDangKy.UseVisualStyleBackColor = false;
            this.btHOME_TienDangKyHOSoCauThu_XacNhanDangKy.Click += new System.EventHandler(this.btHOME_TienDangKyHOSoCauThu_XacNhanDangKy_Click);
            // 
            // rbtHOME_TienDangKyHOSoCauThu_LoaiCauThu_NuocNgoai
            // 
            this.rbtHOME_TienDangKyHOSoCauThu_LoaiCauThu_NuocNgoai.AutoSize = true;
            this.rbtHOME_TienDangKyHOSoCauThu_LoaiCauThu_NuocNgoai.Location = new System.Drawing.Point(130, 210);
            this.rbtHOME_TienDangKyHOSoCauThu_LoaiCauThu_NuocNgoai.Name = "rbtHOME_TienDangKyHOSoCauThu_LoaiCauThu_NuocNgoai";
            this.rbtHOME_TienDangKyHOSoCauThu_LoaiCauThu_NuocNgoai.Size = new System.Drawing.Size(95, 20);
            this.rbtHOME_TienDangKyHOSoCauThu_LoaiCauThu_NuocNgoai.TabIndex = 12;
            this.rbtHOME_TienDangKyHOSoCauThu_LoaiCauThu_NuocNgoai.Text = "Nước ngoài";
            this.rbtHOME_TienDangKyHOSoCauThu_LoaiCauThu_NuocNgoai.UseVisualStyleBackColor = true;
            // 
            // rbtHOME_TienDangKyHOSoCauThu_LoaiCauThu_VietNam
            // 
            this.rbtHOME_TienDangKyHOSoCauThu_LoaiCauThu_VietNam.AutoSize = true;
            this.rbtHOME_TienDangKyHOSoCauThu_LoaiCauThu_VietNam.Checked = true;
            this.rbtHOME_TienDangKyHOSoCauThu_LoaiCauThu_VietNam.Location = new System.Drawing.Point(130, 180);
            this.rbtHOME_TienDangKyHOSoCauThu_LoaiCauThu_VietNam.Name = "rbtHOME_TienDangKyHOSoCauThu_LoaiCauThu_VietNam";
            this.rbtHOME_TienDangKyHOSoCauThu_LoaiCauThu_VietNam.Size = new System.Drawing.Size(81, 20);
            this.rbtHOME_TienDangKyHOSoCauThu_LoaiCauThu_VietNam.TabIndex = 13;
            this.rbtHOME_TienDangKyHOSoCauThu_LoaiCauThu_VietNam.TabStop = true;
            this.rbtHOME_TienDangKyHOSoCauThu_LoaiCauThu_VietNam.Text = "Việt Nam";
            this.rbtHOME_TienDangKyHOSoCauThu_LoaiCauThu_VietNam.UseVisualStyleBackColor = true;
            // 
            // dtpHOME_TienDangKyHOSoCauThu_NgaySinh
            // 
            this.dtpHOME_TienDangKyHOSoCauThu_NgaySinh.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpHOME_TienDangKyHOSoCauThu_NgaySinh.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpHOME_TienDangKyHOSoCauThu_NgaySinh.Location = new System.Drawing.Point(130, 140);
            this.dtpHOME_TienDangKyHOSoCauThu_NgaySinh.Name = "dtpHOME_TienDangKyHOSoCauThu_NgaySinh";
            this.dtpHOME_TienDangKyHOSoCauThu_NgaySinh.Size = new System.Drawing.Size(140, 22);
            this.dtpHOME_TienDangKyHOSoCauThu_NgaySinh.TabIndex = 11;
            this.dtpHOME_TienDangKyHOSoCauThu_NgaySinh.Value = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(30, 60);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(72, 16);
            this.label33.TabIndex = 7;
            this.label33.Text = "Mã cầu thủ";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(30, 180);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(79, 16);
            this.label35.TabIndex = 8;
            this.label35.Text = "Loại cầu thủ";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(30, 140);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(68, 16);
            this.label34.TabIndex = 9;
            this.label34.Text = "Ngày sinh";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(30, 100);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(77, 16);
            this.label29.TabIndex = 10;
            this.label29.Text = "Tên cầu thủ";
            // 
            // tbHOME_TienDangKyHOSoCauThu_TenCauThu
            // 
            this.tbHOME_TienDangKyHOSoCauThu_TenCauThu.Location = new System.Drawing.Point(130, 100);
            this.tbHOME_TienDangKyHOSoCauThu_TenCauThu.Name = "tbHOME_TienDangKyHOSoCauThu_TenCauThu";
            this.tbHOME_TienDangKyHOSoCauThu_TenCauThu.Size = new System.Drawing.Size(140, 22);
            this.tbHOME_TienDangKyHOSoCauThu_TenCauThu.TabIndex = 5;
            // 
            // tbHOME_TienDangKyHOSoCauThu_MaCauThu
            // 
            this.tbHOME_TienDangKyHOSoCauThu_MaCauThu.Location = new System.Drawing.Point(130, 60);
            this.tbHOME_TienDangKyHOSoCauThu_MaCauThu.Name = "tbHOME_TienDangKyHOSoCauThu_MaCauThu";
            this.tbHOME_TienDangKyHOSoCauThu_MaCauThu.ReadOnly = true;
            this.tbHOME_TienDangKyHOSoCauThu_MaCauThu.Size = new System.Drawing.Size(140, 22);
            this.tbHOME_TienDangKyHOSoCauThu_MaCauThu.TabIndex = 6;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btDangKyHoSoDoiBong_XacNhanDangKy_DangKy);
            this.groupBox1.Controls.Add(this.btDangKyHoSoDoiBong_ThongTinDangKy_DangKy);
            this.groupBox1.Controls.Add(this.label30);
            this.groupBox1.Controls.Add(this.tbDangKyHoSoDoiBong_ThongTinDangKy_SanNha);
            this.groupBox1.Controls.Add(this.label31);
            this.groupBox1.Controls.Add(this.tbDangKyHoSoDoiBong_ThongTinDangKy_TenDoiBong);
            this.groupBox1.Controls.Add(this.label32);
            this.groupBox1.Controls.Add(this.tbDangKyHoSoDoiBong_ThongTinDangKy_MaDoiBong);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(315, 540);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Đăng ký đội bóng";
            // 
            // btDangKyHoSoDoiBong_XacNhanDangKy_DangKy
            // 
            this.btDangKyHoSoDoiBong_XacNhanDangKy_DangKy.Location = new System.Drawing.Point(36, 270);
            this.btDangKyHoSoDoiBong_XacNhanDangKy_DangKy.Name = "btDangKyHoSoDoiBong_XacNhanDangKy_DangKy";
            this.btDangKyHoSoDoiBong_XacNhanDangKy_DangKy.Size = new System.Drawing.Size(165, 47);
            this.btDangKyHoSoDoiBong_XacNhanDangKy_DangKy.TabIndex = 0;
            this.btDangKyHoSoDoiBong_XacNhanDangKy_DangKy.Text = "Đăng ký";
            this.btDangKyHoSoDoiBong_XacNhanDangKy_DangKy.UseVisualStyleBackColor = true;
            this.btDangKyHoSoDoiBong_XacNhanDangKy_DangKy.Visible = false;
            // 
            // btDangKyHoSoDoiBong_ThongTinDangKy_DangKy
            // 
            this.btDangKyHoSoDoiBong_ThongTinDangKy_DangKy.BackColor = System.Drawing.Color.LightCyan;
            this.btDangKyHoSoDoiBong_ThongTinDangKy_DangKy.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btDangKyHoSoDoiBong_ThongTinDangKy_DangKy.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btDangKyHoSoDoiBong_ThongTinDangKy_DangKy.ForeColor = System.Drawing.Color.Teal;
            this.btDangKyHoSoDoiBong_ThongTinDangKy_DangKy.Location = new System.Drawing.Point(30, 200);
            this.btDangKyHoSoDoiBong_ThongTinDangKy_DangKy.Name = "btDangKyHoSoDoiBong_ThongTinDangKy_DangKy";
            this.btDangKyHoSoDoiBong_ThongTinDangKy_DangKy.Size = new System.Drawing.Size(150, 30);
            this.btDangKyHoSoDoiBong_ThongTinDangKy_DangKy.TabIndex = 10;
            this.btDangKyHoSoDoiBong_ThongTinDangKy_DangKy.Text = "Xác nhận đăng ký";
            this.btDangKyHoSoDoiBong_ThongTinDangKy_DangKy.UseVisualStyleBackColor = false;
            this.btDangKyHoSoDoiBong_ThongTinDangKy_DangKy.Click += new System.EventHandler(this.btDangKyHoSoDoiBong_ThongTinDangKy_DangKy_Click);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(30, 60);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(83, 16);
            this.label30.TabIndex = 6;
            this.label30.Text = "Mã đội bóng";
            // 
            // tbDangKyHoSoDoiBong_ThongTinDangKy_SanNha
            // 
            this.tbDangKyHoSoDoiBong_ThongTinDangKy_SanNha.Location = new System.Drawing.Point(130, 140);
            this.tbDangKyHoSoDoiBong_ThongTinDangKy_SanNha.Name = "tbDangKyHoSoDoiBong_ThongTinDangKy_SanNha";
            this.tbDangKyHoSoDoiBong_ThongTinDangKy_SanNha.Size = new System.Drawing.Size(140, 22);
            this.tbDangKyHoSoDoiBong_ThongTinDangKy_SanNha.TabIndex = 9;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(30, 100);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(88, 16);
            this.label31.TabIndex = 5;
            this.label31.Text = "Tên đội bóng";
            // 
            // tbDangKyHoSoDoiBong_ThongTinDangKy_TenDoiBong
            // 
            this.tbDangKyHoSoDoiBong_ThongTinDangKy_TenDoiBong.Location = new System.Drawing.Point(130, 100);
            this.tbDangKyHoSoDoiBong_ThongTinDangKy_TenDoiBong.Name = "tbDangKyHoSoDoiBong_ThongTinDangKy_TenDoiBong";
            this.tbDangKyHoSoDoiBong_ThongTinDangKy_TenDoiBong.Size = new System.Drawing.Size(140, 22);
            this.tbDangKyHoSoDoiBong_ThongTinDangKy_TenDoiBong.TabIndex = 7;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(30, 140);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(57, 16);
            this.label32.TabIndex = 4;
            this.label32.Text = "Sân nhà";
            // 
            // tbDangKyHoSoDoiBong_ThongTinDangKy_MaDoiBong
            // 
            this.tbDangKyHoSoDoiBong_ThongTinDangKy_MaDoiBong.Location = new System.Drawing.Point(130, 60);
            this.tbDangKyHoSoDoiBong_ThongTinDangKy_MaDoiBong.Name = "tbDangKyHoSoDoiBong_ThongTinDangKy_MaDoiBong";
            this.tbDangKyHoSoDoiBong_ThongTinDangKy_MaDoiBong.ReadOnly = true;
            this.tbDangKyHoSoDoiBong_ThongTinDangKy_MaDoiBong.Size = new System.Drawing.Size(140, 22);
            this.tbDangKyHoSoDoiBong_ThongTinDangKy_MaDoiBong.TabIndex = 8;
            // 
            // tpHome_HoSoGiaiDau
            // 
            this.tpHome_HoSoGiaiDau.Controls.Add(this.tcHoSoGiaiDau);
            this.tpHome_HoSoGiaiDau.Location = new System.Drawing.Point(4, 25);
            this.tpHome_HoSoGiaiDau.Name = "tpHome_HoSoGiaiDau";
            this.tpHome_HoSoGiaiDau.Padding = new System.Windows.Forms.Padding(3);
            this.tpHome_HoSoGiaiDau.Size = new System.Drawing.Size(962, 546);
            this.tpHome_HoSoGiaiDau.TabIndex = 4;
            this.tpHome_HoSoGiaiDau.Text = "Hồ sơ giải đấu";
            this.tpHome_HoSoGiaiDau.UseVisualStyleBackColor = true;
            // 
            // tcHoSoGiaiDau
            // 
            this.tcHoSoGiaiDau.Controls.Add(this.tpHoSoGiaiDau_HoSoDoiBong);
            this.tcHoSoGiaiDau.Controls.Add(this.tpHoSoGiaiDau_HoSoCauThu);
            this.tcHoSoGiaiDau.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcHoSoGiaiDau.Location = new System.Drawing.Point(3, 3);
            this.tcHoSoGiaiDau.Name = "tcHoSoGiaiDau";
            this.tcHoSoGiaiDau.SelectedIndex = 0;
            this.tcHoSoGiaiDau.Size = new System.Drawing.Size(956, 540);
            this.tcHoSoGiaiDau.TabIndex = 0;
            this.tcHoSoGiaiDau.SelectedIndexChanged += new System.EventHandler(this.tcHoSoGiaiDau_SelectedIndexChanged);
            // 
            // tpHoSoGiaiDau_HoSoDoiBong
            // 
            this.tpHoSoGiaiDau_HoSoDoiBong.Controls.Add(this.groupBox7);
            this.tpHoSoGiaiDau_HoSoDoiBong.Location = new System.Drawing.Point(4, 25);
            this.tpHoSoGiaiDau_HoSoDoiBong.Name = "tpHoSoGiaiDau_HoSoDoiBong";
            this.tpHoSoGiaiDau_HoSoDoiBong.Padding = new System.Windows.Forms.Padding(3);
            this.tpHoSoGiaiDau_HoSoDoiBong.Size = new System.Drawing.Size(948, 511);
            this.tpHoSoGiaiDau_HoSoDoiBong.TabIndex = 0;
            this.tpHoSoGiaiDau_HoSoDoiBong.Text = "Hồ sơ đội bóng";
            this.tpHoSoGiaiDau_HoSoDoiBong.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.dgvDanhSachDoiBongTongHop);
            this.groupBox7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox7.ForeColor = System.Drawing.Color.DarkGreen;
            this.groupBox7.Location = new System.Drawing.Point(3, 3);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(942, 505);
            this.groupBox7.TabIndex = 0;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Danh sách toàn bộ đội bóng đã đăng ký";
            // 
            // dgvDanhSachDoiBongTongHop
            // 
            this.dgvDanhSachDoiBongTongHop.AllowUserToAddRows = false;
            this.dgvDanhSachDoiBongTongHop.AllowUserToDeleteRows = false;
            this.dgvDanhSachDoiBongTongHop.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvDanhSachDoiBongTongHop.BackgroundColor = System.Drawing.Color.Snow;
            this.dgvDanhSachDoiBongTongHop.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDanhSachDoiBongTongHop.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MaDoiBongHS,
            this.TenDoiBongHS,
            this.SanNhaHS});
            this.dgvDanhSachDoiBongTongHop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDanhSachDoiBongTongHop.Location = new System.Drawing.Point(3, 18);
            this.dgvDanhSachDoiBongTongHop.Name = "dgvDanhSachDoiBongTongHop";
            this.dgvDanhSachDoiBongTongHop.ReadOnly = true;
            this.dgvDanhSachDoiBongTongHop.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDanhSachDoiBongTongHop.Size = new System.Drawing.Size(936, 484);
            this.dgvDanhSachDoiBongTongHop.TabIndex = 0;
            // 
            // MaDoiBongHS
            // 
            this.MaDoiBongHS.DataPropertyName = "MaDoiBong";
            this.MaDoiBongHS.HeaderText = "Mã đội bóng";
            this.MaDoiBongHS.Name = "MaDoiBongHS";
            this.MaDoiBongHS.ReadOnly = true;
            // 
            // TenDoiBongHS
            // 
            this.TenDoiBongHS.DataPropertyName = "TenDoiBong";
            this.TenDoiBongHS.HeaderText = "Tên đội bóng";
            this.TenDoiBongHS.Name = "TenDoiBongHS";
            this.TenDoiBongHS.ReadOnly = true;
            // 
            // SanNhaHS
            // 
            this.SanNhaHS.DataPropertyName = "SanNha";
            this.SanNhaHS.HeaderText = "Sân nhà";
            this.SanNhaHS.Name = "SanNhaHS";
            this.SanNhaHS.ReadOnly = true;
            // 
            // tpHoSoGiaiDau_HoSoCauThu
            // 
            this.tpHoSoGiaiDau_HoSoCauThu.Controls.Add(this.groupBox8);
            this.tpHoSoGiaiDau_HoSoCauThu.Location = new System.Drawing.Point(4, 25);
            this.tpHoSoGiaiDau_HoSoCauThu.Name = "tpHoSoGiaiDau_HoSoCauThu";
            this.tpHoSoGiaiDau_HoSoCauThu.Padding = new System.Windows.Forms.Padding(3);
            this.tpHoSoGiaiDau_HoSoCauThu.Size = new System.Drawing.Size(948, 514);
            this.tpHoSoGiaiDau_HoSoCauThu.TabIndex = 1;
            this.tpHoSoGiaiDau_HoSoCauThu.Text = "Hồ sơ cầu thủ";
            this.tpHoSoGiaiDau_HoSoCauThu.UseVisualStyleBackColor = true;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.dgvDanhSachCauThuTongHop);
            this.groupBox8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox8.ForeColor = System.Drawing.Color.DarkGreen;
            this.groupBox8.Location = new System.Drawing.Point(3, 3);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(942, 508);
            this.groupBox8.TabIndex = 1;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Danh sách toàn bộ đội bóng đã đăng ký";
            // 
            // dgvDanhSachCauThuTongHop
            // 
            this.dgvDanhSachCauThuTongHop.AllowUserToAddRows = false;
            this.dgvDanhSachCauThuTongHop.AllowUserToDeleteRows = false;
            this.dgvDanhSachCauThuTongHop.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvDanhSachCauThuTongHop.BackgroundColor = System.Drawing.Color.Snow;
            this.dgvDanhSachCauThuTongHop.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDanhSachCauThuTongHop.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MaCauThuHS,
            this.TenCauThuHS,
            this.NgaySinhHS,
            this.LoaiCauThuHS,
            this.TongSoBanThangHS});
            this.dgvDanhSachCauThuTongHop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDanhSachCauThuTongHop.Location = new System.Drawing.Point(3, 18);
            this.dgvDanhSachCauThuTongHop.Name = "dgvDanhSachCauThuTongHop";
            this.dgvDanhSachCauThuTongHop.ReadOnly = true;
            this.dgvDanhSachCauThuTongHop.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDanhSachCauThuTongHop.Size = new System.Drawing.Size(936, 487);
            this.dgvDanhSachCauThuTongHop.TabIndex = 0;
            // 
            // MaCauThuHS
            // 
            this.MaCauThuHS.DataPropertyName = "MaCauThu";
            this.MaCauThuHS.HeaderText = "Mã loại cầu thủ";
            this.MaCauThuHS.Name = "MaCauThuHS";
            this.MaCauThuHS.ReadOnly = true;
            // 
            // TenCauThuHS
            // 
            this.TenCauThuHS.DataPropertyName = "HoTenCauThu";
            this.TenCauThuHS.HeaderText = "Tên cầu thủ";
            this.TenCauThuHS.Name = "TenCauThuHS";
            this.TenCauThuHS.ReadOnly = true;
            // 
            // NgaySinhHS
            // 
            this.NgaySinhHS.DataPropertyName = "NgaySinh";
            this.NgaySinhHS.HeaderText = "Ngày sinh";
            this.NgaySinhHS.Name = "NgaySinhHS";
            this.NgaySinhHS.ReadOnly = true;
            // 
            // LoaiCauThuHS
            // 
            this.LoaiCauThuHS.DataPropertyName = "MaLoaiCauThu";
            this.LoaiCauThuHS.HeaderText = "Loại cầu thủ";
            this.LoaiCauThuHS.Name = "LoaiCauThuHS";
            this.LoaiCauThuHS.ReadOnly = true;
            // 
            // TongSoBanThangHS
            // 
            this.TongSoBanThangHS.DataPropertyName = "TongSoBanThang";
            this.TongSoBanThangHS.HeaderText = "Tổng số bàn thắng";
            this.TongSoBanThangHS.Name = "TongSoBanThangHS";
            this.TongSoBanThangHS.ReadOnly = true;
            // 
            // tpTiepNhanHoSo
            // 
            this.tpTiepNhanHoSo.BackColor = System.Drawing.Color.Snow;
            this.tpTiepNhanHoSo.Controls.Add(this.tcTiepNhanHoSo);
            this.tpTiepNhanHoSo.Location = new System.Drawing.Point(4, 25);
            this.tpTiepNhanHoSo.Name = "tpTiepNhanHoSo";
            this.tpTiepNhanHoSo.Padding = new System.Windows.Forms.Padding(3);
            this.tpTiepNhanHoSo.Size = new System.Drawing.Size(976, 581);
            this.tpTiepNhanHoSo.TabIndex = 1;
            this.tpTiepNhanHoSo.Text = "Tiếp nhận hồ sơ";
            // 
            // tcTiepNhanHoSo
            // 
            this.tcTiepNhanHoSo.Controls.Add(this.tpTiepNhanHoSo_QuyDinh_DangKyGiaiDau);
            this.tcTiepNhanHoSo.Controls.Add(this.tpTiepNhanHoSo_DangKyTTDoiBong);
            this.tcTiepNhanHoSo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcTiepNhanHoSo.Location = new System.Drawing.Point(3, 3);
            this.tcTiepNhanHoSo.Name = "tcTiepNhanHoSo";
            this.tcTiepNhanHoSo.SelectedIndex = 0;
            this.tcTiepNhanHoSo.Size = new System.Drawing.Size(970, 575);
            this.tcTiepNhanHoSo.TabIndex = 2;
            this.tcTiepNhanHoSo.SelectedIndexChanged += new System.EventHandler(this.tcTiepNhanHoSo_SelectedIndexChanged);
            // 
            // tpTiepNhanHoSo_QuyDinh_DangKyGiaiDau
            // 
            this.tpTiepNhanHoSo_QuyDinh_DangKyGiaiDau.Controls.Add(this.splitContainer21);
            this.tpTiepNhanHoSo_QuyDinh_DangKyGiaiDau.Location = new System.Drawing.Point(4, 25);
            this.tpTiepNhanHoSo_QuyDinh_DangKyGiaiDau.Name = "tpTiepNhanHoSo_QuyDinh_DangKyGiaiDau";
            this.tpTiepNhanHoSo_QuyDinh_DangKyGiaiDau.Padding = new System.Windows.Forms.Padding(3);
            this.tpTiepNhanHoSo_QuyDinh_DangKyGiaiDau.Size = new System.Drawing.Size(962, 546);
            this.tpTiepNhanHoSo_QuyDinh_DangKyGiaiDau.TabIndex = 3;
            this.tpTiepNhanHoSo_QuyDinh_DangKyGiaiDau.Text = "Quy định + Đăng ký giải đấu";
            this.tpTiepNhanHoSo_QuyDinh_DangKyGiaiDau.UseVisualStyleBackColor = true;
            // 
            // splitContainer21
            // 
            this.splitContainer21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer21.Location = new System.Drawing.Point(3, 3);
            this.splitContainer21.Name = "splitContainer21";
            // 
            // splitContainer21.Panel1
            // 
            this.splitContainer21.Panel1.Controls.Add(this.splitContainer22);
            // 
            // splitContainer21.Panel2
            // 
            this.splitContainer21.Panel2.Controls.Add(this.groupBox21);
            this.splitContainer21.Size = new System.Drawing.Size(956, 540);
            this.splitContainer21.SplitterDistance = 275;
            this.splitContainer21.TabIndex = 0;
            // 
            // splitContainer22
            // 
            this.splitContainer22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer22.Location = new System.Drawing.Point(0, 0);
            this.splitContainer22.Name = "splitContainer22";
            this.splitContainer22.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer22.Panel1
            // 
            this.splitContainer22.Panel1.BackColor = System.Drawing.Color.Azure;
            this.splitContainer22.Panel1.Controls.Add(this.groupBox9);
            // 
            // splitContainer22.Panel2
            // 
            this.splitContainer22.Panel2.Controls.Add(this.textBox7);
            this.splitContainer22.Size = new System.Drawing.Size(275, 540);
            this.splitContainer22.SplitterDistance = 106;
            this.splitContainer22.TabIndex = 0;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.label3);
            this.groupBox9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox9.Location = new System.Drawing.Point(0, 0);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(275, 106);
            this.groupBox9.TabIndex = 2;
            this.groupBox9.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Teal;
            this.label3.Location = new System.Drawing.Point(52, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(165, 40);
            this.label3.TabIndex = 2;
            this.label3.Text = "QUY ĐỊNH \r\nĐĂNG KÝ ĐỘI BÓNG";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox7
            // 
            this.textBox7.AllowDrop = true;
            this.textBox7.BackColor = System.Drawing.Color.Azure;
            this.textBox7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox7.ForeColor = System.Drawing.Color.Teal;
            this.textBox7.Location = new System.Drawing.Point(0, 0);
            this.textBox7.Multiline = true;
            this.textBox7.Name = "textBox7";
            this.textBox7.ReadOnly = true;
            this.textBox7.Size = new System.Drawing.Size(275, 430);
            this.textBox7.TabIndex = 1;
            this.textBox7.Text = resources.GetString("textBox7.Text");
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this.dgvTiepNhanHoSo_DangKyGiaiDau);
            this.groupBox21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox21.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox21.Location = new System.Drawing.Point(0, 0);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(677, 540);
            this.groupBox21.TabIndex = 0;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "Tiếp nhận hồ sơ đăng ký giải đấu";
            // 
            // dgvTiepNhanHoSo_DangKyGiaiDau
            // 
            this.dgvTiepNhanHoSo_DangKyGiaiDau.AllowUserToAddRows = false;
            this.dgvTiepNhanHoSo_DangKyGiaiDau.AllowUserToDeleteRows = false;
            this.dgvTiepNhanHoSo_DangKyGiaiDau.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvTiepNhanHoSo_DangKyGiaiDau.BackgroundColor = System.Drawing.Color.Snow;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Salmon;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvTiepNhanHoSo_DangKyGiaiDau.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvTiepNhanHoSo_DangKyGiaiDau.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTiepNhanHoSo_DangKyGiaiDau.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MaGiaiDau,
            this.TenGiaiDau,
            this.SoLuongDoiToiDa,
            this.SoDoiDADangKy,
            this.TrangThaiDK,
            this.CapNhatGiaiDau,
            this.HoanTatGiaiDau});
            this.dgvTiepNhanHoSo_DangKyGiaiDau.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvTiepNhanHoSo_DangKyGiaiDau.Location = new System.Drawing.Point(3, 18);
            this.dgvTiepNhanHoSo_DangKyGiaiDau.Name = "dgvTiepNhanHoSo_DangKyGiaiDau";
            this.dgvTiepNhanHoSo_DangKyGiaiDau.ReadOnly = true;
            this.dgvTiepNhanHoSo_DangKyGiaiDau.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvTiepNhanHoSo_DangKyGiaiDau.Size = new System.Drawing.Size(671, 519);
            this.dgvTiepNhanHoSo_DangKyGiaiDau.TabIndex = 0;
            this.dgvTiepNhanHoSo_DangKyGiaiDau.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTiepNhanHoSo_DangKyGiaiDau_CellContentClick);
            this.dgvTiepNhanHoSo_DangKyGiaiDau.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgvTiepNhanHoSo_DangKyGiaiDau_CellPainting);
            // 
            // MaGiaiDau
            // 
            this.MaGiaiDau.DataPropertyName = "MaMuaGiai";
            this.MaGiaiDau.FillWeight = 57.2527F;
            this.MaGiaiDau.HeaderText = "Mã giải đấu";
            this.MaGiaiDau.Name = "MaGiaiDau";
            this.MaGiaiDau.ReadOnly = true;
            // 
            // TenGiaiDau
            // 
            this.TenGiaiDau.DataPropertyName = "TenMuaGiai";
            this.TenGiaiDau.FillWeight = 55.90679F;
            this.TenGiaiDau.HeaderText = "Tên giải đấu";
            this.TenGiaiDau.Name = "TenGiaiDau";
            this.TenGiaiDau.ReadOnly = true;
            // 
            // SoLuongDoiToiDa
            // 
            this.SoLuongDoiToiDa.DataPropertyName = "SoLuongDoiThamDuToiDa";
            this.SoLuongDoiToiDa.FillWeight = 55.90679F;
            this.SoLuongDoiToiDa.HeaderText = "Số đội tối đa";
            this.SoLuongDoiToiDa.Name = "SoLuongDoiToiDa";
            this.SoLuongDoiToiDa.ReadOnly = true;
            // 
            // SoDoiDADangKy
            // 
            this.SoDoiDADangKy.DataPropertyName = "SoLuongDoiDaDangKy";
            this.SoDoiDADangKy.FillWeight = 55.90679F;
            this.SoDoiDADangKy.HeaderText = "Số đội đã đăng ký";
            this.SoDoiDADangKy.Name = "SoDoiDADangKy";
            this.SoDoiDADangKy.ReadOnly = true;
            // 
            // TrangThaiDK
            // 
            this.TrangThaiDK.DataPropertyName = "HoanTatDangKy";
            this.TrangThaiDK.FillWeight = 55.90679F;
            this.TrangThaiDK.HeaderText = "Trạng thái";
            this.TrangThaiDK.Name = "TrangThaiDK";
            this.TrangThaiDK.ReadOnly = true;
            // 
            // CapNhatGiaiDau
            // 
            this.CapNhatGiaiDau.FillWeight = 50.45746F;
            this.CapNhatGiaiDau.HeaderText = "Cập nhật";
            this.CapNhatGiaiDau.Name = "CapNhatGiaiDau";
            this.CapNhatGiaiDau.ReadOnly = true;
            // 
            // HoanTatGiaiDau
            // 
            this.HoanTatGiaiDau.FillWeight = 59.52168F;
            this.HoanTatGiaiDau.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.HoanTatGiaiDau.HeaderText = "Hoàn tất";
            this.HoanTatGiaiDau.Name = "HoanTatGiaiDau";
            this.HoanTatGiaiDau.ReadOnly = true;
            // 
            // tpTiepNhanHoSo_DangKyTTDoiBong
            // 
            this.tpTiepNhanHoSo_DangKyTTDoiBong.Controls.Add(this.tcTiepNhanHoSoDangKyThongTinDoiBong);
            this.tpTiepNhanHoSo_DangKyTTDoiBong.Location = new System.Drawing.Point(4, 25);
            this.tpTiepNhanHoSo_DangKyTTDoiBong.Name = "tpTiepNhanHoSo_DangKyTTDoiBong";
            this.tpTiepNhanHoSo_DangKyTTDoiBong.Padding = new System.Windows.Forms.Padding(3);
            this.tpTiepNhanHoSo_DangKyTTDoiBong.Size = new System.Drawing.Size(962, 546);
            this.tpTiepNhanHoSo_DangKyTTDoiBong.TabIndex = 2;
            this.tpTiepNhanHoSo_DangKyTTDoiBong.Text = "Đăng ký đội bóng";
            this.tpTiepNhanHoSo_DangKyTTDoiBong.UseVisualStyleBackColor = true;
            // 
            // tcTiepNhanHoSoDangKyThongTinDoiBong
            // 
            this.tcTiepNhanHoSoDangKyThongTinDoiBong.Controls.Add(this.tpTiepNhanHoSo_DangKyThongTinDoiBOng);
            this.tcTiepNhanHoSoDangKyThongTinDoiBong.Controls.Add(this.tpTiepNhanHoSoDangKyThongTinCauThu);
            this.tcTiepNhanHoSoDangKyThongTinDoiBong.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcTiepNhanHoSoDangKyThongTinDoiBong.Location = new System.Drawing.Point(3, 3);
            this.tcTiepNhanHoSoDangKyThongTinDoiBong.Name = "tcTiepNhanHoSoDangKyThongTinDoiBong";
            this.tcTiepNhanHoSoDangKyThongTinDoiBong.SelectedIndex = 0;
            this.tcTiepNhanHoSoDangKyThongTinDoiBong.Size = new System.Drawing.Size(956, 540);
            this.tcTiepNhanHoSoDangKyThongTinDoiBong.TabIndex = 0;
            this.tcTiepNhanHoSoDangKyThongTinDoiBong.SelectedIndexChanged += new System.EventHandler(this.tcTiepNhanHoSoDangKyThongTinDoiBong_SelectedIndexChanged);
            // 
            // tpTiepNhanHoSo_DangKyThongTinDoiBOng
            // 
            this.tpTiepNhanHoSo_DangKyThongTinDoiBOng.Controls.Add(this.splitContainer18);
            this.tpTiepNhanHoSo_DangKyThongTinDoiBOng.Location = new System.Drawing.Point(4, 25);
            this.tpTiepNhanHoSo_DangKyThongTinDoiBOng.Name = "tpTiepNhanHoSo_DangKyThongTinDoiBOng";
            this.tpTiepNhanHoSo_DangKyThongTinDoiBOng.Padding = new System.Windows.Forms.Padding(3);
            this.tpTiepNhanHoSo_DangKyThongTinDoiBOng.Size = new System.Drawing.Size(948, 511);
            this.tpTiepNhanHoSo_DangKyThongTinDoiBOng.TabIndex = 0;
            this.tpTiepNhanHoSo_DangKyThongTinDoiBOng.Text = "Đăng ký thông tin đội bóng";
            this.tpTiepNhanHoSo_DangKyThongTinDoiBOng.UseVisualStyleBackColor = true;
            // 
            // splitContainer18
            // 
            this.splitContainer18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer18.Location = new System.Drawing.Point(3, 3);
            this.splitContainer18.Name = "splitContainer18";
            // 
            // splitContainer18.Panel1
            // 
            this.splitContainer18.Panel1.Controls.Add(this.gbTiepNhanHoSo_DanhSachDoiBongCoTheDangKy);
            // 
            // splitContainer18.Panel2
            // 
            this.splitContainer18.Panel2.Controls.Add(this.gpTiepNhanHOSo_DangKyThongTinDoiBong);
            this.splitContainer18.Size = new System.Drawing.Size(942, 505);
            this.splitContainer18.SplitterDistance = 350;
            this.splitContainer18.TabIndex = 0;
            // 
            // gbTiepNhanHoSo_DanhSachDoiBongCoTheDangKy
            // 
            this.gbTiepNhanHoSo_DanhSachDoiBongCoTheDangKy.Controls.Add(this.dgvChiTietThongTinDoiBOng);
            this.gbTiepNhanHoSo_DanhSachDoiBongCoTheDangKy.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbTiepNhanHoSo_DanhSachDoiBongCoTheDangKy.ForeColor = System.Drawing.Color.Maroon;
            this.gbTiepNhanHoSo_DanhSachDoiBongCoTheDangKy.Location = new System.Drawing.Point(0, 0);
            this.gbTiepNhanHoSo_DanhSachDoiBongCoTheDangKy.Name = "gbTiepNhanHoSo_DanhSachDoiBongCoTheDangKy";
            this.gbTiepNhanHoSo_DanhSachDoiBongCoTheDangKy.Size = new System.Drawing.Size(350, 505);
            this.gbTiepNhanHoSo_DanhSachDoiBongCoTheDangKy.TabIndex = 0;
            this.gbTiepNhanHoSo_DanhSachDoiBongCoTheDangKy.TabStop = false;
            this.gbTiepNhanHoSo_DanhSachDoiBongCoTheDangKy.Text = "Danh sách đội bóng có thể đăng ký";
            // 
            // dgvChiTietThongTinDoiBOng
            // 
            this.dgvChiTietThongTinDoiBOng.AllowUserToAddRows = false;
            this.dgvChiTietThongTinDoiBOng.AllowUserToDeleteRows = false;
            this.dgvChiTietThongTinDoiBOng.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvChiTietThongTinDoiBOng.BackgroundColor = System.Drawing.Color.Snow;
            this.dgvChiTietThongTinDoiBOng.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvChiTietThongTinDoiBOng.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MaDoiBongTC,
            this.TenDoiBOngTC,
            this.AddDB});
            this.dgvChiTietThongTinDoiBOng.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvChiTietThongTinDoiBOng.Location = new System.Drawing.Point(3, 18);
            this.dgvChiTietThongTinDoiBOng.Name = "dgvChiTietThongTinDoiBOng";
            this.dgvChiTietThongTinDoiBOng.ReadOnly = true;
            this.dgvChiTietThongTinDoiBOng.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvChiTietThongTinDoiBOng.Size = new System.Drawing.Size(344, 484);
            this.dgvChiTietThongTinDoiBOng.TabIndex = 0;
            this.dgvChiTietThongTinDoiBOng.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvChiTietThongTinDoiBOng_CellContentClick);
            this.dgvChiTietThongTinDoiBOng.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgvChiTietThongTinDoiBOng_CellPainting);
            // 
            // MaDoiBongTC
            // 
            this.MaDoiBongTC.DataPropertyName = "MaDoiBong";
            this.MaDoiBongTC.FillWeight = 127.1574F;
            this.MaDoiBongTC.HeaderText = "Mã đội bóng";
            this.MaDoiBongTC.Name = "MaDoiBongTC";
            this.MaDoiBongTC.ReadOnly = true;
            // 
            // TenDoiBOngTC
            // 
            this.TenDoiBOngTC.DataPropertyName = "TenDoiBong";
            this.TenDoiBOngTC.FillWeight = 127.1574F;
            this.TenDoiBOngTC.HeaderText = "Tên đội bóng";
            this.TenDoiBOngTC.Name = "TenDoiBOngTC";
            this.TenDoiBOngTC.ReadOnly = true;
            // 
            // AddDB
            // 
            this.AddDB.FillWeight = 45.68528F;
            this.AddDB.HeaderText = "Add";
            this.AddDB.Name = "AddDB";
            this.AddDB.ReadOnly = true;
            // 
            // gpTiepNhanHOSo_DangKyThongTinDoiBong
            // 
            this.gpTiepNhanHOSo_DangKyThongTinDoiBong.Controls.Add(this.gbTiepNhanHoSo_DanhSachDoiBongDaDangKy);
            this.gpTiepNhanHOSo_DangKyThongTinDoiBong.Controls.Add(this.cbbTNHS_DKDB_DKDB_MaDoiBong);
            this.gpTiepNhanHOSo_DangKyThongTinDoiBong.Controls.Add(this.btTiepNhanHoSo_DangKyDoiBong_DangKyDoiBong);
            this.gpTiepNhanHOSo_DangKyThongTinDoiBong.Controls.Add(this.label39);
            this.gpTiepNhanHOSo_DangKyThongTinDoiBong.Controls.Add(this.label40);
            this.gpTiepNhanHOSo_DangKyThongTinDoiBong.Controls.Add(this.label41);
            this.gpTiepNhanHOSo_DangKyThongTinDoiBong.Controls.Add(this.tbTNHS_DKDB_DKDB_SoLuongCauThu);
            this.gpTiepNhanHOSo_DangKyThongTinDoiBong.Controls.Add(this.tbTNHS_DKDB_DKDB_SanNha);
            this.gpTiepNhanHOSo_DangKyThongTinDoiBong.Controls.Add(this.tbTNHS_DKDB_DKDB_TenDoiBong);
            this.gpTiepNhanHOSo_DangKyThongTinDoiBong.Controls.Add(this.label42);
            this.gpTiepNhanHOSo_DangKyThongTinDoiBong.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gpTiepNhanHOSo_DangKyThongTinDoiBong.ForeColor = System.Drawing.Color.DarkGreen;
            this.gpTiepNhanHOSo_DangKyThongTinDoiBong.Location = new System.Drawing.Point(0, 0);
            this.gpTiepNhanHOSo_DangKyThongTinDoiBong.Name = "gpTiepNhanHOSo_DangKyThongTinDoiBong";
            this.gpTiepNhanHOSo_DangKyThongTinDoiBong.Size = new System.Drawing.Size(588, 505);
            this.gpTiepNhanHOSo_DangKyThongTinDoiBong.TabIndex = 2;
            this.gpTiepNhanHOSo_DangKyThongTinDoiBong.TabStop = false;
            this.gpTiepNhanHOSo_DangKyThongTinDoiBong.Text = "Đăng ký thông tin đội bóng";
            // 
            // gbTiepNhanHoSo_DanhSachDoiBongDaDangKy
            // 
            this.gbTiepNhanHoSo_DanhSachDoiBongDaDangKy.Controls.Add(this.dgvTiepNhanHoSo_DanhSachDoiBOngDaDangKy);
            this.gbTiepNhanHoSo_DanhSachDoiBongDaDangKy.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbTiepNhanHoSo_DanhSachDoiBongDaDangKy.ForeColor = System.Drawing.Color.Maroon;
            this.gbTiepNhanHoSo_DanhSachDoiBongDaDangKy.Location = new System.Drawing.Point(3, 18);
            this.gbTiepNhanHoSo_DanhSachDoiBongDaDangKy.Name = "gbTiepNhanHoSo_DanhSachDoiBongDaDangKy";
            this.gbTiepNhanHoSo_DanhSachDoiBongDaDangKy.Size = new System.Drawing.Size(582, 484);
            this.gbTiepNhanHoSo_DanhSachDoiBongDaDangKy.TabIndex = 8;
            this.gbTiepNhanHoSo_DanhSachDoiBongDaDangKy.TabStop = false;
            this.gbTiepNhanHoSo_DanhSachDoiBongDaDangKy.Text = "Danh sách đội bóng đã đăng ký";
            // 
            // dgvTiepNhanHoSo_DanhSachDoiBOngDaDangKy
            // 
            this.dgvTiepNhanHoSo_DanhSachDoiBOngDaDangKy.AllowUserToAddRows = false;
            this.dgvTiepNhanHoSo_DanhSachDoiBOngDaDangKy.AllowUserToDeleteRows = false;
            this.dgvTiepNhanHoSo_DanhSachDoiBOngDaDangKy.BackgroundColor = System.Drawing.Color.Snow;
            this.dgvTiepNhanHoSo_DanhSachDoiBOngDaDangKy.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTiepNhanHoSo_DanhSachDoiBOngDaDangKy.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MaDB,
            this.dataGridViewTextBoxColumn1,
            this.SLCT,
            this.dataGridViewTextBoxColumn2,
            this.Update,
            this.dataGridViewButtonColumn1,
            this.XacNhan});
            this.dgvTiepNhanHoSo_DanhSachDoiBOngDaDangKy.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvTiepNhanHoSo_DanhSachDoiBOngDaDangKy.Location = new System.Drawing.Point(3, 18);
            this.dgvTiepNhanHoSo_DanhSachDoiBOngDaDangKy.Name = "dgvTiepNhanHoSo_DanhSachDoiBOngDaDangKy";
            this.dgvTiepNhanHoSo_DanhSachDoiBOngDaDangKy.ReadOnly = true;
            this.dgvTiepNhanHoSo_DanhSachDoiBOngDaDangKy.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvTiepNhanHoSo_DanhSachDoiBOngDaDangKy.Size = new System.Drawing.Size(576, 463);
            this.dgvTiepNhanHoSo_DanhSachDoiBOngDaDangKy.TabIndex = 7;
            this.dgvTiepNhanHoSo_DanhSachDoiBOngDaDangKy.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTNHS_DKDB_DanhSachDoiBong_CellContentClick);
            this.dgvTiepNhanHoSo_DanhSachDoiBOngDaDangKy.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgvTiepNhanHoSo_DanhSachDoiBOngDaDangKy_CellPainting);
            // 
            // MaDB
            // 
            this.MaDB.DataPropertyName = "MaDoiBong";
            this.MaDB.HeaderText = "Mã đội bóng";
            this.MaDB.Name = "MaDB";
            this.MaDB.ReadOnly = true;
            this.MaDB.Width = 95;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "TenDoiBong";
            this.dataGridViewTextBoxColumn1.FillWeight = 127.1574F;
            this.dataGridViewTextBoxColumn1.HeaderText = "Tên đội bóng";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // SLCT
            // 
            this.SLCT.DataPropertyName = "SoLuongCauThuDangKy";
            this.SLCT.HeaderText = "Số cầu thủ";
            this.SLCT.Name = "SLCT";
            this.SLCT.ReadOnly = true;
            this.SLCT.Width = 82;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "HoanTatDangKy";
            this.dataGridViewTextBoxColumn2.FillWeight = 127.1574F;
            this.dataGridViewTextBoxColumn2.HeaderText = "Trạng thái";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 80;
            // 
            // Update
            // 
            this.Update.HeaderText = "Cập nhật";
            this.Update.Name = "Update";
            this.Update.ReadOnly = true;
            this.Update.Width = 60;
            // 
            // dataGridViewButtonColumn1
            // 
            this.dataGridViewButtonColumn1.FillWeight = 45.68528F;
            this.dataGridViewButtonColumn1.HeaderText = "Xóa";
            this.dataGridViewButtonColumn1.Name = "dataGridViewButtonColumn1";
            this.dataGridViewButtonColumn1.ReadOnly = true;
            this.dataGridViewButtonColumn1.Width = 60;
            // 
            // XacNhan
            // 
            this.XacNhan.HeaderText = "Hoàn tất";
            this.XacNhan.Name = "XacNhan";
            this.XacNhan.ReadOnly = true;
            this.XacNhan.Width = 60;
            // 
            // cbbTNHS_DKDB_DKDB_MaDoiBong
            // 
            this.cbbTNHS_DKDB_DKDB_MaDoiBong.FormattingEnabled = true;
            this.cbbTNHS_DKDB_DKDB_MaDoiBong.Location = new System.Drawing.Point(43, 65);
            this.cbbTNHS_DKDB_DKDB_MaDoiBong.Name = "cbbTNHS_DKDB_DKDB_MaDoiBong";
            this.cbbTNHS_DKDB_DKDB_MaDoiBong.Size = new System.Drawing.Size(120, 24);
            this.cbbTNHS_DKDB_DKDB_MaDoiBong.TabIndex = 6;
            // 
            // btTiepNhanHoSo_DangKyDoiBong_DangKyDoiBong
            // 
            this.btTiepNhanHoSo_DangKyDoiBong_DangKyDoiBong.Location = new System.Drawing.Point(42, 298);
            this.btTiepNhanHoSo_DangKyDoiBong_DangKyDoiBong.Name = "btTiepNhanHoSo_DangKyDoiBong_DangKyDoiBong";
            this.btTiepNhanHoSo_DangKyDoiBong_DangKyDoiBong.Size = new System.Drawing.Size(75, 23);
            this.btTiepNhanHoSo_DangKyDoiBong_DangKyDoiBong.TabIndex = 2;
            this.btTiepNhanHoSo_DangKyDoiBong_DangKyDoiBong.Text = "Đăng ký";
            this.btTiepNhanHoSo_DangKyDoiBong_DangKyDoiBong.UseVisualStyleBackColor = true;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(43, 220);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(106, 16);
            this.label39.TabIndex = 0;
            this.label39.Text = "Số lượng cầu thủ";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(43, 160);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(57, 16);
            this.label40.TabIndex = 0;
            this.label40.Text = "Sân nhà";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(43, 100);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(88, 16);
            this.label41.TabIndex = 0;
            this.label41.Text = "Tên đội bóng";
            // 
            // tbTNHS_DKDB_DKDB_SoLuongCauThu
            // 
            this.tbTNHS_DKDB_DKDB_SoLuongCauThu.Location = new System.Drawing.Point(43, 245);
            this.tbTNHS_DKDB_DKDB_SoLuongCauThu.Name = "tbTNHS_DKDB_DKDB_SoLuongCauThu";
            this.tbTNHS_DKDB_DKDB_SoLuongCauThu.ReadOnly = true;
            this.tbTNHS_DKDB_DKDB_SoLuongCauThu.Size = new System.Drawing.Size(120, 22);
            this.tbTNHS_DKDB_DKDB_SoLuongCauThu.TabIndex = 3;
            // 
            // tbTNHS_DKDB_DKDB_SanNha
            // 
            this.tbTNHS_DKDB_DKDB_SanNha.Location = new System.Drawing.Point(43, 185);
            this.tbTNHS_DKDB_DKDB_SanNha.Name = "tbTNHS_DKDB_DKDB_SanNha";
            this.tbTNHS_DKDB_DKDB_SanNha.ReadOnly = true;
            this.tbTNHS_DKDB_DKDB_SanNha.Size = new System.Drawing.Size(120, 22);
            this.tbTNHS_DKDB_DKDB_SanNha.TabIndex = 3;
            // 
            // tbTNHS_DKDB_DKDB_TenDoiBong
            // 
            this.tbTNHS_DKDB_DKDB_TenDoiBong.Location = new System.Drawing.Point(43, 125);
            this.tbTNHS_DKDB_DKDB_TenDoiBong.Name = "tbTNHS_DKDB_DKDB_TenDoiBong";
            this.tbTNHS_DKDB_DKDB_TenDoiBong.ReadOnly = true;
            this.tbTNHS_DKDB_DKDB_TenDoiBong.Size = new System.Drawing.Size(120, 22);
            this.tbTNHS_DKDB_DKDB_TenDoiBong.TabIndex = 3;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(43, 40);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(83, 16);
            this.label42.TabIndex = 0;
            this.label42.Text = "Mã đội bóng";
            // 
            // tpTiepNhanHoSoDangKyThongTinCauThu
            // 
            this.tpTiepNhanHoSoDangKyThongTinCauThu.Controls.Add(this.splitContainer19);
            this.tpTiepNhanHoSoDangKyThongTinCauThu.Location = new System.Drawing.Point(4, 25);
            this.tpTiepNhanHoSoDangKyThongTinCauThu.Name = "tpTiepNhanHoSoDangKyThongTinCauThu";
            this.tpTiepNhanHoSoDangKyThongTinCauThu.Padding = new System.Windows.Forms.Padding(3);
            this.tpTiepNhanHoSoDangKyThongTinCauThu.Size = new System.Drawing.Size(948, 514);
            this.tpTiepNhanHoSoDangKyThongTinCauThu.TabIndex = 1;
            this.tpTiepNhanHoSoDangKyThongTinCauThu.Text = "Đăng ký thông tin cầu thủ";
            this.tpTiepNhanHoSoDangKyThongTinCauThu.UseVisualStyleBackColor = true;
            // 
            // splitContainer19
            // 
            this.splitContainer19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer19.Location = new System.Drawing.Point(3, 3);
            this.splitContainer19.Name = "splitContainer19";
            // 
            // splitContainer19.Panel1
            // 
            this.splitContainer19.Panel1.Controls.Add(this.gbTiepNhanHoSo_DangKyThongTinDoiBong_DanhSachCauThuCoTheDangKy);
            // 
            // splitContainer19.Panel2
            // 
            this.splitContainer19.Panel2.Controls.Add(this.gbTiepNhanHOSo_DangKYThongTinCauThu);
            this.splitContainer19.Size = new System.Drawing.Size(942, 508);
            this.splitContainer19.SplitterDistance = 400;
            this.splitContainer19.TabIndex = 0;
            // 
            // gbTiepNhanHoSo_DangKyThongTinDoiBong_DanhSachCauThuCoTheDangKy
            // 
            this.gbTiepNhanHoSo_DangKyThongTinDoiBong_DanhSachCauThuCoTheDangKy.Controls.Add(this.dgvChiTietTTCT);
            this.gbTiepNhanHoSo_DangKyThongTinDoiBong_DanhSachCauThuCoTheDangKy.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbTiepNhanHoSo_DangKyThongTinDoiBong_DanhSachCauThuCoTheDangKy.ForeColor = System.Drawing.Color.DarkGreen;
            this.gbTiepNhanHoSo_DangKyThongTinDoiBong_DanhSachCauThuCoTheDangKy.Location = new System.Drawing.Point(0, 0);
            this.gbTiepNhanHoSo_DangKyThongTinDoiBong_DanhSachCauThuCoTheDangKy.Name = "gbTiepNhanHoSo_DangKyThongTinDoiBong_DanhSachCauThuCoTheDangKy";
            this.gbTiepNhanHoSo_DangKyThongTinDoiBong_DanhSachCauThuCoTheDangKy.Size = new System.Drawing.Size(400, 508);
            this.gbTiepNhanHoSo_DangKyThongTinDoiBong_DanhSachCauThuCoTheDangKy.TabIndex = 0;
            this.gbTiepNhanHoSo_DangKyThongTinDoiBong_DanhSachCauThuCoTheDangKy.TabStop = false;
            this.gbTiepNhanHoSo_DangKyThongTinDoiBong_DanhSachCauThuCoTheDangKy.Text = "Danh sách cầu thủ có thể đăng ký";
            // 
            // dgvChiTietTTCT
            // 
            this.dgvChiTietTTCT.AllowUserToAddRows = false;
            this.dgvChiTietTTCT.AllowUserToDeleteRows = false;
            this.dgvChiTietTTCT.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvChiTietTTCT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvChiTietTTCT.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MaCTTC,
            this.TenCTTC,
            this.ThemCauThu});
            this.dgvChiTietTTCT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvChiTietTTCT.Location = new System.Drawing.Point(3, 18);
            this.dgvChiTietTTCT.Name = "dgvChiTietTTCT";
            this.dgvChiTietTTCT.ReadOnly = true;
            this.dgvChiTietTTCT.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvChiTietTTCT.Size = new System.Drawing.Size(394, 487);
            this.dgvChiTietTTCT.TabIndex = 0;
            this.dgvChiTietTTCT.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvChiTietTTCT_CellContentClick);
            this.dgvChiTietTTCT.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgvChiTietTTCT_CellPainting);
            // 
            // MaCTTC
            // 
            this.MaCTTC.DataPropertyName = "MaCauThu";
            this.MaCTTC.FillWeight = 127.1574F;
            this.MaCTTC.HeaderText = "Mã cầu thủ";
            this.MaCTTC.Name = "MaCTTC";
            this.MaCTTC.ReadOnly = true;
            // 
            // TenCTTC
            // 
            this.TenCTTC.DataPropertyName = "HoTenCauThu";
            this.TenCTTC.FillWeight = 127.1574F;
            this.TenCTTC.HeaderText = "Tên cầu thủ";
            this.TenCTTC.Name = "TenCTTC";
            this.TenCTTC.ReadOnly = true;
            // 
            // ThemCauThu
            // 
            this.ThemCauThu.FillWeight = 45.68528F;
            this.ThemCauThu.HeaderText = "Add";
            this.ThemCauThu.Name = "ThemCauThu";
            this.ThemCauThu.ReadOnly = true;
            // 
            // gbTiepNhanHOSo_DangKYThongTinCauThu
            // 
            this.gbTiepNhanHOSo_DangKYThongTinCauThu.Controls.Add(this.groupBox18);
            this.gbTiepNhanHOSo_DangKYThongTinCauThu.Controls.Add(this.tbTNHS_DKDB_DKCT_NgaySinh);
            this.gbTiepNhanHOSo_DangKYThongTinCauThu.Controls.Add(this.cbbTNHS_DKDB_DKCT_MaCauThu);
            this.gbTiepNhanHOSo_DangKYThongTinCauThu.Controls.Add(this.tbTNHS_DKDB_DKCT_LoaiCauThu);
            this.gbTiepNhanHOSo_DangKYThongTinCauThu.Controls.Add(this.tbTNHS_DKDB_DKCT_TenCauThu);
            this.gbTiepNhanHOSo_DangKYThongTinCauThu.Controls.Add(this.btTiepNhanHoSo_DangKyDoiBong_XacNhanHoanTatDangKyDoiBong);
            this.gbTiepNhanHOSo_DangKYThongTinCauThu.Controls.Add(this.btTiepNhanHoSo_DangKyDoiBong_DangKyCauThu);
            this.gbTiepNhanHOSo_DangKYThongTinCauThu.Controls.Add(this.label43);
            this.gbTiepNhanHOSo_DangKYThongTinCauThu.Controls.Add(this.label44);
            this.gbTiepNhanHOSo_DangKYThongTinCauThu.Controls.Add(this.label45);
            this.gbTiepNhanHOSo_DangKYThongTinCauThu.Controls.Add(this.label46);
            this.gbTiepNhanHOSo_DangKYThongTinCauThu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbTiepNhanHOSo_DangKYThongTinCauThu.ForeColor = System.Drawing.Color.DarkGreen;
            this.gbTiepNhanHOSo_DangKYThongTinCauThu.Location = new System.Drawing.Point(0, 0);
            this.gbTiepNhanHOSo_DangKYThongTinCauThu.Name = "gbTiepNhanHOSo_DangKYThongTinCauThu";
            this.gbTiepNhanHOSo_DangKYThongTinCauThu.Size = new System.Drawing.Size(538, 508);
            this.gbTiepNhanHOSo_DangKYThongTinCauThu.TabIndex = 2;
            this.gbTiepNhanHOSo_DangKYThongTinCauThu.TabStop = false;
            this.gbTiepNhanHOSo_DangKYThongTinCauThu.Text = "Đăng ký thông tin cầu thủ";
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.dgvTNHS_DKDB_DKCT_DanhSachCauThu);
            this.groupBox18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox18.Location = new System.Drawing.Point(3, 18);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(532, 487);
            this.groupBox18.TabIndex = 8;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Danh sách cầu thủ đã đăng ký";
            // 
            // dgvTNHS_DKDB_DKCT_DanhSachCauThu
            // 
            this.dgvTNHS_DKDB_DKCT_DanhSachCauThu.AllowUserToAddRows = false;
            this.dgvTNHS_DKDB_DKCT_DanhSachCauThu.AllowUserToDeleteRows = false;
            this.dgvTNHS_DKDB_DKCT_DanhSachCauThu.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvTNHS_DKDB_DKCT_DanhSachCauThu.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTNHS_DKDB_DKCT_DanhSachCauThu.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MaCT,
            this.dataGridViewTextBoxColumn3,
            this.XoaCT});
            this.dgvTNHS_DKDB_DKCT_DanhSachCauThu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvTNHS_DKDB_DKCT_DanhSachCauThu.Location = new System.Drawing.Point(3, 18);
            this.dgvTNHS_DKDB_DKCT_DanhSachCauThu.Name = "dgvTNHS_DKDB_DKCT_DanhSachCauThu";
            this.dgvTNHS_DKDB_DKCT_DanhSachCauThu.ReadOnly = true;
            this.dgvTNHS_DKDB_DKCT_DanhSachCauThu.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvTNHS_DKDB_DKCT_DanhSachCauThu.Size = new System.Drawing.Size(526, 466);
            this.dgvTNHS_DKDB_DKCT_DanhSachCauThu.TabIndex = 0;
            this.dgvTNHS_DKDB_DKCT_DanhSachCauThu.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTNHS_DKDB_DKCT_DanhSachCauThu_CellContentClick);
            this.dgvTNHS_DKDB_DKCT_DanhSachCauThu.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgvTNHS_DKDB_DKCT_DanhSachCauThu_CellPainting);
            // 
            // MaCT
            // 
            this.MaCT.DataPropertyName = "MaCauThu";
            this.MaCT.FillWeight = 83.05085F;
            this.MaCT.HeaderText = "Mã cầu thủ";
            this.MaCT.Name = "MaCT";
            this.MaCT.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "TenCauThu";
            this.dataGridViewTextBoxColumn3.FillWeight = 140.8071F;
            this.dataGridViewTextBoxColumn3.HeaderText = "Tên cầu thủ";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // XoaCT
            // 
            this.XoaCT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.XoaCT.FillWeight = 76.14214F;
            this.XoaCT.HeaderText = "Xóa";
            this.XoaCT.Name = "XoaCT";
            this.XoaCT.ReadOnly = true;
            this.XoaCT.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.XoaCT.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.XoaCT.Width = 50;
            // 
            // tbTNHS_DKDB_DKCT_NgaySinh
            // 
            this.tbTNHS_DKDB_DKCT_NgaySinh.Location = new System.Drawing.Point(124, 205);
            this.tbTNHS_DKDB_DKCT_NgaySinh.Name = "tbTNHS_DKDB_DKCT_NgaySinh";
            this.tbTNHS_DKDB_DKCT_NgaySinh.ReadOnly = true;
            this.tbTNHS_DKDB_DKCT_NgaySinh.Size = new System.Drawing.Size(121, 22);
            this.tbTNHS_DKDB_DKCT_NgaySinh.TabIndex = 7;
            // 
            // cbbTNHS_DKDB_DKCT_MaCauThu
            // 
            this.cbbTNHS_DKDB_DKCT_MaCauThu.FormattingEnabled = true;
            this.cbbTNHS_DKDB_DKCT_MaCauThu.Location = new System.Drawing.Point(124, 48);
            this.cbbTNHS_DKDB_DKCT_MaCauThu.Name = "cbbTNHS_DKDB_DKCT_MaCauThu";
            this.cbbTNHS_DKDB_DKCT_MaCauThu.Size = new System.Drawing.Size(121, 24);
            this.cbbTNHS_DKDB_DKCT_MaCauThu.TabIndex = 6;
            this.cbbTNHS_DKDB_DKCT_MaCauThu.SelectedIndexChanged += new System.EventHandler(this.cbbTNHS_DKDB_DKCT_MaCauThu_SelectedIndexChanged);
            // 
            // tbTNHS_DKDB_DKCT_LoaiCauThu
            // 
            this.tbTNHS_DKDB_DKCT_LoaiCauThu.Location = new System.Drawing.Point(124, 137);
            this.tbTNHS_DKDB_DKCT_LoaiCauThu.Name = "tbTNHS_DKDB_DKCT_LoaiCauThu";
            this.tbTNHS_DKDB_DKCT_LoaiCauThu.ReadOnly = true;
            this.tbTNHS_DKDB_DKCT_LoaiCauThu.Size = new System.Drawing.Size(121, 22);
            this.tbTNHS_DKDB_DKCT_LoaiCauThu.TabIndex = 3;
            // 
            // tbTNHS_DKDB_DKCT_TenCauThu
            // 
            this.tbTNHS_DKDB_DKCT_TenCauThu.Location = new System.Drawing.Point(124, 96);
            this.tbTNHS_DKDB_DKCT_TenCauThu.Name = "tbTNHS_DKDB_DKCT_TenCauThu";
            this.tbTNHS_DKDB_DKCT_TenCauThu.ReadOnly = true;
            this.tbTNHS_DKDB_DKCT_TenCauThu.Size = new System.Drawing.Size(121, 22);
            this.tbTNHS_DKDB_DKCT_TenCauThu.TabIndex = 3;
            // 
            // btTiepNhanHoSo_DangKyDoiBong_XacNhanHoanTatDangKyDoiBong
            // 
            this.btTiepNhanHoSo_DangKyDoiBong_XacNhanHoanTatDangKyDoiBong.Location = new System.Drawing.Point(33, 290);
            this.btTiepNhanHoSo_DangKyDoiBong_XacNhanHoanTatDangKyDoiBong.Name = "btTiepNhanHoSo_DangKyDoiBong_XacNhanHoanTatDangKyDoiBong";
            this.btTiepNhanHoSo_DangKyDoiBong_XacNhanHoanTatDangKyDoiBong.Size = new System.Drawing.Size(75, 23);
            this.btTiepNhanHoSo_DangKyDoiBong_XacNhanHoanTatDangKyDoiBong.TabIndex = 2;
            this.btTiepNhanHoSo_DangKyDoiBong_XacNhanHoanTatDangKyDoiBong.Text = "Hoàn tất";
            this.btTiepNhanHoSo_DangKyDoiBong_XacNhanHoanTatDangKyDoiBong.UseVisualStyleBackColor = true;
            // 
            // btTiepNhanHoSo_DangKyDoiBong_DangKyCauThu
            // 
            this.btTiepNhanHoSo_DangKyDoiBong_DangKyCauThu.Location = new System.Drawing.Point(32, 241);
            this.btTiepNhanHoSo_DangKyDoiBong_DangKyCauThu.Name = "btTiepNhanHoSo_DangKyDoiBong_DangKyCauThu";
            this.btTiepNhanHoSo_DangKyDoiBong_DangKyCauThu.Size = new System.Drawing.Size(75, 23);
            this.btTiepNhanHoSo_DangKyDoiBong_DangKyCauThu.TabIndex = 2;
            this.btTiepNhanHoSo_DangKyDoiBong_DangKyCauThu.Text = "Đăng ký";
            this.btTiepNhanHoSo_DangKyDoiBong_DangKyCauThu.UseVisualStyleBackColor = true;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(35, 205);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(68, 16);
            this.label43.TabIndex = 1;
            this.label43.Text = "Ngày sinh";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(35, 140);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(79, 16);
            this.label44.TabIndex = 1;
            this.label44.Text = "Loại cầu thủ";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(35, 96);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(77, 16);
            this.label45.TabIndex = 1;
            this.label45.Text = "Tên cầu thủ";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(35, 51);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(72, 16);
            this.label46.TabIndex = 1;
            this.label46.Text = "Mã cầu thủ";
            // 
            // tpTaoLichThiDau
            // 
            this.tpTaoLichThiDau.BackColor = System.Drawing.Color.Snow;
            this.tpTaoLichThiDau.Controls.Add(this.tcTaoLichThiDau);
            this.tpTaoLichThiDau.Location = new System.Drawing.Point(4, 25);
            this.tpTaoLichThiDau.Name = "tpTaoLichThiDau";
            this.tpTaoLichThiDau.Padding = new System.Windows.Forms.Padding(3);
            this.tpTaoLichThiDau.Size = new System.Drawing.Size(976, 581);
            this.tpTaoLichThiDau.TabIndex = 2;
            this.tpTaoLichThiDau.Text = "Tạo lịch thi đấu";
            // 
            // tcTaoLichThiDau
            // 
            this.tcTaoLichThiDau.Controls.Add(this.tpTaoLichThiDau_QuyDinh);
            this.tcTaoLichThiDau.Controls.Add(this.tpTaoLichThiDau_TaoLichThiDau);
            this.tcTaoLichThiDau.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcTaoLichThiDau.Location = new System.Drawing.Point(3, 3);
            this.tcTaoLichThiDau.Name = "tcTaoLichThiDau";
            this.tcTaoLichThiDau.SelectedIndex = 0;
            this.tcTaoLichThiDau.Size = new System.Drawing.Size(970, 575);
            this.tcTaoLichThiDau.TabIndex = 0;
            // 
            // tpTaoLichThiDau_QuyDinh
            // 
            this.tpTaoLichThiDau_QuyDinh.Controls.Add(this.splitContainer11);
            this.tpTaoLichThiDau_QuyDinh.Location = new System.Drawing.Point(4, 25);
            this.tpTaoLichThiDau_QuyDinh.Name = "tpTaoLichThiDau_QuyDinh";
            this.tpTaoLichThiDau_QuyDinh.Padding = new System.Windows.Forms.Padding(3);
            this.tpTaoLichThiDau_QuyDinh.Size = new System.Drawing.Size(962, 546);
            this.tpTaoLichThiDau_QuyDinh.TabIndex = 0;
            this.tpTaoLichThiDau_QuyDinh.Text = "Quy định";
            this.tpTaoLichThiDau_QuyDinh.UseVisualStyleBackColor = true;
            // 
            // splitContainer11
            // 
            this.splitContainer11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer11.Location = new System.Drawing.Point(3, 3);
            this.splitContainer11.Name = "splitContainer11";
            // 
            // splitContainer11.Panel1
            // 
            this.splitContainer11.Panel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.splitContainer11.Panel1.Controls.Add(this.splitContainer23);
            // 
            // splitContainer11.Panel2
            // 
            this.splitContainer11.Panel2.Controls.Add(this.groupBox22);
            this.splitContainer11.Size = new System.Drawing.Size(956, 540);
            this.splitContainer11.SplitterDistance = 275;
            this.splitContainer11.TabIndex = 0;
            // 
            // splitContainer23
            // 
            this.splitContainer23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer23.Location = new System.Drawing.Point(0, 0);
            this.splitContainer23.Name = "splitContainer23";
            this.splitContainer23.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer23.Panel1
            // 
            this.splitContainer23.Panel1.BackColor = System.Drawing.Color.Azure;
            this.splitContainer23.Panel1.Controls.Add(this.groupBox10);
            // 
            // splitContainer23.Panel2
            // 
            this.splitContainer23.Panel2.Controls.Add(this.groupBox11);
            this.splitContainer23.Size = new System.Drawing.Size(275, 540);
            this.splitContainer23.SplitterDistance = 100;
            this.splitContainer23.TabIndex = 0;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.label37);
            this.groupBox10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox10.Location = new System.Drawing.Point(0, 0);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(275, 100);
            this.groupBox10.TabIndex = 3;
            this.groupBox10.TabStop = false;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Teal;
            this.label37.Location = new System.Drawing.Point(56, 28);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(151, 40);
            this.label37.TabIndex = 4;
            this.label37.Text = "QUY ĐỊNH\r\nTẠO LỊCH THI ĐẤU";
            this.label37.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.textBox5);
            this.groupBox11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox11.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox11.Location = new System.Drawing.Point(0, 0);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(275, 436);
            this.groupBox11.TabIndex = 1;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Bảng quy định tạo lịch thi đấu";
            // 
            // textBox5
            // 
            this.textBox5.BackColor = System.Drawing.Color.Azure;
            this.textBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox5.ForeColor = System.Drawing.Color.Teal;
            this.textBox5.Location = new System.Drawing.Point(3, 18);
            this.textBox5.Multiline = true;
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(269, 415);
            this.textBox5.TabIndex = 0;
            this.textBox5.Text = resources.GetString("textBox5.Text");
            // 
            // groupBox22
            // 
            this.groupBox22.Controls.Add(this.dgvTaoLichThiDau_DanhSachMuaGiai);
            this.groupBox22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox22.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox22.Location = new System.Drawing.Point(0, 0);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Size = new System.Drawing.Size(677, 540);
            this.groupBox22.TabIndex = 0;
            this.groupBox22.TabStop = false;
            this.groupBox22.Text = "Danh sách mùa giải ";
            // 
            // dgvTaoLichThiDau_DanhSachMuaGiai
            // 
            this.dgvTaoLichThiDau_DanhSachMuaGiai.AllowUserToAddRows = false;
            this.dgvTaoLichThiDau_DanhSachMuaGiai.AllowUserToDeleteRows = false;
            this.dgvTaoLichThiDau_DanhSachMuaGiai.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvTaoLichThiDau_DanhSachMuaGiai.BackgroundColor = System.Drawing.Color.Snow;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Salmon;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Teal;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvTaoLichThiDau_DanhSachMuaGiai.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvTaoLichThiDau_DanhSachMuaGiai.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTaoLichThiDau_DanhSachMuaGiai.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewButtonColumn2});
            this.dgvTaoLichThiDau_DanhSachMuaGiai.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvTaoLichThiDau_DanhSachMuaGiai.Location = new System.Drawing.Point(3, 18);
            this.dgvTaoLichThiDau_DanhSachMuaGiai.Name = "dgvTaoLichThiDau_DanhSachMuaGiai";
            this.dgvTaoLichThiDau_DanhSachMuaGiai.ReadOnly = true;
            this.dgvTaoLichThiDau_DanhSachMuaGiai.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvTaoLichThiDau_DanhSachMuaGiai.Size = new System.Drawing.Size(671, 519);
            this.dgvTaoLichThiDau_DanhSachMuaGiai.TabIndex = 1;
            this.dgvTaoLichThiDau_DanhSachMuaGiai.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTaoLichThiDau_DanhSachMuaGiai_CellContentClick);
            this.dgvTaoLichThiDau_DanhSachMuaGiai.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgvTaoLichThiDau_DanhSachMuaGiai_CellPainting);
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "MaMuaGiai";
            this.dataGridViewTextBoxColumn7.FillWeight = 32.447F;
            this.dataGridViewTextBoxColumn7.HeaderText = "Mã giải đấu";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "TenMuaGiai";
            this.dataGridViewTextBoxColumn8.FillWeight = 61.35218F;
            this.dataGridViewTextBoxColumn8.HeaderText = "Tên giải đấu";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            // 
            // dataGridViewButtonColumn2
            // 
            this.dataGridViewButtonColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewButtonColumn2.FillWeight = 96.70018F;
            this.dataGridViewButtonColumn2.HeaderText = "Cập nhật";
            this.dataGridViewButtonColumn2.Name = "dataGridViewButtonColumn2";
            this.dataGridViewButtonColumn2.ReadOnly = true;
            // 
            // tpTaoLichThiDau_TaoLichThiDau
            // 
            this.tpTaoLichThiDau_TaoLichThiDau.Controls.Add(this.splitContainer2);
            this.tpTaoLichThiDau_TaoLichThiDau.Location = new System.Drawing.Point(4, 25);
            this.tpTaoLichThiDau_TaoLichThiDau.Name = "tpTaoLichThiDau_TaoLichThiDau";
            this.tpTaoLichThiDau_TaoLichThiDau.Padding = new System.Windows.Forms.Padding(3);
            this.tpTaoLichThiDau_TaoLichThiDau.Size = new System.Drawing.Size(962, 546);
            this.tpTaoLichThiDau_TaoLichThiDau.TabIndex = 1;
            this.tpTaoLichThiDau_TaoLichThiDau.Text = "Tạo lịch thi đấu";
            this.tpTaoLichThiDau_TaoLichThiDau.UseVisualStyleBackColor = true;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(3, 3);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.gbTaoLichThiDau_TaoLichThiDau_QuyDinh);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.gbTaoLichThiDau_TaoLichThiDau_TenMuaGiai);
            this.splitContainer2.Size = new System.Drawing.Size(956, 540);
            this.splitContainer2.SplitterDistance = 300;
            this.splitContainer2.TabIndex = 0;
            // 
            // gbTaoLichThiDau_TaoLichThiDau_QuyDinh
            // 
            this.gbTaoLichThiDau_TaoLichThiDau_QuyDinh.BackColor = System.Drawing.Color.Azure;
            this.gbTaoLichThiDau_TaoLichThiDau_QuyDinh.Controls.Add(this.btTaoLichThiDau_TaoLichThiDau);
            this.gbTaoLichThiDau_TaoLichThiDau_QuyDinh.Controls.Add(this.btTaoLichThiDau_XacNhanLichThiDau);
            this.gbTaoLichThiDau_TaoLichThiDau_QuyDinh.Controls.Add(this.nudstep);
            this.gbTaoLichThiDau_TaoLichThiDau_QuyDinh.Controls.Add(this.label7);
            this.gbTaoLichThiDau_TaoLichThiDau_QuyDinh.Controls.Add(this.label8);
            this.gbTaoLichThiDau_TaoLichThiDau_QuyDinh.Controls.Add(this.nudfirstmatch);
            this.gbTaoLichThiDau_TaoLichThiDau_QuyDinh.Controls.Add(this.label6);
            this.gbTaoLichThiDau_TaoLichThiDau_QuyDinh.Controls.Add(this.nudlastmatch);
            this.gbTaoLichThiDau_TaoLichThiDau_QuyDinh.Controls.Add(this.nudThoiGianNghiGiuaHaiLuot);
            this.gbTaoLichThiDau_TaoLichThiDau_QuyDinh.Controls.Add(this.label5);
            this.gbTaoLichThiDau_TaoLichThiDau_QuyDinh.Controls.Add(this.label4);
            this.gbTaoLichThiDau_TaoLichThiDau_QuyDinh.Controls.Add(this.nudKhoangCachVongDau);
            this.gbTaoLichThiDau_TaoLichThiDau_QuyDinh.Controls.Add(this.label2);
            this.gbTaoLichThiDau_TaoLichThiDau_QuyDinh.Controls.Add(this.dtpNgayBatDau);
            this.gbTaoLichThiDau_TaoLichThiDau_QuyDinh.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbTaoLichThiDau_TaoLichThiDau_QuyDinh.ForeColor = System.Drawing.Color.DarkGreen;
            this.gbTaoLichThiDau_TaoLichThiDau_QuyDinh.Location = new System.Drawing.Point(0, 0);
            this.gbTaoLichThiDau_TaoLichThiDau_QuyDinh.Name = "gbTaoLichThiDau_TaoLichThiDau_QuyDinh";
            this.gbTaoLichThiDau_TaoLichThiDau_QuyDinh.Size = new System.Drawing.Size(300, 540);
            this.gbTaoLichThiDau_TaoLichThiDau_QuyDinh.TabIndex = 2;
            this.gbTaoLichThiDau_TaoLichThiDau_QuyDinh.TabStop = false;
            this.gbTaoLichThiDau_TaoLichThiDau_QuyDinh.Text = "Quy định";
            // 
            // btTaoLichThiDau_TaoLichThiDau
            // 
            this.btTaoLichThiDau_TaoLichThiDau.BackColor = System.Drawing.Color.Azure;
            this.btTaoLichThiDau_TaoLichThiDau.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btTaoLichThiDau_TaoLichThiDau.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btTaoLichThiDau_TaoLichThiDau.ForeColor = System.Drawing.Color.Teal;
            this.btTaoLichThiDau_TaoLichThiDau.Location = new System.Drawing.Point(23, 324);
            this.btTaoLichThiDau_TaoLichThiDau.Name = "btTaoLichThiDau_TaoLichThiDau";
            this.btTaoLichThiDau_TaoLichThiDau.Size = new System.Drawing.Size(249, 40);
            this.btTaoLichThiDau_TaoLichThiDau.TabIndex = 2;
            this.btTaoLichThiDau_TaoLichThiDau.Text = "Phát sinh ngẫu nhiên lịch thi đấu";
            this.btTaoLichThiDau_TaoLichThiDau.UseVisualStyleBackColor = false;
            this.btTaoLichThiDau_TaoLichThiDau.Click += new System.EventHandler(this.btTaoLichThiDau_TaoLichThiDau_Click);
            // 
            // btTaoLichThiDau_XacNhanLichThiDau
            // 
            this.btTaoLichThiDau_XacNhanLichThiDau.BackColor = System.Drawing.Color.Azure;
            this.btTaoLichThiDau_XacNhanLichThiDau.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btTaoLichThiDau_XacNhanLichThiDau.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btTaoLichThiDau_XacNhanLichThiDau.ForeColor = System.Drawing.Color.Teal;
            this.btTaoLichThiDau_XacNhanLichThiDau.Location = new System.Drawing.Point(23, 382);
            this.btTaoLichThiDau_XacNhanLichThiDau.Name = "btTaoLichThiDau_XacNhanLichThiDau";
            this.btTaoLichThiDau_XacNhanLichThiDau.Size = new System.Drawing.Size(249, 35);
            this.btTaoLichThiDau_XacNhanLichThiDau.TabIndex = 3;
            this.btTaoLichThiDau_XacNhanLichThiDau.Text = "Xác nhận lịch thi đấu";
            this.btTaoLichThiDau_XacNhanLichThiDau.UseVisualStyleBackColor = false;
            this.btTaoLichThiDau_XacNhanLichThiDau.Click += new System.EventHandler(this.btTaoLichThiDau_XacNhanLichThiDau_Click);
            // 
            // nudstep
            // 
            this.nudstep.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudstep.Increment = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.nudstep.Location = new System.Drawing.Point(228, 258);
            this.nudstep.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.nudstep.Name = "nudstep";
            this.nudstep.Size = new System.Drawing.Size(44, 21);
            this.nudstep.TabIndex = 44;
            this.nudstep.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(20, 220);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(150, 15);
            this.label7.TabIndex = 41;
            this.label7.Text = "Giờ bắt đầu trận cuối cùng";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(20, 260);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(81, 15);
            this.label8.TabIndex = 43;
            this.label8.Text = "Thời gian chờ";
            // 
            // nudfirstmatch
            // 
            this.nudfirstmatch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudfirstmatch.Location = new System.Drawing.Point(228, 178);
            this.nudfirstmatch.Maximum = new decimal(new int[] {
            23,
            0,
            0,
            0});
            this.nudfirstmatch.Name = "nudfirstmatch";
            this.nudfirstmatch.Size = new System.Drawing.Size(44, 21);
            this.nudfirstmatch.TabIndex = 40;
            this.nudfirstmatch.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(20, 180);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(141, 15);
            this.label6.TabIndex = 42;
            this.label6.Text = "Giờ bắt đầu trận thứ nhất";
            // 
            // nudlastmatch
            // 
            this.nudlastmatch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudlastmatch.Location = new System.Drawing.Point(228, 218);
            this.nudlastmatch.Maximum = new decimal(new int[] {
            23,
            0,
            0,
            0});
            this.nudlastmatch.Name = "nudlastmatch";
            this.nudlastmatch.Size = new System.Drawing.Size(44, 21);
            this.nudlastmatch.TabIndex = 39;
            this.nudlastmatch.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // nudThoiGianNghiGiuaHaiLuot
            // 
            this.nudThoiGianNghiGiuaHaiLuot.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudThoiGianNghiGiuaHaiLuot.Location = new System.Drawing.Point(228, 140);
            this.nudThoiGianNghiGiuaHaiLuot.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudThoiGianNghiGiuaHaiLuot.Name = "nudThoiGianNghiGiuaHaiLuot";
            this.nudThoiGianNghiGiuaHaiLuot.Size = new System.Drawing.Size(44, 20);
            this.nudThoiGianNghiGiuaHaiLuot.TabIndex = 36;
            this.nudThoiGianNghiGiuaHaiLuot.Value = new decimal(new int[] {
            14,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(20, 140);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(195, 15);
            this.label5.TabIndex = 35;
            this.label5.Text = "Thời gian nghỉ giữa hai lượt thi đấu";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(20, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(179, 15);
            this.label4.TabIndex = 33;
            this.label4.Text = "Khoảng cách giữa hai vòng đấu";
            // 
            // nudKhoangCachVongDau
            // 
            this.nudKhoangCachVongDau.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudKhoangCachVongDau.Location = new System.Drawing.Point(228, 100);
            this.nudKhoangCachVongDau.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudKhoangCachVongDau.Name = "nudKhoangCachVongDau";
            this.nudKhoangCachVongDau.Size = new System.Drawing.Size(44, 20);
            this.nudKhoangCachVongDau.TabIndex = 34;
            this.nudKhoangCachVongDau.Value = new decimal(new int[] {
            7,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(20, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(130, 15);
            this.label2.TabIndex = 29;
            this.label2.Text = "Ngày bắt đầu mùa giải";
            // 
            // dtpNgayBatDau
            // 
            this.dtpNgayBatDau.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayBatDau.Location = new System.Drawing.Point(23, 58);
            this.dtpNgayBatDau.Name = "dtpNgayBatDau";
            this.dtpNgayBatDau.Size = new System.Drawing.Size(249, 21);
            this.dtpNgayBatDau.TabIndex = 30;
            // 
            // gbTaoLichThiDau_TaoLichThiDau_TenMuaGiai
            // 
            this.gbTaoLichThiDau_TaoLichThiDau_TenMuaGiai.Controls.Add(this.dgvListXepHang);
            this.gbTaoLichThiDau_TaoLichThiDau_TenMuaGiai.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbTaoLichThiDau_TaoLichThiDau_TenMuaGiai.ForeColor = System.Drawing.Color.DarkGreen;
            this.gbTaoLichThiDau_TaoLichThiDau_TenMuaGiai.Location = new System.Drawing.Point(0, 0);
            this.gbTaoLichThiDau_TaoLichThiDau_TenMuaGiai.Name = "gbTaoLichThiDau_TaoLichThiDau_TenMuaGiai";
            this.gbTaoLichThiDau_TaoLichThiDau_TenMuaGiai.Size = new System.Drawing.Size(652, 540);
            this.gbTaoLichThiDau_TaoLichThiDau_TenMuaGiai.TabIndex = 35;
            this.gbTaoLichThiDau_TaoLichThiDau_TenMuaGiai.TabStop = false;
            this.gbTaoLichThiDau_TaoLichThiDau_TenMuaGiai.Text = "Tên mùa giải";
            // 
            // dgvListXepHang
            // 
            this.dgvListXepHang.AllowUserToAddRows = false;
            this.dgvListXepHang.AllowUserToDeleteRows = false;
            this.dgvListXepHang.AllowUserToOrderColumns = true;
            this.dgvListXepHang.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvListXepHang.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvListXepHang.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MaVongDau,
            this.MaTranDau,
            this.NgayThiDau,
            this.GioThiDau,
            this.ChuNha,
            this.Khach});
            this.dgvListXepHang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvListXepHang.Location = new System.Drawing.Point(3, 18);
            this.dgvListXepHang.Name = "dgvListXepHang";
            this.dgvListXepHang.ReadOnly = true;
            this.dgvListXepHang.Size = new System.Drawing.Size(646, 519);
            this.dgvListXepHang.TabIndex = 34;
            // 
            // MaVongDau
            // 
            this.MaVongDau.DataPropertyName = "MaVongDau";
            this.MaVongDau.HeaderText = "Vòng đấu";
            this.MaVongDau.Name = "MaVongDau";
            this.MaVongDau.ReadOnly = true;
            // 
            // MaTranDau
            // 
            this.MaTranDau.DataPropertyName = "MaTranDau";
            this.MaTranDau.HeaderText = "Trận đấu";
            this.MaTranDau.Name = "MaTranDau";
            this.MaTranDau.ReadOnly = true;
            // 
            // NgayThiDau
            // 
            this.NgayThiDau.DataPropertyName = "NgayThiDau";
            this.NgayThiDau.HeaderText = "Ngày";
            this.NgayThiDau.Name = "NgayThiDau";
            this.NgayThiDau.ReadOnly = true;
            // 
            // GioThiDau
            // 
            this.GioThiDau.DataPropertyName = "GioThiDau";
            this.GioThiDau.HeaderText = "Giờ";
            this.GioThiDau.Name = "GioThiDau";
            this.GioThiDau.ReadOnly = true;
            // 
            // ChuNha
            // 
            this.ChuNha.DataPropertyName = "TenChuNha";
            this.ChuNha.HeaderText = "Chủ nhà";
            this.ChuNha.Name = "ChuNha";
            this.ChuNha.ReadOnly = true;
            // 
            // Khach
            // 
            this.Khach.DataPropertyName = "TenKhach";
            this.Khach.HeaderText = "Khách";
            this.Khach.Name = "Khach";
            this.Khach.ReadOnly = true;
            // 
            // tpCapNhatKetQua
            // 
            this.tpCapNhatKetQua.BackColor = System.Drawing.Color.Snow;
            this.tpCapNhatKetQua.Controls.Add(this.tcCapNhatKetQua);
            this.tpCapNhatKetQua.Location = new System.Drawing.Point(4, 25);
            this.tpCapNhatKetQua.Name = "tpCapNhatKetQua";
            this.tpCapNhatKetQua.Padding = new System.Windows.Forms.Padding(3);
            this.tpCapNhatKetQua.Size = new System.Drawing.Size(976, 581);
            this.tpCapNhatKetQua.TabIndex = 3;
            this.tpCapNhatKetQua.Text = "Cập nhật kết quả trận đấu";
            // 
            // tcCapNhatKetQua
            // 
            this.tcCapNhatKetQua.Controls.Add(this.tpCapNhatKetQua_QuyDinh);
            this.tcCapNhatKetQua.Controls.Add(this.tpCapNhatKetQua_CapNhatKetQuaTranDau);
            this.tcCapNhatKetQua.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcCapNhatKetQua.Location = new System.Drawing.Point(3, 3);
            this.tcCapNhatKetQua.Name = "tcCapNhatKetQua";
            this.tcCapNhatKetQua.SelectedIndex = 0;
            this.tcCapNhatKetQua.Size = new System.Drawing.Size(970, 575);
            this.tcCapNhatKetQua.TabIndex = 0;
            // 
            // tpCapNhatKetQua_QuyDinh
            // 
            this.tpCapNhatKetQua_QuyDinh.Controls.Add(this.splitContainer17);
            this.tpCapNhatKetQua_QuyDinh.Location = new System.Drawing.Point(4, 25);
            this.tpCapNhatKetQua_QuyDinh.Name = "tpCapNhatKetQua_QuyDinh";
            this.tpCapNhatKetQua_QuyDinh.Padding = new System.Windows.Forms.Padding(3);
            this.tpCapNhatKetQua_QuyDinh.Size = new System.Drawing.Size(962, 546);
            this.tpCapNhatKetQua_QuyDinh.TabIndex = 0;
            this.tpCapNhatKetQua_QuyDinh.Text = "Quy định";
            this.tpCapNhatKetQua_QuyDinh.UseVisualStyleBackColor = true;
            // 
            // splitContainer17
            // 
            this.splitContainer17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer17.Location = new System.Drawing.Point(3, 3);
            this.splitContainer17.Name = "splitContainer17";
            // 
            // splitContainer17.Panel1
            // 
            this.splitContainer17.Panel1.BackColor = System.Drawing.Color.Azure;
            this.splitContainer17.Panel1.Controls.Add(this.groupBox16);
            this.splitContainer17.Panel1.Controls.Add(this.groupBox13);
            // 
            // splitContainer17.Panel2
            // 
            this.splitContainer17.Panel2.Controls.Add(this.groupBox12);
            this.splitContainer17.Size = new System.Drawing.Size(956, 540);
            this.splitContainer17.SplitterDistance = 275;
            this.splitContainer17.TabIndex = 0;
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.textBox4);
            this.groupBox16.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox16.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox16.Location = new System.Drawing.Point(0, 106);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(275, 434);
            this.groupBox16.TabIndex = 6;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Danh sách quy định";
            // 
            // textBox4
            // 
            this.textBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox4.ForeColor = System.Drawing.Color.Teal;
            this.textBox4.Location = new System.Drawing.Point(3, 18);
            this.textBox4.Multiline = true;
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(269, 413);
            this.textBox4.TabIndex = 6;
            this.textBox4.Text = resources.GetString("textBox4.Text");
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.label22);
            this.groupBox13.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox13.Location = new System.Drawing.Point(0, 0);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(275, 100);
            this.groupBox13.TabIndex = 0;
            this.groupBox13.TabStop = false;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Teal;
            this.label22.Location = new System.Drawing.Point(50, 33);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(175, 40);
            this.label22.TabIndex = 5;
            this.label22.Text = "QUY ĐỊNH CẬP NHẬT \r\nKẾT QUẢ TRẬN ĐẤU";
            this.label22.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.dgvCapNhatKetQua_DanhSachTranDau);
            this.groupBox12.Controls.Add(this.menuStrip2);
            this.groupBox12.Controls.Add(this.textBox6);
            this.groupBox12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox12.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox12.Location = new System.Drawing.Point(0, 0);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(677, 540);
            this.groupBox12.TabIndex = 1;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Bảng quy định cập nhật kết quả trận đấu";
            // 
            // dgvCapNhatKetQua_DanhSachTranDau
            // 
            this.dgvCapNhatKetQua_DanhSachTranDau.AllowUserToAddRows = false;
            this.dgvCapNhatKetQua_DanhSachTranDau.AllowUserToDeleteRows = false;
            this.dgvCapNhatKetQua_DanhSachTranDau.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvCapNhatKetQua_DanhSachTranDau.BackgroundColor = System.Drawing.Color.Snow;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Teal;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCapNhatKetQua_DanhSachTranDau.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvCapNhatKetQua_DanhSachTranDau.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCapNhatKetQua_DanhSachTranDau.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MaTranDauCNKQ,
            this.NgayThiDauCNKQ,
            this.GioThiDauCNKQ,
            this.TrangThai,
            this.ChuNhaCNKQ,
            this.KhachCNKQ,
            this.CapNhatCNKQ,
            this.HoanTatKNKQ});
            this.dgvCapNhatKetQua_DanhSachTranDau.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCapNhatKetQua_DanhSachTranDau.Location = new System.Drawing.Point(3, 47);
            this.dgvCapNhatKetQua_DanhSachTranDau.Name = "dgvCapNhatKetQua_DanhSachTranDau";
            this.dgvCapNhatKetQua_DanhSachTranDau.ReadOnly = true;
            this.dgvCapNhatKetQua_DanhSachTranDau.Size = new System.Drawing.Size(671, 490);
            this.dgvCapNhatKetQua_DanhSachTranDau.TabIndex = 5;
            this.dgvCapNhatKetQua_DanhSachTranDau.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCapNhatKetQua_DanhSachTranDau_CellContentClick);
            this.dgvCapNhatKetQua_DanhSachTranDau.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgvCapNhatKetQua_DanhSachTranDau_CellPainting);
            // 
            // MaTranDauCNKQ
            // 
            this.MaTranDauCNKQ.DataPropertyName = "MaTranDau";
            this.MaTranDauCNKQ.HeaderText = "Mã trận đấu";
            this.MaTranDauCNKQ.Name = "MaTranDauCNKQ";
            this.MaTranDauCNKQ.ReadOnly = true;
            // 
            // NgayThiDauCNKQ
            // 
            this.NgayThiDauCNKQ.DataPropertyName = "NgayThiDau";
            this.NgayThiDauCNKQ.HeaderText = "Ngày thi đấu";
            this.NgayThiDauCNKQ.Name = "NgayThiDauCNKQ";
            this.NgayThiDauCNKQ.ReadOnly = true;
            // 
            // GioThiDauCNKQ
            // 
            this.GioThiDauCNKQ.DataPropertyName = "GioThiDau";
            this.GioThiDauCNKQ.HeaderText = "Giờ thi đấu";
            this.GioThiDauCNKQ.Name = "GioThiDauCNKQ";
            this.GioThiDauCNKQ.ReadOnly = true;
            // 
            // TrangThai
            // 
            this.TrangThai.DataPropertyName = "DaThiDau";
            this.TrangThai.HeaderText = "Trạng thái";
            this.TrangThai.Name = "TrangThai";
            this.TrangThai.ReadOnly = true;
            // 
            // ChuNhaCNKQ
            // 
            this.ChuNhaCNKQ.DataPropertyName = "MaChuNha";
            this.ChuNhaCNKQ.HeaderText = "Chủ nhà";
            this.ChuNhaCNKQ.Name = "ChuNhaCNKQ";
            this.ChuNhaCNKQ.ReadOnly = true;
            // 
            // KhachCNKQ
            // 
            this.KhachCNKQ.DataPropertyName = "MaKhach";
            this.KhachCNKQ.HeaderText = "Khách";
            this.KhachCNKQ.Name = "KhachCNKQ";
            this.KhachCNKQ.ReadOnly = true;
            // 
            // CapNhatCNKQ
            // 
            this.CapNhatCNKQ.HeaderText = "Cập nhật";
            this.CapNhatCNKQ.Name = "CapNhatCNKQ";
            this.CapNhatCNKQ.ReadOnly = true;
            // 
            // HoanTatKNKQ
            // 
            this.HoanTatKNKQ.HeaderText = "Hoàn tất";
            this.HoanTatKNKQ.Name = "HoanTatKNKQ";
            this.HoanTatKNKQ.ReadOnly = true;
            this.HoanTatKNKQ.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.HoanTatKNKQ.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // menuStrip2
            // 
            this.menuStrip2.BackColor = System.Drawing.Color.Azure;
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiCapNhatKetQua_MuaGiai,
            this.tsmiCapNhatKetQua_VongDau});
            this.menuStrip2.Location = new System.Drawing.Point(3, 18);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(671, 29);
            this.menuStrip2.TabIndex = 4;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // tsmiCapNhatKetQua_MuaGiai
            // 
            this.tsmiCapNhatKetQua_MuaGiai.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tsmiCapNhatKetQua_MuaGiai.ForeColor = System.Drawing.Color.Teal;
            this.tsmiCapNhatKetQua_MuaGiai.Name = "tsmiCapNhatKetQua_MuaGiai";
            this.tsmiCapNhatKetQua_MuaGiai.Size = new System.Drawing.Size(82, 25);
            this.tsmiCapNhatKetQua_MuaGiai.Text = "Mùa giải";
            // 
            // tsmiCapNhatKetQua_VongDau
            // 
            this.tsmiCapNhatKetQua_VongDau.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tsmiCapNhatKetQua_VongDau.ForeColor = System.Drawing.Color.Teal;
            this.tsmiCapNhatKetQua_VongDau.Name = "tsmiCapNhatKetQua_VongDau";
            this.tsmiCapNhatKetQua_VongDau.Size = new System.Drawing.Size(88, 25);
            this.tsmiCapNhatKetQua_VongDau.Text = "Vòng đấu";
            // 
            // textBox6
            // 
            this.textBox6.BackColor = System.Drawing.SystemColors.Info;
            this.textBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox6.Location = new System.Drawing.Point(3, 18);
            this.textBox6.Multiline = true;
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(671, 519);
            this.textBox6.TabIndex = 0;
            // 
            // tpCapNhatKetQua_CapNhatKetQuaTranDau
            // 
            this.tpCapNhatKetQua_CapNhatKetQuaTranDau.Controls.Add(this.splitContainer4);
            this.tpCapNhatKetQua_CapNhatKetQuaTranDau.Location = new System.Drawing.Point(4, 25);
            this.tpCapNhatKetQua_CapNhatKetQuaTranDau.Name = "tpCapNhatKetQua_CapNhatKetQuaTranDau";
            this.tpCapNhatKetQua_CapNhatKetQuaTranDau.Padding = new System.Windows.Forms.Padding(3);
            this.tpCapNhatKetQua_CapNhatKetQuaTranDau.Size = new System.Drawing.Size(962, 546);
            this.tpCapNhatKetQua_CapNhatKetQuaTranDau.TabIndex = 2;
            this.tpCapNhatKetQua_CapNhatKetQuaTranDau.Text = "Cập nhật kết quá trận đấu";
            this.tpCapNhatKetQua_CapNhatKetQuaTranDau.UseVisualStyleBackColor = true;
            // 
            // splitContainer4
            // 
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.Location = new System.Drawing.Point(3, 3);
            this.splitContainer4.Name = "splitContainer4";
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this.splitContainer10);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.Controls.Add(this.splitContainer24);
            this.splitContainer4.Size = new System.Drawing.Size(956, 540);
            this.splitContainer4.SplitterDistance = 450;
            this.splitContainer4.TabIndex = 0;
            // 
            // splitContainer10
            // 
            this.splitContainer10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer10.Location = new System.Drawing.Point(0, 0);
            this.splitContainer10.Name = "splitContainer10";
            this.splitContainer10.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer10.Panel1
            // 
            this.splitContainer10.Panel1.Controls.Add(this.gbCapNhatKetQua_ChuNha);
            // 
            // splitContainer10.Panel2
            // 
            this.splitContainer10.Panel2.Controls.Add(this.gbCapNhatKetQua_Khach);
            this.splitContainer10.Size = new System.Drawing.Size(450, 540);
            this.splitContainer10.SplitterDistance = 261;
            this.splitContainer10.TabIndex = 0;
            // 
            // gbCapNhatKetQua_ChuNha
            // 
            this.gbCapNhatKetQua_ChuNha.Controls.Add(this.dgvCapNhatKetQua_ChuNha);
            this.gbCapNhatKetQua_ChuNha.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbCapNhatKetQua_ChuNha.Location = new System.Drawing.Point(0, 0);
            this.gbCapNhatKetQua_ChuNha.Name = "gbCapNhatKetQua_ChuNha";
            this.gbCapNhatKetQua_ChuNha.Size = new System.Drawing.Size(450, 261);
            this.gbCapNhatKetQua_ChuNha.TabIndex = 0;
            this.gbCapNhatKetQua_ChuNha.TabStop = false;
            this.gbCapNhatKetQua_ChuNha.Text = "Chủ nhà";
            // 
            // dgvCapNhatKetQua_ChuNha
            // 
            this.dgvCapNhatKetQua_ChuNha.AllowUserToAddRows = false;
            this.dgvCapNhatKetQua_ChuNha.AllowUserToDeleteRows = false;
            this.dgvCapNhatKetQua_ChuNha.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvCapNhatKetQua_ChuNha.BackgroundColor = System.Drawing.Color.Snow;
            this.dgvCapNhatKetQua_ChuNha.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCapNhatKetQua_ChuNha.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MaCauThuCN,
            this.TenCauThuCN,
            this.CapNhatCN});
            this.dgvCapNhatKetQua_ChuNha.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCapNhatKetQua_ChuNha.Location = new System.Drawing.Point(3, 18);
            this.dgvCapNhatKetQua_ChuNha.Name = "dgvCapNhatKetQua_ChuNha";
            this.dgvCapNhatKetQua_ChuNha.ReadOnly = true;
            this.dgvCapNhatKetQua_ChuNha.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvCapNhatKetQua_ChuNha.Size = new System.Drawing.Size(444, 240);
            this.dgvCapNhatKetQua_ChuNha.TabIndex = 0;
            this.dgvCapNhatKetQua_ChuNha.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCapNhatKetQua_ChuNha_CellContentClick);
            this.dgvCapNhatKetQua_ChuNha.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgvCapNhatKetQua_ChuNha_CellPainting);
            // 
            // MaCauThuCN
            // 
            this.MaCauThuCN.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.MaCauThuCN.DataPropertyName = "MaCauThu";
            this.MaCauThuCN.FillWeight = 127.1574F;
            this.MaCauThuCN.HeaderText = "Mã cầu thủ";
            this.MaCauThuCN.Name = "MaCauThuCN";
            this.MaCauThuCN.ReadOnly = true;
            this.MaCauThuCN.Width = 120;
            // 
            // TenCauThuCN
            // 
            this.TenCauThuCN.DataPropertyName = "TenCauThu";
            this.TenCauThuCN.FillWeight = 127.1574F;
            this.TenCauThuCN.HeaderText = "Tên cầu thủ";
            this.TenCauThuCN.Name = "TenCauThuCN";
            this.TenCauThuCN.ReadOnly = true;
            // 
            // CapNhatCN
            // 
            this.CapNhatCN.FillWeight = 45.68528F;
            this.CapNhatCN.HeaderText = "Cập nhật";
            this.CapNhatCN.Name = "CapNhatCN";
            this.CapNhatCN.ReadOnly = true;
            // 
            // gbCapNhatKetQua_Khach
            // 
            this.gbCapNhatKetQua_Khach.Controls.Add(this.dgvCapNhatKetQua_Khach);
            this.gbCapNhatKetQua_Khach.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbCapNhatKetQua_Khach.Location = new System.Drawing.Point(0, 0);
            this.gbCapNhatKetQua_Khach.Name = "gbCapNhatKetQua_Khach";
            this.gbCapNhatKetQua_Khach.Size = new System.Drawing.Size(450, 275);
            this.gbCapNhatKetQua_Khach.TabIndex = 0;
            this.gbCapNhatKetQua_Khach.TabStop = false;
            this.gbCapNhatKetQua_Khach.Text = "Khách";
            // 
            // dgvCapNhatKetQua_Khach
            // 
            this.dgvCapNhatKetQua_Khach.AllowUserToAddRows = false;
            this.dgvCapNhatKetQua_Khach.AllowUserToDeleteRows = false;
            this.dgvCapNhatKetQua_Khach.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvCapNhatKetQua_Khach.BackgroundColor = System.Drawing.Color.Snow;
            this.dgvCapNhatKetQua_Khach.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCapNhatKetQua_Khach.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewButtonColumn3});
            this.dgvCapNhatKetQua_Khach.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCapNhatKetQua_Khach.Location = new System.Drawing.Point(3, 18);
            this.dgvCapNhatKetQua_Khach.Name = "dgvCapNhatKetQua_Khach";
            this.dgvCapNhatKetQua_Khach.ReadOnly = true;
            this.dgvCapNhatKetQua_Khach.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvCapNhatKetQua_Khach.Size = new System.Drawing.Size(444, 254);
            this.dgvCapNhatKetQua_Khach.TabIndex = 1;
            this.dgvCapNhatKetQua_Khach.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCapNhatKetQua_ChuNha_CellContentClick);
            this.dgvCapNhatKetQua_Khach.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgvCapNhatKetQua_ChuNha_CellPainting);
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn9.DataPropertyName = "MaCauThu";
            this.dataGridViewTextBoxColumn9.FillWeight = 127.1574F;
            this.dataGridViewTextBoxColumn9.HeaderText = "Mã cầu thủ";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Width = 120;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "TenCauThu";
            this.dataGridViewTextBoxColumn10.FillWeight = 127.1574F;
            this.dataGridViewTextBoxColumn10.HeaderText = "Tên cầu thủ";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            // 
            // dataGridViewButtonColumn3
            // 
            this.dataGridViewButtonColumn3.FillWeight = 45.68528F;
            this.dataGridViewButtonColumn3.HeaderText = "Cập nhật";
            this.dataGridViewButtonColumn3.Name = "dataGridViewButtonColumn3";
            this.dataGridViewButtonColumn3.ReadOnly = true;
            // 
            // splitContainer24
            // 
            this.splitContainer24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer24.Location = new System.Drawing.Point(0, 0);
            this.splitContainer24.Name = "splitContainer24";
            this.splitContainer24.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer24.Panel1
            // 
            this.splitContainer24.Panel1.Controls.Add(this.groupBox23);
            // 
            // splitContainer24.Panel2
            // 
            this.splitContainer24.Panel2.BackColor = System.Drawing.Color.LightGoldenrodYellow;
            this.splitContainer24.Panel2.Controls.Add(this.btCapNhatKetQua_CapNhatThongTin_CapNhat);
            this.splitContainer24.Panel2.Controls.Add(this.tbCapNhatKetQua_MaDoiBong_hidden);
            this.splitContainer24.Panel2.Controls.Add(this.tbTenDoiBong);
            this.splitContainer24.Panel2.Controls.Add(this.label1);
            this.splitContainer24.Panel2.Controls.Add(this.rbtbanthangtructiep);
            this.splitContainer24.Panel2.Controls.Add(this.tbTenCauThu);
            this.splitContainer24.Panel2.Controls.Add(this.label9);
            this.splitContainer24.Panel2.Controls.Add(this.rbtpenalty);
            this.splitContainer24.Panel2.Controls.Add(this.label10);
            this.splitContainer24.Panel2.Controls.Add(this.label11);
            this.splitContainer24.Panel2.Controls.Add(this.nudGiay);
            this.splitContainer24.Panel2.Controls.Add(this.label21);
            this.splitContainer24.Panel2.Controls.Add(this.rbtphanluoi);
            this.splitContainer24.Panel2.Controls.Add(this.label12);
            this.splitContainer24.Panel2.Controls.Add(this.label13);
            this.splitContainer24.Panel2.Controls.Add(this.nudPhut);
            this.splitContainer24.Size = new System.Drawing.Size(502, 540);
            this.splitContainer24.SplitterDistance = 200;
            this.splitContainer24.TabIndex = 0;
            // 
            // groupBox23
            // 
            this.groupBox23.Controls.Add(this.tbCapNhatKetQuaTranDau_CapNhat_TranDau);
            this.groupBox23.Controls.Add(this.tbCapNhatKetQuaTranDau_CapNhat_VongDau);
            this.groupBox23.Controls.Add(this.tbCapNhatKetQuaTranDau_CapNhat_MuaGiai);
            this.groupBox23.Controls.Add(this.tbkhach);
            this.groupBox23.Controls.Add(this.tbbanthangkhach);
            this.groupBox23.Controls.Add(this.tbbanthangchunha);
            this.groupBox23.Controls.Add(this.tbtrangthai);
            this.groupBox23.Controls.Add(this.tbgiothidau);
            this.groupBox23.Controls.Add(this.tbngaythidau);
            this.groupBox23.Controls.Add(this.tbchunha);
            this.groupBox23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox23.Location = new System.Drawing.Point(0, 0);
            this.groupBox23.Name = "groupBox23";
            this.groupBox23.Size = new System.Drawing.Size(502, 200);
            this.groupBox23.TabIndex = 0;
            this.groupBox23.TabStop = false;
            this.groupBox23.Text = "Thông tin trận đấu";
            // 
            // tbCapNhatKetQuaTranDau_CapNhat_TranDau
            // 
            this.tbCapNhatKetQuaTranDau_CapNhat_TranDau.BackColor = System.Drawing.Color.Azure;
            this.tbCapNhatKetQuaTranDau_CapNhat_TranDau.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCapNhatKetQuaTranDau_CapNhat_TranDau.ForeColor = System.Drawing.Color.Teal;
            this.tbCapNhatKetQuaTranDau_CapNhat_TranDau.Location = new System.Drawing.Point(247, 64);
            this.tbCapNhatKetQuaTranDau_CapNhat_TranDau.Name = "tbCapNhatKetQuaTranDau_CapNhat_TranDau";
            this.tbCapNhatKetQuaTranDau_CapNhat_TranDau.ReadOnly = true;
            this.tbCapNhatKetQuaTranDau_CapNhat_TranDau.Size = new System.Drawing.Size(240, 24);
            this.tbCapNhatKetQuaTranDau_CapNhat_TranDau.TabIndex = 29;
            this.tbCapNhatKetQuaTranDau_CapNhat_TranDau.Text = "Trận đấu";
            this.tbCapNhatKetQuaTranDau_CapNhat_TranDau.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbCapNhatKetQuaTranDau_CapNhat_VongDau
            // 
            this.tbCapNhatKetQuaTranDau_CapNhat_VongDau.BackColor = System.Drawing.Color.Azure;
            this.tbCapNhatKetQuaTranDau_CapNhat_VongDau.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCapNhatKetQuaTranDau_CapNhat_VongDau.ForeColor = System.Drawing.Color.Teal;
            this.tbCapNhatKetQuaTranDau_CapNhat_VongDau.Location = new System.Drawing.Point(18, 64);
            this.tbCapNhatKetQuaTranDau_CapNhat_VongDau.Name = "tbCapNhatKetQuaTranDau_CapNhat_VongDau";
            this.tbCapNhatKetQuaTranDau_CapNhat_VongDau.ReadOnly = true;
            this.tbCapNhatKetQuaTranDau_CapNhat_VongDau.Size = new System.Drawing.Size(223, 24);
            this.tbCapNhatKetQuaTranDau_CapNhat_VongDau.TabIndex = 30;
            this.tbCapNhatKetQuaTranDau_CapNhat_VongDau.Text = "Vòng đấu";
            this.tbCapNhatKetQuaTranDau_CapNhat_VongDau.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbCapNhatKetQuaTranDau_CapNhat_MuaGiai
            // 
            this.tbCapNhatKetQuaTranDau_CapNhat_MuaGiai.BackColor = System.Drawing.Color.Azure;
            this.tbCapNhatKetQuaTranDau_CapNhat_MuaGiai.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCapNhatKetQuaTranDau_CapNhat_MuaGiai.ForeColor = System.Drawing.Color.Teal;
            this.tbCapNhatKetQuaTranDau_CapNhat_MuaGiai.Location = new System.Drawing.Point(18, 26);
            this.tbCapNhatKetQuaTranDau_CapNhat_MuaGiai.Name = "tbCapNhatKetQuaTranDau_CapNhat_MuaGiai";
            this.tbCapNhatKetQuaTranDau_CapNhat_MuaGiai.ReadOnly = true;
            this.tbCapNhatKetQuaTranDau_CapNhat_MuaGiai.Size = new System.Drawing.Size(469, 24);
            this.tbCapNhatKetQuaTranDau_CapNhat_MuaGiai.TabIndex = 31;
            this.tbCapNhatKetQuaTranDau_CapNhat_MuaGiai.Text = "Mùa giải";
            this.tbCapNhatKetQuaTranDau_CapNhat_MuaGiai.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbkhach
            // 
            this.tbkhach.BackColor = System.Drawing.Color.Azure;
            this.tbkhach.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbkhach.ForeColor = System.Drawing.Color.Teal;
            this.tbkhach.Location = new System.Drawing.Point(282, 144);
            this.tbkhach.Name = "tbkhach";
            this.tbkhach.ReadOnly = true;
            this.tbkhach.Size = new System.Drawing.Size(205, 24);
            this.tbkhach.TabIndex = 22;
            this.tbkhach.Text = "Khách";
            // 
            // tbbanthangkhach
            // 
            this.tbbanthangkhach.BackColor = System.Drawing.Color.Azure;
            this.tbbanthangkhach.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbbanthangkhach.ForeColor = System.Drawing.Color.Teal;
            this.tbbanthangkhach.Location = new System.Drawing.Point(246, 144);
            this.tbbanthangkhach.Name = "tbbanthangkhach";
            this.tbbanthangkhach.ReadOnly = true;
            this.tbbanthangkhach.Size = new System.Drawing.Size(30, 24);
            this.tbbanthangkhach.TabIndex = 23;
            this.tbbanthangkhach.Text = "0";
            this.tbbanthangkhach.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbbanthangchunha
            // 
            this.tbbanthangchunha.BackColor = System.Drawing.Color.Azure;
            this.tbbanthangchunha.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbbanthangchunha.ForeColor = System.Drawing.Color.Teal;
            this.tbbanthangchunha.Location = new System.Drawing.Point(211, 144);
            this.tbbanthangchunha.Name = "tbbanthangchunha";
            this.tbbanthangchunha.ReadOnly = true;
            this.tbbanthangchunha.Size = new System.Drawing.Size(30, 24);
            this.tbbanthangchunha.TabIndex = 24;
            this.tbbanthangchunha.Text = "0";
            this.tbbanthangchunha.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbtrangthai
            // 
            this.tbtrangthai.BackColor = System.Drawing.Color.Azure;
            this.tbtrangthai.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbtrangthai.ForeColor = System.Drawing.Color.Teal;
            this.tbtrangthai.Location = new System.Drawing.Point(319, 101);
            this.tbtrangthai.Name = "tbtrangthai";
            this.tbtrangthai.ReadOnly = true;
            this.tbtrangthai.Size = new System.Drawing.Size(168, 24);
            this.tbtrangthai.TabIndex = 25;
            this.tbtrangthai.Text = "Trạng thái";
            this.tbtrangthai.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbgiothidau
            // 
            this.tbgiothidau.BackColor = System.Drawing.Color.Azure;
            this.tbgiothidau.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbgiothidau.ForeColor = System.Drawing.Color.Teal;
            this.tbgiothidau.Location = new System.Drawing.Point(172, 101);
            this.tbgiothidau.Name = "tbgiothidau";
            this.tbgiothidau.ReadOnly = true;
            this.tbgiothidau.Size = new System.Drawing.Size(137, 24);
            this.tbgiothidau.TabIndex = 26;
            this.tbgiothidau.Text = "Giờ thi đấu";
            this.tbgiothidau.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbngaythidau
            // 
            this.tbngaythidau.BackColor = System.Drawing.Color.Azure;
            this.tbngaythidau.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbngaythidau.ForeColor = System.Drawing.Color.Teal;
            this.tbngaythidau.Location = new System.Drawing.Point(18, 101);
            this.tbngaythidau.Name = "tbngaythidau";
            this.tbngaythidau.ReadOnly = true;
            this.tbngaythidau.Size = new System.Drawing.Size(148, 24);
            this.tbngaythidau.TabIndex = 27;
            this.tbngaythidau.Text = "Ngày thi đấu";
            this.tbngaythidau.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbchunha
            // 
            this.tbchunha.BackColor = System.Drawing.Color.Azure;
            this.tbchunha.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbchunha.ForeColor = System.Drawing.Color.Teal;
            this.tbchunha.Location = new System.Drawing.Point(16, 144);
            this.tbchunha.Name = "tbchunha";
            this.tbchunha.ReadOnly = true;
            this.tbchunha.Size = new System.Drawing.Size(189, 24);
            this.tbchunha.TabIndex = 28;
            this.tbchunha.Text = "Chủ nhà";
            this.tbchunha.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btCapNhatKetQua_CapNhatThongTin_CapNhat
            // 
            this.btCapNhatKetQua_CapNhatThongTin_CapNhat.BackColor = System.Drawing.Color.LightCyan;
            this.btCapNhatKetQua_CapNhatThongTin_CapNhat.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btCapNhatKetQua_CapNhatThongTin_CapNhat.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btCapNhatKetQua_CapNhatThongTin_CapNhat.ForeColor = System.Drawing.Color.Teal;
            this.btCapNhatKetQua_CapNhatThongTin_CapNhat.Location = new System.Drawing.Point(104, 242);
            this.btCapNhatKetQua_CapNhatThongTin_CapNhat.Name = "btCapNhatKetQua_CapNhatThongTin_CapNhat";
            this.btCapNhatKetQua_CapNhatThongTin_CapNhat.Size = new System.Drawing.Size(93, 30);
            this.btCapNhatKetQua_CapNhatThongTin_CapNhat.TabIndex = 33;
            this.btCapNhatKetQua_CapNhatThongTin_CapNhat.Text = "Cập nhật kết quả";
            this.btCapNhatKetQua_CapNhatThongTin_CapNhat.UseVisualStyleBackColor = false;
            this.btCapNhatKetQua_CapNhatThongTin_CapNhat.Click += new System.EventHandler(this.btCapNhatKetQua_CapNhatThongTin_CapNhat_Click_1);
            // 
            // tbCapNhatKetQua_MaDoiBong_hidden
            // 
            this.tbCapNhatKetQua_MaDoiBong_hidden.Location = new System.Drawing.Point(236, 3);
            this.tbCapNhatKetQua_MaDoiBong_hidden.Name = "tbCapNhatKetQua_MaDoiBong_hidden";
            this.tbCapNhatKetQua_MaDoiBong_hidden.ReadOnly = true;
            this.tbCapNhatKetQua_MaDoiBong_hidden.Size = new System.Drawing.Size(165, 22);
            this.tbCapNhatKetQua_MaDoiBong_hidden.TabIndex = 24;
            this.tbCapNhatKetQua_MaDoiBong_hidden.Visible = false;
            // 
            // tbTenDoiBong
            // 
            this.tbTenDoiBong.Location = new System.Drawing.Point(230, 37);
            this.tbTenDoiBong.Name = "tbTenDoiBong";
            this.tbTenDoiBong.ReadOnly = true;
            this.tbTenDoiBong.Size = new System.Drawing.Size(165, 22);
            this.tbTenDoiBong.TabIndex = 24;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(370, 200);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 16);
            this.label1.TabIndex = 31;
            this.label1.Text = "Giây";
            // 
            // rbtbanthangtructiep
            // 
            this.rbtbanthangtructiep.AutoSize = true;
            this.rbtbanthangtructiep.Location = new System.Drawing.Point(230, 120);
            this.rbtbanthangtructiep.Name = "rbtbanthangtructiep";
            this.rbtbanthangtructiep.Size = new System.Drawing.Size(135, 20);
            this.rbtbanthangtructiep.TabIndex = 26;
            this.rbtbanthangtructiep.TabStop = true;
            this.rbtbanthangtructiep.Text = "Bàn thắng trực tiếp";
            this.rbtbanthangtructiep.UseVisualStyleBackColor = true;
            // 
            // tbTenCauThu
            // 
            this.tbTenCauThu.Location = new System.Drawing.Point(230, 77);
            this.tbTenCauThu.Name = "tbTenCauThu";
            this.tbTenCauThu.ReadOnly = true;
            this.tbTenCauThu.Size = new System.Drawing.Size(165, 22);
            this.tbTenCauThu.TabIndex = 25;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(280, 200);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(34, 16);
            this.label9.TabIndex = 32;
            this.label9.Text = "Phút";
            // 
            // rbtpenalty
            // 
            this.rbtpenalty.AutoSize = true;
            this.rbtpenalty.Location = new System.Drawing.Point(230, 145);
            this.rbtpenalty.Name = "rbtpenalty";
            this.rbtpenalty.Size = new System.Drawing.Size(71, 20);
            this.rbtpenalty.TabIndex = 27;
            this.rbtpenalty.TabStop = true;
            this.rbtpenalty.Text = "Penalty";
            this.rbtpenalty.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(100, 120);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(96, 16);
            this.label10.TabIndex = 20;
            this.label10.Text = "Loại bàn thắng";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(100, 200);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(115, 16);
            this.label11.TabIndex = 21;
            this.label11.Text = "Thời điểm ghi bàn";
            // 
            // nudGiay
            // 
            this.nudGiay.Location = new System.Drawing.Point(320, 198);
            this.nudGiay.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.nudGiay.Name = "nudGiay";
            this.nudGiay.Size = new System.Drawing.Size(40, 22);
            this.nudGiay.TabIndex = 29;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(101, 2);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(83, 16);
            this.label21.TabIndex = 22;
            this.label21.Text = "Mã đội bóng";
            this.label21.Visible = false;
            // 
            // rbtphanluoi
            // 
            this.rbtphanluoi.AutoSize = true;
            this.rbtphanluoi.Location = new System.Drawing.Point(230, 170);
            this.rbtphanluoi.Name = "rbtphanluoi";
            this.rbtphanluoi.Size = new System.Drawing.Size(106, 20);
            this.rbtphanluoi.TabIndex = 28;
            this.rbtphanluoi.TabStop = true;
            this.rbtphanluoi.Text = "Phản lưới nhà";
            this.rbtphanluoi.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(100, 40);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(88, 16);
            this.label12.TabIndex = 22;
            this.label12.Text = "Tên đội bóng";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(100, 80);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(77, 16);
            this.label13.TabIndex = 23;
            this.label13.Text = "Tên cầu thủ";
            // 
            // nudPhut
            // 
            this.nudPhut.Location = new System.Drawing.Point(230, 198);
            this.nudPhut.Maximum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.nudPhut.Name = "nudPhut";
            this.nudPhut.Size = new System.Drawing.Size(40, 22);
            this.nudPhut.TabIndex = 30;
            // 
            // tpMenuLichThiDau
            // 
            this.tpMenuLichThiDau.Controls.Add(this.dgvLichThiDau);
            this.tpMenuLichThiDau.Controls.Add(this.menuStrip4);
            this.tpMenuLichThiDau.Location = new System.Drawing.Point(4, 25);
            this.tpMenuLichThiDau.Name = "tpMenuLichThiDau";
            this.tpMenuLichThiDau.Padding = new System.Windows.Forms.Padding(3);
            this.tpMenuLichThiDau.Size = new System.Drawing.Size(976, 581);
            this.tpMenuLichThiDau.TabIndex = 7;
            this.tpMenuLichThiDau.Text = "Lịch thi đấu";
            this.tpMenuLichThiDau.UseVisualStyleBackColor = true;
            // 
            // dgvLichThiDau
            // 
            this.dgvLichThiDau.AllowUserToAddRows = false;
            this.dgvLichThiDau.AllowUserToDeleteRows = false;
            this.dgvLichThiDau.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvLichThiDau.BackgroundColor = System.Drawing.Color.Snow;
            this.dgvLichThiDau.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLichThiDau.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MaVongDauLTD,
            this.MaTranDauLTD,
            this.TinhTrangLTD,
            this.NgayThiDauLTD,
            this.GioThiDauLTD,
            this.ChuNhaLTD,
            this.DiemChuNha,
            this.DiemKhachLTD,
            this.KhachLTD});
            this.dgvLichThiDau.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvLichThiDau.Location = new System.Drawing.Point(3, 32);
            this.dgvLichThiDau.Name = "dgvLichThiDau";
            this.dgvLichThiDau.ReadOnly = true;
            this.dgvLichThiDau.Size = new System.Drawing.Size(970, 546);
            this.dgvLichThiDau.TabIndex = 2;
            // 
            // MaVongDauLTD
            // 
            this.MaVongDauLTD.DataPropertyName = "MaVongDau";
            this.MaVongDauLTD.HeaderText = "Mã vòng đấu";
            this.MaVongDauLTD.Name = "MaVongDauLTD";
            this.MaVongDauLTD.ReadOnly = true;
            // 
            // MaTranDauLTD
            // 
            this.MaTranDauLTD.DataPropertyName = "MaTranDau";
            this.MaTranDauLTD.HeaderText = "Mã trận đấu";
            this.MaTranDauLTD.Name = "MaTranDauLTD";
            this.MaTranDauLTD.ReadOnly = true;
            // 
            // TinhTrangLTD
            // 
            this.TinhTrangLTD.DataPropertyName = "MaMuaGiai";
            this.TinhTrangLTD.HeaderText = "Tình trạng";
            this.TinhTrangLTD.Name = "TinhTrangLTD";
            this.TinhTrangLTD.ReadOnly = true;
            // 
            // NgayThiDauLTD
            // 
            this.NgayThiDauLTD.DataPropertyName = "NgayThiDau";
            this.NgayThiDauLTD.HeaderText = "Ngày thi đấu";
            this.NgayThiDauLTD.Name = "NgayThiDauLTD";
            this.NgayThiDauLTD.ReadOnly = true;
            // 
            // GioThiDauLTD
            // 
            this.GioThiDauLTD.DataPropertyName = "GioThiDau";
            this.GioThiDauLTD.HeaderText = "Giờ thi đấu";
            this.GioThiDauLTD.Name = "GioThiDauLTD";
            this.GioThiDauLTD.ReadOnly = true;
            // 
            // ChuNhaLTD
            // 
            this.ChuNhaLTD.DataPropertyName = "MaChuNha";
            this.ChuNhaLTD.HeaderText = "Chủ nhà";
            this.ChuNhaLTD.Name = "ChuNhaLTD";
            this.ChuNhaLTD.ReadOnly = true;
            // 
            // DiemChuNha
            // 
            this.DiemChuNha.DataPropertyName = "SoBanThangChuNha";
            this.DiemChuNha.HeaderText = "Bàn thắng";
            this.DiemChuNha.Name = "DiemChuNha";
            this.DiemChuNha.ReadOnly = true;
            // 
            // DiemKhachLTD
            // 
            this.DiemKhachLTD.DataPropertyName = "SoBanThangKhach";
            this.DiemKhachLTD.HeaderText = "Bàn thắng";
            this.DiemKhachLTD.Name = "DiemKhachLTD";
            this.DiemKhachLTD.ReadOnly = true;
            // 
            // KhachLTD
            // 
            this.KhachLTD.DataPropertyName = "MaKhach";
            this.KhachLTD.HeaderText = "Khách";
            this.KhachLTD.Name = "KhachLTD";
            this.KhachLTD.ReadOnly = true;
            // 
            // menuStrip4
            // 
            this.menuStrip4.BackColor = System.Drawing.Color.Azure;
            this.menuStrip4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiLichThiDau_MaMuaGiai,
            this.tsmiLichThiDau_MaVongDau});
            this.menuStrip4.Location = new System.Drawing.Point(3, 3);
            this.menuStrip4.Name = "menuStrip4";
            this.menuStrip4.Size = new System.Drawing.Size(970, 29);
            this.menuStrip4.TabIndex = 1;
            this.menuStrip4.Text = "menuStrip4";
            // 
            // tsmiLichThiDau_MaMuaGiai
            // 
            this.tsmiLichThiDau_MaMuaGiai.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tsmiLichThiDau_MaMuaGiai.ForeColor = System.Drawing.Color.Teal;
            this.tsmiLichThiDau_MaMuaGiai.Name = "tsmiLichThiDau_MaMuaGiai";
            this.tsmiLichThiDau_MaMuaGiai.Size = new System.Drawing.Size(82, 25);
            this.tsmiLichThiDau_MaMuaGiai.Text = "Mùa giải";
            // 
            // tsmiLichThiDau_MaVongDau
            // 
            this.tsmiLichThiDau_MaVongDau.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tsmiLichThiDau_MaVongDau.ForeColor = System.Drawing.Color.Teal;
            this.tsmiLichThiDau_MaVongDau.Name = "tsmiLichThiDau_MaVongDau";
            this.tsmiLichThiDau_MaVongDau.Size = new System.Drawing.Size(88, 25);
            this.tsmiLichThiDau_MaVongDau.Text = "Vòng đấu";
            // 
            // tpBangXepHang
            // 
            this.tpBangXepHang.BackColor = System.Drawing.Color.Snow;
            this.tpBangXepHang.Controls.Add(this.dgvBangXepHangBangXepHang);
            this.tpBangXepHang.Controls.Add(this.menuStrip1);
            this.tpBangXepHang.Location = new System.Drawing.Point(4, 25);
            this.tpBangXepHang.Name = "tpBangXepHang";
            this.tpBangXepHang.Padding = new System.Windows.Forms.Padding(3);
            this.tpBangXepHang.Size = new System.Drawing.Size(976, 581);
            this.tpBangXepHang.TabIndex = 4;
            this.tpBangXepHang.Text = "Bảng xếp hạng";
            // 
            // dgvBangXepHangBangXepHang
            // 
            this.dgvBangXepHangBangXepHang.AllowUserToAddRows = false;
            this.dgvBangXepHangBangXepHang.AllowUserToDeleteRows = false;
            this.dgvBangXepHangBangXepHang.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvBangXepHangBangXepHang.BackgroundColor = System.Drawing.Color.Snow;
            this.dgvBangXepHangBangXepHang.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBangXepHangBangXepHang.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MaDoiBong,
            this.SoTranDaThiDau,
            this.TongSoTranThang,
            this.TongSoTranHoa,
            this.TongSoTranThua,
            this.TongSoBanThang,
            this.TongSoBanThua,
            this.TongHieuSo,
            this.TongDiem,
            this.Hang});
            this.dgvBangXepHangBangXepHang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvBangXepHangBangXepHang.Location = new System.Drawing.Point(3, 34);
            this.dgvBangXepHangBangXepHang.Name = "dgvBangXepHangBangXepHang";
            this.dgvBangXepHangBangXepHang.ReadOnly = true;
            this.dgvBangXepHangBangXepHang.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvBangXepHangBangXepHang.Size = new System.Drawing.Size(970, 544);
            this.dgvBangXepHangBangXepHang.TabIndex = 1;
            // 
            // MaDoiBong
            // 
            this.MaDoiBong.DataPropertyName = "MaDoiBong";
            this.MaDoiBong.HeaderText = "Mã đội bóng";
            this.MaDoiBong.Name = "MaDoiBong";
            this.MaDoiBong.ReadOnly = true;
            // 
            // SoTranDaThiDau
            // 
            this.SoTranDaThiDau.DataPropertyName = "SoTranDaThiDau";
            this.SoTranDaThiDau.HeaderText = "ĐĐ";
            this.SoTranDaThiDau.Name = "SoTranDaThiDau";
            this.SoTranDaThiDau.ReadOnly = true;
            // 
            // TongSoTranThang
            // 
            this.TongSoTranThang.DataPropertyName = "TongSoTranThang";
            this.TongSoTranThang.HeaderText = "Thắng";
            this.TongSoTranThang.Name = "TongSoTranThang";
            this.TongSoTranThang.ReadOnly = true;
            // 
            // TongSoTranHoa
            // 
            this.TongSoTranHoa.DataPropertyName = "TongSoTranHoa";
            this.TongSoTranHoa.HeaderText = "Hòa";
            this.TongSoTranHoa.Name = "TongSoTranHoa";
            this.TongSoTranHoa.ReadOnly = true;
            // 
            // TongSoTranThua
            // 
            this.TongSoTranThua.DataPropertyName = "TongSoTranThua";
            this.TongSoTranThua.HeaderText = "Thua";
            this.TongSoTranThua.Name = "TongSoTranThua";
            this.TongSoTranThua.ReadOnly = true;
            // 
            // TongSoBanThang
            // 
            this.TongSoBanThang.DataPropertyName = "TongSoBanThang";
            this.TongSoBanThang.HeaderText = "Bàn thắng";
            this.TongSoBanThang.Name = "TongSoBanThang";
            this.TongSoBanThang.ReadOnly = true;
            // 
            // TongSoBanThua
            // 
            this.TongSoBanThua.DataPropertyName = "TongSoBanThua";
            this.TongSoBanThua.HeaderText = "Bàn thua";
            this.TongSoBanThua.Name = "TongSoBanThua";
            this.TongSoBanThua.ReadOnly = true;
            // 
            // TongHieuSo
            // 
            this.TongHieuSo.DataPropertyName = "TongHieuSo";
            this.TongHieuSo.HeaderText = "Hiệu số";
            this.TongHieuSo.Name = "TongHieuSo";
            this.TongHieuSo.ReadOnly = true;
            // 
            // TongDiem
            // 
            this.TongDiem.DataPropertyName = "TongDiem";
            this.TongDiem.HeaderText = "Điểm";
            this.TongDiem.Name = "TongDiem";
            this.TongDiem.ReadOnly = true;
            // 
            // Hang
            // 
            this.Hang.DataPropertyName = "Hang";
            this.Hang.HeaderText = "Hạng";
            this.Hang.Name = "Hang";
            this.Hang.ReadOnly = true;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Azure;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiBangXepHang_MuaGiai,
            this.tsmiBangXepHang_VongDau,
            this.tsmiBangXepHang_NgayBatDau});
            this.menuStrip1.Location = new System.Drawing.Point(3, 3);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(970, 31);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // tsmiBangXepHang_MuaGiai
            // 
            this.tsmiBangXepHang_MuaGiai.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tsmiBangXepHang_MuaGiai.ForeColor = System.Drawing.Color.Teal;
            this.tsmiBangXepHang_MuaGiai.Name = "tsmiBangXepHang_MuaGiai";
            this.tsmiBangXepHang_MuaGiai.Size = new System.Drawing.Size(82, 27);
            this.tsmiBangXepHang_MuaGiai.Text = "Mùa giải";
            // 
            // tsmiBangXepHang_VongDau
            // 
            this.tsmiBangXepHang_VongDau.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tsmiBangXepHang_VongDau.ForeColor = System.Drawing.Color.Teal;
            this.tsmiBangXepHang_VongDau.Name = "tsmiBangXepHang_VongDau";
            this.tsmiBangXepHang_VongDau.Size = new System.Drawing.Size(88, 27);
            this.tsmiBangXepHang_VongDau.Text = "Vòng đấu";
            // 
            // tsmiBangXepHang_NgayBatDau
            // 
            this.tsmiBangXepHang_NgayBatDau.BackColor = System.Drawing.Color.Azure;
            this.tsmiBangXepHang_NgayBatDau.Font = new System.Drawing.Font("Segoe UI", 11F);
            this.tsmiBangXepHang_NgayBatDau.ForeColor = System.Drawing.Color.Teal;
            this.tsmiBangXepHang_NgayBatDau.Name = "tsmiBangXepHang_NgayBatDau";
            this.tsmiBangXepHang_NgayBatDau.ReadOnly = true;
            this.tsmiBangXepHang_NgayBatDau.Size = new System.Drawing.Size(100, 27);
            this.tsmiBangXepHang_NgayBatDau.Text = "Thời gian                                       .";
            // 
            // tpSoLieuThongKe
            // 
            this.tpSoLieuThongKe.BackColor = System.Drawing.Color.Snow;
            this.tpSoLieuThongKe.Controls.Add(this.dgvSoLieuThongKeCauThu);
            this.tpSoLieuThongKe.Controls.Add(this.menuStrip3);
            this.tpSoLieuThongKe.Location = new System.Drawing.Point(4, 25);
            this.tpSoLieuThongKe.Name = "tpSoLieuThongKe";
            this.tpSoLieuThongKe.Padding = new System.Windows.Forms.Padding(3);
            this.tpSoLieuThongKe.Size = new System.Drawing.Size(976, 581);
            this.tpSoLieuThongKe.TabIndex = 5;
            this.tpSoLieuThongKe.Text = "Số liệu thống kê";
            // 
            // dgvSoLieuThongKeCauThu
            // 
            this.dgvSoLieuThongKeCauThu.AllowUserToAddRows = false;
            this.dgvSoLieuThongKeCauThu.AllowUserToDeleteRows = false;
            this.dgvSoLieuThongKeCauThu.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvSoLieuThongKeCauThu.BackgroundColor = System.Drawing.Color.Snow;
            this.dgvSoLieuThongKeCauThu.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSoLieuThongKeCauThu.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MaCauThu,
            this.HoTenCauThu,
            this.TongSoBanThangCT,
            this.SoBanThang,
            this.HangSLCT});
            this.dgvSoLieuThongKeCauThu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSoLieuThongKeCauThu.Location = new System.Drawing.Point(3, 34);
            this.dgvSoLieuThongKeCauThu.Name = "dgvSoLieuThongKeCauThu";
            this.dgvSoLieuThongKeCauThu.ReadOnly = true;
            this.dgvSoLieuThongKeCauThu.Size = new System.Drawing.Size(970, 544);
            this.dgvSoLieuThongKeCauThu.TabIndex = 1;
            // 
            // MaCauThu
            // 
            this.MaCauThu.DataPropertyName = "MaCauThu";
            this.MaCauThu.HeaderText = "Mã cầu thủ";
            this.MaCauThu.Name = "MaCauThu";
            this.MaCauThu.ReadOnly = true;
            // 
            // HoTenCauThu
            // 
            this.HoTenCauThu.DataPropertyName = "TenCauThu";
            this.HoTenCauThu.HeaderText = "Tên cầu thủ";
            this.HoTenCauThu.Name = "HoTenCauThu";
            this.HoTenCauThu.ReadOnly = true;
            // 
            // TongSoBanThangCT
            // 
            this.TongSoBanThangCT.DataPropertyName = "TongSoBanThang";
            this.TongSoBanThangCT.HeaderText = "Tổng số bàn thắng";
            this.TongSoBanThangCT.Name = "TongSoBanThangCT";
            this.TongSoBanThangCT.ReadOnly = true;
            // 
            // SoBanThang
            // 
            this.SoBanThang.DataPropertyName = "SoBanThang";
            this.SoBanThang.HeaderText = "Số bàn thắng";
            this.SoBanThang.Name = "SoBanThang";
            this.SoBanThang.ReadOnly = true;
            // 
            // HangSLCT
            // 
            this.HangSLCT.DataPropertyName = "Hang";
            this.HangSLCT.HeaderText = "Hạng";
            this.HangSLCT.Name = "HangSLCT";
            this.HangSLCT.ReadOnly = true;
            // 
            // menuStrip3
            // 
            this.menuStrip3.BackColor = System.Drawing.Color.Azure;
            this.menuStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiBangXepHang_CauThu_MuaGiai,
            this.tsmiBangXepHang_CauThu_VongDau,
            this.tsmiBangXepHang_CauThu_ThoiGian});
            this.menuStrip3.Location = new System.Drawing.Point(3, 3);
            this.menuStrip3.Name = "menuStrip3";
            this.menuStrip3.Size = new System.Drawing.Size(970, 31);
            this.menuStrip3.TabIndex = 0;
            this.menuStrip3.Text = "menuStrip3";
            // 
            // tsmiBangXepHang_CauThu_MuaGiai
            // 
            this.tsmiBangXepHang_CauThu_MuaGiai.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tsmiBangXepHang_CauThu_MuaGiai.ForeColor = System.Drawing.Color.Teal;
            this.tsmiBangXepHang_CauThu_MuaGiai.Name = "tsmiBangXepHang_CauThu_MuaGiai";
            this.tsmiBangXepHang_CauThu_MuaGiai.Size = new System.Drawing.Size(108, 27);
            this.tsmiBangXepHang_CauThu_MuaGiai.Text = "Mã mùa giải";
            // 
            // tsmiBangXepHang_CauThu_VongDau
            // 
            this.tsmiBangXepHang_CauThu_VongDau.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tsmiBangXepHang_CauThu_VongDau.ForeColor = System.Drawing.Color.Teal;
            this.tsmiBangXepHang_CauThu_VongDau.Name = "tsmiBangXepHang_CauThu_VongDau";
            this.tsmiBangXepHang_CauThu_VongDau.Size = new System.Drawing.Size(113, 27);
            this.tsmiBangXepHang_CauThu_VongDau.Text = "Mã vòng đấu";
            // 
            // tpTraCuuCauThu
            // 
            this.tpTraCuuCauThu.BackColor = System.Drawing.Color.Snow;
            this.tpTraCuuCauThu.Controls.Add(this.groupBox2);
            this.tpTraCuuCauThu.Location = new System.Drawing.Point(4, 25);
            this.tpTraCuuCauThu.Name = "tpTraCuuCauThu";
            this.tpTraCuuCauThu.Padding = new System.Windows.Forms.Padding(3);
            this.tpTraCuuCauThu.Size = new System.Drawing.Size(976, 581);
            this.tpTraCuuCauThu.TabIndex = 6;
            this.tpTraCuuCauThu.Text = "Tra cứu cầu thủ";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Azure;
            this.groupBox2.Controls.Add(this.splitContainer3);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(970, 575);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Quy định";
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(3, 18);
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.splitContainer20);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.groupBox14);
            this.splitContainer3.Size = new System.Drawing.Size(964, 554);
            this.splitContainer3.SplitterDistance = 250;
            this.splitContainer3.TabIndex = 0;
            // 
            // splitContainer20
            // 
            this.splitContainer20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer20.Location = new System.Drawing.Point(0, 0);
            this.splitContainer20.Name = "splitContainer20";
            this.splitContainer20.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer20.Panel1
            // 
            this.splitContainer20.Panel1.BackColor = System.Drawing.Color.Azure;
            this.splitContainer20.Panel1.Controls.Add(this.label54);
            // 
            // splitContainer20.Panel2
            // 
            this.splitContainer20.Panel2.BackColor = System.Drawing.Color.Snow;
            this.splitContainer20.Panel2.Controls.Add(this.groupBox20);
            this.splitContainer20.Size = new System.Drawing.Size(250, 554);
            this.splitContainer20.SplitterDistance = 83;
            this.splitContainer20.TabIndex = 0;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.ForeColor = System.Drawing.Color.Teal;
            this.label54.Location = new System.Drawing.Point(37, 18);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(167, 40);
            this.label54.TabIndex = 3;
            this.label54.Text = "QUY ĐỊNH TRA CỨU \r\nCẦU THỦ";
            this.label54.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // groupBox20
            // 
            this.groupBox20.BackColor = System.Drawing.Color.Azure;
            this.groupBox20.Controls.Add(this.groupBox17);
            this.groupBox20.Controls.Add(this.groupBox19);
            this.groupBox20.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox20.ForeColor = System.Drawing.Color.DarkGreen;
            this.groupBox20.Location = new System.Drawing.Point(0, 0);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(270, 467);
            this.groupBox20.TabIndex = 44;
            this.groupBox20.TabStop = false;
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.cbbTraCuu_DoiBong);
            this.groupBox17.Controls.Add(this.label52);
            this.groupBox17.Controls.Add(this.btTraCuu_LocTimKiem);
            this.groupBox17.Controls.Add(this.rbtTraCuu_LoaiCauThu_NuocNgoai);
            this.groupBox17.Controls.Add(this.label50);
            this.groupBox17.Controls.Add(this.cbbTraCuu_MuaGiai);
            this.groupBox17.Controls.Add(this.rbt_TraCuu_LoaiCauThu_VietNam);
            this.groupBox17.Controls.Add(this.label49);
            this.groupBox17.Controls.Add(this.label48);
            this.groupBox17.Controls.Add(this.nudTraCuu_TuoiToiDa);
            this.groupBox17.Controls.Add(this.nudTraCuu_TuoiToiThieu);
            this.groupBox17.Controls.Add(this.label53);
            this.groupBox17.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox17.Location = new System.Drawing.Point(3, 185);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(264, 279);
            this.groupBox17.TabIndex = 44;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "Lọc thông tin";
            // 
            // cbbTraCuu_DoiBong
            // 
            this.cbbTraCuu_DoiBong.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbTraCuu_DoiBong.FormattingEnabled = true;
            this.cbbTraCuu_DoiBong.Location = new System.Drawing.Point(120, 72);
            this.cbbTraCuu_DoiBong.Name = "cbbTraCuu_DoiBong";
            this.cbbTraCuu_DoiBong.Size = new System.Drawing.Size(110, 24);
            this.cbbTraCuu_DoiBong.TabIndex = 43;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(20, 40);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(59, 16);
            this.label52.TabIndex = 42;
            this.label52.Text = "Mùa giải";
            // 
            // btTraCuu_LocTimKiem
            // 
            this.btTraCuu_LocTimKiem.BackColor = System.Drawing.Color.LightCyan;
            this.btTraCuu_LocTimKiem.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btTraCuu_LocTimKiem.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btTraCuu_LocTimKiem.ForeColor = System.Drawing.Color.DarkGreen;
            this.btTraCuu_LocTimKiem.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btTraCuu_LocTimKiem.Location = new System.Drawing.Point(20, 220);
            this.btTraCuu_LocTimKiem.Name = "btTraCuu_LocTimKiem";
            this.btTraCuu_LocTimKiem.Size = new System.Drawing.Size(120, 35);
            this.btTraCuu_LocTimKiem.TabIndex = 41;
            this.btTraCuu_LocTimKiem.Text = "Lọc tìm kiếm";
            this.btTraCuu_LocTimKiem.UseVisualStyleBackColor = false;
            this.btTraCuu_LocTimKiem.Click += new System.EventHandler(this.btTraCuu_LocTimKiem_Click);
            // 
            // rbtTraCuu_LoaiCauThu_NuocNgoai
            // 
            this.rbtTraCuu_LoaiCauThu_NuocNgoai.AutoSize = true;
            this.rbtTraCuu_LoaiCauThu_NuocNgoai.Location = new System.Drawing.Point(40, 160);
            this.rbtTraCuu_LoaiCauThu_NuocNgoai.Name = "rbtTraCuu_LoaiCauThu_NuocNgoai";
            this.rbtTraCuu_LoaiCauThu_NuocNgoai.Size = new System.Drawing.Size(95, 20);
            this.rbtTraCuu_LoaiCauThu_NuocNgoai.TabIndex = 39;
            this.rbtTraCuu_LoaiCauThu_NuocNgoai.Text = "Nước ngoài";
            this.rbtTraCuu_LoaiCauThu_NuocNgoai.UseVisualStyleBackColor = true;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(20, 75);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(62, 16);
            this.label50.TabIndex = 9;
            this.label50.Text = "Đội bóng";
            // 
            // cbbTraCuu_MuaGiai
            // 
            this.cbbTraCuu_MuaGiai.BackColor = System.Drawing.Color.Azure;
            this.cbbTraCuu_MuaGiai.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbTraCuu_MuaGiai.ForeColor = System.Drawing.Color.Teal;
            this.cbbTraCuu_MuaGiai.FormattingEnabled = true;
            this.cbbTraCuu_MuaGiai.Items.AddRange(new object[] {
            "1 a fdsf\t",
            "fdsfs",
            "fdsfds",
            "fdsfds"});
            this.cbbTraCuu_MuaGiai.Location = new System.Drawing.Point(120, 37);
            this.cbbTraCuu_MuaGiai.Name = "cbbTraCuu_MuaGiai";
            this.cbbTraCuu_MuaGiai.Size = new System.Drawing.Size(110, 24);
            this.cbbTraCuu_MuaGiai.TabIndex = 38;
            this.cbbTraCuu_MuaGiai.SelectedIndexChanged += new System.EventHandler(this.cbbTraCuu_MuaGiai_SelectedIndexChanged);
            // 
            // rbt_TraCuu_LoaiCauThu_VietNam
            // 
            this.rbt_TraCuu_LoaiCauThu_VietNam.AutoSize = true;
            this.rbt_TraCuu_LoaiCauThu_VietNam.Checked = true;
            this.rbt_TraCuu_LoaiCauThu_VietNam.Location = new System.Drawing.Point(40, 140);
            this.rbt_TraCuu_LoaiCauThu_VietNam.Name = "rbt_TraCuu_LoaiCauThu_VietNam";
            this.rbt_TraCuu_LoaiCauThu_VietNam.Size = new System.Drawing.Size(81, 20);
            this.rbt_TraCuu_LoaiCauThu_VietNam.TabIndex = 40;
            this.rbt_TraCuu_LoaiCauThu_VietNam.TabStop = true;
            this.rbt_TraCuu_LoaiCauThu_VietNam.Text = "Việt Nam";
            this.rbt_TraCuu_LoaiCauThu_VietNam.UseVisualStyleBackColor = true;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(20, 110);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(79, 16);
            this.label49.TabIndex = 8;
            this.label49.Text = "Loại cầu thủ";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(20, 190);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(49, 16);
            this.label48.TabIndex = 7;
            this.label48.Text = "Độ tuổi";
            // 
            // nudTraCuu_TuoiToiDa
            // 
            this.nudTraCuu_TuoiToiDa.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudTraCuu_TuoiToiDa.Increment = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.nudTraCuu_TuoiToiDa.Location = new System.Drawing.Point(165, 190);
            this.nudTraCuu_TuoiToiDa.Maximum = new decimal(new int[] {
            40,
            0,
            0,
            0});
            this.nudTraCuu_TuoiToiDa.Minimum = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.nudTraCuu_TuoiToiDa.Name = "nudTraCuu_TuoiToiDa";
            this.nudTraCuu_TuoiToiDa.Size = new System.Drawing.Size(45, 20);
            this.nudTraCuu_TuoiToiDa.TabIndex = 36;
            this.nudTraCuu_TuoiToiDa.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
            // 
            // nudTraCuu_TuoiToiThieu
            // 
            this.nudTraCuu_TuoiToiThieu.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudTraCuu_TuoiToiThieu.Increment = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.nudTraCuu_TuoiToiThieu.Location = new System.Drawing.Point(75, 190);
            this.nudTraCuu_TuoiToiThieu.Maximum = new decimal(new int[] {
            40,
            0,
            0,
            0});
            this.nudTraCuu_TuoiToiThieu.Minimum = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.nudTraCuu_TuoiToiThieu.Name = "nudTraCuu_TuoiToiThieu";
            this.nudTraCuu_TuoiToiThieu.Size = new System.Drawing.Size(45, 20);
            this.nudTraCuu_TuoiToiThieu.TabIndex = 35;
            this.nudTraCuu_TuoiToiThieu.Value = new decimal(new int[] {
            16,
            0,
            0,
            0});
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(130, 190);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(31, 16);
            this.label53.TabIndex = 37;
            this.label53.Text = "đến";
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this.rbtTraCuu_TenCauThu);
            this.groupBox19.Controls.Add(this.rbtTraCuu_TenDoiBong);
            this.groupBox19.Controls.Add(this.btTraCuu_TimKiem);
            this.groupBox19.Controls.Add(this.label51);
            this.groupBox19.Controls.Add(this.label47);
            this.groupBox19.Controls.Add(this.tbTraCuuCauThu_ThongTin);
            this.groupBox19.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox19.Location = new System.Drawing.Point(3, 18);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(264, 180);
            this.groupBox19.TabIndex = 43;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "Tra cứu cầu thủ";
            // 
            // rbtTraCuu_TenCauThu
            // 
            this.rbtTraCuu_TenCauThu.AutoSize = true;
            this.rbtTraCuu_TenCauThu.Location = new System.Drawing.Point(40, 70);
            this.rbtTraCuu_TenCauThu.Name = "rbtTraCuu_TenCauThu";
            this.rbtTraCuu_TenCauThu.Size = new System.Drawing.Size(124, 20);
            this.rbtTraCuu_TenCauThu.TabIndex = 39;
            this.rbtTraCuu_TenCauThu.TabStop = true;
            this.rbtTraCuu_TenCauThu.Text = "Theo tên cầu thủ";
            this.rbtTraCuu_TenCauThu.UseVisualStyleBackColor = true;
            // 
            // rbtTraCuu_TenDoiBong
            // 
            this.rbtTraCuu_TenDoiBong.AutoSize = true;
            this.rbtTraCuu_TenDoiBong.Location = new System.Drawing.Point(40, 90);
            this.rbtTraCuu_TenDoiBong.Name = "rbtTraCuu_TenDoiBong";
            this.rbtTraCuu_TenDoiBong.Size = new System.Drawing.Size(135, 20);
            this.rbtTraCuu_TenDoiBong.TabIndex = 40;
            this.rbtTraCuu_TenDoiBong.TabStop = true;
            this.rbtTraCuu_TenDoiBong.Text = "Theo tên đội bóng";
            this.rbtTraCuu_TenDoiBong.UseVisualStyleBackColor = true;
            // 
            // btTraCuu_TimKiem
            // 
            this.btTraCuu_TimKiem.BackColor = System.Drawing.Color.LightCyan;
            this.btTraCuu_TimKiem.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btTraCuu_TimKiem.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btTraCuu_TimKiem.ForeColor = System.Drawing.Color.DarkGreen;
            this.btTraCuu_TimKiem.Location = new System.Drawing.Point(20, 120);
            this.btTraCuu_TimKiem.Name = "btTraCuu_TimKiem";
            this.btTraCuu_TimKiem.Size = new System.Drawing.Size(120, 35);
            this.btTraCuu_TimKiem.TabIndex = 13;
            this.btTraCuu_TimKiem.Text = "Tìm kiếm";
            this.btTraCuu_TimKiem.UseVisualStyleBackColor = false;
            this.btTraCuu_TimKiem.Click += new System.EventHandler(this.btTraCuu_TimKiem_Click);
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(23, 78);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(0, 16);
            this.label51.TabIndex = 10;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(20, 40);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(93, 16);
            this.label47.TabIndex = 6;
            this.label47.Text = "Nhập thông tin";
            // 
            // tbTraCuuCauThu_ThongTin
            // 
            this.tbTraCuuCauThu_ThongTin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbTraCuuCauThu_ThongTin.Location = new System.Drawing.Point(120, 37);
            this.tbTraCuuCauThu_ThongTin.Name = "tbTraCuuCauThu_ThongTin";
            this.tbTraCuuCauThu_ThongTin.Size = new System.Drawing.Size(110, 22);
            this.tbTraCuuCauThu_ThongTin.TabIndex = 5;
            // 
            // groupBox14
            // 
            this.groupBox14.BackColor = System.Drawing.Color.Azure;
            this.groupBox14.Controls.Add(this.dgvTraCuu_DanhSachTraCuu);
            this.groupBox14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox14.Location = new System.Drawing.Point(0, 0);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(710, 554);
            this.groupBox14.TabIndex = 0;
            this.groupBox14.TabStop = false;
            // 
            // dgvTraCuu_DanhSachTraCuu
            // 
            this.dgvTraCuu_DanhSachTraCuu.AllowUserToAddRows = false;
            this.dgvTraCuu_DanhSachTraCuu.AllowUserToDeleteRows = false;
            this.dgvTraCuu_DanhSachTraCuu.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvTraCuu_DanhSachTraCuu.BackgroundColor = System.Drawing.Color.Snow;
            this.dgvTraCuu_DanhSachTraCuu.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTraCuu_DanhSachTraCuu.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ma,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.Loai,
            this.So});
            this.dgvTraCuu_DanhSachTraCuu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvTraCuu_DanhSachTraCuu.Location = new System.Drawing.Point(3, 18);
            this.dgvTraCuu_DanhSachTraCuu.Name = "dgvTraCuu_DanhSachTraCuu";
            this.dgvTraCuu_DanhSachTraCuu.ReadOnly = true;
            this.dgvTraCuu_DanhSachTraCuu.Size = new System.Drawing.Size(704, 533);
            this.dgvTraCuu_DanhSachTraCuu.TabIndex = 42;
            // 
            // ma
            // 
            this.ma.DataPropertyName = "MaMuaGiai";
            this.ma.FillWeight = 103.84F;
            this.ma.HeaderText = "Mã mùa giải";
            this.ma.Name = "ma";
            this.ma.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "MaCauThu";
            this.dataGridViewTextBoxColumn4.FillWeight = 103.84F;
            this.dataGridViewTextBoxColumn4.HeaderText = "Mã cầu thủ";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "TenCauThu";
            this.dataGridViewTextBoxColumn5.FillWeight = 103.84F;
            this.dataGridViewTextBoxColumn5.HeaderText = "Tên cầu thủ";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "TenDoiBong";
            this.dataGridViewTextBoxColumn6.FillWeight = 103.84F;
            this.dataGridViewTextBoxColumn6.HeaderText = "Tên đội";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // Loai
            // 
            this.Loai.DataPropertyName = "MaLoaiCauThu";
            this.Loai.FillWeight = 93.26946F;
            this.Loai.HeaderText = "Loại cầu thủ";
            this.Loai.Name = "Loai";
            this.Loai.ReadOnly = true;
            // 
            // So
            // 
            this.So.DataPropertyName = "TongSoBanThang";
            this.So.FillWeight = 91.37056F;
            this.So.HeaderText = "Số bàn thắng";
            this.So.Name = "So";
            this.So.ReadOnly = true;
            // 
            // tpThayDoiQuyDinh
            // 
            this.tpThayDoiQuyDinh.Controls.Add(this.tcQuyDinh);
            this.tpThayDoiQuyDinh.Location = new System.Drawing.Point(4, 25);
            this.tpThayDoiQuyDinh.Name = "tpThayDoiQuyDinh";
            this.tpThayDoiQuyDinh.Padding = new System.Windows.Forms.Padding(3);
            this.tpThayDoiQuyDinh.Size = new System.Drawing.Size(976, 581);
            this.tpThayDoiQuyDinh.TabIndex = 8;
            this.tpThayDoiQuyDinh.Text = "Quy định";
            this.tpThayDoiQuyDinh.UseVisualStyleBackColor = true;
            // 
            // tcQuyDinh
            // 
            this.tcQuyDinh.Controls.Add(this.tpQuyDinh_TongHopQuyDinh);
            this.tcQuyDinh.Controls.Add(this.tpQuyDinh_SuaQuyDinh);
            this.tcQuyDinh.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcQuyDinh.Location = new System.Drawing.Point(3, 3);
            this.tcQuyDinh.Multiline = true;
            this.tcQuyDinh.Name = "tcQuyDinh";
            this.tcQuyDinh.SelectedIndex = 0;
            this.tcQuyDinh.Size = new System.Drawing.Size(970, 575);
            this.tcQuyDinh.TabIndex = 0;
            this.tcQuyDinh.SelectedIndexChanged += new System.EventHandler(this.tcQuyDinh_SelectedIndexChanged);
            // 
            // tpQuyDinh_TongHopQuyDinh
            // 
            this.tpQuyDinh_TongHopQuyDinh.Controls.Add(this.tcQuyDinh_DanhSachQuyDinh);
            this.tpQuyDinh_TongHopQuyDinh.Location = new System.Drawing.Point(4, 25);
            this.tpQuyDinh_TongHopQuyDinh.Name = "tpQuyDinh_TongHopQuyDinh";
            this.tpQuyDinh_TongHopQuyDinh.Padding = new System.Windows.Forms.Padding(3);
            this.tpQuyDinh_TongHopQuyDinh.Size = new System.Drawing.Size(962, 546);
            this.tpQuyDinh_TongHopQuyDinh.TabIndex = 0;
            this.tpQuyDinh_TongHopQuyDinh.Text = "Danh sách quy định";
            this.tpQuyDinh_TongHopQuyDinh.UseVisualStyleBackColor = true;
            // 
            // tcQuyDinh_DanhSachQuyDinh
            // 
            this.tcQuyDinh_DanhSachQuyDinh.Controls.Add(this.tpQuyDinh_QuyDinhChung);
            this.tcQuyDinh_DanhSachQuyDinh.Controls.Add(this.tp_QuyDinh_ChiTiet);
            this.tcQuyDinh_DanhSachQuyDinh.Controls.Add(this.tpQuyDinh_ChiTietQuyDinh);
            this.tcQuyDinh_DanhSachQuyDinh.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcQuyDinh_DanhSachQuyDinh.Location = new System.Drawing.Point(3, 3);
            this.tcQuyDinh_DanhSachQuyDinh.Name = "tcQuyDinh_DanhSachQuyDinh";
            this.tcQuyDinh_DanhSachQuyDinh.SelectedIndex = 0;
            this.tcQuyDinh_DanhSachQuyDinh.Size = new System.Drawing.Size(956, 540);
            this.tcQuyDinh_DanhSachQuyDinh.TabIndex = 0;
            // 
            // tpQuyDinh_QuyDinhChung
            // 
            this.tpQuyDinh_QuyDinhChung.Controls.Add(this.groupBox24);
            this.tpQuyDinh_QuyDinhChung.Location = new System.Drawing.Point(4, 25);
            this.tpQuyDinh_QuyDinhChung.Name = "tpQuyDinh_QuyDinhChung";
            this.tpQuyDinh_QuyDinhChung.Padding = new System.Windows.Forms.Padding(3);
            this.tpQuyDinh_QuyDinhChung.Size = new System.Drawing.Size(948, 511);
            this.tpQuyDinh_QuyDinhChung.TabIndex = 0;
            this.tpQuyDinh_QuyDinhChung.Text = "Quy định chung";
            this.tpQuyDinh_QuyDinhChung.UseVisualStyleBackColor = true;
            // 
            // groupBox24
            // 
            this.groupBox24.Controls.Add(this.textBox8);
            this.groupBox24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox24.Location = new System.Drawing.Point(3, 3);
            this.groupBox24.Name = "groupBox24";
            this.groupBox24.Size = new System.Drawing.Size(942, 505);
            this.groupBox24.TabIndex = 2;
            this.groupBox24.TabStop = false;
            this.groupBox24.Text = "Quy định sử dụng phầm mềm";
            // 
            // textBox8
            // 
            this.textBox8.BackColor = System.Drawing.Color.Azure;
            this.textBox8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox8.ForeColor = System.Drawing.Color.Teal;
            this.textBox8.Location = new System.Drawing.Point(3, 18);
            this.textBox8.Multiline = true;
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(936, 484);
            this.textBox8.TabIndex = 0;
            this.textBox8.Text = resources.GetString("textBox8.Text");
            // 
            // tp_QuyDinh_ChiTiet
            // 
            this.tp_QuyDinh_ChiTiet.Controls.Add(this.groupBox27);
            this.tp_QuyDinh_ChiTiet.Controls.Add(this.groupBox26);
            this.tp_QuyDinh_ChiTiet.Controls.Add(this.groupBox25);
            this.tp_QuyDinh_ChiTiet.Location = new System.Drawing.Point(4, 25);
            this.tp_QuyDinh_ChiTiet.Name = "tp_QuyDinh_ChiTiet";
            this.tp_QuyDinh_ChiTiet.Padding = new System.Windows.Forms.Padding(3);
            this.tp_QuyDinh_ChiTiet.Size = new System.Drawing.Size(948, 511);
            this.tp_QuyDinh_ChiTiet.TabIndex = 1;
            this.tp_QuyDinh_ChiTiet.Text = "Chi tiết quy định";
            this.tp_QuyDinh_ChiTiet.UseVisualStyleBackColor = true;
            // 
            // groupBox27
            // 
            this.groupBox27.Controls.Add(this.textBox11);
            this.groupBox27.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupBox27.Location = new System.Drawing.Point(606, 3);
            this.groupBox27.Name = "groupBox27";
            this.groupBox27.Size = new System.Drawing.Size(339, 505);
            this.groupBox27.TabIndex = 11;
            this.groupBox27.TabStop = false;
            this.groupBox27.Text = "Quy định tiếp nhận hồ sơ đăng ký đội bóng";
            // 
            // textBox11
            // 
            this.textBox11.AllowDrop = true;
            this.textBox11.BackColor = System.Drawing.Color.Azure;
            this.textBox11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox11.ForeColor = System.Drawing.Color.Teal;
            this.textBox11.Location = new System.Drawing.Point(3, 18);
            this.textBox11.Multiline = true;
            this.textBox11.Name = "textBox11";
            this.textBox11.ReadOnly = true;
            this.textBox11.Size = new System.Drawing.Size(333, 484);
            this.textBox11.TabIndex = 11;
            this.textBox11.Text = resources.GetString("textBox11.Text");
            // 
            // groupBox26
            // 
            this.groupBox26.Controls.Add(this.textBox10);
            this.groupBox26.Location = new System.Drawing.Point(0, 230);
            this.groupBox26.Name = "groupBox26";
            this.groupBox26.Size = new System.Drawing.Size(600, 274);
            this.groupBox26.TabIndex = 10;
            this.groupBox26.TabStop = false;
            this.groupBox26.Text = "Quy định tiền đăng ký đội bóng và cầu thủ";
            // 
            // textBox10
            // 
            this.textBox10.BackColor = System.Drawing.Color.Snow;
            this.textBox10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox10.ForeColor = System.Drawing.Color.Teal;
            this.textBox10.Location = new System.Drawing.Point(3, 18);
            this.textBox10.Multiline = true;
            this.textBox10.Name = "textBox10";
            this.textBox10.ReadOnly = true;
            this.textBox10.Size = new System.Drawing.Size(594, 253);
            this.textBox10.TabIndex = 0;
            this.textBox10.Text = resources.GetString("textBox10.Text");
            // 
            // groupBox25
            // 
            this.groupBox25.Controls.Add(this.textBox9);
            this.groupBox25.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox25.Location = new System.Drawing.Point(0, 0);
            this.groupBox25.Name = "groupBox25";
            this.groupBox25.Size = new System.Drawing.Size(600, 228);
            this.groupBox25.TabIndex = 9;
            this.groupBox25.TabStop = false;
            this.groupBox25.Text = "Quy định đăng ký giải đấu";
            // 
            // textBox9
            // 
            this.textBox9.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.textBox9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox9.ForeColor = System.Drawing.Color.Teal;
            this.textBox9.Location = new System.Drawing.Point(3, 18);
            this.textBox9.Multiline = true;
            this.textBox9.Name = "textBox9";
            this.textBox9.ReadOnly = true;
            this.textBox9.Size = new System.Drawing.Size(594, 207);
            this.textBox9.TabIndex = 7;
            this.textBox9.Text = resources.GetString("textBox9.Text");
            // 
            // tpQuyDinh_ChiTietQuyDinh
            // 
            this.tpQuyDinh_ChiTietQuyDinh.Controls.Add(this.groupBox29);
            this.tpQuyDinh_ChiTietQuyDinh.Controls.Add(this.groupBox28);
            this.tpQuyDinh_ChiTietQuyDinh.Location = new System.Drawing.Point(4, 25);
            this.tpQuyDinh_ChiTietQuyDinh.Name = "tpQuyDinh_ChiTietQuyDinh";
            this.tpQuyDinh_ChiTietQuyDinh.Padding = new System.Windows.Forms.Padding(3);
            this.tpQuyDinh_ChiTietQuyDinh.Size = new System.Drawing.Size(948, 511);
            this.tpQuyDinh_ChiTietQuyDinh.TabIndex = 2;
            this.tpQuyDinh_ChiTietQuyDinh.Text = "Chi tiết quy định";
            this.tpQuyDinh_ChiTietQuyDinh.UseVisualStyleBackColor = true;
            // 
            // groupBox29
            // 
            this.groupBox29.Controls.Add(this.textBox13);
            this.groupBox29.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupBox29.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox29.Location = new System.Drawing.Point(485, 3);
            this.groupBox29.Name = "groupBox29";
            this.groupBox29.Size = new System.Drawing.Size(460, 505);
            this.groupBox29.TabIndex = 7;
            this.groupBox29.TabStop = false;
            this.groupBox29.Text = "Quy định cập nhật kết quả trận đấu";
            // 
            // textBox13
            // 
            this.textBox13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox13.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox13.ForeColor = System.Drawing.Color.Teal;
            this.textBox13.Location = new System.Drawing.Point(3, 18);
            this.textBox13.Multiline = true;
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(454, 484);
            this.textBox13.TabIndex = 6;
            this.textBox13.Text = resources.GetString("textBox13.Text");
            // 
            // groupBox28
            // 
            this.groupBox28.Controls.Add(this.textBox12);
            this.groupBox28.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox28.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox28.Location = new System.Drawing.Point(3, 3);
            this.groupBox28.Name = "groupBox28";
            this.groupBox28.Size = new System.Drawing.Size(480, 505);
            this.groupBox28.TabIndex = 2;
            this.groupBox28.TabStop = false;
            this.groupBox28.Text = "Bảng quy định tạo lịch thi đấu";
            // 
            // textBox12
            // 
            this.textBox12.BackColor = System.Drawing.Color.Azure;
            this.textBox12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox12.ForeColor = System.Drawing.Color.Teal;
            this.textBox12.Location = new System.Drawing.Point(3, 18);
            this.textBox12.Multiline = true;
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(474, 484);
            this.textBox12.TabIndex = 0;
            this.textBox12.Text = resources.GetString("textBox12.Text");
            // 
            // tpQuyDinh_SuaQuyDinh
            // 
            this.tpQuyDinh_SuaQuyDinh.Controls.Add(this.groupBox31);
            this.tpQuyDinh_SuaQuyDinh.Controls.Add(this.groupBox30);
            this.tpQuyDinh_SuaQuyDinh.Location = new System.Drawing.Point(4, 25);
            this.tpQuyDinh_SuaQuyDinh.Name = "tpQuyDinh_SuaQuyDinh";
            this.tpQuyDinh_SuaQuyDinh.Padding = new System.Windows.Forms.Padding(3);
            this.tpQuyDinh_SuaQuyDinh.Size = new System.Drawing.Size(962, 546);
            this.tpQuyDinh_SuaQuyDinh.TabIndex = 1;
            this.tpQuyDinh_SuaQuyDinh.Text = "Sửa quy định";
            this.tpQuyDinh_SuaQuyDinh.UseVisualStyleBackColor = true;
            // 
            // groupBox31
            // 
            this.groupBox31.Controls.Add(this.tbQuyDinh_ThoiDiemGhiBanToiDa);
            this.groupBox31.Controls.Add(this.tbQuyDinh_TuoiToiThieu);
            this.groupBox31.Controls.Add(this.tbQuyDinh_TuoiToiDa);
            this.groupBox31.Controls.Add(this.tbQuyDinh_SoLuongCauThuNuocNgoai);
            this.groupBox31.Controls.Add(this.tbQuyDinh_SoLuongCauThuToiThieu);
            this.groupBox31.Controls.Add(this.tbQuyDinh_SoLuongCauThuToiDa);
            this.groupBox31.Controls.Add(this.label38);
            this.groupBox31.Controls.Add(this.label55);
            this.groupBox31.Controls.Add(this.label56);
            this.groupBox31.Controls.Add(this.label57);
            this.groupBox31.Controls.Add(this.label58);
            this.groupBox31.Controls.Add(this.label59);
            this.groupBox31.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupBox31.Location = new System.Drawing.Point(464, 3);
            this.groupBox31.Name = "groupBox31";
            this.groupBox31.Size = new System.Drawing.Size(495, 540);
            this.groupBox31.TabIndex = 4;
            this.groupBox31.TabStop = false;
            this.groupBox31.Text = "Danh sách quy định có thể thay đổi";
            // 
            // tbQuyDinh_ThoiDiemGhiBanToiDa
            // 
            this.tbQuyDinh_ThoiDiemGhiBanToiDa.Location = new System.Drawing.Point(377, 297);
            this.tbQuyDinh_ThoiDiemGhiBanToiDa.Name = "tbQuyDinh_ThoiDiemGhiBanToiDa";
            this.tbQuyDinh_ThoiDiemGhiBanToiDa.ReadOnly = true;
            this.tbQuyDinh_ThoiDiemGhiBanToiDa.Size = new System.Drawing.Size(100, 22);
            this.tbQuyDinh_ThoiDiemGhiBanToiDa.TabIndex = 7;
            // 
            // tbQuyDinh_TuoiToiThieu
            // 
            this.tbQuyDinh_TuoiToiThieu.Location = new System.Drawing.Point(377, 247);
            this.tbQuyDinh_TuoiToiThieu.Name = "tbQuyDinh_TuoiToiThieu";
            this.tbQuyDinh_TuoiToiThieu.ReadOnly = true;
            this.tbQuyDinh_TuoiToiThieu.Size = new System.Drawing.Size(100, 22);
            this.tbQuyDinh_TuoiToiThieu.TabIndex = 7;
            // 
            // tbQuyDinh_TuoiToiDa
            // 
            this.tbQuyDinh_TuoiToiDa.Location = new System.Drawing.Point(377, 197);
            this.tbQuyDinh_TuoiToiDa.Name = "tbQuyDinh_TuoiToiDa";
            this.tbQuyDinh_TuoiToiDa.ReadOnly = true;
            this.tbQuyDinh_TuoiToiDa.Size = new System.Drawing.Size(100, 22);
            this.tbQuyDinh_TuoiToiDa.TabIndex = 7;
            // 
            // tbQuyDinh_SoLuongCauThuNuocNgoai
            // 
            this.tbQuyDinh_SoLuongCauThuNuocNgoai.Location = new System.Drawing.Point(377, 147);
            this.tbQuyDinh_SoLuongCauThuNuocNgoai.Name = "tbQuyDinh_SoLuongCauThuNuocNgoai";
            this.tbQuyDinh_SoLuongCauThuNuocNgoai.ReadOnly = true;
            this.tbQuyDinh_SoLuongCauThuNuocNgoai.Size = new System.Drawing.Size(100, 22);
            this.tbQuyDinh_SoLuongCauThuNuocNgoai.TabIndex = 7;
            // 
            // tbQuyDinh_SoLuongCauThuToiThieu
            // 
            this.tbQuyDinh_SoLuongCauThuToiThieu.Location = new System.Drawing.Point(377, 97);
            this.tbQuyDinh_SoLuongCauThuToiThieu.Name = "tbQuyDinh_SoLuongCauThuToiThieu";
            this.tbQuyDinh_SoLuongCauThuToiThieu.ReadOnly = true;
            this.tbQuyDinh_SoLuongCauThuToiThieu.Size = new System.Drawing.Size(100, 22);
            this.tbQuyDinh_SoLuongCauThuToiThieu.TabIndex = 7;
            // 
            // tbQuyDinh_SoLuongCauThuToiDa
            // 
            this.tbQuyDinh_SoLuongCauThuToiDa.Location = new System.Drawing.Point(377, 48);
            this.tbQuyDinh_SoLuongCauThuToiDa.Name = "tbQuyDinh_SoLuongCauThuToiDa";
            this.tbQuyDinh_SoLuongCauThuToiDa.ReadOnly = true;
            this.tbQuyDinh_SoLuongCauThuToiDa.Size = new System.Drawing.Size(100, 22);
            this.tbQuyDinh_SoLuongCauThuToiDa.TabIndex = 7;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(30, 50);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(256, 16);
            this.label38.TabIndex = 1;
            this.label38.Text = "Số lượng cầu thủ tối đa trong một đội bóng";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(30, 100);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(268, 16);
            this.label55.TabIndex = 2;
            this.label55.Text = "Số lượng cầu thủ tối thiểu trong một đội bóng";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(30, 250);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(153, 16);
            this.label56.TabIndex = 3;
            this.label56.Text = "Tuổi tối thiểu của cầu thủ";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(30, 150);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(325, 16);
            this.label57.TabIndex = 4;
            this.label57.Text = "Số lượng cầu thủ nước ngoài tối đa trong một đội bóng";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(30, 200);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(141, 16);
            this.label58.TabIndex = 5;
            this.label58.Text = "Tuổi tối đa của cầu thủ";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(30, 300);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(151, 16);
            this.label59.TabIndex = 6;
            this.label59.Text = "Thời điểm ghi bàn tối đa";
            // 
            // groupBox30
            // 
            this.groupBox30.Controls.Add(this.label23);
            this.groupBox30.Controls.Add(this.nudQuyDinh_ThoiDiemGhiBanToiDa);
            this.groupBox30.Controls.Add(this.btQuyDinh_XacNhan);
            this.groupBox30.Controls.Add(this.nudQuyDinh_TuoiToiThieu);
            this.groupBox30.Controls.Add(this.label24);
            this.groupBox30.Controls.Add(this.nudQuyDinh_TuoiToiDa);
            this.groupBox30.Controls.Add(this.nudQuyDinh_SoLuongCauThuNuocNgoai);
            this.groupBox30.Controls.Add(this.label27);
            this.groupBox30.Controls.Add(this.nudQuyDinh_SoLuongCauThuToiThieu);
            this.groupBox30.Controls.Add(this.label25);
            this.groupBox30.Controls.Add(this.nudQuyDinh_SoLuongCauThuToiDa);
            this.groupBox30.Controls.Add(this.label26);
            this.groupBox30.Controls.Add(this.label28);
            this.groupBox30.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox30.Location = new System.Drawing.Point(3, 3);
            this.groupBox30.Name = "groupBox30";
            this.groupBox30.Size = new System.Drawing.Size(455, 540);
            this.groupBox30.TabIndex = 3;
            this.groupBox30.TabStop = false;
            this.groupBox30.Text = "Thay đổi quy định";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(30, 50);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(256, 16);
            this.label23.TabIndex = 0;
            this.label23.Text = "Số lượng cầu thủ tối đa trong một đội bóng";
            // 
            // nudQuyDinh_ThoiDiemGhiBanToiDa
            // 
            this.nudQuyDinh_ThoiDiemGhiBanToiDa.Location = new System.Drawing.Point(380, 298);
            this.nudQuyDinh_ThoiDiemGhiBanToiDa.Name = "nudQuyDinh_ThoiDiemGhiBanToiDa";
            this.nudQuyDinh_ThoiDiemGhiBanToiDa.Size = new System.Drawing.Size(40, 22);
            this.nudQuyDinh_ThoiDiemGhiBanToiDa.TabIndex = 1;
            // 
            // btQuyDinh_XacNhan
            // 
            this.btQuyDinh_XacNhan.Location = new System.Drawing.Point(33, 344);
            this.btQuyDinh_XacNhan.Name = "btQuyDinh_XacNhan";
            this.btQuyDinh_XacNhan.Size = new System.Drawing.Size(75, 23);
            this.btQuyDinh_XacNhan.TabIndex = 2;
            this.btQuyDinh_XacNhan.Text = "Xác nhận";
            this.btQuyDinh_XacNhan.UseVisualStyleBackColor = true;
            this.btQuyDinh_XacNhan.Click += new System.EventHandler(this.btQuyDinh_XacNhan_Click);
            // 
            // nudQuyDinh_TuoiToiThieu
            // 
            this.nudQuyDinh_TuoiToiThieu.Location = new System.Drawing.Point(380, 248);
            this.nudQuyDinh_TuoiToiThieu.Name = "nudQuyDinh_TuoiToiThieu";
            this.nudQuyDinh_TuoiToiThieu.Size = new System.Drawing.Size(40, 22);
            this.nudQuyDinh_TuoiToiThieu.TabIndex = 1;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(30, 100);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(268, 16);
            this.label24.TabIndex = 0;
            this.label24.Text = "Số lượng cầu thủ tối thiểu trong một đội bóng";
            // 
            // nudQuyDinh_TuoiToiDa
            // 
            this.nudQuyDinh_TuoiToiDa.Location = new System.Drawing.Point(380, 198);
            this.nudQuyDinh_TuoiToiDa.Name = "nudQuyDinh_TuoiToiDa";
            this.nudQuyDinh_TuoiToiDa.Size = new System.Drawing.Size(40, 22);
            this.nudQuyDinh_TuoiToiDa.TabIndex = 1;
            // 
            // nudQuyDinh_SoLuongCauThuNuocNgoai
            // 
            this.nudQuyDinh_SoLuongCauThuNuocNgoai.Location = new System.Drawing.Point(380, 148);
            this.nudQuyDinh_SoLuongCauThuNuocNgoai.Name = "nudQuyDinh_SoLuongCauThuNuocNgoai";
            this.nudQuyDinh_SoLuongCauThuNuocNgoai.Size = new System.Drawing.Size(40, 22);
            this.nudQuyDinh_SoLuongCauThuNuocNgoai.TabIndex = 1;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(30, 250);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(153, 16);
            this.label27.TabIndex = 0;
            this.label27.Text = "Tuổi tối thiểu của cầu thủ";
            // 
            // nudQuyDinh_SoLuongCauThuToiThieu
            // 
            this.nudQuyDinh_SoLuongCauThuToiThieu.Location = new System.Drawing.Point(380, 98);
            this.nudQuyDinh_SoLuongCauThuToiThieu.Name = "nudQuyDinh_SoLuongCauThuToiThieu";
            this.nudQuyDinh_SoLuongCauThuToiThieu.Size = new System.Drawing.Size(40, 22);
            this.nudQuyDinh_SoLuongCauThuToiThieu.TabIndex = 1;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(30, 150);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(325, 16);
            this.label25.TabIndex = 0;
            this.label25.Text = "Số lượng cầu thủ nước ngoài tối đa trong một đội bóng";
            // 
            // nudQuyDinh_SoLuongCauThuToiDa
            // 
            this.nudQuyDinh_SoLuongCauThuToiDa.Location = new System.Drawing.Point(380, 48);
            this.nudQuyDinh_SoLuongCauThuToiDa.Name = "nudQuyDinh_SoLuongCauThuToiDa";
            this.nudQuyDinh_SoLuongCauThuToiDa.Size = new System.Drawing.Size(40, 22);
            this.nudQuyDinh_SoLuongCauThuToiDa.TabIndex = 1;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(30, 200);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(141, 16);
            this.label26.TabIndex = 0;
            this.label26.Text = "Tuổi tối đa của cầu thủ";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(30, 300);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(151, 16);
            this.label28.TabIndex = 0;
            this.label28.Text = "Thời điểm ghi bàn tối đa";
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.Color.Azure;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tcMenu);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.OldLace;
            this.splitContainer1.Size = new System.Drawing.Size(984, 668);
            this.splitContainer1.SplitterDistance = 610;
            this.splitContainer1.TabIndex = 7;
            // 
            // groupBox4
            // 
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(0, 0);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(394, 540);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(56, 104);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(83, 16);
            this.label17.TabIndex = 0;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(56, 62);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(78, 16);
            this.label18.TabIndex = 1;
            // 
            // cbbTiepNhanHoSo_DanhSachGiaiDauCoTheThamDu
            // 
            this.cbbTiepNhanHoSo_DanhSachGiaiDauCoTheThamDu.FormattingEnabled = true;
            this.cbbTiepNhanHoSo_DanhSachGiaiDauCoTheThamDu.Location = new System.Drawing.Point(168, 59);
            this.cbbTiepNhanHoSo_DanhSachGiaiDauCoTheThamDu.Name = "cbbTiepNhanHoSo_DanhSachGiaiDauCoTheThamDu";
            this.cbbTiepNhanHoSo_DanhSachGiaiDauCoTheThamDu.Size = new System.Drawing.Size(162, 21);
            this.cbbTiepNhanHoSo_DanhSachGiaiDauCoTheThamDu.TabIndex = 2;
            this.cbbTiepNhanHoSo_DanhSachGiaiDauCoTheThamDu.SelectedIndexChanged += new System.EventHandler(this.cbbTiepNhanHoSo_DanhSachGiaiDauCoTheThamDu_SelectedIndexChanged);
            // 
            // tbTiepNhanHoSo_DanhSachGiaiDauCoTheThamDu
            // 
            this.tbTiepNhanHoSo_DanhSachGiaiDauCoTheThamDu.Location = new System.Drawing.Point(168, 98);
            this.tbTiepNhanHoSo_DanhSachGiaiDauCoTheThamDu.Multiline = true;
            this.tbTiepNhanHoSo_DanhSachGiaiDauCoTheThamDu.Name = "tbTiepNhanHoSo_DanhSachGiaiDauCoTheThamDu";
            this.tbTiepNhanHoSo_DanhSachGiaiDauCoTheThamDu.ReadOnly = true;
            this.tbTiepNhanHoSo_DanhSachGiaiDauCoTheThamDu.Size = new System.Drawing.Size(162, 75);
            this.tbTiepNhanHoSo_DanhSachGiaiDauCoTheThamDu.TabIndex = 3;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(58, 230);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(169, 16);
            this.label19.TabIndex = 8;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(61, 260);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(168, 16);
            this.label20.TabIndex = 10;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(57, 290);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(203, 16);
            this.label36.TabIndex = 9;
            // 
            // tbTiepNhanHoSo_SoLuongDoiToiDa
            // 
            this.tbTiepNhanHoSo_SoLuongDoiToiDa.Location = new System.Drawing.Point(275, 221);
            this.tbTiepNhanHoSo_SoLuongDoiToiDa.Name = "tbTiepNhanHoSo_SoLuongDoiToiDa";
            this.tbTiepNhanHoSo_SoLuongDoiToiDa.ReadOnly = true;
            this.tbTiepNhanHoSo_SoLuongDoiToiDa.Size = new System.Drawing.Size(55, 20);
            this.tbTiepNhanHoSo_SoLuongDoiToiDa.TabIndex = 13;
            // 
            // tbTiepNhanHoSo_SoLuongDoiDaDangKy
            // 
            this.tbTiepNhanHoSo_SoLuongDoiDaDangKy.Location = new System.Drawing.Point(275, 260);
            this.tbTiepNhanHoSo_SoLuongDoiDaDangKy.Name = "tbTiepNhanHoSo_SoLuongDoiDaDangKy";
            this.tbTiepNhanHoSo_SoLuongDoiDaDangKy.ReadOnly = true;
            this.tbTiepNhanHoSo_SoLuongDoiDaDangKy.Size = new System.Drawing.Size(55, 20);
            this.tbTiepNhanHoSo_SoLuongDoiDaDangKy.TabIndex = 12;
            // 
            // tbTiepNhanHoSo_SoLuongDoiDaHoanTatDangKy
            // 
            this.tbTiepNhanHoSo_SoLuongDoiDaHoanTatDangKy.Location = new System.Drawing.Point(275, 290);
            this.tbTiepNhanHoSo_SoLuongDoiDaHoanTatDangKy.Name = "tbTiepNhanHoSo_SoLuongDoiDaHoanTatDangKy";
            this.tbTiepNhanHoSo_SoLuongDoiDaHoanTatDangKy.ReadOnly = true;
            this.tbTiepNhanHoSo_SoLuongDoiDaHoanTatDangKy.Size = new System.Drawing.Size(55, 20);
            this.tbTiepNhanHoSo_SoLuongDoiDaHoanTatDangKy.TabIndex = 11;
            // 
            // btTiepNhanHoSo_DangKy
            // 
            this.btTiepNhanHoSo_DangKy.BackColor = System.Drawing.Color.Chartreuse;
            this.btTiepNhanHoSo_DangKy.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btTiepNhanHoSo_DangKy.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btTiepNhanHoSo_DangKy.Location = new System.Drawing.Point(73, 328);
            this.btTiepNhanHoSo_DangKy.Name = "btTiepNhanHoSo_DangKy";
            this.btTiepNhanHoSo_DangKy.Size = new System.Drawing.Size(91, 39);
            this.btTiepNhanHoSo_DangKy.TabIndex = 14;
            this.btTiepNhanHoSo_DangKy.Text = "Đăng ký";
            this.btTiepNhanHoSo_DangKy.UseVisualStyleBackColor = false;
            this.btTiepNhanHoSo_DangKy.Click += new System.EventHandler(this.btTiepNhanHoSo_DangKy_Click);
            // 
            // splitContainer9
            // 
            this.splitContainer9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer9.Location = new System.Drawing.Point(0, 0);
            this.splitContainer9.Name = "splitContainer9";
            this.splitContainer9.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer9.Panel1
            // 
            this.splitContainer9.Panel1.BackColor = System.Drawing.Color.Azure;
            this.splitContainer9.Size = new System.Drawing.Size(558, 540);
            this.splitContainer9.SplitterDistance = 136;
            this.splitContainer9.TabIndex = 0;
            // 
            // gb_QuyDinhDangKy
            // 
            this.gb_QuyDinhDangKy.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gb_QuyDinhDangKy.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gb_QuyDinhDangKy.Location = new System.Drawing.Point(0, 0);
            this.gb_QuyDinhDangKy.Name = "gb_QuyDinhDangKy";
            this.gb_QuyDinhDangKy.Size = new System.Drawing.Size(558, 400);
            this.gb_QuyDinhDangKy.TabIndex = 0;
            this.gb_QuyDinhDangKy.TabStop = false;
            // 
            // textBox1
            // 
            this.textBox1.AllowDrop = true;
            this.textBox1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(3, 25);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(552, 372);
            this.textBox1.TabIndex = 0;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Teal;
            this.label14.Location = new System.Drawing.Point(50, 35);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(420, 31);
            this.label14.TabIndex = 0;
            // 
            // tsmiBangXepHang_CauThu_ThoiGian
            // 
            this.tsmiBangXepHang_CauThu_ThoiGian.BackColor = System.Drawing.Color.Azure;
            this.tsmiBangXepHang_CauThu_ThoiGian.Font = new System.Drawing.Font("Segoe UI", 11F);
            this.tsmiBangXepHang_CauThu_ThoiGian.ForeColor = System.Drawing.Color.Teal;
            this.tsmiBangXepHang_CauThu_ThoiGian.Name = "tsmiBangXepHang_CauThu_ThoiGian";
            this.tsmiBangXepHang_CauThu_ThoiGian.ReadOnly = true;
            this.tsmiBangXepHang_CauThu_ThoiGian.Size = new System.Drawing.Size(100, 27);
            this.tsmiBangXepHang_CauThu_ThoiGian.Text = "Thời gian                                       .";
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(984, 668);
            this.Controls.Add(this.splitContainer1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Menu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quản lý đội bóng";
            this.Load += new System.EventHandler(this.Menu_Load);
            this.tcMenu.ResumeLayout(false);
            this.tpHome.ResumeLayout(false);
            this.tcHOME.ResumeLayout(false);
            this.tpHOME_DangKyGiaiDau.ResumeLayout(false);
            this.splitContainer12.Panel1.ResumeLayout(false);
            this.splitContainer12.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer12)).EndInit();
            this.splitContainer12.ResumeLayout(false);
            this.gb_HOME_DangKyGiaiDau_QuyDinh.ResumeLayout(false);
            this.gb_HOME_DangKyGiaiDau_QuyDinh.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudHOME_DangKyGiaiDau_SoLuongDoi)).EndInit();
            this.gb_HOME_DangKyGiaiDau_DanhSachGiai.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDanhSachMuaGiai)).EndInit();
            this.tpHOME_TienDangKyDoiBong.ResumeLayout(false);
            this.splitContainer15.Panel1.ResumeLayout(false);
            this.splitContainer15.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer15)).EndInit();
            this.splitContainer15.ResumeLayout(false);
            this.splitContainer16.Panel1.ResumeLayout(false);
            this.splitContainer16.Panel1.PerformLayout();
            this.splitContainer16.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer16)).EndInit();
            this.splitContainer16.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tpHome_HoSoGiaiDau.ResumeLayout(false);
            this.tcHoSoGiaiDau.ResumeLayout(false);
            this.tpHoSoGiaiDau_HoSoDoiBong.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDanhSachDoiBongTongHop)).EndInit();
            this.tpHoSoGiaiDau_HoSoCauThu.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDanhSachCauThuTongHop)).EndInit();
            this.tpTiepNhanHoSo.ResumeLayout(false);
            this.tcTiepNhanHoSo.ResumeLayout(false);
            this.tpTiepNhanHoSo_QuyDinh_DangKyGiaiDau.ResumeLayout(false);
            this.splitContainer21.Panel1.ResumeLayout(false);
            this.splitContainer21.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer21)).EndInit();
            this.splitContainer21.ResumeLayout(false);
            this.splitContainer22.Panel1.ResumeLayout(false);
            this.splitContainer22.Panel2.ResumeLayout(false);
            this.splitContainer22.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer22)).EndInit();
            this.splitContainer22.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox21.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTiepNhanHoSo_DangKyGiaiDau)).EndInit();
            this.tpTiepNhanHoSo_DangKyTTDoiBong.ResumeLayout(false);
            this.tcTiepNhanHoSoDangKyThongTinDoiBong.ResumeLayout(false);
            this.tpTiepNhanHoSo_DangKyThongTinDoiBOng.ResumeLayout(false);
            this.splitContainer18.Panel1.ResumeLayout(false);
            this.splitContainer18.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer18)).EndInit();
            this.splitContainer18.ResumeLayout(false);
            this.gbTiepNhanHoSo_DanhSachDoiBongCoTheDangKy.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvChiTietThongTinDoiBOng)).EndInit();
            this.gpTiepNhanHOSo_DangKyThongTinDoiBong.ResumeLayout(false);
            this.gpTiepNhanHOSo_DangKyThongTinDoiBong.PerformLayout();
            this.gbTiepNhanHoSo_DanhSachDoiBongDaDangKy.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTiepNhanHoSo_DanhSachDoiBOngDaDangKy)).EndInit();
            this.tpTiepNhanHoSoDangKyThongTinCauThu.ResumeLayout(false);
            this.splitContainer19.Panel1.ResumeLayout(false);
            this.splitContainer19.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer19)).EndInit();
            this.splitContainer19.ResumeLayout(false);
            this.gbTiepNhanHoSo_DangKyThongTinDoiBong_DanhSachCauThuCoTheDangKy.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvChiTietTTCT)).EndInit();
            this.gbTiepNhanHOSo_DangKYThongTinCauThu.ResumeLayout(false);
            this.gbTiepNhanHOSo_DangKYThongTinCauThu.PerformLayout();
            this.groupBox18.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTNHS_DKDB_DKCT_DanhSachCauThu)).EndInit();
            this.tpTaoLichThiDau.ResumeLayout(false);
            this.tcTaoLichThiDau.ResumeLayout(false);
            this.tpTaoLichThiDau_QuyDinh.ResumeLayout(false);
            this.splitContainer11.Panel1.ResumeLayout(false);
            this.splitContainer11.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer11)).EndInit();
            this.splitContainer11.ResumeLayout(false);
            this.splitContainer23.Panel1.ResumeLayout(false);
            this.splitContainer23.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer23)).EndInit();
            this.splitContainer23.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox22.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTaoLichThiDau_DanhSachMuaGiai)).EndInit();
            this.tpTaoLichThiDau_TaoLichThiDau.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.gbTaoLichThiDau_TaoLichThiDau_QuyDinh.ResumeLayout(false);
            this.gbTaoLichThiDau_TaoLichThiDau_QuyDinh.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudstep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudfirstmatch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudlastmatch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudThoiGianNghiGiuaHaiLuot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudKhoangCachVongDau)).EndInit();
            this.gbTaoLichThiDau_TaoLichThiDau_TenMuaGiai.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvListXepHang)).EndInit();
            this.tpCapNhatKetQua.ResumeLayout(false);
            this.tcCapNhatKetQua.ResumeLayout(false);
            this.tpCapNhatKetQua_QuyDinh.ResumeLayout(false);
            this.splitContainer17.Panel1.ResumeLayout(false);
            this.splitContainer17.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer17)).EndInit();
            this.splitContainer17.ResumeLayout(false);
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCapNhatKetQua_DanhSachTranDau)).EndInit();
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.tpCapNhatKetQua_CapNhatKetQuaTranDau.ResumeLayout(false);
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
            this.splitContainer4.ResumeLayout(false);
            this.splitContainer10.Panel1.ResumeLayout(false);
            this.splitContainer10.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer10)).EndInit();
            this.splitContainer10.ResumeLayout(false);
            this.gbCapNhatKetQua_ChuNha.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCapNhatKetQua_ChuNha)).EndInit();
            this.gbCapNhatKetQua_Khach.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCapNhatKetQua_Khach)).EndInit();
            this.splitContainer24.Panel1.ResumeLayout(false);
            this.splitContainer24.Panel2.ResumeLayout(false);
            this.splitContainer24.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer24)).EndInit();
            this.splitContainer24.ResumeLayout(false);
            this.groupBox23.ResumeLayout(false);
            this.groupBox23.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudGiay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPhut)).EndInit();
            this.tpMenuLichThiDau.ResumeLayout(false);
            this.tpMenuLichThiDau.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLichThiDau)).EndInit();
            this.menuStrip4.ResumeLayout(false);
            this.menuStrip4.PerformLayout();
            this.tpBangXepHang.ResumeLayout(false);
            this.tpBangXepHang.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBangXepHangBangXepHang)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tpSoLieuThongKe.ResumeLayout(false);
            this.tpSoLieuThongKe.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSoLieuThongKeCauThu)).EndInit();
            this.menuStrip3.ResumeLayout(false);
            this.menuStrip3.PerformLayout();
            this.tpTraCuuCauThu.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.splitContainer20.Panel1.ResumeLayout(false);
            this.splitContainer20.Panel1.PerformLayout();
            this.splitContainer20.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer20)).EndInit();
            this.splitContainer20.ResumeLayout(false);
            this.groupBox20.ResumeLayout(false);
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudTraCuu_TuoiToiDa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTraCuu_TuoiToiThieu)).EndInit();
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTraCuu_DanhSachTraCuu)).EndInit();
            this.tpThayDoiQuyDinh.ResumeLayout(false);
            this.tcQuyDinh.ResumeLayout(false);
            this.tpQuyDinh_TongHopQuyDinh.ResumeLayout(false);
            this.tcQuyDinh_DanhSachQuyDinh.ResumeLayout(false);
            this.tpQuyDinh_QuyDinhChung.ResumeLayout(false);
            this.groupBox24.ResumeLayout(false);
            this.groupBox24.PerformLayout();
            this.tp_QuyDinh_ChiTiet.ResumeLayout(false);
            this.groupBox27.ResumeLayout(false);
            this.groupBox27.PerformLayout();
            this.groupBox26.ResumeLayout(false);
            this.groupBox26.PerformLayout();
            this.groupBox25.ResumeLayout(false);
            this.groupBox25.PerformLayout();
            this.tpQuyDinh_ChiTietQuyDinh.ResumeLayout(false);
            this.groupBox29.ResumeLayout(false);
            this.groupBox29.PerformLayout();
            this.groupBox28.ResumeLayout(false);
            this.groupBox28.PerformLayout();
            this.tpQuyDinh_SuaQuyDinh.ResumeLayout(false);
            this.groupBox31.ResumeLayout(false);
            this.groupBox31.PerformLayout();
            this.groupBox30.ResumeLayout(false);
            this.groupBox30.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudQuyDinh_ThoiDiemGhiBanToiDa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudQuyDinh_TuoiToiThieu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudQuyDinh_TuoiToiDa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudQuyDinh_SoLuongCauThuNuocNgoai)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudQuyDinh_SoLuongCauThuToiThieu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudQuyDinh_SoLuongCauThuToiDa)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer9)).EndInit();
            this.splitContainer9.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tcMenu;
        private System.Windows.Forms.TabPage tpHome;
        private System.Windows.Forms.TabPage tpTiepNhanHoSo;
        private System.Windows.Forms.TabPage tpTaoLichThiDau;
        private System.Windows.Forms.TabPage tpCapNhatKetQua;
        private System.Windows.Forms.TabControl tcCapNhatKetQua;
        private System.Windows.Forms.TabPage tpCapNhatKetQua_QuyDinh;
        private System.Windows.Forms.TabPage tpBangXepHang;
        private System.Windows.Forms.DataGridView dgvBangXepHangBangXepHang;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaDoiBong;
        private System.Windows.Forms.DataGridViewTextBoxColumn SoTranDaThiDau;
        private System.Windows.Forms.DataGridViewTextBoxColumn TongSoTranThang;
        private System.Windows.Forms.DataGridViewTextBoxColumn TongSoTranHoa;
        private System.Windows.Forms.DataGridViewTextBoxColumn TongSoTranThua;
        private System.Windows.Forms.DataGridViewTextBoxColumn TongSoBanThang;
        private System.Windows.Forms.DataGridViewTextBoxColumn TongSoBanThua;
        private System.Windows.Forms.DataGridViewTextBoxColumn TongHieuSo;
        private System.Windows.Forms.DataGridViewTextBoxColumn TongDiem;
        private System.Windows.Forms.DataGridViewTextBoxColumn Hang;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsmiBangXepHang_MuaGiai;
        private System.Windows.Forms.ToolStripMenuItem tsmiBangXepHang_VongDau;
        private System.Windows.Forms.TabPage tpSoLieuThongKe;
        private System.Windows.Forms.TabPage tpTraCuuCauThu;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TabControl tcTaoLichThiDau;
        private System.Windows.Forms.TabPage tpTaoLichThiDau_QuyDinh;
        private System.Windows.Forms.TabPage tpTaoLichThiDau_TaoLichThiDau;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.DataGridView dgvListXepHang;
        private System.Windows.Forms.TabControl tcHOME;
        private System.Windows.Forms.TabPage tpHOME_DangKyGiaiDau;
        private System.Windows.Forms.SplitContainer splitContainer12;
        private System.Windows.Forms.GroupBox gb_HOME_DangKyGiaiDau_QuyDinh;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lbMaGiai;
        private System.Windows.Forms.GroupBox gb_HOME_DangKyGiaiDau_DanhSachGiai;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.NumericUpDown nudHOME_DangKyGiaiDau_SoLuongDoi;
        private System.Windows.Forms.DataGridView dgvDanhSachMuaGiai;
        private System.Windows.Forms.TextBox tbHOME_DangKyGiaiDau_MaGiaiDau;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button btHOME_DangKyGiaiDau_DangKy;
        private System.Windows.Forms.ComboBox cbbHOME_DangKyGiaiDau_TenGiaiDau;
        private System.Windows.Forms.GroupBox gbTaoLichThiDau_TaoLichThiDau_TenMuaGiai;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaMuaGiaiDSMG;
        private System.Windows.Forms.DataGridViewTextBoxColumn TenMuaGiaiDSMG;
        private System.Windows.Forms.DataGridViewTextBoxColumn SoLuongDoiTuyenDSMG;
        private System.Windows.Forms.TabPage tpHOME_TienDangKyDoiBong;
        private System.Windows.Forms.SplitContainer splitContainer15;
        private System.Windows.Forms.SplitContainer splitContainer16;
        private System.Windows.Forms.TextBox tbHOME_DangKyDanhSachDoiBong_TieuDe;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TabPage tpHome_HoSoGiaiDau;
        private System.Windows.Forms.TabControl tcHoSoGiaiDau;
        private System.Windows.Forms.TabPage tpHoSoGiaiDau_HoSoDoiBong;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.DataGridView dgvDanhSachDoiBongTongHop;
        private System.Windows.Forms.TabPage tpHoSoGiaiDau_HoSoCauThu;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.DataGridView dgvDanhSachCauThuTongHop;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaDoiBongHS;
        private System.Windows.Forms.DataGridViewTextBoxColumn TenDoiBongHS;
        private System.Windows.Forms.DataGridViewTextBoxColumn SanNhaHS;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaCauThuHS;
        private System.Windows.Forms.DataGridViewTextBoxColumn TenCauThuHS;
        private System.Windows.Forms.DataGridViewTextBoxColumn NgaySinhHS;
        private System.Windows.Forms.DataGridViewTextBoxColumn LoaiCauThuHS;
        private System.Windows.Forms.DataGridViewTextBoxColumn TongSoBanThangHS;
        private System.Windows.Forms.SplitContainer splitContainer11;
        private System.Windows.Forms.SplitContainer splitContainer17;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.DataGridView dgvSoLieuThongKeCauThu;
        private System.Windows.Forms.MenuStrip menuStrip3;
        private System.Windows.Forms.ToolStripMenuItem tsmiBangXepHang_CauThu_MuaGiai;
        private System.Windows.Forms.ToolStripMenuItem tsmiBangXepHang_CauThu_VongDau;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.DataGridView dgvTraCuu_DanhSachTraCuu;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.SplitContainer splitContainer20;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaVongDau;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaTranDau;
        private System.Windows.Forms.DataGridViewTextBoxColumn NgayThiDau;
        private System.Windows.Forms.DataGridViewTextBoxColumn GioThiDau;
        private System.Windows.Forms.DataGridViewTextBoxColumn ChuNha;
        private System.Windows.Forms.DataGridViewTextBoxColumn Khach;
        private System.Windows.Forms.SplitContainer splitContainer23;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.GroupBox groupBox22;
        private System.Windows.Forms.DataGridView dgvTaoLichThiDau_DanhSachMuaGiai;
        private System.Windows.Forms.TabControl tcTiepNhanHoSo;
        private System.Windows.Forms.TabPage tpTiepNhanHoSo_QuyDinh_DangKyGiaiDau;
        private System.Windows.Forms.SplitContainer splitContainer21;
        private System.Windows.Forms.SplitContainer splitContainer22;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.DataGridView dgvTiepNhanHoSo_DangKyGiaiDau;
        private System.Windows.Forms.TabPage tpTiepNhanHoSo_DangKyTTDoiBong;
        private System.Windows.Forms.TabControl tcTiepNhanHoSoDangKyThongTinDoiBong;
        private System.Windows.Forms.TabPage tpTiepNhanHoSo_DangKyThongTinDoiBOng;
        private System.Windows.Forms.SplitContainer splitContainer18;
        private System.Windows.Forms.GroupBox gbTiepNhanHoSo_DanhSachDoiBongCoTheDangKy;
        private System.Windows.Forms.DataGridView dgvChiTietThongTinDoiBOng;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaDoiBongTC;
        private System.Windows.Forms.DataGridViewTextBoxColumn TenDoiBOngTC;
        private System.Windows.Forms.DataGridViewButtonColumn AddDB;
        private System.Windows.Forms.GroupBox gpTiepNhanHOSo_DangKyThongTinDoiBong;
        private System.Windows.Forms.GroupBox gbTiepNhanHoSo_DanhSachDoiBongDaDangKy;
        private System.Windows.Forms.DataGridView dgvTiepNhanHoSo_DanhSachDoiBOngDaDangKy;
        private System.Windows.Forms.ComboBox cbbTNHS_DKDB_DKDB_MaDoiBong;
        private System.Windows.Forms.Button btTiepNhanHoSo_DangKyDoiBong_DangKyDoiBong;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox tbTNHS_DKDB_DKDB_SoLuongCauThu;
        private System.Windows.Forms.TextBox tbTNHS_DKDB_DKDB_SanNha;
        private System.Windows.Forms.TextBox tbTNHS_DKDB_DKDB_TenDoiBong;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TabPage tpTiepNhanHoSoDangKyThongTinCauThu;
        private System.Windows.Forms.SplitContainer splitContainer19;
        private System.Windows.Forms.GroupBox gbTiepNhanHoSo_DangKyThongTinDoiBong_DanhSachCauThuCoTheDangKy;
        private System.Windows.Forms.DataGridView dgvChiTietTTCT;
        private System.Windows.Forms.GroupBox gbTiepNhanHOSo_DangKYThongTinCauThu;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.DataGridView dgvTNHS_DKDB_DKCT_DanhSachCauThu;
        private System.Windows.Forms.TextBox tbTNHS_DKDB_DKCT_NgaySinh;
        private System.Windows.Forms.ComboBox cbbTNHS_DKDB_DKCT_MaCauThu;
        private System.Windows.Forms.TextBox tbTNHS_DKDB_DKCT_LoaiCauThu;
        private System.Windows.Forms.TextBox tbTNHS_DKDB_DKCT_TenCauThu;
        private System.Windows.Forms.Button btTiepNhanHoSo_DangKyDoiBong_XacNhanHoanTatDangKyDoiBong;
        private System.Windows.Forms.Button btTiepNhanHoSo_DangKyDoiBong_DangKyCauThu;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox cbbTiepNhanHoSo_DanhSachGiaiDauCoTheThamDu;
        private System.Windows.Forms.TextBox tbTiepNhanHoSo_DanhSachGiaiDauCoTheThamDu;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox tbTiepNhanHoSo_SoLuongDoiToiDa;
        private System.Windows.Forms.TextBox tbTiepNhanHoSo_SoLuongDoiDaDangKy;
        private System.Windows.Forms.TextBox tbTiepNhanHoSo_SoLuongDoiDaHoanTatDangKy;
        private System.Windows.Forms.Button btTiepNhanHoSo_DangKy;
        private System.Windows.Forms.SplitContainer splitContainer9;
        private System.Windows.Forms.GroupBox gb_QuyDinhDangKy;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DataGridView dgvCapNhatKetQua_DanhSachTranDau;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem tsmiCapNhatKetQua_MuaGiai;
        private System.Windows.Forms.ToolStripMenuItem tsmiCapNhatKetQua_VongDau;
        private System.Windows.Forms.TabPage tpCapNhatKetQua_CapNhatKetQuaTranDau;
        private System.Windows.Forms.SplitContainer splitContainer4;
        private System.Windows.Forms.SplitContainer splitContainer10;
        private System.Windows.Forms.GroupBox gbCapNhatKetQua_ChuNha;
        private System.Windows.Forms.DataGridView dgvCapNhatKetQua_ChuNha;
        private System.Windows.Forms.GroupBox gbCapNhatKetQua_Khach;
        private System.Windows.Forms.DataGridView dgvCapNhatKetQua_Khach;
        private System.Windows.Forms.SplitContainer splitContainer24;
        private System.Windows.Forms.GroupBox groupBox23;
        private System.Windows.Forms.TextBox tbCapNhatKetQuaTranDau_CapNhat_TranDau;
        private System.Windows.Forms.TextBox tbCapNhatKetQuaTranDau_CapNhat_VongDau;
        private System.Windows.Forms.TextBox tbCapNhatKetQuaTranDau_CapNhat_MuaGiai;
        private System.Windows.Forms.TextBox tbkhach;
        private System.Windows.Forms.TextBox tbbanthangkhach;
        private System.Windows.Forms.TextBox tbbanthangchunha;
        private System.Windows.Forms.TextBox tbtrangthai;
        private System.Windows.Forms.TextBox tbgiothidau;
        private System.Windows.Forms.TextBox tbngaythidau;
        private System.Windows.Forms.TextBox tbchunha;
        private System.Windows.Forms.Button btCapNhatKetQua_CapNhatThongTin_CapNhat;
        private System.Windows.Forms.TextBox tbTenDoiBong;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rbtbanthangtructiep;
        private System.Windows.Forms.TextBox tbTenCauThu;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.RadioButton rbtpenalty;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown nudGiay;
        private System.Windows.Forms.RadioButton rbtphanluoi;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.NumericUpDown nudPhut;
        private System.Windows.Forms.TextBox tbCapNhatKetQua_MaDoiBong_hidden;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaCauThu;
        private System.Windows.Forms.DataGridViewTextBoxColumn HoTenCauThu;
        private System.Windows.Forms.DataGridViewTextBoxColumn TongSoBanThangCT;
        private System.Windows.Forms.DataGridViewTextBoxColumn SoBanThang;
        private System.Windows.Forms.DataGridViewTextBoxColumn HangSLCT;
        private System.Windows.Forms.GroupBox groupBox20;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.ComboBox cbbTraCuu_DoiBong;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Button btTraCuu_LocTimKiem;
        private System.Windows.Forms.RadioButton rbtTraCuu_LoaiCauThu_NuocNgoai;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.ComboBox cbbTraCuu_MuaGiai;
        private System.Windows.Forms.RadioButton rbt_TraCuu_LoaiCauThu_VietNam;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.NumericUpDown nudTraCuu_TuoiToiDa;
        private System.Windows.Forms.NumericUpDown nudTraCuu_TuoiToiThieu;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.RadioButton rbtTraCuu_TenCauThu;
        private System.Windows.Forms.RadioButton rbtTraCuu_TenDoiBong;
        private System.Windows.Forms.Button btTraCuu_TimKiem;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox tbTraCuuCauThu_ThongTin;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btHOME_TienDangKyHOSoCauThu_XacNhanDangKy;
        private System.Windows.Forms.RadioButton rbtHOME_TienDangKyHOSoCauThu_LoaiCauThu_NuocNgoai;
        private System.Windows.Forms.RadioButton rbtHOME_TienDangKyHOSoCauThu_LoaiCauThu_VietNam;
        private System.Windows.Forms.DateTimePicker dtpHOME_TienDangKyHOSoCauThu_NgaySinh;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox tbHOME_TienDangKyHOSoCauThu_TenCauThu;
        private System.Windows.Forms.TextBox tbHOME_TienDangKyHOSoCauThu_MaCauThu;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btDangKyHoSoDoiBong_XacNhanDangKy_DangKy;
        private System.Windows.Forms.Button btDangKyHoSoDoiBong_ThongTinDangKy_DangKy;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox tbDangKyHoSoDoiBong_ThongTinDangKy_SanNha;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox tbDangKyHoSoDoiBong_ThongTinDangKy_TenDoiBong;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox tbDangKyHoSoDoiBong_ThongTinDangKy_MaDoiBong;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaTranDauCNKQ;
        private System.Windows.Forms.DataGridViewTextBoxColumn NgayThiDauCNKQ;
        private System.Windows.Forms.DataGridViewTextBoxColumn GioThiDauCNKQ;
        private System.Windows.Forms.DataGridViewTextBoxColumn TrangThai;
        private System.Windows.Forms.DataGridViewTextBoxColumn ChuNhaCNKQ;
        private System.Windows.Forms.DataGridViewTextBoxColumn KhachCNKQ;
        private System.Windows.Forms.DataGridViewButtonColumn CapNhatCNKQ;
        private System.Windows.Forms.DataGridViewButtonColumn HoanTatKNKQ;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaGiaiDau;
        private System.Windows.Forms.DataGridViewTextBoxColumn TenGiaiDau;
        private System.Windows.Forms.DataGridViewTextBoxColumn SoLuongDoiToiDa;
        private System.Windows.Forms.DataGridViewTextBoxColumn SoDoiDADangKy;
        private System.Windows.Forms.DataGridViewTextBoxColumn TrangThaiDK;
        private System.Windows.Forms.DataGridViewButtonColumn CapNhatGiaiDau;
        private System.Windows.Forms.DataGridViewButtonColumn HoanTatGiaiDau;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaDB;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn SLCT;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewButtonColumn Update;
        private System.Windows.Forms.DataGridViewButtonColumn dataGridViewButtonColumn1;
        private System.Windows.Forms.DataGridViewButtonColumn XacNhan;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaCTTC;
        private System.Windows.Forms.DataGridViewTextBoxColumn TenCTTC;
        private System.Windows.Forms.DataGridViewButtonColumn ThemCauThu;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaCT;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewButtonColumn XoaCT;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewButtonColumn dataGridViewButtonColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaCauThuCN;
        private System.Windows.Forms.DataGridViewTextBoxColumn TenCauThuCN;
        private System.Windows.Forms.DataGridViewButtonColumn CapNhatCN;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewButtonColumn dataGridViewButtonColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn ma;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Loai;
        private System.Windows.Forms.DataGridViewTextBoxColumn So;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ToolStripTextBox tsmiBangXepHang_NgayBatDau;
        private System.Windows.Forms.TabPage tpMenuLichThiDau;
        private System.Windows.Forms.DataGridView dgvLichThiDau;
        private System.Windows.Forms.MenuStrip menuStrip4;
        private System.Windows.Forms.ToolStripMenuItem tsmiLichThiDau_MaMuaGiai;
        private System.Windows.Forms.ToolStripMenuItem tsmiLichThiDau_MaVongDau;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaVongDauLTD;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaTranDauLTD;
        private System.Windows.Forms.DataGridViewTextBoxColumn TinhTrangLTD;
        private System.Windows.Forms.DataGridViewTextBoxColumn NgayThiDauLTD;
        private System.Windows.Forms.DataGridViewTextBoxColumn GioThiDauLTD;
        private System.Windows.Forms.DataGridViewTextBoxColumn ChuNhaLTD;
        private System.Windows.Forms.DataGridViewTextBoxColumn DiemChuNha;
        private System.Windows.Forms.DataGridViewTextBoxColumn DiemKhachLTD;
        private System.Windows.Forms.DataGridViewTextBoxColumn KhachLTD;
        private System.Windows.Forms.TabPage tpThayDoiQuyDinh;
        private System.Windows.Forms.TabControl tcQuyDinh;
        private System.Windows.Forms.TabPage tpQuyDinh_TongHopQuyDinh;
        private System.Windows.Forms.TabPage tpQuyDinh_SuaQuyDinh;
        private System.Windows.Forms.TabControl tcQuyDinh_DanhSachQuyDinh;
        private System.Windows.Forms.TabPage tpQuyDinh_QuyDinhChung;
        private System.Windows.Forms.GroupBox groupBox24;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TabPage tp_QuyDinh_ChiTiet;
        private System.Windows.Forms.GroupBox groupBox27;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.GroupBox groupBox26;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.GroupBox groupBox25;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TabPage tpQuyDinh_ChiTietQuyDinh;
        private System.Windows.Forms.GroupBox groupBox29;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.GroupBox groupBox28;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.GroupBox groupBox30;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.NumericUpDown nudQuyDinh_ThoiDiemGhiBanToiDa;
        private System.Windows.Forms.Button btQuyDinh_XacNhan;
        private System.Windows.Forms.NumericUpDown nudQuyDinh_TuoiToiThieu;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.NumericUpDown nudQuyDinh_TuoiToiDa;
        private System.Windows.Forms.NumericUpDown nudQuyDinh_SoLuongCauThuNuocNgoai;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.NumericUpDown nudQuyDinh_SoLuongCauThuToiThieu;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.NumericUpDown nudQuyDinh_SoLuongCauThuToiDa;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.GroupBox groupBox31;
        private System.Windows.Forms.TextBox tbQuyDinh_ThoiDiemGhiBanToiDa;
        private System.Windows.Forms.TextBox tbQuyDinh_TuoiToiThieu;
        private System.Windows.Forms.TextBox tbQuyDinh_TuoiToiDa;
        private System.Windows.Forms.TextBox tbQuyDinh_SoLuongCauThuNuocNgoai;
        private System.Windows.Forms.TextBox tbQuyDinh_SoLuongCauThuToiThieu;
        private System.Windows.Forms.TextBox tbQuyDinh_SoLuongCauThuToiDa;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.GroupBox gbTaoLichThiDau_TaoLichThiDau_QuyDinh;
        private System.Windows.Forms.Button btTaoLichThiDau_TaoLichThiDau;
        private System.Windows.Forms.Button btTaoLichThiDau_XacNhanLichThiDau;
        private System.Windows.Forms.NumericUpDown nudstep;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown nudfirstmatch;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown nudlastmatch;
        private System.Windows.Forms.NumericUpDown nudThoiGianNghiGiuaHaiLuot;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown nudKhoangCachVongDau;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpNgayBatDau;
        private System.Windows.Forms.ToolStripTextBox tsmiBangXepHang_CauThu_ThoiGian;
    }
}

