﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BusinessLayer;
using DTO;

namespace GUI
{
    public partial class FormDangNhap : Form
    {
        public FormDangNhap()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult h = MessageBox.Show("Bạn có chắc muốn thoát không ?","Error", MessageBoxButtons.OKCancel);
            if (h == DialogResult.OK)
            {
                Application.Exit();
            }

        }

        private void btDangNhap_DangNhap_Click(object sender, EventArgs e)
        {
            BUSKiemTraDangNhap ktDangNhap = new BUSKiemTraDangNhap();
            int kt = ktDangNhap.KiemTra(tbDangNhap_TenTaiKhoan.Text, tbDangNhap_MatKhau.Text);
            if (tbDangNhap_TenTaiKhoan.Text == "")
            {
                MessageBox.Show("Vui lòng nhập tên đăng nhập", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            else if (tbDangNhap_MatKhau.Text == "")
            {
                MessageBox.Show("Vui lòng nhập mật khẩu", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (kt == -2)
            {
                MessageBox.Show("Không tồn tại tài khoản");
                return;
            }
            else if(kt == -1)
            {
                MessageBox.Show("Mật khẩu không đúng");
                return;
            }
            else if (kt == 1)
            {
                this.Hide();
                var menu = new Menu(1);
                menu.FormClosed += (s, args) => this.Close();
                menu.Show();
            }
            else if (kt == 2)
            {
                this.Hide();
                var menu = new Menu(2);
                menu.FormClosed += (s, args) => this.Close();
                menu.Show();
            }
        }

        private void FormDangNhap_Load(object sender, EventArgs e)
        {
        }

        private void FormDangNhap_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btDangNhap_DangNhap.PerformClick();
            }
        }
    }
}
