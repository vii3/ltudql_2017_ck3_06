﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BusinessLayer;

namespace GUI
{
    public partial class Menu : Form
    {
        #region Biến toàn cục
        TransferData data;
        UpdateData uData;
        List<DTOMuaGiai> danhSachMuaGiai;
        int _maTaiKhoan;
        #endregion

        #region Khởi tạo
        public Menu(int maTaiKhoan)
        {
            InitializeComponent();
            _maTaiKhoan = maTaiKhoan;
            this.tcMenu.DrawMode = TabDrawMode.OwnerDrawFixed;
            this.tcMenu.DrawItem += tcMenu_DrawItem;

            this.tcTiepNhanHoSo.DrawMode = TabDrawMode.OwnerDrawFixed;
            this.tcTiepNhanHoSo.DrawItem += tcTiepNhanHoSo_DrawItem;

            this.tcTaoLichThiDau.DrawMode = TabDrawMode.OwnerDrawFixed;
            this.tcTaoLichThiDau.DrawItem += tcTaoLichThiDau_DrawItem;

            this.tcQuyDinh.DrawMode = TabDrawMode.OwnerDrawFixed;
            this.tcQuyDinh.DrawItem += tcQuyDinh_DrawItem;

            this.tcHOME.DrawMode = TabDrawMode.OwnerDrawFixed;
            this.tcHOME.DrawItem += tcHOME_DrawItem;
            
            this.tcHoSoGiaiDau.DrawMode = TabDrawMode.OwnerDrawFixed;
            this.tcHoSoGiaiDau.DrawItem += tcHoSoGiaiDau_DrawItem;
            
            this.tcTiepNhanHoSoDangKyThongTinDoiBong.DrawMode = TabDrawMode.OwnerDrawFixed;
            this.tcTiepNhanHoSoDangKyThongTinDoiBong.DrawItem += tcTiepNhanHoSoDangKyThongTinDoiBong_DrawItem;

            this.tcCapNhatKetQua.DrawMode = TabDrawMode.OwnerDrawFixed;
            this.tcCapNhatKetQua.DrawItem += tcCapNhatKetQua_DrawItem;
            
            this.tcQuyDinh_DanhSachQuyDinh.DrawMode = TabDrawMode.OwnerDrawFixed;
            this.tcQuyDinh_DanhSachQuyDinh.DrawItem += tcQuyDinh_DanhSachQuyDinh_DrawItem;
        }

        private void Menu_Load(object sender, EventArgs e)
        {
            data = new TransferData();
            uData = new UpdateData();
            danhSachMuaGiai = new List<DTOMuaGiai>();

            if (_maTaiKhoan == 2)
            {
                tcHOME.TabPages.Remove(tpHOME_DangKyGiaiDau);
                tcHOME.TabPages.Remove(tpHOME_TienDangKyDoiBong);

                tcMenu.TabPages.Remove(tpTiepNhanHoSo);
                tcMenu.TabPages.Remove(tpTaoLichThiDau);
                tcMenu.TabPages.Remove(tpCapNhatKetQua);
                tcMenu.TabPages.Remove(tpThayDoiQuyDinh);
            }

            var w = GUI.Properties.Resources.edit.Width;
            var h = GUI.Properties.Resources.edit.Height;

            #region test
            //// Thêm danh sách cầu thủ
            //for(int i = 0; i < 400; i++)
            //{
            //    DTOCauThu _newCauThu = new DTOCauThu();
            //    _newCauThu.MaCauThu = "CT" + i.ToString("000");
            //    _newCauThu.HoTenCauThu = "Cầu thủ " + i.ToString();
            //    _newCauThu.NgaySinh = Convert.ToDateTime("1/1/2000");
            //    _newCauThu.MaLoaiCauThu = 1;
            //    uData.InsertCauThu(_newCauThu);
            //}
            //// Thêm danh sách đội bóng
            //for(int i = 0; i < 20; i++)
            //{
            //    DTODoiBong _newDoiBong = new DTODoiBong();
            //    _newDoiBong.MaDoiBong = "DB" + i.ToString("000");
            //    _newDoiBong.TenDoiBong = "Đội bóng " + i.ToString();
            //    _newDoiBong.SanNha = "Sân nhà " + i.ToString();
            //    uData.InsertDoiBong(_newDoiBong);
            //}

            //// Thêm danh sách đội bóng tham gia giải đấu
            //for (int i = 0; i < 20; i++)
            //{
            //    DTODoiBongThamGiaGiaiDau _newDoiBong = new DTODoiBongThamGiaGiaiDau();
            //    _newDoiBong.MaMuaGiai = "MG001";
            //    _newDoiBong.MaDoiBong = "DB" + i.ToString("000");
            //    _newDoiBong.TenDoiBong = "Đội bóng " + i.ToString();
            //    uData.InsertDoiBongThamGiaGiaiDau(_newDoiBong);
            //}

            //// Thêm danh sách cầu thủ thuộc đội bóng tham gia giải đấu
            //int madb = 0;
            //int j = 0;
            //for (int i = 0; i < 400; i++)
            //{
            //    DTOCauThuThuocDoiBongThamGiaGiaiDau _newCauThu = new DTOCauThuThuocDoiBongThamGiaGiaiDau();
            //    _newCauThu.MaMuaGiai = "MG001";
            //    _newCauThu.MaCauThu = "CT" + i.ToString("000");
            //    _newCauThu.TenCauThu = "Cầu thủ " + i.ToString();
            //    _newCauThu.MaLoaiCauThu = 1;
            //    _newCauThu.MaDoiBong = "DB" + madb.ToString("000");
            //    _newCauThu.TenDoiBong = "Đội bóng " + madb.ToString();
            //    j++;
            //    if (j == 20)
            //    {
            //        j = 0;
            //        madb++;
            //    }
            //    uData.InsertCauThuThamGiaGiaiDau(_newCauThu);
            //}
            #endregion
        }
        #endregion

        #region Tô màu tabpage

        private void tcMenu_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index == tcMenu.SelectedIndex)
            {
                e.Graphics.DrawString(tcMenu.TabPages[e.Index].Text,
                    new Font("Arial", 10.25f, FontStyle.Regular),
                    Brushes.DarkRed,
                    new PointF(e.Bounds.X + 3, e.Bounds.Y + 3));
            }
            else
            {
                e.Graphics.DrawString(tcMenu.TabPages[e.Index].Text,
                    tcMenu.Font,
                    Brushes.DarkGreen,
                    new PointF(e.Bounds.X + 3, e.Bounds.Y + 3));
            }
        }
        private void tcHOME_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index == tcHOME.SelectedIndex)
            {
                e.Graphics.DrawString(tcHOME.TabPages[e.Index].Text,
                    new Font("Arial", 10.25f, FontStyle.Regular),
                    Brushes.DarkRed,
                    new PointF(e.Bounds.X + 3, e.Bounds.Y + 3));
            }
            else
            {
                e.Graphics.DrawString(tcHOME.TabPages[e.Index].Text,
                    tcMenu.Font,
                    Brushes.DarkGreen,
                    new PointF(e.Bounds.X + 3, e.Bounds.Y + 3));
            }
        }
        private void tcHoSoGiaiDau_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index == tcHoSoGiaiDau.SelectedIndex)
            {
                e.Graphics.DrawString(tcHoSoGiaiDau.TabPages[e.Index].Text,
                    new Font("Arial", 10.25f, FontStyle.Regular),
                    Brushes.DarkRed,
                    new PointF(e.Bounds.X + 3, e.Bounds.Y + 3));
            }
            else
            {
                e.Graphics.DrawString(tcHoSoGiaiDau.TabPages[e.Index].Text,
                    tcMenu.Font,
                    Brushes.DarkGreen,
                    new PointF(e.Bounds.X + 3, e.Bounds.Y + 3));
            }
        }
        private void tcTiepNhanHoSo_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index == tcTiepNhanHoSo.SelectedIndex)
            {
                e.Graphics.DrawString(tcTiepNhanHoSo.TabPages[e.Index].Text,
                    new Font("Arial", 10.25f, FontStyle.Regular),
                    Brushes.DarkRed,
                    new PointF(e.Bounds.X + 3, e.Bounds.Y + 3));
            }
            else
            {
                e.Graphics.DrawString(tcTiepNhanHoSo.TabPages[e.Index].Text,
                    tcMenu.Font,
                    Brushes.DarkGreen,
                    new PointF(e.Bounds.X + 3, e.Bounds.Y + 3));
            }
        }
        private void tcTiepNhanHoSoDangKyThongTinDoiBong_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index == tcTiepNhanHoSoDangKyThongTinDoiBong.SelectedIndex)
            {
                e.Graphics.DrawString(tcTiepNhanHoSoDangKyThongTinDoiBong.TabPages[e.Index].Text,
                    new Font("Arial", 10.25f, FontStyle.Regular),
                    Brushes.DarkRed,
                    new PointF(e.Bounds.X + 3, e.Bounds.Y + 3));
            }
            else
            {
                e.Graphics.DrawString(tcTiepNhanHoSoDangKyThongTinDoiBong.TabPages[e.Index].Text,
                    tcMenu.Font,
                    Brushes.DarkGreen,
                    new PointF(e.Bounds.X + 3, e.Bounds.Y + 3));
            }
        }
        private void tcCapNhatKetQua_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index == tcCapNhatKetQua.SelectedIndex)
            {
                e.Graphics.DrawString(tcCapNhatKetQua.TabPages[e.Index].Text,
                    new Font("Arial", 10.25f, FontStyle.Regular),
                    Brushes.DarkRed,
                    new PointF(e.Bounds.X + 3, e.Bounds.Y + 3));
            }
            else
            {
                e.Graphics.DrawString(tcCapNhatKetQua.TabPages[e.Index].Text,
                    tcMenu.Font,
                    Brushes.DarkGreen,
                    new PointF(e.Bounds.X + 3, e.Bounds.Y + 3));
            }
        }
        private void tcTaoLichThiDau_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index == tcTaoLichThiDau.SelectedIndex)
            {
                e.Graphics.DrawString(tcTaoLichThiDau.TabPages[e.Index].Text,
                    new Font("Arial", 10.25f, FontStyle.Regular),
                    Brushes.DarkRed,
                    new PointF(e.Bounds.X + 3, e.Bounds.Y + 3));
            }
            else
            {
                e.Graphics.DrawString(tcTaoLichThiDau.TabPages[e.Index].Text,
                    tcMenu.Font,
                    Brushes.DarkGreen,
                    new PointF(e.Bounds.X + 3, e.Bounds.Y + 3));
            }
        }
        private void tcQuyDinh_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index == tcQuyDinh.SelectedIndex)
            {
                e.Graphics.DrawString(tcQuyDinh.TabPages[e.Index].Text,
                    new Font("Arial", 10.25f, FontStyle.Regular),
                    Brushes.DarkRed,
                    new PointF(e.Bounds.X + 3, e.Bounds.Y + 3));
            }
            else
            {
                e.Graphics.DrawString(tcQuyDinh.TabPages[e.Index].Text,
                    tcMenu.Font,
                    Brushes.DarkGreen,
                    new PointF(e.Bounds.X + 3, e.Bounds.Y + 3));
            }
        }
        private void tcQuyDinh_DanhSachQuyDinh_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index == tcQuyDinh_DanhSachQuyDinh.SelectedIndex)
            {
                e.Graphics.DrawString(tcQuyDinh_DanhSachQuyDinh.TabPages[e.Index].Text,
                    new Font("Arial", 10.25f, FontStyle.Regular),
                    Brushes.DarkRed,
                    new PointF(e.Bounds.X + 3, e.Bounds.Y + 3));
            }
            else
            {
                e.Graphics.DrawString(tcQuyDinh_DanhSachQuyDinh.TabPages[e.Index].Text,
                    tcMenu.Font,
                    Brushes.DarkGreen,
                    new PointF(e.Bounds.X + 3, e.Bounds.Y + 3));
            }
        }
        #endregion

        #region Cellpainting button datagridview
        private void dgvTiepNhanHoSo_DangKyGiaiDau_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex < 0)
                return;

            //I supposed your button column is at index 0
            if (e.ColumnIndex == 0)
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                var w = GUI.Properties.Resources.edit.Width;
                var h = GUI.Properties.Resources.edit.Height;
                var x = e.CellBounds.Left + (e.CellBounds.Width - w) / 2;
                var y = e.CellBounds.Top + (e.CellBounds.Height - h) / 2;

                e.Graphics.DrawImage(GUI.Properties.Resources.edit, new Rectangle(x, y, w, h));
                e.Handled = true;
            }
            if (e.ColumnIndex == 1)
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                var w = GUI.Properties.Resources.done.Width;
                var h = GUI.Properties.Resources.done.Height;
                var x = e.CellBounds.Left + (e.CellBounds.Width - w) / 2;
                var y = e.CellBounds.Top + (e.CellBounds.Height - h) / 2;

                e.Graphics.DrawImage(GUI.Properties.Resources.done, new Rectangle(x, y, w, h));
                e.Handled = true;
            }
        }
        private void dgvChiTietThongTinDoiBOng_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex < 0)
                return;

            //I supposed your button column is at index 0
            if (e.ColumnIndex == 0)
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                var w = GUI.Properties.Resources.add.Width;
                var h = GUI.Properties.Resources.add.Height;
                var x = e.CellBounds.Left + (e.CellBounds.Width - w) / 2;
                var y = e.CellBounds.Top + (e.CellBounds.Height - h) / 2;

                e.Graphics.DrawImage(GUI.Properties.Resources.add, new Rectangle(x, y, w, h));
                e.Handled = true;
            }
        }
        private void dgvTiepNhanHoSo_DanhSachDoiBOngDaDangKy_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex < 0)
                return;

            //I supposed your button column is at index 0
            if (e.ColumnIndex == 0)
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                var w = GUI.Properties.Resources.edit.Width;
                var h = GUI.Properties.Resources.edit.Height;
                var x = e.CellBounds.Left + (e.CellBounds.Width - w) / 2;
                var y = e.CellBounds.Top + (e.CellBounds.Height - h) / 2;

                e.Graphics.DrawImage(GUI.Properties.Resources.edit, new Rectangle(x, y, w, h));
                e.Handled = true;
            }
            else if (e.ColumnIndex == 1)
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                var w = GUI.Properties.Resources.delete.Width;
                var h = GUI.Properties.Resources.delete.Height;
                var x = e.CellBounds.Left + (e.CellBounds.Width - w) / 2;
                var y = e.CellBounds.Top + (e.CellBounds.Height - h) / 2;

                e.Graphics.DrawImage(GUI.Properties.Resources.delete, new Rectangle(x, y, w, h));
                e.Handled = true;
            }
            else if (e.ColumnIndex == 2)
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                var w = GUI.Properties.Resources.done.Width;
                var h = GUI.Properties.Resources.done.Height;
                var x = e.CellBounds.Left + (e.CellBounds.Width - w) / 2;
                var y = e.CellBounds.Top + (e.CellBounds.Height - h) / 2;

                e.Graphics.DrawImage(GUI.Properties.Resources.done, new Rectangle(x, y, w, h));
                e.Handled = true;
            }
        }
        private void dgvChiTietTTCT_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex < 0)
                return;

            //I supposed your button column is at index 0
            if (e.ColumnIndex == 0)
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                var w = GUI.Properties.Resources.add.Width;
                var h = GUI.Properties.Resources.add.Height;
                var x = e.CellBounds.Left + (e.CellBounds.Width - w) / 2;
                var y = e.CellBounds.Top + (e.CellBounds.Height - h) / 2;

                e.Graphics.DrawImage(GUI.Properties.Resources.add, new Rectangle(x, y, w, h));
                e.Handled = true;
            }
        }
        private void dgvTNHS_DKDB_DKCT_DanhSachCauThu_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex < 0)
                return;

            //I supposed your button column is at index 0
            if (e.ColumnIndex == 0)
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                var w = GUI.Properties.Resources.delete.Width;
                var h = GUI.Properties.Resources.delete.Height;
                var x = e.CellBounds.Left + (e.CellBounds.Width - w) / 2;
                var y = e.CellBounds.Top + (e.CellBounds.Height - h) / 2;

                e.Graphics.DrawImage(GUI.Properties.Resources.delete, new Rectangle(x, y, w, h));
                e.Handled = true;
            }
        }
        private void dgvCapNhatKetQua_DanhSachTranDau_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex < 0)
                return;

            //I supposed your button column is at index 0
            if (e.ColumnIndex == 0)
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                var w = GUI.Properties.Resources.edit.Width;
                var h = GUI.Properties.Resources.edit.Height;
                var x = e.CellBounds.Left + (e.CellBounds.Width - w) / 2;
                var y = e.CellBounds.Top + (e.CellBounds.Height - h) / 2;

                e.Graphics.DrawImage(GUI.Properties.Resources.edit, new Rectangle(x, y, w, h));
                e.Handled = true;
            }
            else if (e.ColumnIndex == 1)
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                var w = GUI.Properties.Resources.done.Width;
                var h = GUI.Properties.Resources.done.Height;
                var x = e.CellBounds.Left + (e.CellBounds.Width - w) / 2;
                var y = e.CellBounds.Top + (e.CellBounds.Height - h) / 2;

                e.Graphics.DrawImage(GUI.Properties.Resources.done, new Rectangle(x, y, w, h));
                e.Handled = true;
            }
        }
        private void dgvCapNhatKetQua_ChuNha_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex < 0)
                return;

            //I supposed your button column is at index 0
            if (e.ColumnIndex == 0)
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                var w = GUI.Properties.Resources.edit.Width;
                var h = GUI.Properties.Resources.edit.Height;
                var x = e.CellBounds.Left + (e.CellBounds.Width - w) / 2;
                var y = e.CellBounds.Top + (e.CellBounds.Height - h) / 2;

                e.Graphics.DrawImage(GUI.Properties.Resources.edit, new Rectangle(x, y, w, h));
                e.Handled = true;
            }
        }
        private void dgvTaoLichThiDau_DanhSachMuaGiai_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex < 0)
                return;

            //I supposed your button column is at index 0
            if (e.ColumnIndex == 0)
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                var w = GUI.Properties.Resources.edit.Width;
                var h = GUI.Properties.Resources.edit.Height;
                var x = e.CellBounds.Left + (e.CellBounds.Width - w) / 2;
                var y = e.CellBounds.Top + (e.CellBounds.Height - h) / 2;

                e.Graphics.DrawImage(GUI.Properties.Resources.edit, new Rectangle(x, y, w, h));
                e.Handled = true;
            }
        }
        #endregion

        #region Quy định
        private void LoadDanhSachQuyDinh()
        {
            nudQuyDinh_SoLuongCauThuToiDa.Value = BUSQuyDinh.SoLuongCauThuToiDa;
            nudQuyDinh_SoLuongCauThuToiThieu.Value = BUSQuyDinh.SoLuongCauThuToiThieu;
            nudQuyDinh_SoLuongCauThuNuocNgoai.Value = BUSQuyDinh.SoLuongCauThuNuocNgoaiToiDa;
            nudQuyDinh_TuoiToiDa.Value = BUSQuyDinh.DoTuoiToiDa;
            nudQuyDinh_TuoiToiThieu.Value = BUSQuyDinh.DoTuoiToiThieu;
            nudQuyDinh_ThoiDiemGhiBanToiDa.Value = BUSQuyDinh.ThoiDiemGhiBanToiDa;

            tbQuyDinh_SoLuongCauThuToiDa.Text = BUSQuyDinh.SoLuongCauThuToiDa.ToString();
            tbQuyDinh_SoLuongCauThuToiThieu.Text = BUSQuyDinh.SoLuongCauThuToiThieu.ToString();
            tbQuyDinh_SoLuongCauThuNuocNgoai.Text = BUSQuyDinh.SoLuongCauThuNuocNgoaiToiDa.ToString();
            tbQuyDinh_TuoiToiDa.Text = BUSQuyDinh.DoTuoiToiDa.ToString();
            tbQuyDinh_TuoiToiThieu.Text = BUSQuyDinh.DoTuoiToiThieu.ToString();
            tbQuyDinh_ThoiDiemGhiBanToiDa.Text = BUSQuyDinh.ThoiDiemGhiBanToiDa.ToString();
        }
        private void btQuyDinh_XacNhan_Click(object sender, EventArgs e)
        {
            BUSQuyDinh.SoLuongCauThuToiDa = Convert.ToInt32(nudQuyDinh_SoLuongCauThuToiDa.Value);
            BUSQuyDinh.SoLuongCauThuToiThieu = Convert.ToInt32(nudQuyDinh_SoLuongCauThuToiThieu.Value);
            BUSQuyDinh.SoLuongCauThuNuocNgoaiToiDa = Convert.ToInt32(nudQuyDinh_SoLuongCauThuNuocNgoai.Value);
            BUSQuyDinh.DoTuoiToiDa = Convert.ToInt32(nudQuyDinh_TuoiToiDa.Value);
            BUSQuyDinh.DoTuoiToiThieu = Convert.ToInt32(nudQuyDinh_TuoiToiThieu.Value);
            BUSQuyDinh.ThoiDiemGhiBanToiDa = Convert.ToInt32(nudQuyDinh_ThoiDiemGhiBanToiDa.Value);
            MessageBox.Show("Cập nhật quy định thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

            tbQuyDinh_SoLuongCauThuToiDa.Text = BUSQuyDinh.SoLuongCauThuToiDa.ToString();
            tbQuyDinh_SoLuongCauThuToiThieu.Text = BUSQuyDinh.SoLuongCauThuToiThieu.ToString();
            tbQuyDinh_SoLuongCauThuNuocNgoai.Text = BUSQuyDinh.SoLuongCauThuNuocNgoaiToiDa.ToString();
            tbQuyDinh_TuoiToiDa.Text = BUSQuyDinh.DoTuoiToiDa.ToString();
            tbQuyDinh_TuoiToiThieu.Text = BUSQuyDinh.DoTuoiToiThieu.ToString();
            tbQuyDinh_ThoiDiemGhiBanToiDa.Text = BUSQuyDinh.ThoiDiemGhiBanToiDa.ToString();
        }
        #endregion

        #region Lịch thi đấu
        private void LoadMenuLichThiDau()
        {
            tsmiLichThiDau_MaMuaGiai.DropDownItems.Clear();
            int id = 0;
            danhSachMuaGiai = data.DanhSachMuaGiai();
            foreach (var muaGiai in danhSachMuaGiai)
            {
                if (muaGiai.TenMuaGiai.Substring(muaGiai.TenMuaGiai.Length - 4).All(char.IsDigit) == true)
                {
                    ToolStripMenuItem item = new ToolStripMenuItem(muaGiai.MaMuaGiai);
                    item.Tag = id;
                    id++;
                    item.Click += new EventHandler(itemClick_SelectMuaGiaiLichThiDau);
                    tsmiLichThiDau_MaMuaGiai.DropDownItems.Add(item);
                }
            }
        }
        private void itemClick_SelectMuaGiaiLichThiDau(object sender, EventArgs e)
        {
            data = new TransferData();
            tsmiLichThiDau_MaVongDau.Text = "Vòng đấu";
            tsmiLichThiDau_MaMuaGiai.Text = sender.ToString();
            int id = 0;
            tsmiLichThiDau_MaVongDau.DropDownItems.Clear();


            List<DTOChiTietTranDau> lichThiDau = data.LichThiDau(sender.ToString());
            danhSachVongDau = new HashSet<string>(lichThiDau.Select(x => x.MaVongDau));
            foreach (var vongDau in danhSachVongDau)
            {
                ToolStripMenuItem item = new ToolStripMenuItem(vongDau);
                item.Tag = id;
                id++;
                item.Click += new EventHandler(itemClick_SelectVongDauLichThiDau);
                tsmiLichThiDau_MaVongDau.DropDownItems.Add(item);
            }

            var lichHoanChinh = LichThiDau(tsmiLichThiDau_MaMuaGiai.Text);
            dgvLichThiDau.DataSource = lichHoanChinh;
            dgvLichThiDau.Columns["DaThiDau"].Visible = false;
        }
        private List<DTOChiTietTranDau> LichThiDau(string maMuaGiai)
        {
            data = new TransferData();
            List<DTOChiTietTranDau> dsTranDau = data.LichThiDau(tsmiLichThiDau_MaMuaGiai.Text);
            List<DTODoiBong> dsDoiBong = data.DanhSachDoiBong();
            var lichHoanChinh = dsTranDau.ToList();

            int i = 0;
            foreach (var td in dsTranDau)
            {
                if (td.DaThiDau == -1)
                {
                    lichHoanChinh[i].MaMuaGiai = "Chưa thi đấu";
                }
                else if (td.DaThiDau == 0)
                {
                    lichHoanChinh[i].MaMuaGiai = "Đã kết thúc";
                }
                else if (td.DaThiDau == 1)
                {
                    lichHoanChinh[i].MaMuaGiai = "Đang cập nhật";
                }
                if (dsDoiBong.Any(x => x.MaDoiBong == td.MaChuNha) == true)
                {
                    lichHoanChinh[i].MaChuNha = dsDoiBong.Where(x => x.MaDoiBong == td.MaChuNha).ToList()[0].TenDoiBong;
                }
                if (dsDoiBong.Any(x => x.MaDoiBong == td.MaKhach) == true)
                {
                    lichHoanChinh[i].MaKhach = dsDoiBong.Where(x => x.MaDoiBong == td.MaKhach).ToList()[0].TenDoiBong;
                }
                i++;
            }
            return lichHoanChinh;
        }
        private void itemClick_SelectVongDauLichThiDau(object sender, EventArgs e)
        {
            tsmiLichThiDau_MaVongDau.Text = sender.ToString();
            var lichHoanChinh = LichThiDau(tsmiLichThiDau_MaMuaGiai.Text);
            var final = lichHoanChinh.Where(x => x.MaVongDau == tsmiLichThiDau_MaVongDau.Text).ToList();
            dgvLichThiDau.DataSource = final;
            dgvLichThiDau.Columns["DaThiDau"].Visible = false;
        }
        #endregion

        #region Tra cứu cầu thủ
        private void LoadThongTinTraCuu()
        {
            rbtTraCuu_TenCauThu.Checked = true;
            //cbbTraCuu_MuaGiai.Items.Clear();
            //cbbTraCuu_DoiBong.Items.Clear();

            data = new TransferData();
            List<DTOMuaGiai> dsMuaGiai = data.DanhSachMuaGiai();
            List<string> dsMaMuaGiai = new List<string>();
            dsMaMuaGiai = dsMuaGiai.Select(x => x.MaMuaGiai).ToList();
            dsMaMuaGiai.Insert(0, "ALL");
            cbbTraCuu_MuaGiai.DataSource = dsMaMuaGiai;            
        }
        private void LoadThongTinGiaiDau_LoadMaDoiBong()
        {
            data = new TransferData();
            List<DTODoiBongThamGiaGiaiDau> dsDoiBong = data.DanhSachDoiBongDaDangKyGiaiDau(cbbTraCuu_MuaGiai.SelectedItem.ToString());
            List<string> dsmaDoiBong = new List<string>();
            dsmaDoiBong = dsDoiBong.Select(x => x.MaDoiBong).ToList();
            dsmaDoiBong.Insert(0, "ALL");
            cbbTraCuu_DoiBong.DataSource = dsmaDoiBong;
        }
        private void cbbTraCuu_MuaGiai_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadThongTinGiaiDau_LoadMaDoiBong();
        }

        List<DTOCauThuThuocDoiBongThamGiaGiaiDau> dsCauThuCanTimKiem = new List<DTOCauThuThuocDoiBongThamGiaGiaiDau>();
        private void btTraCuu_TimKiem_Click(object sender, EventArgs e)
        {
            if (tbTraCuuCauThu_ThongTin.Text == "")
            {
                MessageBox.Show("Vui lòng nhập thông tin cần tra cứu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            BUSTraCuuCauThu traCuu = new BUSTraCuuCauThu();
            if (rbtTraCuu_TenCauThu.Checked == true)
            {
                dsCauThuCanTimKiem = traCuu.TimKiemCauThu(tbTraCuuCauThu_ThongTin.Text, 1);
                dgvTraCuu_DanhSachTraCuu.DataSource = dsCauThuCanTimKiem;
            }
            else if (rbtTraCuu_TenDoiBong.Checked == true)
            {
                dsCauThuCanTimKiem = traCuu.TimKiemCauThu(tbTraCuuCauThu_ThongTin.Text, 2);
                dgvTraCuu_DanhSachTraCuu.DataSource = dsCauThuCanTimKiem;
            }
            DanhSachCauThu_TraCuu_HideItem();
        }
        private void btTraCuu_LocTimKiem_Click(object sender, EventArgs e)
        {
            List<DTOCauThuThuocDoiBongThamGiaGiaiDau> ketQua = new List<DTOCauThuThuocDoiBongThamGiaGiaiDau>();
            BUSTraCuuCauThu traCuu = new BUSTraCuuCauThu();
            if (rbt_TraCuu_LoaiCauThu_VietNam.Checked == true)
            {
                ketQua = traCuu.LocDanhSachTimKiem(
                    dsCauThuCanTimKiem,
                    cbbTraCuu_MuaGiai.SelectedItem.ToString(),
                    cbbTraCuu_DoiBong.SelectedItem.ToString(),
                    1,
                    Convert.ToInt32(nudTraCuu_TuoiToiThieu.Value),
                    Convert.ToInt32(nudTraCuu_TuoiToiDa.Value)
                    );
            }
            else
            {
                ketQua = traCuu.LocDanhSachTimKiem(
                    dsCauThuCanTimKiem,
                    cbbTraCuu_MuaGiai.SelectedItem.ToString(),
                    cbbTraCuu_DoiBong.SelectedItem.ToString(),
                    2,
                    Convert.ToInt32(nudTraCuu_TuoiToiThieu.Value),
                    Convert.ToInt32(nudTraCuu_TuoiToiDa.Value)
                    );
            }
            dgvTraCuu_DanhSachTraCuu.DataSource = ketQua;
            DanhSachCauThu_TraCuu_HideItem();
        }
        private void DanhSachCauThu_TraCuu_HideItem()
        {
            dgvTraCuu_DanhSachTraCuu.Columns["MaDoiBong"].Visible = false;
        }
        #endregion

        #region Bảng xếp hạng
        private void LoadMenuBangXepHang()
        {
            tsmiBangXepHang_MuaGiai.DropDownItems.Clear();
            int id = 0;
            danhSachMuaGiai = data.DanhSachMuaGiai();
            foreach (var muaGiai in danhSachMuaGiai)
            {
                if (muaGiai.TenMuaGiai.Substring(muaGiai.TenMuaGiai.Length - 4).All(char.IsDigit) == true)
                {
                    ToolStripMenuItem item = new ToolStripMenuItem(muaGiai.MaMuaGiai);
                    item.Tag = id;
                    id++;
                    item.Click += new EventHandler(itemClick_SelectMuaGiaiBangXepHang);
                    tsmiBangXepHang_MuaGiai.DropDownItems.Add(item);
                }
            }
        }

        private void itemClick_SelectMuaGiaiBangXepHang(object sender, EventArgs e)
        {
            data = new TransferData();
            tsmiBangXepHang_VongDau.Text = "Vòng đấu";
            tsmiBangXepHang_NgayBatDau.Text = "Thời gian";
            tsmiBangXepHang_MuaGiai.Text = sender.ToString();
            int id = 0;
            tsmiBangXepHang_VongDau.DropDownItems.Clear();

            
            List<DTOChiTietTranDau> lichThiDau = data.LichThiDau(sender.ToString());
            danhSachVongDau = new HashSet<string>(lichThiDau.Select(x => x.MaVongDau));
            foreach (var vongDau in danhSachVongDau)
            {
                ToolStripMenuItem item = new ToolStripMenuItem(vongDau);
                item.Tag = id;
                id++;
                item.Click += new EventHandler(itemClick_SelectVongDauBangXepHang);
                tsmiBangXepHang_VongDau.DropDownItems.Add(item);
            }
        }
        private void itemClick_SelectVongDauBangXepHang(object sender, EventArgs e)
        {
            tsmiBangXepHang_VongDau.Text = sender.ToString();
            List<DTOSoLieuThongKeDoiBong> bangXepHang = new List<DTOSoLieuThongKeDoiBong>();
            bangXepHang = data.BangXepHang(tsmiBangXepHang_MuaGiai.Text, sender.ToString());

            int check = 0;
            foreach(var vd in bangXepHang)
            {
                if (vd.SoTranDaThiDau == 0)
                {
                    check++;
                }
            }
            if (check == bangXepHang.Count())
            {
                MessageBox.Show("Vòng đấu chưa diễn ra", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            List<DTOSoLieuThongKeDoiBong> soLieuHoanChinh = bangXepHang.ToList();

            int ma = int.Parse(bangXepHang[0].MaVongDau.Substring(3)) - 1;

            string maVongDauTruoc = "VD" + ma.ToString("000");
            List<DTOSoLieuThongKeDoiBong> bangXepHangVongDauTruoc = data.BangXepHang(tsmiBangXepHang_MuaGiai.Text, maVongDauTruoc);

            if (ma >= 1)
            {
                int i = 0;
                DTOSoLieuThongKeDoiBong soLieuVongDauTruoc = new DTOSoLieuThongKeDoiBong();
                foreach (var vd in bangXepHang)
                {
                    if (vd.SoTranDaThiDau == 0)
                    {
                        soLieuVongDauTruoc = bangXepHangVongDauTruoc.Where(x => x.MaDoiBong == vd.MaDoiBong).ToList()[0];
                        soLieuHoanChinh[i] = soLieuVongDauTruoc;
                    }
                    i++;
                }
            }
            for(int j = 0; j < soLieuHoanChinh.Count(); j++)
            {
                soLieuHoanChinh[j].Hang = 0;
            }
            soLieuHoanChinh = soLieuHoanChinh
                .OrderByDescending(x => x.TongDiem)
                .ThenByDescending(x => x.TongHieuSo)
                .ThenByDescending(x => x.TongSoBanThang)
                .ToList();
            for(int j = 0; j < soLieuHoanChinh.Count(); j++)
            {
                soLieuHoanChinh[j].Hang = j + 1;
            }

           


            // Sắp xếp bảng xếp hạng
            BindingSource bxh = new BindingSource();
            bxh.DataSource = soLieuHoanChinh.OrderBy(r => r.Hang).ToList();

            dgvBangXepHangBangXepHang.DataSource = bxh;
            if (dgvBangXepHangBangXepHang.Rows.Count > 3)
            {
                dgvBangXepHangBangXepHang.Rows[0].DefaultCellStyle.BackColor = Color.Aqua;
                dgvBangXepHangBangXepHang.Rows[1].DefaultCellStyle.BackColor = Color.Aqua;
                dgvBangXepHangBangXepHang.Rows[2].DefaultCellStyle.BackColor = Color.Aqua;
            }
            


            dgvBangXepHangBangXepHang.Columns["MaMuaGiai"].Visible = false;
            dgvBangXepHangBangXepHang.Columns["MaVongDau"].Visible = false;
            dgvBangXepHangBangXepHang.Columns["SoBanThang"].Visible = false;
            dgvBangXepHangBangXepHang.Columns["SoBanThua"].Visible = false;
            dgvBangXepHangBangXepHang.Columns["Diem"].Visible = false;
            dgvBangXepHangBangXepHang.Columns["HieuSo"].Visible = false;

            List<DTOChiTietTranDau> dsTranDau = data.LichThiDau(tsmiBangXepHang_MuaGiai.Text);
            dsTranDau = dsTranDau.Where(x => x.MaVongDau == tsmiBangXepHang_VongDau.Text).ToList();
            int index = dsTranDau[0].NgayThiDau.ToString().IndexOf(" ");
            tsmiBangXepHang_NgayBatDau.Text = dsTranDau[0].NgayThiDau.Date.ToString().Substring(0, index);
        }
        #endregion

        #region Số liệu thống kê cầu thủ
        private void CapNhatSoLieuThongKeCauThu(string maMuaGiai, string maVongDau)
        {
            data = new TransferData();
            uData = new UpdateData();
            List<DTOSoLieuThongKeCauThu> soLieuCauThu = data.SoLieuCauThu(maMuaGiai, maVongDau);
            int check = 0;
            foreach(var sl in soLieuCauThu)
            {
                if (sl.Hang == 0)
                {
                    check = 1;
                    break;
                }
            }
            if (check == 1)
            {
                List<DTOSoLieuThongKeCauThu> soLieuHoanChinh = soLieuCauThu.ToList();
                int ma = int.Parse(maVongDau.Substring(3)) - 1;
                string maVongDauTruoc = "VD" + ma.ToString("000");
                if (ma >= 1)
                {
                    List<DTOSoLieuThongKeCauThu> soLieuCauThuVongDauTruoc = data.SoLieuCauThu(maMuaGiai, maVongDauTruoc);
                    DTOSoLieuThongKeCauThu soLieu = new DTOSoLieuThongKeCauThu();

                    int i = 0;
                    foreach (var sl in soLieuCauThu)
                    {
                        soLieu = soLieuCauThuVongDauTruoc.Where(x => x.MaCauThu == sl.MaCauThu).ToList()[0];
                        soLieu.MaVongDau = maVongDau;
                        soLieuHoanChinh[i] = soLieu;
                        i++;
                    }
                    foreach (var sl in soLieuHoanChinh)
                    {
                        uData.UpdateSoLieuThongKeCauThu(sl);
                    }
                }
                
            }
        }
        private void LoadMenuSoLieuThongKe()
        {
            tsmiBangXepHang_CauThu_MuaGiai.DropDownItems.Clear();
            int id = 0;
            danhSachMuaGiai = data.DanhSachMuaGiai();
            foreach (var muaGiai in danhSachMuaGiai)
            {
                if (muaGiai.TenMuaGiai.Substring(muaGiai.TenMuaGiai.Length - 4).All(char.IsDigit) == true)
                {
                    ToolStripMenuItem item = new ToolStripMenuItem(muaGiai.MaMuaGiai);
                    item.Tag = id;
                    id++;
                    item.Click += new EventHandler(itemClick_SelectMuaGiaiSoLieuThongKe);
                    tsmiBangXepHang_CauThu_MuaGiai.DropDownItems.Add(item);
                }
            }
        }
        private void itemClick_SelectMuaGiaiSoLieuThongKe(object sender, EventArgs e)
        {
            data = new TransferData();
            tsmiBangXepHang_CauThu_VongDau.Text = "Vòng đấu";
            tsmiBangXepHang_CauThu_ThoiGian.Text = "Thời gian";
            tsmiBangXepHang_CauThu_MuaGiai.Text = sender.ToString();
            int id = 0;
            tsmiBangXepHang_CauThu_VongDau.DropDownItems.Clear();


            List<DTOChiTietTranDau> lichThiDau = data.LichThiDau(sender.ToString());
            danhSachVongDau = new HashSet<string>(lichThiDau.Select(x => x.MaVongDau));
            foreach (var vongDau in danhSachVongDau)
            {
                ToolStripMenuItem item = new ToolStripMenuItem(vongDau);
                item.Tag = id;
                id++;
                item.Click += new EventHandler(itemClick_SelectVongDauSoLieuThongKe);
                tsmiBangXepHang_CauThu_VongDau.DropDownItems.Add(item);
            }
        }
        private void itemClick_SelectVongDauSoLieuThongKe(object sender, EventArgs e)
        {
            tsmiBangXepHang_CauThu_VongDau.Text = sender.ToString();

            List<DTOSoLieuThongKeCauThu> bangXepHangCauThu = new List<DTOSoLieuThongKeCauThu>();
            bangXepHangCauThu = data.SoLieuCauThu(tsmiBangXepHang_CauThu_MuaGiai.Text, sender.ToString());

            // Kiểm tra vòng đấu đã diễn ra hay chưa
            List<DTOSoLieuThongKeDoiBong> bangXepHang = data.BangXepHang(tsmiBangXepHang_CauThu_MuaGiai.Text, sender.ToString());
            int check = 0;
            foreach (var vd in bangXepHang)
            {
                if (vd.SoTranDaThiDau == 0)
                {
                    check++;
                }
            }
            if (check == bangXepHang.Count())
            {
                MessageBox.Show("Vòng đấu chưa diễn ra", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            // Kết thúc kiểm tra

            
            

            List<DTOSoLieuThongKeCauThu> bxhCauThu = data.SoLieuCauThu(tsmiBangXepHang_CauThu_MuaGiai.Text, sender.ToString());
            // Sắp xếp bảng xếp hạng
            BindingSource bxh = new BindingSource();
            bxh.DataSource = bxhCauThu.OrderBy(r => r.Hang).ToList();

            dgvSoLieuThongKeCauThu.DataSource = bxh;
            dgvSoLieuThongKeCauThu.Rows[0].DefaultCellStyle.BackColor = Color.Aqua;
            dgvSoLieuThongKeCauThu.Rows[1].DefaultCellStyle.BackColor = Color.Aqua;
            dgvSoLieuThongKeCauThu.Rows[2].DefaultCellStyle.BackColor = Color.Aqua;


            dgvSoLieuThongKeCauThu.Columns["MaMuaGiai"].Visible = false;
            dgvSoLieuThongKeCauThu.Columns["MaVongDau"].Visible = false;

            List<DTOChiTietTranDau> dsTranDau = data.LichThiDau(tsmiBangXepHang_CauThu_MuaGiai.Text);
            dsTranDau = dsTranDau.Where(x => x.MaVongDau == tsmiBangXepHang_CauThu_VongDau.Text).ToList();
            int index = dsTranDau[0].NgayThiDau.ToString().IndexOf(" ");
            tsmiBangXepHang_CauThu_ThoiGian.Text = dsTranDau[0].NgayThiDau.Date.ToString().Substring(0, index);
        }
        #endregion

        #region Cập nhật kết quả trận đấu
        HashSet<string> danhSachVongDau;     
        //----------------------------------------------------------------------
        // Load Danh sách mùa giải
        private void LoadDanhSachMuaGiai_CapNhatKetQua()
        {
            tsmiCapNhatKetQua_MuaGiai.DropDownItems.Clear();
            int id = 0;
            danhSachMuaGiai = data.DanhSachMuaGiai();
            foreach (var muaGiai in danhSachMuaGiai)
            {
                if (muaGiai.TenMuaGiai.Substring(muaGiai.TenMuaGiai.Length - 4).All(char.IsDigit) == true)
                {
                    ToolStripMenuItem item = new ToolStripMenuItem(muaGiai.MaMuaGiai);
                    item.Tag = id;
                    id++;
                    item.Click += new EventHandler(itemClick_SelectMuaGiaiCapNhatKetQua);
                    tsmiCapNhatKetQua_MuaGiai.DropDownItems.Add(item);
                }      
            }
        }
        private void LoadMenuCapNhatKetQua()
        {
            int id = 0;
            danhSachMuaGiai = data.DanhSachMuaGiai();
            foreach (var muaGiai in danhSachMuaGiai)
            {
                ToolStripMenuItem item = new ToolStripMenuItem(muaGiai.MaMuaGiai);
                item.Tag = id;
                id++;
                item.Click += new EventHandler(itemClick_SelectMuaGiaiCapNhatKetQua);
                tsmiCapNhatKetQua_MuaGiai.DropDownItems.Add(item);
            }
        }

        // Load danh sách vòng đấu khi chọn một mùa giải
        private void itemClick_SelectMuaGiaiCapNhatKetQua(object sender, EventArgs e)
        {
            tsmiCapNhatKetQua_VongDau.Text = "Vòng đấu";
            tsmiCapNhatKetQua_MuaGiai.Text = sender.ToString();
            int id = 0;
            tsmiCapNhatKetQua_VongDau.DropDownItems.Clear();

            data = new TransferData();

            List<DTOChiTietTranDau> lichThiDau = data.LichThiDau(sender.ToString());
            danhSachVongDau = new HashSet<string>(lichThiDau.Select(x => x.MaVongDau));
            foreach (var vongDau in danhSachVongDau)
            {
                ToolStripMenuItem item = new ToolStripMenuItem(vongDau);
                item.Tag = id;
                id++;
                item.Click += new EventHandler(itemClick_SelectVongDauCapNhatKetQua);
                tsmiCapNhatKetQua_VongDau.DropDownItems.Add(item);
            }
        }
        
        // Load danh sách các trận đấu trong vòng đấu hiển thị lên Datagridview
        private void LoadDanhSachVongDauThuocMuaGiai_CapNhatKetQua(string maVongDau)
        {
            data = new TransferData();

            List<DTODoiBong> dsDoiBong = new List<DTODoiBong>();
            dsDoiBong = data.DanhSachDoiBong();

            tsmiCapNhatKetQua_VongDau.Text = maVongDau;

            List<DTOChiTietTranDau> dsTranDau = data.LichThiDau(tsmiCapNhatKetQua_MuaGiai.Text);
            dsTranDau = dsTranDau
                .Where(x => x.MaVongDau == maVongDau).ToList();
            dsTranDau = dsTranDau
                .Select(x => new DTOChiTietTranDau()
                {
                    MaVongDau = x.MaVongDau,
                    MaTranDau = x.MaTranDau,
                    DaThiDau = x.DaThiDau,
                    GioThiDau = x.GioThiDau,
                    NgayThiDau = x.NgayThiDau,
                    MaMuaGiai = x.MaMuaGiai,
                    SoBanThangChuNha = x.SoBanThangChuNha,
                    SoBanThangKhach = x.SoBanThangKhach,
                    MaKhach = dsDoiBong.Where(y => y.MaDoiBong == x.MaKhach).ToList()[0].TenDoiBong,
                    MaChuNha = dsDoiBong.Where(y => y.MaDoiBong == x.MaChuNha).ToList()[0].TenDoiBong
                }).ToList();
            dgvCapNhatKetQua_DanhSachTranDau.DataSource = dsTranDau;
            dgvCapNhatKetQua_DanhSachTranDau.Columns["MaMuaGiai"].Visible = false;
            dgvCapNhatKetQua_DanhSachTranDau.Columns["MaVongDau"].Visible = false;
            dgvCapNhatKetQua_DanhSachTranDau.Columns["SoBanThangChuNha"].Visible = false;
            dgvCapNhatKetQua_DanhSachTranDau.Columns["SoBanThangKhach"].Visible = false;
            dgvCapNhatKetQua_DanhSachTranDau.Columns["CapNhatCNKQ"].DisplayIndex = 7;
            dgvCapNhatKetQua_DanhSachTranDau.Columns["HoanTatKNKQ"].DisplayIndex = 8;

        }
        private void itemClick_SelectVongDauCapNhatKetQua(object sender, EventArgs e)
        {
            LoadDanhSachVongDauThuocMuaGiai_CapNhatKetQua(sender.ToString());
        }

        // Cập nhật, hoàn tất một trận đấu
        string maVongDauSelected_CapNhatKetQua;
        string maMuaGiaiSelected_CapNhatKetQua;
        string maTranDauSelected_CapNhatKetQua;
        string maCauThuSelected_CapNhatKetQua;
        string maDoiBongSelected_CapNhatKetQua;
        string maChuNhaSeclected_CapNhatKetQua;
        string maKhachSelected_CapNhatKetQua;
        private void dgvCapNhatKetQua_DanhSachTranDau_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;
            data = new TransferData();
            uData = new UpdateData();
            maVongDauSelected_CapNhatKetQua = tsmiCapNhatKetQua_VongDau.Text;
            maMuaGiaiSelected_CapNhatKetQua = tsmiCapNhatKetQua_MuaGiai.Text;
            maTranDauSelected_CapNhatKetQua= senderGrid.Rows[e.RowIndex].Cells[4].Value.ToString();

            // Kiểm tra hoàn tất cập nhật thông tin tất cả các trận đấu của vòng đấu trước hay chưa
            int ma = int.Parse(tsmiCapNhatKetQua_VongDau.Text.Substring(3)) - 1;
            if (ma >= 1)
            {
                string maVongDauTruoc = "VD" + ma.ToString("000");
                data = new TransferData();
                List<DTOChiTietTranDau> dsTranDauVongDauTruoc = data.LichThiDau(maMuaGiaiSelected_CapNhatKetQua);
                dsTranDauVongDauTruoc = dsTranDauVongDauTruoc
                    .Where(x => x.MaVongDau == maVongDauTruoc).ToList();
                foreach(var td in dsTranDauVongDauTruoc)
                {
                    if (td.DaThiDau == -1)
                    {
                        MessageBox.Show("Chưa hoàn tất cập nhật thông tin các trận vòng đấu trước, vui lòng kiểm tra lại");
                        return;
                    }
                }
            }
            // Hoàn tất kiểm tra
            

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && senderGrid.Columns[e.ColumnIndex].HeaderText == "Cập nhật" && e.RowIndex >= 0)
            {
                if (senderGrid.Rows[e.RowIndex].Cells[9].Value.ToString() == "0")
                {
                    DialogResult result = MessageBox.Show("Trận đấu đã hoàn tất cập nhật kết quả, Xác nhận hủy nếu muốn cập nhật lại", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (result == DialogResult.No)
                    {
                        return;
                    }
                    else if(result==DialogResult.Yes)
                    {
                        uData = new UpdateData();
                        data = new TransferData();
                        DTOChiTietBanThang newBanThang = new DTOChiTietBanThang();
                        List<DTOChiTietBanThang> dsChiTietTranDau = data.DuLieuTranDau(maMuaGiaiSelected_CapNhatKetQua, maVongDauSelected_CapNhatKetQua, maTranDauSelected_CapNhatKetQua);
                        foreach(var ct in dsChiTietTranDau)
                        {
                            if(ct.LoaiBanThang == 1 ||ct.LoaiBanThang == 2 || ct.LoaiBanThang == -11)
                            {
                                newBanThang.MaCauThu = ct.MaCauThu;
                                newBanThang.MaDoiBong = ct.MaDoiBong;
                                newBanThang.MaMuaGiai = ct.MaMuaGiai;
                                newBanThang.MaTranDau = ct.MaTranDau;
                                newBanThang.MaVongDau = ct.MaVongDau;
                                newBanThang.ThoiDiem = ct.ThoiDiem;
                                newBanThang.LoaiBanThang = -100;

                                uData.DeleteChiTietBanThang(ct);
                                uData.UpdateChiTietBanThang(newBanThang);
                            }
                        }
                    }
                }

                tcCapNhatKetQua.SelectedTab = tpCapNhatKetQua_CapNhatKetQuaTranDau;

                uData.UpdateDaThiDau(maMuaGiaiSelected_CapNhatKetQua, maVongDauSelected_CapNhatKetQua, maTranDauSelected_CapNhatKetQua, 1);
                CapNhatSoLieuThongKeCauThu(maMuaGiaiSelected_CapNhatKetQua, maVongDauSelected_CapNhatKetQua);
                data = new TransferData();
                // 7 Chu nha 8 khach
                List<DTOChiTietTranDau> dsTranDau = new List<DTOChiTietTranDau>(); // Toàn bô
                dsTranDau = data.LichThiDau(maMuaGiaiSelected_CapNhatKetQua);

                // convert tên đội bóng sang mã đội bóng
                string machunha = dsTranDau
                    .Where(y => y.MaVongDau == maVongDauSelected_CapNhatKetQua)
                    .Where(y => y.MaTranDau == maTranDauSelected_CapNhatKetQua)
                    .ToList()[0].MaChuNha;
                string makhach = dsTranDau
                    .Where(y => y.MaVongDau == maVongDauSelected_CapNhatKetQua)
                    .Where(y => y.MaTranDau == maTranDauSelected_CapNhatKetQua)
                    .ToList()[0].MaKhach;

                // Lưu mã chủ nhà và mã khách vào biến global
                maChuNhaSeclected_CapNhatKetQua = machunha;
                maKhachSelected_CapNhatKetQua = makhach;


                // Danh sách cầu thủ thuộc độ bóng tham gia giải đấu
                List<DTOChiTietBanThang> dsCauThuChuNha =
                    data.DuLieuTranDau(maMuaGiaiSelected_CapNhatKetQua, maVongDauSelected_CapNhatKetQua, maTranDauSelected_CapNhatKetQua)
                    .Where(x => x.MaDoiBong == machunha)
                    .ToList();
                List<DTOChiTietBanThang> dsCauThuKhach =
                    data.DuLieuTranDau(maMuaGiaiSelected_CapNhatKetQua, maVongDauSelected_CapNhatKetQua, maTranDauSelected_CapNhatKetQua)
                    .Where(x => x.MaDoiBong == makhach)
                    .ToList();

                // Convert thông tin cầu thủ thuộc đội bóng tham gia giải đấu sang thông tin cầu thủ
                List<DTOThongTinCauThu> dsThongTinCauThuChuNha = dsCauThuChuNha
                    .Select(x => new DTOThongTinCauThu()
                    {
                        MaCauThu = x.MaCauThu,
                        TenCauThu = data.DanhSachCauThu().Where(y => y.MaCauThu == x.MaCauThu).ToList()[0].HoTenCauThu,
                        MaDoiBong = machunha,
                        TenDoiBong = data.DanhSachDoiBong().Where(y => y.MaDoiBong == machunha).ToList()[0].TenDoiBong
                    }).ToList();

                List<DTOThongTinCauThu> dsThongTinCauThuKhach = dsCauThuKhach
                    .Select(x => new DTOThongTinCauThu()
                    {
                        MaCauThu = x.MaCauThu,
                        TenCauThu = data.DanhSachCauThu().Where(y => y.MaCauThu == x.MaCauThu).ToList()[0].HoTenCauThu,
                        MaDoiBong = makhach,
                        TenDoiBong = data.DanhSachDoiBong().Where(y => y.MaDoiBong == makhach).ToList()[0].TenDoiBong
                    }).ToList();


                // Hiển thị
                dgvCapNhatKetQua_ChuNha.DataSource = dsThongTinCauThuChuNha;
                dgvCapNhatKetQua_ChuNha.Columns["MaDoiBong"].Visible = false;
                dgvCapNhatKetQua_ChuNha.Columns["TenDoiBong"].Visible = false;


                dgvCapNhatKetQua_Khach.DataSource = dsThongTinCauThuKhach;
                dgvCapNhatKetQua_Khach.Columns["MaDoiBong"].Visible = false;
                dgvCapNhatKetQua_Khach.Columns["TenDoiBong"].Visible = false;

                tbCapNhatKetQuaTranDau_CapNhat_MuaGiai.Text = maMuaGiaiSelected_CapNhatKetQua;
                tbCapNhatKetQuaTranDau_CapNhat_VongDau.Text = maVongDauSelected_CapNhatKetQua;
                tbCapNhatKetQuaTranDau_CapNhat_TranDau.Text = maTranDauSelected_CapNhatKetQua;
                int index = senderGrid.Rows[e.RowIndex].Cells[5].Value.ToString().IndexOf(" ");
                tbngaythidau.Text = senderGrid.Rows[e.RowIndex].Cells[5].Value.ToString().Substring(0, index);
                tbgiothidau.Text = senderGrid.Rows[e.RowIndex].Cells[6].Value.ToString();
                if (senderGrid.Rows[e.RowIndex].Cells[9].Value.ToString() == "1")
                {
                    tbtrangthai.Text = "Đang thi đấu";
                }
                tbchunha.Text = senderGrid.Rows[e.RowIndex].Cells[7].Value.ToString();
                tbkhach.Text = senderGrid.Rows[e.RowIndex].Cells[8].Value.ToString();

                gbCapNhatKetQua_ChuNha.Text = tbchunha.Text;
                gbCapNhatKetQua_Khach.Text = tbkhach.Text;
                rbtbanthangtructiep.Checked = true;

                LoadBanThang_ChuNha_Khach(maMuaGiaiSelected_CapNhatKetQua, maVongDauSelected_CapNhatKetQua, maTranDauSelected_CapNhatKetQua, maChuNhaSeclected_CapNhatKetQua, maKhachSelected_CapNhatKetQua);
            }
            else if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && senderGrid.Columns[e.ColumnIndex].HeaderText == "Hoàn tất" && e.RowIndex >= 0)
            {
                if (senderGrid.Rows[e.RowIndex].Cells[9].Value.ToString() == "0")
                {
                    MessageBox.Show("Trận đấu đã hoàn tất cập nhật kết quả", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                uData = new UpdateData();
                if(uData.UpdateDaThiDau(maMuaGiaiSelected_CapNhatKetQua, maVongDauSelected_CapNhatKetQua, maTranDauSelected_CapNhatKetQua, 0) == 1)
                {
                    MessageBox.Show("Hoàn tất cập nhật kết quả trận đấu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LoadDanhSachVongDauThuocMuaGiai_CapNhatKetQua(maVongDauSelected_CapNhatKetQua);
                    return;
                }
            }
        }
        private void LoadBanThang_ChuNha_Khach(string maMuaGiai, string maVongDau, string maTranDau, string maChuNha, string maKhach)
        {
            data = new TransferData();
            List<DTOChiTietBanThang> chiTietTranDau = data.DuLieuTranDau(maMuaGiai, maVongDau, maTranDau);
            int btChuNha = chiTietTranDau
                .Where(x => x.MaDoiBong == maChuNha)
                .Where(x => x.LoaiBanThang != -1)
                .Where(x => x.LoaiBanThang != -11)
                .Where(x => x.LoaiBanThang != -100)
                .ToList().Count();
            int btKhach = chiTietTranDau
                .Where(x => x.MaDoiBong == maKhach)
                .Where(x => x.LoaiBanThang != -1)
                .Where(x => x.LoaiBanThang != -11)
                .Where(x => x.LoaiBanThang != -100)
                .ToList().Count();
            int phanLuoiChuNha = chiTietTranDau
                .Where(x => x.MaDoiBong == maChuNha)
                .Where(x => x.LoaiBanThang == -11)
                .ToList().Count();
            int phanLuoiKhach = chiTietTranDau
                .Where(x => x.MaDoiBong == maKhach)
                .Where(x => x.LoaiBanThang == -11)
                .ToList().Count();

            tbbanthangchunha.Text = (btChuNha + phanLuoiKhach).ToString();
            tbbanthangkhach.Text = (btKhach + phanLuoiChuNha).ToString();
        }

        private void dgvCapNhatKetQua_ChuNha_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;
            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && senderGrid.Columns[e.ColumnIndex].HeaderText == "Cập nhật" && e.RowIndex >= 0)
            {
                tbTenDoiBong.Text = senderGrid.Rows[e.RowIndex].Cells[2].Value.ToString();
                tbTenCauThu.Text = senderGrid.Rows[e.RowIndex].Cells[1].Value.ToString();
                maCauThuSelected_CapNhatKetQua = senderGrid.Rows[e.RowIndex].Cells[3].Value.ToString();
                maDoiBongSelected_CapNhatKetQua = senderGrid.Rows[e.RowIndex].Cells[4].Value.ToString();
                tbCapNhatKetQua_MaDoiBong_hidden.Text = senderGrid.Rows[e.RowIndex].Cells[4].Value.ToString();
            }
        }
        private void btCapNhatKetQua_CapNhatThongTin_CapNhat_Click_1(object sender, EventArgs e)
        {
            if (tbTenDoiBong.Text == "")
            {
                MessageBox.Show("Vui lòng chọn cầu thủ cần cập nhật thông tin", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            DTOChiTietBanThang _newBanThang = new DTOChiTietBanThang();
            _newBanThang.MaMuaGiai = maMuaGiaiSelected_CapNhatKetQua;
            _newBanThang.MaVongDau = maVongDauSelected_CapNhatKetQua;
            _newBanThang.MaTranDau = maTranDauSelected_CapNhatKetQua;
            _newBanThang.MaDoiBong = maDoiBongSelected_CapNhatKetQua;
            _newBanThang.MaCauThu = maCauThuSelected_CapNhatKetQua;
            if (rbtbanthangtructiep.Checked == true)
            {
                _newBanThang.LoaiBanThang = 1;
            }
            else if (rbtpenalty.Checked == true)
            {
                _newBanThang.LoaiBanThang = 2;
            }
            else if (rbtphanluoi.Checked == true)
            {
                _newBanThang.LoaiBanThang = -11;
            }
            else
            {
                MessageBox.Show("Error", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            uData.UpdateDaThiDau(_newBanThang.MaMuaGiai, _newBanThang.MaVongDau, _newBanThang.MaTranDau, 1);

            _newBanThang.ThoiDiem = new TimeSpan(0, Convert.ToInt32(nudPhut.Value), Convert.ToInt32(nudGiay.Value));
            if (uData.UpdateChiTietBanThang(_newBanThang) == 1)
            {
                MessageBox.Show("Cập nhật kết quả thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                data = new TransferData();
                List<DTOChiTietBanThang> chiTietTranDau = data.DuLieuTranDau(maMuaGiaiSelected_CapNhatKetQua, maVongDauSelected_CapNhatKetQua, maTranDauSelected_CapNhatKetQua);

                LoadBanThang_ChuNha_Khach(maMuaGiaiSelected_CapNhatKetQua, maVongDauSelected_CapNhatKetQua, maTranDauSelected_CapNhatKetQua, maChuNhaSeclected_CapNhatKetQua, maKhachSelected_CapNhatKetQua);
                LoadDanhSachVongDauThuocMuaGiai_CapNhatKetQua(maVongDauSelected_CapNhatKetQua);
                tbTenCauThu.Text = "";
                tbTenDoiBong.Text = "";
                tbCapNhatKetQua_MaDoiBong_hidden.Text = "";
            }
        }

        #endregion

        #region Tiền Đăng ký hồ sơ đội bóng
        private void LoadThongTinTienDangKy_HOME()
        {
            List<DTODoiBong> dsDoiBong = data.DanhSachDoiBong();

            if (dsDoiBong.Count > 0)
            {
                dsDoiBong.OrderBy(r => r.MaDoiBong).ToList();
                string ma = dsDoiBong[dsDoiBong.Count - 1].MaDoiBong;
                int id = int.Parse(ma.Substring(2)) + 1;
                tbDangKyHoSoDoiBong_ThongTinDangKy_MaDoiBong.Text = "DB" + id.ToString("00");
            }
            else
            {
                tbDangKyHoSoDoiBong_ThongTinDangKy_MaDoiBong.Text = "DB01";
            }

            List<DTOCauThu> dsCauThu = new List<DTOCauThu>();
            dsCauThu = data.DanhSachCauThu();
            if (dsCauThu.Count > 0)
            {
                dsCauThu.OrderBy(r => r.MaCauThu).ToList();
                string ma = dsCauThu[dsCauThu.Count - 1].MaCauThu;
                int id = int.Parse(ma.Substring(2)) + 1;
                tbHOME_TienDangKyHOSoCauThu_MaCauThu.Text = "CT" + id.ToString("000");
            }
            else
            {
                tbHOME_TienDangKyHOSoCauThu_MaCauThu.Text = "CT001";
            }
        }
        //private void btDangKyHoSoDoiBong_XacNhanDangKy_DangKy_Click(object sender, EventArgs e)
        //{
        //    ((Control)this.tpDangKyHoSoDoiBong_ThongTinDangKy).Enabled = true;
        //    tcDangKyHoSoDoiBong_DangKy.SelectedTab = tpDangKyHoSoDoiBong_ThongTinDangKy;
        //    btDangKyHoSoDoiBong_XacNhanDangKy_DangKy.Enabled = false;
        //    btDangKyHoSoDoiBong_XacNhanDangKy_DangKy.Text = "Đang đăng ký hồ sơ";
        //}
       
        private void btDangKyHoSoDoiBong_ThongTinDangKy_DangKy_Click(object sender, EventArgs e)
        {
            DTODoiBong _newDoiBong = new DTODoiBong();
            _newDoiBong.MaDoiBong = tbDangKyHoSoDoiBong_ThongTinDangKy_MaDoiBong.Text;
            _newDoiBong.TenDoiBong = tbDangKyHoSoDoiBong_ThongTinDangKy_TenDoiBong.Text;
            _newDoiBong.SanNha = tbDangKyHoSoDoiBong_ThongTinDangKy_SanNha.Text;

            if (_newDoiBong.TenDoiBong == "")
            {
                MessageBox.Show("Vui lòng nhập tên đội bóng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            else if (_newDoiBong.SanNha == "")
            {
                MessageBox.Show("Vui lòng nhập tên Sân nhà", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (uData.InsertDoiBong(_newDoiBong) == 1)
            {
                MessageBox.Show("Thêm đội bóng thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                List<DTODoiBong> dsDoiBong = data.DanhSachDoiBong();

                LoadThongTinTienDangKy_HOME();
                tbDangKyHoSoDoiBong_ThongTinDangKy_TenDoiBong.Text = "";
                tbDangKyHoSoDoiBong_ThongTinDangKy_SanNha.Text = "";
            }
        }
        #endregion

        #region Tiền đăng ký hồ sơ cầu thủ
        //private void btDangKyHoSoCauThu_XacNhanDangKy_DangKy_Click(object sender, EventArgs e)
        //{
        //    ((Control)this.tpHOME_DangKyCauThu_FormDangKyThongTin).Enabled = true;
        //    ((Control)this.btDangKyHoSoCauThu_XacNhanDangKy_DangKy).Enabled = false;
        //    btDangKyHoSoCauThu_XacNhanDangKy_DangKy.Text = "Đang đăng ký cầu thủ";
        //    tcHOME_DangKyCauThu_ThongTinDangKY.SelectedTab = tpHOME_DangKyCauThu_FormDangKyThongTin;
        //}
        
        private void btHOME_TienDangKyHOSoCauThu_XacNhanDangKy_Click(object sender, EventArgs e)
        {
            DTOCauThu _newCauThu = new DTOCauThu();
            _newCauThu.MaCauThu = tbHOME_TienDangKyHOSoCauThu_MaCauThu.Text;
            _newCauThu.HoTenCauThu = tbHOME_TienDangKyHOSoCauThu_TenCauThu.Text;
            _newCauThu.NgaySinh = dtpHOME_TienDangKyHOSoCauThu_NgaySinh.Value;
            if (rbtHOME_TienDangKyHOSoCauThu_LoaiCauThu_VietNam.Checked == true)
            {
                _newCauThu.MaLoaiCauThu = 1;
            }
            else if (rbtHOME_TienDangKyHOSoCauThu_LoaiCauThu_NuocNgoai.Checked == true)
            {
                _newCauThu.MaLoaiCauThu = 2;
            }
            if (uData.InsertCauThu(_newCauThu) == 1)
            {
                MessageBox.Show("Thêm cầu thủ thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                tbHOME_TienDangKyHOSoCauThu_MaCauThu.Text = "";
                tbHOME_TienDangKyHOSoCauThu_TenCauThu.Text = "";
                dtpHOME_TienDangKyHOSoCauThu_NgaySinh.Value = Convert.ToDateTime("1/1/2000");
                rbtHOME_TienDangKyHOSoCauThu_LoaiCauThu_VietNam.Checked = true;

                List<DTOCauThu> dsCauThu = new List<DTOCauThu>();
                dsCauThu = data.DanhSachCauThu();
                if (dsCauThu.Count > 0)
                {
                    dsCauThu.OrderBy(r => r.MaCauThu).ToList();
                    string ma = dsCauThu[dsCauThu.Count - 1].MaCauThu;
                    int id = int.Parse(ma.Substring(2)) + 1;
                    tbHOME_TienDangKyHOSoCauThu_MaCauThu.Text = "CT" + id.ToString("000");
                }
                else
                {
                    tbHOME_TienDangKyHOSoCauThu_MaCauThu.Text = "CT001";
                }
            }
        }
        #endregion

        #region Tạo lịch thi đấu
        private void btTaoLichThiDau_TaoLichThiDau_Click(object sender, EventArgs e)
        {
            BUSTaoLichThiDau tl = new BUSTaoLichThiDau();
            List<DTOChiTietVongDau> lichThiDau = tl.TaoLichThiDau(_maMuaGiaiSelected_TaoLichThiDau, _tenMuagiaiSelected_TaoLichThiDau, dtpNgayBatDau.Value, Decimal.ToInt32(nudfirstmatch.Value), Decimal.ToInt32(nudlastmatch.Value), Decimal.ToInt32(nudKhoangCachVongDau.Value), Decimal.ToInt32(nudThoiGianNghiGiuaHaiLuot.Value));

            BindingSource source1 = new BindingSource();
            source1.DataSource = lichThiDau.OrderBy(r => r.MaVongDau).ToList();
            dgvListXepHang.DataSource = source1;
            dgvListXepHang.Columns["MaMuaGiai"].Visible = false;
        }

        private void btTaoLichThiDau_XacNhanLichThiDau_Click(object sender, EventArgs e)
        {
            UpdateData upd = new UpdateData();
            TransferData data = new TransferData();


            DTOMuaGiai uMuaGiai = new DTOMuaGiai();
            List<DTOMuaGiai> dsMuaGiai = data.DanhSachMuaGiai();
            foreach(var mg in dsMuaGiai)
            {
                if(mg.MaMuaGiai == _maMuaGiaiSelected_TaoLichThiDau)
                {
                    uMuaGiai = mg;
                    break;
                }
            }

            

            List<DTODoiBong> danhSachDoiBong = data.DanhSachDoiBong();
            DTOChiTietTranDau tranDau = new DTOChiTietTranDau();
            List<DTOChiTietTranDau> lichThiDau = new List<DTOChiTietTranDau>();
            foreach (DataGridViewRow row in dgvListXepHang.Rows)
            {
                tranDau.MaMuaGiai = _maMuaGiaiSelected_TaoLichThiDau;
                tranDau.MaVongDau = row.Cells[1].Value.ToString();
                tranDau.MaTranDau = row.Cells[2].Value.ToString();
                string chuNha = row.Cells[5].Value.ToString();
                string khach = row.Cells[6].Value.ToString();
                foreach (var db in danhSachDoiBong)
                {
                    if (chuNha == db.TenDoiBong)
                    {
                        tranDau.MaChuNha = db.MaDoiBong;
                    }
                    else if (khach == db.TenDoiBong)
                    {
                        tranDau.MaKhach = db.MaDoiBong;
                    }
                    else if (tranDau.MaChuNha != null && tranDau.MaKhach != null)
                    {
                        break;
                    }
                }
                tranDau.NgayThiDau = Convert.ToDateTime(row.Cells[3].Value);
                tranDau.GioThiDau = TimeSpan.Parse(row.Cells[4].Value.ToString());
                lichThiDau.Add(tranDau);
                tranDau = null;
                tranDau = new DTOChiTietTranDau();
            }
            if (uData.UpdateLichThiDau(lichThiDau) == 1)
            {
                uMuaGiai.TenMuaGiai = dgvListXepHang.Rows[0].Cells[7].Value.ToString();
                upd.UpdateMuaGiai(uMuaGiai.MaMuaGiai, uMuaGiai);

                MessageBox.Show("Tạo lịch thi đấu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                ((Control)this.gbTaoLichThiDau_TaoLichThiDau_QuyDinh).Enabled = false;
                LoadDanhSachMuaGiai_TaoLichThiDau();
            }
        }

        private void LoadDanhSachMuaGiai_TaoLichThiDau()
        {
            data = new TransferData();
            List<DTOMuaGiai> dsMuaGiai = data.DanhSachMuaGiai();
            List<DTOMuaGiai> dsMuaGiaiHoanTatDangKy = dsMuaGiai
                .Where(x => x.HoanTatDangKy == true).ToList();

            dgvTaoLichThiDau_DanhSachMuaGiai.DataSource = dsMuaGiaiHoanTatDangKy;
            dgvTaoLichThiDau_DanhSachMuaGiai.Columns["SoLuongDoiThamDuToiDa"].Visible = false;
            dgvTaoLichThiDau_DanhSachMuaGiai.Columns["SoLuongDoiDaDangKy"].Visible = false;
            dgvTaoLichThiDau_DanhSachMuaGiai.Columns["HoanTatDangKy"].Visible = false;
            //dgvTaoLichThiDau_DanhSachMuaGiai.Columns["TrangThai"]. = "False";

        }
        string _maMuaGiaiSelected_TaoLichThiDau;
        string _tenMuagiaiSelected_TaoLichThiDau;
        private void dgvTaoLichThiDau_DanhSachMuaGiai_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;
            _maMuaGiaiSelected_TaoLichThiDau = senderGrid.Rows[e.RowIndex].Cells[1].Value.ToString();
            _tenMuagiaiSelected_TaoLichThiDau = senderGrid.Rows[e.RowIndex].Cells[2].Value.ToString();
            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0)
            {
                data = new TransferData();
                List<DTOMuaGiai> dsMuaGiai = data.DanhSachMuaGiai();
                foreach(var mg in dsMuaGiai)
                {
                    if (mg.MaMuaGiai == _maMuaGiaiSelected_TaoLichThiDau)
                    {
                        string check = mg.TenMuaGiai.Substring(mg.TenMuaGiai.Length - 4);
                        if(check.All(char.IsDigit) == true)
                        {
                            MessageBox.Show("GIẢI ĐẤU ĐÃ HOÀN TẤT TẠO LỊCH THI ĐẤU", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    }
                }
                tcTaoLichThiDau.SelectedTab = tpTaoLichThiDau_TaoLichThiDau;
                ((Control)this.gbTaoLichThiDau_TaoLichThiDau_QuyDinh).Enabled = true;
            }
        }
        #endregion

        #region Đăng ký giải đấu
        private void btHOME_DangKyGiaiDau_DangKy_Click(object sender, EventArgs e)
        {
            DTOMuaGiai _newMuaGiai = new DTOMuaGiai();
            _newMuaGiai.MaMuaGiai = tbHOME_DangKyGiaiDau_MaGiaiDau.Text;
            _newMuaGiai.TenMuaGiai = cbbHOME_DangKyGiaiDau_TenGiaiDau.Text;
            _newMuaGiai.SoLuongDoiThamDuToiDa = Convert.ToInt32(nudHOME_DangKyGiaiDau_SoLuongDoi.Value);
            if (uData.InsertMuaGiai(_newMuaGiai) == 1)
            {
                MessageBox.Show("Thêm thông tin thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                LoadDanhSachGiaiDaDangKy();
            }
            else
            {
                MessageBox.Show("Lỗi", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
        private void LoadDanhSachGiaiDaDangKy()
        {
            string _maMuaGiai;
            List<DTOMuaGiai> dsMuaGiai = data.DanhSachMuaGiai();
            dgvDanhSachMuaGiai.DataSource = dsMuaGiai;
            dgvDanhSachMuaGiai.Columns["SoLuongDoiDaDangKy"].Visible = false;
            dgvDanhSachMuaGiai.Columns["HoanTatDangKy"].Visible = false;
            if (dsMuaGiai.Count > 0)
            {
                string _ma = dgvDanhSachMuaGiai.Rows[dgvDanhSachMuaGiai.Rows.Count - 1].Cells["MaMuaGiaiDSMG"].Value.ToString();
                int _id = int.Parse(_ma.Substring(2)) + 1;
                _maMuaGiai = "MG" + _id.ToString("000");
            }
            else
            {
                _maMuaGiai = "MG001";
            }
            tbHOME_DangKyGiaiDau_MaGiaiDau.Text = _maMuaGiai;

        }
        #endregion

        #region Tiếp nhận hồ sơ

        #region Tiếp nhận hồ sơ - Quy định
        // Load danh sách Mã giải thi đấu chưa hoàn tất đăng ký có thể đăng ký thêm đội tuyển tham dự
        // Tabpage Quy định đăng ký
        private void LoadDanhSachGiaiDau()
        {
            data = new TransferData();
            List<DTOMuaGiai> dsMuaGiai = data.DanhSachMuaGiai();
            dgvTiepNhanHoSo_DangKyGiaiDau.DataSource = dsMuaGiai;
        }
        
        // Từ Mã giải đấu đã chọn cung cấp Tên giải, Số lượng đội bóng tham dự tối đa và số lượng đội bóng đã đăng ký
        private void cbbTiepNhanHoSo_DanhSachGiaiDauCoTheThamDu_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<DTOMuaGiai> dsGiaiDuocPhepDangKY = data.DanhSachMuaGiai();
            foreach (var mg in dsGiaiDuocPhepDangKY)
            {
                if (mg.MaMuaGiai == _maMuaGiaiSelected)
                {
                    tbTiepNhanHoSo_DanhSachGiaiDauCoTheThamDu.Text = mg.TenMuaGiai;
                    //_soDoiBoiToiDaSelected = mg.SoLuongDoiThamDuToiDa.ToString();
                    tbTiepNhanHoSo_SoLuongDoiDaHoanTatDangKy.Text = mg.SoLuongDoiDaDangKy.ToString();
                }
            }
            List<DTODoiBongThamGiaGiaiDau> dsDoiBongDaDangKy = data.DanhSachDoiBongDaDangKyGiaiDau(_maMuaGiaiSelected);
            tbTiepNhanHoSo_SoLuongDoiDaDangKy.Text = dsDoiBongDaDangKy.Count().ToString();
        }
        private void btTiepNhanHoSo_DangKy_Click(object sender, EventArgs e)
        {
            if (_maMuaGiaiSelected == "")
            {
                MessageBox.Show("Vui lòng chọn mùa giải", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            btTiepNhanHoSo_DangKy.Text = "Đang đăng ký đội bóng";
            btTiepNhanHoSo_DangKy.Enabled = false;
            tcTiepNhanHoSo.SelectedTab = tpTiepNhanHoSo_DangKyTTDoiBong;
            
            cbbTiepNhanHoSo_DanhSachGiaiDauCoTheThamDu.Enabled = false;
        }

        string _maMuaGiaiSelected;
        int _soDoiBoiToiDaSelected;
        private void dgvTiepNhanHoSo_DangKyGiaiDau_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;
            _maMuaGiaiSelected = senderGrid.Rows[e.RowIndex].Cells[2].Value.ToString();
            _soDoiBoiToiDaSelected = Convert.ToInt32(senderGrid.Rows[e.RowIndex].Cells[4].Value.ToString());
            if (senderGrid.Rows[e.RowIndex].Cells[6].Value.ToString() == "True")
            {
                MessageBox.Show("GIẢI ĐẤU ĐÃ HOÀN TẤT ĐĂNG KÝ", "Trạng thái", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && senderGrid.Columns[e.ColumnIndex].HeaderText == "Cập nhật" && e.RowIndex >= 0)
            {

                ((Control)this.gbTiepNhanHoSo_DangKyThongTinDoiBong_DanhSachCauThuCoTheDangKy).Enabled = false;
                ((Control)this.gbTiepNhanHOSo_DangKYThongTinCauThu).Enabled = false;

                tcTiepNhanHoSo.SelectedTab = tpTiepNhanHoSo_DangKyTTDoiBong;

                LoadDanhSachDoiBongDaDangKy();
            }
            else if(senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && senderGrid.Columns[e.ColumnIndex].HeaderText == "Hoàn tất" && e.RowIndex >= 0)
            {
                if (BUSKiemTraHoanTatDangKyGiaiDau.KiemTraHoanTatDangKy(_maMuaGiaiSelected) == -1)
                {
                    MessageBox.Show("Tồn tại đội bóng chưa hoàn tất đăng ký", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                else if (BUSKiemTraHoanTatDangKyGiaiDau.KiemTraHoanTatDangKy(_maMuaGiaiSelected) == -2)
                {
                    MessageBox.Show("Chưa đủ số lượng đội bóng quy định", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                else if (BUSKiemTraHoanTatDangKyGiaiDau.KiemTraHoanTatDangKy(_maMuaGiaiSelected) == 1)
                {
                    DTOMuaGiai mg = new DTOMuaGiai();
                    mg.MaMuaGiai = senderGrid.Rows[e.RowIndex].Cells[2].Value.ToString();
                    mg.TenMuaGiai = senderGrid.Rows[e.RowIndex].Cells[3].Value.ToString();
                    mg.SoLuongDoiDaDangKy = Convert.ToInt32(senderGrid.Rows[e.RowIndex].Cells[4].Value.ToString());
                    mg.SoLuongDoiThamDuToiDa= Convert.ToInt32(senderGrid.Rows[e.RowIndex].Cells[5].Value.ToString());
                    mg.HoanTatDangKy = true;
                    uData = new UpdateData();
                    if(uData.UpdateMuaGiai(mg.MaMuaGiai, mg) == 1)
                    {
                        MessageBox.Show("Cập nhật mùa giải thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LoadDanhSachGiaiDau();
                    }
                    else
                    {
                        MessageBox.Show("Lỗi", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        
        }
        #endregion

        #region Tiếp nhận hồ sơ - Đăng ký đội bóng
        private void LoadDanhSachMaDoiBong_TiepNhanHoSo()
        {
            List<DTODoiBong> dsDoiBong = new List<DTODoiBong>();
            dsDoiBong = data.DanhSachDoiBong();
            cbbTNHS_DKDB_DKDB_MaDoiBong.Items.Clear();
            foreach (var db in dsDoiBong)
            {
                cbbTNHS_DKDB_DKDB_MaDoiBong.Items.Add(db.MaDoiBong);
            }
        }
        
        private void LoadDanhSachDoiBongDaDangKy()
        {
            ((Control)this.gbTiepNhanHoSo_DanhSachDoiBongCoTheDangKy).Enabled = true;
            ((Control)this.gpTiepNhanHOSo_DangKyThongTinDoiBong).Enabled = true;

            gbTiepNhanHoSo_DanhSachDoiBongCoTheDangKy.Text = "Danh sách đội bóng có thể đăng ký";
            gpTiepNhanHOSo_DangKyThongTinDoiBong.Text = "Danh sách đội bóng đã đăng ký";
            gbTiepNhanHoSo_DanhSachDoiBongCoTheDangKy.Text = gbTiepNhanHoSo_DanhSachDoiBongCoTheDangKy.Text + " " + _maMuaGiaiSelected;
            gpTiepNhanHOSo_DangKyThongTinDoiBong.Text = gpTiepNhanHOSo_DangKyThongTinDoiBong.Text + " " + _maMuaGiaiSelected;

            dgvTiepNhanHoSo_DanhSachDoiBOngDaDangKy.Refresh();
            List<DTODoiBongThamGiaGiaiDau> dsDoiBong = new List<DTODoiBongThamGiaGiaiDau>();
            data = new TransferData();
            dsDoiBong = data.DanhSachDoiBongDaDangKyGiaiDau(_maMuaGiaiSelected);

            dgvTiepNhanHoSo_DanhSachDoiBOngDaDangKy.DataSource = dsDoiBong;
            dgvTiepNhanHoSo_DanhSachDoiBOngDaDangKy.Columns["MaMuaGiai"].Visible = false;
            //dgvTiepNhanHoSo_DanhSachDoiBOngDaDangKy.Columns["MaDoiBong"].Visible = false;
            //dgvTiepNhanHoSo_DanhSachDoiBOngDaDangKy.Columns["SoLuongCauThuDangKy"].Visible = false;
            dgvTiepNhanHoSo_DanhSachDoiBOngDaDangKy.Columns["SoLuongCauThuNuocNgoai"].Visible = false;
            foreach (DataGridViewRow row in dgvTiepNhanHoSo_DanhSachDoiBOngDaDangKy.Rows)
            {
                if (row.Cells[8].Value.ToString() == "True")
                {
                }
            }
            LoadDanhSachGiaiDau();
        }

        // Xóa đội bóng dùng DataGridview
        private void dgvTNHS_DKDB_DanhSachDoiBong_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            uData = new UpdateData();
            var senderGrid = (DataGridView)sender;
            if(senderGrid.Rows[e.RowIndex].Cells[8].Value.ToString()== "True")
            {
                return;
            }
            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && senderGrid.Columns[e.ColumnIndex].HeaderText == "Xóa" && e.RowIndex >= 0)
            {
                DTODoiBongThamGiaGiaiDau db = new DTODoiBongThamGiaGiaiDau();
                db.MaMuaGiai = senderGrid.Rows[e.RowIndex].Cells[3].Value.ToString();
                db.MaDoiBong = senderGrid.Rows[e.RowIndex].Cells[4].Value.ToString();
                string xacNhanXoaDoiBong = "Xác nhận xóa đội bóng khỏi danh sách";
                string xoaDoiBongCaption = "Xóa đội bóng";
                var result = MessageBox.Show(xacNhanXoaDoiBong, xoaDoiBongCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.No)
                {
                    return;
                }
                else if (result == DialogResult.Yes)
                {
                    if (uData.DeleteDoiBongThamGiaGiaiDau(db) == 1)
                    {
                        DTOCauThuThuocDoiBongThamGiaGiaiDau cauThu = new DTOCauThuThuocDoiBongThamGiaGiaiDau();
                        cauThu.MaMuaGiai = _maMuaGiaiSelected;
                        cauThu.MaDoiBong = senderGrid.Rows[e.RowIndex].Cells[4].Value.ToString();
                        if (uData.DeleteCauThuThamGiaGiaiDau(cauThu) == 1)
                        {
                            MessageBox.Show("Xóa danh sách cầu thủ thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        MessageBox.Show("Xóa đội bóng thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        // Reset Datagridview
                        LoadDanhSachDoiBongDaDangKy();
                    }
                    else
                    {
                        MessageBox.Show("Lỗi", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    LoadDanhSachDoiBongCoTheDangKy();
                }
            }
            else if(senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && senderGrid.Columns[e.ColumnIndex].HeaderText == "Cập nhật" && e.RowIndex >= 0)
            {
                LoadDanhSachCauThuDaDangKyThuocDoiBong(senderGrid.Rows[e.RowIndex].Cells[3].Value.ToString(), senderGrid.Rows[e.RowIndex].Cells[4].Value.ToString());
                ((Control)this.gbTiepNhanHOSo_DangKYThongTinCauThu).Enabled = true;
                tcTiepNhanHoSoDangKyThongTinDoiBong.SelectedTab = tpTiepNhanHoSoDangKyThongTinCauThu;
                _maDoiBongSelectedTiepNhanHoSo = senderGrid.Rows[e.RowIndex].Cells[4].Value.ToString();
                _tenDoiBongSelectedTiepNhanHoSo = senderGrid.Rows[e.RowIndex].Cells[5].Value.ToString();
            }
            else if(senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && senderGrid.Columns[e.ColumnIndex].HeaderText == "Hoàn tất" && e.RowIndex >= 0)
            {
                int kiemTra = BUSKiemTraHoanTatDangKyDoiBong.KiemTraHoanTatDangKyDoiBong(senderGrid.Rows[e.RowIndex].Cells[3].Value.ToString(), senderGrid.Rows[e.RowIndex].Cells[4].Value.ToString());
                if (kiemTra == 1)
                {
                    if (uData.UpdateDoiBongThamGiaGiaiDau(senderGrid.Rows[e.RowIndex].Cells[3].Value.ToString(), senderGrid.Rows[e.RowIndex].Cells[4].Value.ToString(), true) == 1)
                    {
                        MessageBox.Show("Xác nhận hoàn tất đăng ký đội bóng thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LoadDanhSachDoiBongDaDangKy();
                        return;
                    }
                }
                else if (kiemTra == -1)
                {
                    MessageBox.Show("Đội bóng chưa đăng ký đủ số lượng cẩu thủ tối thiểu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                else if (kiemTra == -2)
                {
                    MessageBox.Show("Đội bóng đã đăng ký số lượng cầu thủ vượt quá số lượng cầu thủ cho phép", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                else if (kiemTra == -3)
                {
                    MessageBox.Show("Đội bóng đã đăng ký số lượng cầu thủ nước ngoài vượt quá quy định", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
        }
        private void LoadDanhSachDoiBongCoTheDangKy()
        {
            // Toàn bộ danh sách đội bóng
            List<DTODoiBong> dsDoiBong = data.DanhSachDoiBong();

            // Danh sách đội bóng đã đăng ký tham gia thi đấu tại một mùa giải nhất định
            List<DTODoiBongThamGiaGiaiDau> dsDoiBongThamGiaGiaiDau = data.DanhSachDoiBongDaDangKyGiaiDau(_maMuaGiaiSelected);

            var dsDTODoiBongDaDangKy = dsDoiBongThamGiaGiaiDau.
                Select(x => new DTODoiBong()
                {
                    MaDoiBong = x.MaDoiBong,
                    TenDoiBong = x.TenDoiBong,
                }).ToList();

            List<DTODoiBong> dsDoiBongCoTheDangKy = new List<DTODoiBong>();
            foreach (var db in dsDoiBong)
            {
                if (dsDoiBongThamGiaGiaiDau.Any(x => x.MaDoiBong == db.MaDoiBong) == false)
                {
                    dsDoiBongCoTheDangKy.Add(db);
                }
            }

            dgvChiTietThongTinDoiBOng.DataSource = dsDoiBongCoTheDangKy;
            try
            {
                dgvChiTietThongTinDoiBOng.Columns["DaXoa"].Visible = false;
                dgvChiTietThongTinDoiBOng.Columns["GhiChu"].Visible = false;
                dgvChiTietThongTinDoiBOng.Columns["SanNha"].Visible = false;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void dgvChiTietThongTinDoiBOng_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;
            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0)
            {
                DTODoiBongThamGiaGiaiDau _newDoiBong = new DTODoiBongThamGiaGiaiDau();
                List<DTODoiBongThamGiaGiaiDau> dsDoiBongDaDangKy = data.DanhSachDoiBongDaDangKyGiaiDau(_maMuaGiaiSelected);
                tbTiepNhanHoSo_SoLuongDoiDaDangKy.Text = dsDoiBongDaDangKy.Count().ToString();

                List<DTODoiBongThamGiaGiaiDau> dsDoiBongDaDangKy1 = data.DanhSachDoiBongDaDangKyGiaiDau(_maMuaGiaiSelected, senderGrid.Rows[e.RowIndex].Cells[1].Value.ToString());

                if (dsDoiBongDaDangKy1.Count != 0)
                {
                    MessageBox.Show("Tiếp tục cập nhật thông tin đội bóng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (dsDoiBongDaDangKy1.Count == 0 && dsDoiBongDaDangKy.Count() < _soDoiBoiToiDaSelected)
                {
                    _newDoiBong.MaMuaGiai = _maMuaGiaiSelected;
                    _newDoiBong.MaDoiBong = senderGrid.Rows[e.RowIndex].Cells[1].Value.ToString();
                    _newDoiBong.TenDoiBong = senderGrid.Rows[e.RowIndex].Cells[2].Value.ToString();
                    _newDoiBong.HoanTatDangKy = false;
                    if (uData.InsertDoiBongThamGiaGiaiDau(_newDoiBong) == 1)
                    {
                        MessageBox.Show("Hoàn tất đăng ký mới đội bóng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("Lỗi", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("Số lượng đội bóng đăng ký đã đủ, vui lòng kiểm tra lại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                LoadDanhSachDoiBongDaDangKy();
                LoadDanhSachDoiBongCoTheDangKy();
            }
        }

        #endregion

        #region Tiếp nhận hồ sơ - Đăng ký danh sách cầu thủ

        // ????????
        private void cbbTNHS_DKDB_DKCT_MaCauThu_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<DTOCauThu> dsCauThuTuDo = new List<DTOCauThu>();
            dsCauThuTuDo = data.DanhSachCauThu();
            foreach (var ct in dsCauThuTuDo)
            {
                if (ct.MaCauThu == cbbTNHS_DKDB_DKCT_MaCauThu.SelectedItem.ToString())
                {
                    tbTNHS_DKDB_DKCT_TenCauThu.Text = ct.HoTenCauThu;
                    if (ct.MaLoaiCauThu == 1)
                    {
                        tbTNHS_DKDB_DKCT_LoaiCauThu.Text = "Việt Nam";
                    }
                    else if (ct.MaLoaiCauThu == 2)
                    {
                        tbTNHS_DKDB_DKCT_LoaiCauThu.Text = "Nước ngoài";
                    }
                    tbTNHS_DKDB_DKCT_NgaySinh.Text = ct.NgaySinh.ToString();
                }
            }
        }    

        private void dgvTNHS_DKDB_DKCT_DanhSachCauThu_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;
            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0)
            {
                DTOCauThuThuocDoiBongThamGiaGiaiDau ct = new DTOCauThuThuocDoiBongThamGiaGiaiDau();
                ct.MaCauThu = senderGrid.Rows[e.RowIndex].Cells[2].Value.ToString();
                ct.MaMuaGiai = senderGrid.Rows[e.RowIndex].Cells[1].Value.ToString();
                //ct.MaDoiBong = senderGrid.Rows[e.RowIndex].Cells[4].Value.ToString();

                const string xacNhanXoaCauThu = "Xác nhận xóa cầu thủ khỏi danh sách";
                const string xoaCauThuCaption = "Xóa cầu thủ";
                var result = MessageBox.Show(xacNhanXoaCauThu, xoaCauThuCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.No)
                {
                    return;
                }
                else if (result == DialogResult.Yes)
                {
                    if (uData.DeleteCauThuThamGiaGiaiDau(ct) == 1)
                    {
                        //MessageBox.Show("Xóa cầu thủ thành công");

                        // Reset Datagridview
                        LoadDanhSachCauThuDaDangKyThuocDoiBong(ct.MaMuaGiai, senderGrid.Rows[e.RowIndex].Cells[4].Value.ToString());
                    }
                }
                LoadDanhSachCauThuCoTheDangKy();
            }
        }

        private void LoadDanhSachCauThuCoTheDangKy()
        {
            // Toàn bộ danh sách cầu thủ
            List<DTOCauThu> dsCauThu = data.DanhSachCauThu();

            // Danh sách cầu thủ đã đăng ký tham gia thi đấu tại một mùa giải nhất định
            List<DTOCauThuThuocDoiBongThamGiaGiaiDau> dsCauThuThamGiaGiaiDau = data.DanhSachCauThuThamGiaGiaiDau(_maMuaGiaiSelected);


            var dsDTOCauThuDaDangKy = dsCauThuThamGiaGiaiDau.
                Select(x => new DTOCauThu()
                {
                    MaCauThu = x.MaCauThu,
                    HoTenCauThu = x.TenCauThu
                }).ToList();

            List<DTOCauThu> dsCauThuCoTheDangKy = new List<DTOCauThu>();
            foreach (var ct in dsCauThu)
            {
                if (dsCauThuThamGiaGiaiDau.Any(x => x.MaCauThu == ct.MaCauThu) == false)
                {
                    dsCauThuCoTheDangKy.Add(ct);
                }
            }

            dgvChiTietTTCT.DataSource = dsCauThuCoTheDangKy;
            try
            {
                dgvChiTietTTCT.Columns["MaLoaiCauThu"].Visible = false;
                dgvChiTietTTCT.Columns["NgaySinh"].Visible = false;
                dgvChiTietTTCT.Columns["MaDoiBong"].Visible = false;
                dgvChiTietTTCT.Columns["TongSoBanThang"].Visible = false;
                dgvChiTietTTCT.Columns["DaXoa"].Visible = false;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void LoadDanhSachCauThuDaDangKyThuocDoiBong(string _maMuaGiai, string _maDoiBong)
        {
            ((Control)this.gbTiepNhanHoSo_DangKyThongTinDoiBong_DanhSachCauThuCoTheDangKy).Enabled = true;
            ((Control)this.gbTiepNhanHOSo_DangKYThongTinCauThu).Enabled = true;
            List<DTOCauThuThuocDoiBongThamGiaGiaiDau> dsCauThuThamGiaGiai = new List<DTOCauThuThuocDoiBongThamGiaGiaiDau>();
            dsCauThuThamGiaGiai = data.DanhSachCauThuThamGiaGiaiDau(_maMuaGiai, _maDoiBong);
            tbTNHS_DKDB_DKDB_SoLuongCauThu.Text = dsCauThuThamGiaGiai.Count.ToString();
            dgvTNHS_DKDB_DKCT_DanhSachCauThu.DataSource = dsCauThuThamGiaGiai;
            dgvTNHS_DKDB_DKCT_DanhSachCauThu.Columns["MaMuaGiai"].Visible = false;
            dgvTNHS_DKDB_DKCT_DanhSachCauThu.Columns["MaDoiBong"].Visible = false;
            dgvTNHS_DKDB_DKCT_DanhSachCauThu.Columns["TenDoiBong"].Visible = false;
            dgvTNHS_DKDB_DKCT_DanhSachCauThu.Columns["TongSoBanThang"].Visible = false;
            dgvTNHS_DKDB_DKCT_DanhSachCauThu.Columns["MaLoaiCauThu"].Visible = false;
        }

        string _maDoiBongSelectedTiepNhanHoSo;
        string _tenDoiBongSelectedTiepNhanHoSo;
        private void dgvChiTietTTCT_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
            var senderGrid = (DataGridView)sender;
            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0)
            {
                DTOCauThuThuocDoiBongThamGiaGiaiDau _newCauThu = new DTOCauThuThuocDoiBongThamGiaGiaiDau();
                _newCauThu.MaMuaGiai = _maMuaGiaiSelected;
                _newCauThu.MaDoiBong = _maDoiBongSelectedTiepNhanHoSo;
                _newCauThu.MaCauThu = senderGrid.Rows[e.RowIndex].Cells[1].Value.ToString();
                _newCauThu.TenDoiBong = _tenDoiBongSelectedTiepNhanHoSo;
                _newCauThu.TenCauThu = senderGrid.Rows[e.RowIndex].Cells[2].Value.ToString();
                _newCauThu.MaLoaiCauThu = Convert.ToInt32(senderGrid.Rows[e.RowIndex].Cells[4].Value.ToString());
                _newCauThu.TongSoBanThang = 0;

                if (BUSKiemTraDangKyCauThu.KiemTraThongTinDangKy(_newCauThu, Convert.ToDateTime(senderGrid.Rows[e.RowIndex].Cells[3].Value.ToString())) == -1)
                {
                    MessageBox.Show("Cầu thủ vượt quá độ tuổi quy định, vui lòng chọn cầu thủ khác", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                else if (BUSKiemTraDangKyCauThu.KiemTraThongTinDangKy(_newCauThu, Convert.ToDateTime(senderGrid.Rows[e.RowIndex].Cells[3].Value.ToString())) == -2)
                {
                    MessageBox.Show("Cầu thủ chưa đủ độ tuổi quy định, vui lòng chọn cầu thủ khác", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (uData.InsertCauThuThamGiaGiaiDau(_newCauThu) == 1)
                {
                    //MessageBox.Show("Đã thêm cầu thủ thành công");
                }
                List<DTOCauThuThuocDoiBongThamGiaGiaiDau> dsCauThuThamGiaGiai = new List<DTOCauThuThuocDoiBongThamGiaGiaiDau>();

                dsCauThuThamGiaGiai = data.DanhSachCauThuThamGiaGiaiDau(_maMuaGiaiSelected, _newCauThu.MaDoiBong);
                tbTNHS_DKDB_DKDB_SoLuongCauThu.Text = dsCauThuThamGiaGiai.Count.ToString();


                //dgvTNHS_DKDB_DKCT_DanhSachCauThu.DataSource = dsCauThuThamGiaGiai;
                //dgvTNHS_DKDB_DKCT_DanhSachCauThu.Columns["MaMuaGiai"].Visible = false;
                //dgvTNHS_DKDB_DKCT_DanhSachCauThu.Columns["MaDoiBong"].Visible = false;
                //dgvTNHS_DKDB_DKCT_DanhSachCauThu.Columns["TenDoiBong"].Visible = false;
                //dgvTNHS_DKDB_DKCT_DanhSachCauThu.Columns["MaCauThu"].Visible = false;
                //dgvTNHS_DKDB_DKCT_DanhSachCauThu.Columns["TongSoBanThang"].Visible = false;
                //dgvTNHS_DKDB_DKCT_DanhSachCauThu.Columns["MaLoaiCauThu"].Visible = false;
                LoadDanhSachDoiBongDaDangKy();
                LoadDanhSachCauThuCoTheDangKy();
                LoadDanhSachCauThuDaDangKyThuocDoiBong(_newCauThu.MaMuaGiai, _newCauThu.MaDoiBong);
            }
        }

        #endregion

        #endregion

        #region MENU CONTROL
        private void tcMenu_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcMenu.SelectedTab == tpBangXepHang)
            {
                LoadMenuBangXepHang();
            }
            else if (tcMenu.SelectedTab == tpTiepNhanHoSo)
            {
                LoadDanhSachGiaiDau();
                ((Control)this.gbTiepNhanHoSo_DanhSachDoiBongCoTheDangKy).Enabled = false;
                ((Control)this.gpTiepNhanHOSo_DangKyThongTinDoiBong).Enabled = false;
            }
            else if (tcMenu.SelectedTab == tpTaoLichThiDau)
            {
                LoadDanhSachMuaGiai_TaoLichThiDau();
            }
            else if (tcMenu.SelectedTab == tpCapNhatKetQua)
            {
                LoadDanhSachMuaGiai_CapNhatKetQua();
            }
            else if (tcMenu.SelectedTab == tpSoLieuThongKe)
            {
                LoadMenuSoLieuThongKe();
            }
            else if (tcMenu.SelectedTab == tpTraCuuCauThu)
            {
                LoadThongTinTraCuu();
            }
            else if (tcMenu.SelectedTab == tpMenuLichThiDau)
            {
                LoadMenuLichThiDau();
            }
        }
        #endregion

        #region HOME CONTROL
        private void tcHOME_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcHOME.SelectedTab == tpHOME_DangKyGiaiDau)
            {
                LoadDanhSachGiaiDaDangKy();
            }
            else if (tcHOME.SelectedTab == tpHOME_TienDangKyDoiBong)
            {
                //((Control)this.tpDangKyHoSoDoiBong_ThongTinDangKy).Enabled = false;
                //((Control)this.btDangKyHoSoDoiBong_XacNhanDangKy_DangKy).Enabled = true;
                //btDangKyHoSoDoiBong_XacNhanDangKy_DangKy.Text = "Đăng ký";
                LoadThongTinTienDangKy_HOME();
            }
            else if (tcHOME.SelectedTab == tpHome_HoSoGiaiDau)
            {
                tcHoSoGiaiDau.SelectedTab = tpHoSoGiaiDau_HoSoDoiBong;
                List<DTODoiBong> dsDoiBong = data.DanhSachDoiBong();
                dgvDanhSachDoiBongTongHop.DataSource = dsDoiBong;
                try
                {
                    dgvDanhSachDoiBongTongHop.Columns["GhiChu"].Visible = false;
                    dgvDanhSachDoiBongTongHop.Columns["DaXoa"].Visible = false;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        #endregion

        #region TIEPNHANHOSO CONTROL
        private void tcTiepNhanHoSo_SelectedIndexChanged(object sender, EventArgs e)
        {

            if(tcTiepNhanHoSo.SelectedTab == tpTiepNhanHoSo_DangKyTTDoiBong)
            {
                LoadDanhSachMaDoiBong_TiepNhanHoSo();
                tcTiepNhanHoSoDangKyThongTinDoiBong.SelectedTab = tpTiepNhanHoSo_DangKyThongTinDoiBOng;
                List<DTODoiBong> dsDoiBong = data.DanhSachDoiBong();
                dgvChiTietThongTinDoiBOng.DataSource = dsDoiBong;
                try
                {
                    dgvChiTietThongTinDoiBOng.Columns["GhiChu"].Visible = false;
                    dgvChiTietThongTinDoiBOng.Columns["DaXoa"].Visible = false;
                    dgvChiTietThongTinDoiBOng.Columns["SanNha"].Visible = false;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                LoadDanhSachDoiBongCoTheDangKy();
            }
            if (tcTiepNhanHoSo.SelectedTab == tpTiepNhanHoSo_QuyDinh_DangKyGiaiDau)
            {
                ((Control)this.gbTiepNhanHoSo_DanhSachDoiBongCoTheDangKy).Enabled = false;
                ((Control)this.gpTiepNhanHOSo_DangKyThongTinDoiBong).Enabled = false;
            }
        }
        #endregion

        #region HOSOGIAIDAU CONTROL
        private void tcHoSoGiaiDau_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcHoSoGiaiDau.SelectedTab == tpHoSoGiaiDau_HoSoDoiBong)
            {
                List<DTODoiBong> dsDoiBong = data.DanhSachDoiBong();
                dgvDanhSachDoiBongTongHop.DataSource = dsDoiBong;
                try
                {
                    dgvDanhSachDoiBongTongHop.Columns["GhiChu"].Visible = false;
                    dgvDanhSachDoiBongTongHop.Columns["DaXoa"].Visible = false;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else if (tcHoSoGiaiDau.SelectedTab == tpHoSoGiaiDau_HoSoCauThu)
            {
                List<DTOCauThu> dsCauThu = data.DanhSachCauThu();
                dgvDanhSachCauThuTongHop.DataSource = dsCauThu;
                try
                {
                    dgvDanhSachCauThuTongHop.Columns["MaDoiBong"].Visible = false;
                    dgvDanhSachCauThuTongHop.Columns["DaXoa"].Visible = false;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }


        #endregion

        #region DANGKYHOSODOIBONG CONTROL
        private void tcTiepNhanHoSoDangKyThongTinDoiBong_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(tcTiepNhanHoSoDangKyThongTinDoiBong.SelectedTab == tpTiepNhanHoSoDangKyThongTinCauThu)
            {
                LoadDanhSachCauThuCoTheDangKy();
            }
            else if (tcTiepNhanHoSoDangKyThongTinDoiBong.SelectedTab == tpTiepNhanHoSo_DangKyThongTinDoiBOng)
            {
                ((Control)this.gbTiepNhanHoSo_DangKyThongTinDoiBong_DanhSachCauThuCoTheDangKy).Enabled = false;
                ((Control)this.gbTiepNhanHOSo_DangKYThongTinCauThu).Enabled = false;
            }
        }

        #endregion

        #region QUYDINH CONTROL
        private void tcQuyDinh_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(tcQuyDinh.SelectedTab == tpQuyDinh_SuaQuyDinh)
            {
                LoadDanhSachQuyDinh();
            }
        }
        #endregion

        #region temp
        private void btnDangNhap_Click(object sender, EventArgs e)
        {
            FormDangNhap dnhap = new FormDangNhap();
            dnhap.Show();
        }

        private void dangky_Click(object sender, EventArgs e)
        {
            FormDangKy dky = new FormDangKy();
            dky.Show();
        }











        #endregion

    }
}
