﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAcessLayer;
using DTO;


namespace BusinessLayer
{
    public class BUSTaoLichThiDau
    {
        BUSGenerateRoundRobinList gr = new BUSGenerateRoundRobinList();
        IDoiBongThamGiaGiaiDau db = new IpmDoiBongThamGiaGiaiDau();

        private int SoLuongDoiBong(string maMuaGiai)
        {
            int soLUongDoiBong = db.LoadDanhSachDoiBong(maMuaGiai).Count;
            return soLUongDoiBong;
        }
        public List<DTOChiTietVongDau> TaoLichThiDau(string maMuaGiai, string tenMuaGiai, DateTime firstMatch, int fg, int lg, int khoangCachNgayThiDau, int thoiGianNghiGiuaHaiLuot)
        {
            int num_teams = SoLuongDoiBong(maMuaGiai);
            int[,] results = gr.GenerateRoundRobin(num_teams);

            Random rd = new Random();
            List<DoiBongThamGiaGiaiDau> danhSachDoiBong = new List<DoiBongThamGiaGiaiDau>();
            List<DTOChiTietVongDau> lichThiDau = new List<DTOChiTietVongDau>();

            danhSachDoiBong = db.LoadDanhSachDoiBong();

            DateTime fNgayThiDau = firstMatch;
            DateTime fNgayThiDauLuotVe = fNgayThiDau.AddDays(thoiGianNghiGiuaHaiLuot + ((num_teams  - 1) * khoangCachNgayThiDau));
            DateTime ngayThiDauCuoiCung = fNgayThiDauLuotVe.AddDays((num_teams - 2) * khoangCachNgayThiDau);
 
            for (int round = 0; round <= results.GetUpperBound(1); round++)
            {
                int i = 1;
                for (int team = 0; team < num_teams; team++)
                {
                    DateTime rNgayThiDau = fNgayThiDau.AddDays(rd.Next(0, 2));
                    DateTime rNgayThiDauLuotVe = fNgayThiDauLuotVe.AddDays(rd.Next(0, 2));
                    TimeSpan rGioThiDau = new TimeSpan(rd.Next(fg, lg), rd.Next(0, 11) * 5, 0);
                    if (results[team, round] == BUSGenerateRoundRobinList.BYE)
                    {
                    }
                    else if (team < results[team, round])
                    {
                        if (fNgayThiDau.Year == ngayThiDauCuoiCung.Year)
                        {
                            lichThiDau.Add(new DTOChiTietVongDau()
                            {
                                MaTranDau = "TD" + i.ToString("000"),
                                MaVongDau = "VD" + (round + 1).ToString("000"),
                                TenChuNha = danhSachDoiBong[team].TenDoiBong,
                                TenKhach = danhSachDoiBong[results[team, round]].TenDoiBong,
                                NgayThiDau = rNgayThiDau,
                                GioThiDau = rGioThiDau,
                                TenMuaGiai = tenMuaGiai + " " + fNgayThiDau.Year.ToString()
                            });
                            lichThiDau.Add(new DTOChiTietVongDau()
                            {
                                MaTranDau = "TD" + i.ToString("000"),
                                MaVongDau = "VD" + (round + num_teams).ToString("000"),
                                TenChuNha = danhSachDoiBong[results[team, round]].TenDoiBong,
                                TenKhach = danhSachDoiBong[team].TenDoiBong,
                                NgayThiDau = rNgayThiDauLuotVe,
                                GioThiDau = rGioThiDau,
                                TenMuaGiai = tenMuaGiai + " " + fNgayThiDau.Year.ToString()
                            });
                        }
                        else
                        {
                            lichThiDau.Add(new DTOChiTietVongDau()
                            {
                                MaMuaGiai = "MG" + fNgayThiDau.Year,
                                MaTranDau = "TD" + i.ToString("000"),
                                MaVongDau = "VD" + (round + 1).ToString("000"),
                                TenChuNha = danhSachDoiBong[team].TenDoiBong,
                                TenKhach = danhSachDoiBong[results[team, round]].TenDoiBong,
                                NgayThiDau = rNgayThiDau,
                                GioThiDau = rGioThiDau,
                                TenMuaGiai = tenMuaGiai + " " + fNgayThiDau.Year.ToString() + "-" + ngayThiDauCuoiCung.Year.ToString()
                            });
                            lichThiDau.Add(new DTOChiTietVongDau()
                            {
                                MaMuaGiai = "MG" + fNgayThiDau.Year,
                                MaTranDau = "TD" + i.ToString("000"),
                                MaVongDau = "VD" + (round + num_teams).ToString("000"),
                                TenChuNha = danhSachDoiBong[results[team, round]].TenDoiBong,
                                TenKhach = danhSachDoiBong[team].TenDoiBong,
                                NgayThiDau = rNgayThiDauLuotVe,
                                GioThiDau = rGioThiDau,
                                TenMuaGiai = tenMuaGiai + " " + fNgayThiDau.Year.ToString() + "-" + ngayThiDauCuoiCung.Year.ToString()
                            });
                        }
                        
                        i++;
                    }
                }
                fNgayThiDau = fNgayThiDau.AddDays(khoangCachNgayThiDau);
                fNgayThiDauLuotVe = fNgayThiDauLuotVe.AddDays(khoangCachNgayThiDau);
            }
            //BindingSource source1 = new BindingSource();
            //source1.DataSource = lichThiDau.OrderBy(r => r.MaVongDau).ToList();
            //dtg_lichthidau.DataSource = source1;

            return lichThiDau;
        }
    }
}
