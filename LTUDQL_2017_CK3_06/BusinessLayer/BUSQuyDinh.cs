﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public static class BUSQuyDinh
    {
        public static int DoTuoiToiDa = 40;
        public static int DoTuoiToiThieu = 16;
        public static int SoLuongCauThuToiDa = 22;
        public static int SoLuongCauThuToiThieu = 15;
        public static int SoLuongCauThuNuocNgoaiToiDa = 3;
        public static int ThoiDiemGhiBanToiDa = 96;
    }
}
