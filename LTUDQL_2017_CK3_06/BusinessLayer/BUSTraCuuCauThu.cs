﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAcessLayer;
using DTO;

namespace BusinessLayer
{
    public class BUSTraCuuCauThu
    {
        public List<DTOCauThuThuocDoiBongThamGiaGiaiDau> TimKiemCauThu(string thongTin, int loaiTimKiem)
        {
            try
            {
                TransferData data = new TransferData();
                data = new TransferData();
                List<DTOCauThuThuocDoiBongThamGiaGiaiDau> dsCauThu = data.DanhSachCauThuThamGiaGiaiDau();
                List<DTOCauThuThuocDoiBongThamGiaGiaiDau> locDanhSach = new List<DTOCauThuThuocDoiBongThamGiaGiaiDau>();

                if (loaiTimKiem == 1)
                {
                    locDanhSach = dsCauThu
                        .Where(x => x.TenCauThu.Contains(thongTin) == true).ToList();
                    return locDanhSach;
                }
                else if (loaiTimKiem == 2)
                {
                    locDanhSach = dsCauThu
                        .Where(x => x.TenDoiBong.Contains(thongTin) == true).ToList();
                    return locDanhSach;
                }
                else
                {
                    return null;
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        public List<DTOCauThuThuocDoiBongThamGiaGiaiDau> LocDanhSachTimKiem(
            List<DTOCauThuThuocDoiBongThamGiaGiaiDau> dsCauThu,
            string maMuaGiai,
            string maDoiBong,
            int loaiCauThu,
            int tuoiToiThieu,
            int tuoiToiDa)
        {
            List<DTOCauThuThuocDoiBongThamGiaGiaiDau> ketQua = new List<DTOCauThuThuocDoiBongThamGiaGiaiDau>();
            if (maMuaGiai == "ALL")
            {
                //return dsCauThu;
            }
            else
            {
                if (maDoiBong == "ALL")
                {
                    dsCauThu = dsCauThu
                    .Where(x => x.MaMuaGiai == maMuaGiai).ToList();
                }
                else
                {
                    dsCauThu = dsCauThu
                        .Where(x => x.MaMuaGiai == maMuaGiai)
                        .Where(x => x.MaDoiBong == maDoiBong).ToList();
                }
            }
            if (loaiCauThu == 1)
            {
                dsCauThu = dsCauThu
                    .Where(x => x.MaLoaiCauThu == 1).ToList();
            }
            else if (loaiCauThu == 2)
            {
                dsCauThu = dsCauThu
                    .Where(x => x.MaLoaiCauThu == 2).ToList();
            }
            TransferData data = new TransferData();
            List<DTOCauThu> dsToanBoCauThu = data.DanhSachCauThu();

            //var result = from x in dsCauThu
            //             join y in dsToanBoCauThu on x.MaCauThu equals y.MaCauThu
            //             where DateTime.Now.Year - y.NgaySinh.Year >= tuoiToiThieu
            //             where DateTime.Now.Year - y.NgaySinh.Year <= tuoiToiDa
            //             select new
            //             {
            //                 MaDoiBong = x.MaDoiBong,
            //                 TenCauThu = y.HoTenCauThu,
            //                 MaCauThu = x.MaCauThu,
            //                 TenDoiBong = x.TenDoiBong,
            //                 MaMuaGiai = x.MaMuaGiai,
            //                 MaLoaiCauThu = x.MaLoaiCauThu,
            //                 TongSoBanThang = x.TongSoBanThang
            //             };


            var result = from x in dsCauThu
                         join y in dsToanBoCauThu on x.MaCauThu equals y.MaCauThu
                         where (DateTime.Now.Year - y.NgaySinh.Year) >= tuoiToiThieu
                         && (DateTime.Now.Year - y.NgaySinh.Year) <= tuoiToiDa
                         select new
                         {
                             MaDoiBong = x.MaDoiBong,
                             TenCauThu = y.HoTenCauThu,
                             MaCauThu = x.MaCauThu,
                             TenDoiBong = x.TenDoiBong,
                             MaMuaGiai = x.MaMuaGiai,
                             MaLoaiCauThu = x.MaLoaiCauThu,
                             TongSoBanThang = x.TongSoBanThang,
                             NgaySinh = y.NgaySinh
                         };

            dsCauThu = result.Select(x => new DTOCauThuThuocDoiBongThamGiaGiaiDau()
            {
                MaCauThu = x.MaCauThu,
                TenCauThu = x.TenCauThu,
                MaDoiBong = x.MaDoiBong,
                MaMuaGiai = x.MaMuaGiai,
                MaLoaiCauThu = Convert.ToInt32(x.MaLoaiCauThu),
                TongSoBanThang = Convert.ToInt32(x.TongSoBanThang),
                TenDoiBong = x.TenDoiBong
            }).ToList();

            return dsCauThu;
        }

    }

}