﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DataAcessLayer;

namespace BusinessLayer
{
    public class BUSDangKyCauThu
    {
        ICauThu _cauThu = new IpmCauThu();
        public BUSDangKyCauThu(ICauThu cauThu)
        {
            _cauThu = cauThu;
        }
        public int ThemCauThu(DTOCauThu _newCauThu)
        {
            try
            {
                List<CauThu> danhSachCauThu  = _cauThu.LoadDanhSachCauThu();

                foreach (var cauThu in danhSachCauThu)
                {
                    if (cauThu.MaCauThu == _newCauThu.MaCauThu)
                    {
                        return -1;
                        // Đã tồn tại mã cầu thủ

                    }
                    else if (BUSQuyDinh.DoTuoiToiDa - (DateTime.Now.Year - cauThu.NgaySinh.Value.Year) < 0)
                    {
                        return -2;
                    }
                    else if( (DateTime.Now.Year - cauThu.NgaySinh.Value.Year)-BUSQuyDinh.DoTuoiToiThieu < 0)
                    {
                        return -3;
                    }
                    //else if(QuyDinh.SoLuongCauThuToiDa -_cauThu.LoadDanhSachCauThu(cauThu.MaDoiBong).Count >0 )
                    //{

                    //}

                    //else if(QuyDinh.SoLuongCauThuToiThieu -)

                    //else if(QuyDinh.SoLuongCauThuNuocNgoaiToiDa)
                }
                _cauThu.InsertCauThu(_newCauThu);
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        }
    
    }
   

