﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAcessLayer;
using Microsoft.Extensions.DependencyInjection;
using DTO;

namespace BusinessLayer
{
    public class BUSKiemTraHoanTatDangKyDoiBong
    {
        ICauThuThuocDoiBongThamGiaGiaiDau cauThuThamGiaGiaiDau = new IpmCauThuThuocDoiBongThamGiaGiaiDau();
        public static int KiemTraHoanTatDangKyDoiBong(string _maMuaGiai, string _maDoiBong)
        {
            TransferData data = new TransferData();
            List<DTOCauThuThuocDoiBongThamGiaGiaiDau> dsCauThu = data.DanhSachCauThuThamGiaGiaiDau(_maMuaGiai, _maDoiBong);
            int soLuongCauThuDaDangKy = dsCauThu.Count;
            var listCauThuNuocNgoai = dsCauThu.Where(x => x.MaLoaiCauThu == 2).Select(x => x.MaCauThu);
            int soLuongCauThuNuocNgoai = listCauThuNuocNgoai.Count();
            if (soLuongCauThuDaDangKy < BUSQuyDinh.SoLuongCauThuToiThieu)
            {
                // Đội bóng chưa đăng ký đủ số lượng cẩu thủ tối thiểu
                return -1;
            }
            else if (soLuongCauThuDaDangKy > BUSQuyDinh.SoLuongCauThuToiDa)
            {
                // Đội bóng đã đăng ký số lượng cầu thủ vượt quá số lượng cầu thủ cho phép
                return -2;
            }
            else if (soLuongCauThuNuocNgoai > BUSQuyDinh.SoLuongCauThuNuocNgoaiToiDa)
            {
                // Đội bóng đã đăng ký số lượng cầu thủ nước ngoài vượt quá quy định
                return -3;
            }
            return 1;
        }
    }
}
