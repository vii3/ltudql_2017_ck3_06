﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
namespace BusinessLayer
{
    public class BUSKiemTraDangNhap
    {
        public int KiemTra(string tenDangNhap, string matKhau)
        {
            TransferData data = new TransferData();
            DTOTaiKhoan taiKhoan = data.ThongTinTaiKhoan(tenDangNhap);
            if (taiKhoan == null)
            {
                return -2;
            }
            else
            {
                if (taiKhoan.MatKhau != matKhau)
                {
                    return -1;
                }
                if (taiKhoan.MaLoaiTaiKhoan == 1)
                {
                    return 1;
                }
                else if (taiKhoan.MaLoaiTaiKhoan == 2)
                {
                    return 2;
                }
            }
            return -11;
        }
    }
}
