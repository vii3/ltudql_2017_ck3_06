﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAcessLayer;
using DTO;

namespace BusinessLayer
{
    public class BUSKiemTraHoanTatDangKyGiaiDau
    {
        IMuaGiai muaGiai = new IpmMuaGiai();
        public static int KiemTraHoanTatDangKy(string maMuaGiai)
        {
            TransferData data = new TransferData();
            List<DTOMuaGiai> dsMuaGiai = data.DanhSachMuaGiai();
            DTOMuaGiai mg = new DTOMuaGiai();
            foreach(var muagiai in dsMuaGiai)
            {
                if (muagiai.MaMuaGiai == maMuaGiai)
                {
                    mg = muagiai;
                }
            }

            List<DTODoiBongThamGiaGiaiDau> dsDoiBong = data.DanhSachDoiBongDaDangKyGiaiDau(maMuaGiai);
            foreach(var doiBong in dsDoiBong)
            {
                if(doiBong.HoanTatDangKy == false)
                {
                    return -1;
                }
            }
            if(mg.SoLuongDoiDaDangKy != mg.SoLuongDoiThamDuToiDa)
            {
                return -2;
            }
            return 1;
        }
    }
}
