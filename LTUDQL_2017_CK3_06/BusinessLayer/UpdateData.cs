﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAcessLayer;
using DTO;

namespace BusinessLayer
{
    public class UpdateData
    {
        #region Properties
        IChiTietTranDau tranDau = new IpmChiTietTranDau();
        IChiTietBanThang banThang = new IpmChiTietBanThang();
        ICauThuThuocDoiBongThamGiaGiaiDau cauThuThamGiaGiaiDau = new IpmCauThuThuocDoiBongThamGiaGiaiDau();
        IMuaGiai muaGiai = new IpmMuaGiai();
        IDoiBong doiBong = new IpmDoiBong();
        ICauThu cauThu = new IpmCauThu();
        IDoiBongThamGiaGiaiDau doiBongThamGiaGiaiDau = new IpmDoiBongThamGiaGiaiDau();
        ISoLieuThongKeCauThu soLieuCauThu = new IpmSoLieuThongKeCauThu();
        #endregion

        #region CauThu
        public int InsertCauThu(DTOCauThu _newCauThu)
        {
            try
            {
                cauThu.InsertCauThu(_newCauThu);
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region DoiBong
        public int InsertDoiBong(DTODoiBong _newDoiBong)
        {
            try
            {
                doiBong.InsertDoiBong(_newDoiBong);
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region ChiTietTranDau
        public int UpdateLichThiDau(List<DTOChiTietTranDau> _lichThiDau)
        {
            foreach (DTOChiTietTranDau _td in _lichThiDau)
            {
                tranDau = new IpmChiTietTranDau();
                tranDau.InsertChiTietTranDau(_td);
            }
            return 1;
        }
        public int UpdateDaThiDau(string _maMuaGiai, string _maVongDau, string _maTranDau, int _daThiDau)
        {
            try
            {
                tranDau.UpdateChiTietTranDau(_maMuaGiai, _maVongDau, _maTranDau, _daThiDau);
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region ChiTietBanThang
        public int UpdateChiTietBanThang(DTOChiTietBanThang _newBanThang)
        {
            try
            {
                banThang.InsertChiTietBanThang(_newBanThang);
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int UpdateChiTietBanThang(DTOChiTietBanThang _oldBangThang, DTOChiTietBanThang _newbanThang)
        {
            banThang.UpdateChiTietBanThang(_oldBangThang, _newbanThang);
            return 1;
        }
        public int DeleteChiTietBanThang(DTOChiTietBanThang _banThang)
        {
            banThang.DeleteChiTietBanThang(_banThang);
            return 1;
        }
        #endregion

        #region DoiBongThamGiaGiaiDau
        public int InsertDoiBongThamGiaGiaiDau(DTODoiBongThamGiaGiaiDau _newDoiBong)
        {
            try
            {
                doiBongThamGiaGiaiDau.InsertDoiBong(_newDoiBong);
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int UpdateDoiBongThamGiaGiaiDau(string _maMuaGiai, string _maDoiBong, bool _hoanTatDangKy)
        {
            try
            {
                doiBongThamGiaGiaiDau.UpdateDoiBong(_maMuaGiai, _maDoiBong, _hoanTatDangKy);
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int DeleteDoiBongThamGiaGiaiDau(DTODoiBongThamGiaGiaiDau _doiBong)
        {
            try
            {
                doiBongThamGiaGiaiDau.DeleteDoiBong(_doiBong.MaMuaGiai, _doiBong.MaDoiBong);
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region CauThuThamGiaiGiaiDau
        public int InsertCauThuThamGiaGiaiDau(DTOCauThuThuocDoiBongThamGiaGiaiDau _newCauThu)
        {
            try
            {
                cauThuThamGiaGiaiDau.InsertCauThuThuocDoiBongThamGiaGiaiDau(_newCauThu);
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int DeleteCauThuThamGiaGiaiDau(DTOCauThuThuocDoiBongThamGiaGiaiDau _cauThu)
        {
            try
            {
                cauThuThamGiaGiaiDau.DeleteCauThuThuocDoiBongThamGiaGiaiDau(_cauThu);
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region MuaGiai
        public int InsertMuaGiai(DTOMuaGiai _newMuaGiai)
        {
            try
            {
                muaGiai.InsertMuaGiai(_newMuaGiai);
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int UpdateMuaGiai(string _maMuaGiai, DTOMuaGiai _newThongTin)
        {
            try
            {
                muaGiai.UpdateMuaGiai(_maMuaGiai, _newThongTin);
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region SoLieuThongKeCauThu
        public int UpdateSoLieuThongKeCauThu(DTOSoLieuThongKeCauThu cauThu)
        {
            soLieuCauThu.Update(cauThu);
            return 1;
        }
        #endregion
    }
}
