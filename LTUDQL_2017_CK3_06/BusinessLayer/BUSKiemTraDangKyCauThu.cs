﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DataAcessLayer;

namespace BusinessLayer
{
    public class BUSKiemTraDangKyCauThu
    {
        //ICauThu cauThu = new IpmCauThu();
        //ICauThuThuocDoiBongThamGiaGiaiDau cauThuThamGiaGiaiDau = new IpmCauThuThuocDoiBongThamGiaGiaiDau();
        public static int KiemTraThongTinDangKy(DTOCauThuThuocDoiBongThamGiaGiaiDau _newCauThu, DateTime _ngaySinh)
        {
            if (BUSQuyDinh.DoTuoiToiDa - (DateTime.Now.Year - _ngaySinh.Year) < 0)
            {
                // Cầu thủ quá tuổi được phép đăng ký
                return -1;
            }
            else if ((DateTime.Now.Year - _ngaySinh.Year) - BUSQuyDinh.DoTuoiToiThieu < 0)
            {
                // Cầu thủ chưa đủ tuổi được phép đăng ký
                return -2;
            }
            else
            {
                return 1;
            }
        }
    }
}