﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DataAcessLayer;

namespace BusinessLayer
{
    public class TransferData
    {
        #region Properties
        IDoiBong doiBong = new IpmDoiBong();
        ICauThu cauThu = new IpmCauThu();
        IMuaGiai muaGiai = new IpmMuaGiai();
        ILoaiCauThu loaiCauThu = new IpmLoaiCauThu();
        IChiTietTranDau tranDau = new IpmChiTietTranDau();
        IChiTietBanThang banThang = new IpmChiTietBanThang();
        ISoLieuThongKeCauThu solieuCauThu = new IpmSoLieuThongKeCauThu();
        ISoLieuThongKeDoiBong soLieuDoiBong = new IpmSoLieuThongKeDoiBong();
        ICauThuThuocDoiBongThamGiaGiaiDau cauThuThamGiaGiaiDau = new IpmCauThuThuocDoiBongThamGiaGiaiDau();
        IDoiBongThamGiaGiaiDau doiBongThamGiaGiaiDau = new IpmDoiBongThamGiaGiaiDau();
        ITaiKhoan taiKhoan = new IpmTaiKhoan();
        #endregion

        #region CauThu
        public List<DTOCauThu> DanhSachCauThu()
        {
            try
            {
                List<CauThu> danhSachCauThu = cauThu.LoadDanhSachCauThu();
                var danhSachCauThuDTO = danhSachCauThu
                  .Select(x => new DTOCauThu()
                  {
                      MaCauThu = x.MaCauThu,
                      HoTenCauThu = x.HoTenCauThu,
                      MaDoiBong = x.MaDoiBong,
                      NgaySinh = Convert.ToDateTime(x.NgaySinh),
                      DaXoa = Convert.ToBoolean(x.DaXoa),
                      MaLoaiCauThu = Convert.ToInt32(x.MaLoaiCauThu),
                      TongSoBanThang = Convert.ToInt32(x.TongSoBanThang)
                  })
                  .ToList();
                return danhSachCauThuDTO;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //public List<DTOCauThu> DanhSachCauThu(string maDB)
        //{
        //    try
        //    {
        //        List<CauThu> danhSachCauThu = cauThu.LoadDanhSachCauThu(maDB);
        //        var danhSachCauThuDTO = danhSachCauThu
        //          .Select(x => new DTOCauThu()
        //          {
        //              MaCauThu = x.MaCauThu,
        //              HoTenCauThu = x.HoTenCauThu,
        //              MaDoiBong = x.MaDoiBong,
        //              NgaySinh = Convert.ToDateTime(x.NgaySinh),
        //              DaXoa = Convert.ToBoolean(x.DaXoa),
        //              MaLoaiCauThu = Convert.ToInt32(x.MaLoaiCauThu),
        //              TongSoBanThang = Convert.ToInt32(x.TongSoBanThang)
        //          })
        //          .ToList();
        //        return danhSachCauThuDTO;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        #endregion

        #region DoiBong
        public List<DTODoiBong> DanhSachDoiBong()
        {
            try
            {
                List<DoiBong> danhSachDoiBong = doiBong.LoadDanhSachDoiBong();
                var danhSachDoiBongDTO = danhSachDoiBong
                  .Select(x => new DTODoiBong() { MaDoiBong = x.MaDoiBong, TenDoiBong = x.TenDoiBong, SanNha = x.SanNha, DaXoa = Convert.ToBoolean(x.DaXoa), GhiChu = Convert.ToBoolean(x.GhiChu) })
                  .ToList();
                return danhSachDoiBongDTO;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region MuaGiai
        public List<DTOMuaGiai> DanhSachMuaGiai()
        {
            try
            {
                List<MuaGiai> danhSachMuaGiai = muaGiai.LoaiDanhSachMuaGiai();
                var danhSachMuaGiaiDTO = danhSachMuaGiai
                  .Select(x => new DTOMuaGiai()
                  {
                      MaMuaGiai = x.MaMuaGiai,
                      TenMuaGiai = x.TenMuaGiai,
                      SoLuongDoiThamDuToiDa = Convert.ToInt32(x.SoLuongDoiThamDuToiDa),
                      SoLuongDoiDaDangKy = Convert.ToInt32(x.SoLuongDoiDaDangKy),
                      HoanTatDangKy = Convert.ToBoolean(x.HoanTatDangKy)
                  })
                  .ToList();
                return danhSachMuaGiaiDTO;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region DanhSachLoaiCauThu
        public List<DTOLoaiCauThu> DanhSachLoaiCauThu()
        {
            try
            {
                List<LoaiCauThu> danhSachLoaiCauThu = loaiCauThu.LoadDanhSachLoaiCauThu();
                var danhSachLoaiCauThuDTO = danhSachLoaiCauThu
                  .Select(x => new DTOLoaiCauThu() { MaLoaiCauThu = x.MaLoaiCauThu, TenLoaiCauThu = x.TenLoaiCauThu })
                  .ToList();
                return danhSachLoaiCauThuDTO;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region ChiTietTranDau
        public List<DTOChiTietTranDau> LichThiDau(string _maMuaGiai)
        {
            try
            {
                List<ChiTietTranDau> lichThiDau = tranDau.LoadChiTietTranDau(_maMuaGiai);
                var lichThiDauDTO = lichThiDau
                  .Select(x => new DTOChiTietTranDau()
                  {
                      MaMuaGiai = x.MaMuaGiai,
                      MaVongDau = x.MaVongDau,
                      MaTranDau = x.MaTranDau,
                      DaThiDau = Convert.ToInt32(x.DaThiDau),
                      MaChuNha = x.ChuNha,
                      MaKhach = x.Khach,
                      NgayThiDau = Convert.ToDateTime(x.NgayThiDau),
                      GioThiDau = TimeSpan.Parse(x.GioThiDau.ToString()),
                      SoBanThangChuNha = Convert.ToInt32(x.SoBanThang_ChuNha),
                      SoBanThangKhach = Convert.ToInt32(x.SoBanThang_Khach)
                  })
                  .ToList();
                return lichThiDauDTO;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region ChiTietBanThang
        public List<DTOChiTietBanThang> DuLieuTranDau(string _maMuaGiai)
        {
            try
            {
                List<ChiTietBanThang> duLieuTranDau = banThang.LoadChiTietBanThang(_maMuaGiai);
                var duLieuTranDauDTO = duLieuTranDau
                  .Select(x => new DTOChiTietBanThang()
                  {
                      MaMuaGiai = x.MaMuaGiai,
                      MaVongDau = x.MaVongDau,
                      MaTranDau = x.MaTranDau,
                      MaDoiBong = x.MaDoiBong,
                      MaCauThu = x.MaCauThu,
                      LoaiBanThang = x.LoaiBanThang,
                      ThoiDiem = x.ThoiDiem
                  })
                  .ToList();
                return duLieuTranDauDTO;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DTOChiTietBanThang> DuLieuTranDau(string _maMuaGiai, string _maVongDau)
        {
            try
            {
                List<ChiTietBanThang> duLieuTranDau = banThang.LoadChiTietBanThang(_maMuaGiai, _maVongDau);
                var duLieuTranDauDTO = duLieuTranDau
                  .Select(x => new DTOChiTietBanThang()
                  {
                      MaMuaGiai = x.MaMuaGiai,
                      MaVongDau = x.MaVongDau,
                      MaTranDau = x.MaTranDau,
                      MaDoiBong = x.MaDoiBong,
                      MaCauThu = x.MaCauThu,
                      LoaiBanThang = x.LoaiBanThang,
                      ThoiDiem = x.ThoiDiem
                  })
                  .ToList();
                return duLieuTranDauDTO;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DTOChiTietBanThang> DuLieuTranDau(string _maMuaGiai, string _maVongDau, string _maTranDau)
        {
            try
            {
                List<ChiTietBanThang> duLieuTranDau = banThang.LoadChiTietBanThang(_maMuaGiai, _maVongDau, _maTranDau);
                var duLieuTranDauDTO = duLieuTranDau
                  .Select(x => new DTOChiTietBanThang()
                  {
                      MaMuaGiai = x.MaMuaGiai,
                      MaVongDau = x.MaVongDau,
                      MaTranDau = x.MaTranDau,
                      MaDoiBong = x.MaDoiBong,
                      MaCauThu = x.MaCauThu,
                      LoaiBanThang = x.LoaiBanThang,
                      ThoiDiem = x.ThoiDiem
                  })
                  .ToList();
                return duLieuTranDauDTO;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region SoLieuThongKeCauThu
        public List<DTOSoLieuThongKeCauThu> SoLieuCauThu(string _maMuaGiai)
        {
            try
            {
                List<SoLieuThongKeCauThu> thongKeCauThu = solieuCauThu.LoadSoLieuThongKe(_maMuaGiai);
                var thongKeCauThuDTO = thongKeCauThu
                  .Select(x => new DTOSoLieuThongKeCauThu()
                  {
                      MaMuaGiai = x.MaMuaGiai,
                      MaVongDau = x.MaVongDau,
                      MaCauThu = x.MaCauThu,
                      MaDoiBong = x.MaDoiBong,
                      SoBanThang = Convert.ToInt32(x.SoBanThang),
                      TongSoBanThang = Convert.ToInt32(x.TongSoBanThang),
                      Hang = Convert.ToInt32(x.Hang)
                  })
                  .ToList();
                return thongKeCauThuDTO;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<DTOSoLieuThongKeCauThu> SoLieuCauThu(string _maMuaGiai, string _maVongDau)
        {
            try
            {
                List<SoLieuThongKeCauThu> thongKeCauThu = solieuCauThu.LoadSoLieuThongKe(_maMuaGiai, _maVongDau);
                var thongKeCauThuDTO = thongKeCauThu
                  .Select(x => new DTOSoLieuThongKeCauThu()
                  {
                      MaMuaGiai = x.MaMuaGiai,
                      MaVongDau = x.MaVongDau,
                      MaCauThu = x.MaCauThu,
                      MaDoiBong = x.MaDoiBong,
                      SoBanThang = Convert.ToInt32(x.SoBanThang),
                      TongSoBanThang = Convert.ToInt32(x.TongSoBanThang),
                      Hang = Convert.ToInt32(x.Hang)
                  })
                  .ToList();
                return thongKeCauThuDTO;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region SoLieuThongKeDoiBong
        public List<DTOSoLieuThongKeDoiBong> BangXepHang(string _maMuaGiai)
        {
            try
            {
                List<SoLieuThongKeDoiBong> thongKeDoiBong = soLieuDoiBong.LoadSoLieuThongKe(_maMuaGiai);
                var thongKeDoiBongDTO = thongKeDoiBong
                  .Select(x => new DTOSoLieuThongKeDoiBong()
                  {
                      MaMuaGiai = x.MaMuaGiai,
                      MaVongDau = x.MaVongDau,
                      MaDoiBong = x.MaDoiBong,
                      Diem = Convert.ToInt32(x.Diem),
                      TongDiem = Convert.ToInt32(x.TongDiem),
                      HieuSo = Convert.ToInt32(x.HieuSo),
                      TongHieuSo = Convert.ToInt32(x.TongHieuSo),
                      SoTranDaThiDau = Convert.ToInt32(x.SoTranDaThiDau),
                      TongSoBanThang = Convert.ToInt32(x.TongSoBanThang),
                      TongSoBanThua = Convert.ToInt32(x.TongSoBanThua),
                      TongSoTranThang = Convert.ToInt32(x.TongSoTranThang),
                      TongSoTranHoa = Convert.ToInt32(x.TongSoTranHoa),
                      TongSoTranThua = Convert.ToInt32(x.TongSoTranThua),
                      SoBanThang = Convert.ToInt32(x.SoBanThang),
                      SoBanThua = Convert.ToInt32(x.SoBanThua),
                      Hang = Convert.ToInt32(x.Hang),
                  })
                  .ToList();
                return thongKeDoiBongDTO;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<DTOSoLieuThongKeDoiBong> BangXepHang(string _maMuaGiai, string _maVongDau)
        {
            try
            {
                List<SoLieuThongKeDoiBong> thongKeDoiBong = soLieuDoiBong.LoadSoLieuThongKe(_maMuaGiai, _maVongDau);
                var thongKeDoiBongDTO = thongKeDoiBong
                  .Select(x => new DTOSoLieuThongKeDoiBong()
                  {
                      MaMuaGiai = x.MaMuaGiai,
                      MaVongDau = x.MaVongDau,
                      MaDoiBong = x.MaDoiBong,
                      Diem = Convert.ToInt32(x.Diem),
                      TongDiem = Convert.ToInt32(x.TongDiem),
                      HieuSo = Convert.ToInt32(x.HieuSo),
                      TongHieuSo = Convert.ToInt32(x.TongHieuSo),
                      SoTranDaThiDau = Convert.ToInt32(x.SoTranDaThiDau),
                      TongSoBanThang = Convert.ToInt32(x.TongSoBanThang),
                      TongSoBanThua = Convert.ToInt32(x.TongSoBanThua),
                      TongSoTranThang = Convert.ToInt32(x.TongSoTranThang),
                      TongSoTranHoa = Convert.ToInt32(x.TongSoTranHoa),
                      TongSoTranThua = Convert.ToInt32(x.TongSoTranThua),
                      SoBanThang = Convert.ToInt32(x.SoBanThang),
                      SoBanThua = Convert.ToInt32(x.SoBanThua),
                      Hang = Convert.ToInt32(x.Hang),
                  })
                  .ToList();
                return thongKeDoiBongDTO;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region CauThuThuocDoiBongThamGiaGiaiDau
        public List<DTOCauThuThuocDoiBongThamGiaGiaiDau> DanhSachCauThuThamGiaGiaiDau()
        {
            List<MuaGiai> dsMuaGiai = muaGiai.LoaiDanhSachMuaGiai();
            List<string> dsMaMuaGiai = dsMuaGiai.Select(x => x.MaMuaGiai).ToList();
            List<CauThuThuocDoiBongThamGiaGiaiDau> dsCauThu = new List<CauThuThuocDoiBongThamGiaGiaiDau>();
            foreach(var mg in dsMaMuaGiai)
            {
                dsCauThu.AddRange(cauThuThamGiaGiaiDau.LoadCauThuThuocDoiBongThamGiaGiaiDau(mg));
            }

            var dsCauThuDTO = dsCauThu
                .Select(x => new DTOCauThuThuocDoiBongThamGiaGiaiDau()
                {
                    MaCauThu = x.MaCauThu,
                    MaDoiBong = x.MaDoiBong,
                    MaLoaiCauThu = Convert.ToInt32(x.MaLoaiCauThu),
                    MaMuaGiai = x.MaGiaiDau,
                    TenCauThu = x.TenCauThu,
                    TenDoiBong = x.TenDoiBong,
                    TongSoBanThang = Convert.ToInt32(x.TongSoBanThang)
                }).ToList();
            return dsCauThuDTO;
        }
        public List<DTOCauThuThuocDoiBongThamGiaGiaiDau> DanhSachCauThuThamGiaGiaiDau(string _maMuaGiai)
        {
            List<CauThuThuocDoiBongThamGiaGiaiDau> dsCauThu = cauThuThamGiaGiaiDau.LoadCauThuThuocDoiBongThamGiaGiaiDau(_maMuaGiai);
            var dsCauThuDTO = dsCauThu
                  .Select(x => new DTOCauThuThuocDoiBongThamGiaGiaiDau()
                  {
                      MaMuaGiai = x.MaGiaiDau,
                      MaCauThu = x.MaCauThu,
                      MaDoiBong = x.MaDoiBong,
                      TenCauThu = x.TenCauThu,
                      TenDoiBong = x.TenDoiBong,
                      MaLoaiCauThu = Convert.ToInt32(x.MaLoaiCauThu),
                      TongSoBanThang = Convert.ToInt32(x.TongSoBanThang)
                  })
                  .ToList();
            return dsCauThuDTO;
        }
        public List<DTOCauThuThuocDoiBongThamGiaGiaiDau> DanhSachCauThuThamGiaGiaiDau(string _maMuaGiai, string _maDoiBong)
        {
            List<CauThuThuocDoiBongThamGiaGiaiDau> dsCauThu = cauThuThamGiaGiaiDau.LoadCauThuThuocDoiBongThamGiaGiaiDau(_maMuaGiai, _maDoiBong);
            var dsCauThuDTO = dsCauThu
                  .Select(x => new DTOCauThuThuocDoiBongThamGiaGiaiDau()
                  {
                      MaMuaGiai = x.MaGiaiDau,
                      MaCauThu = x.MaCauThu,
                      MaDoiBong = x.MaDoiBong,
                      TenCauThu = x.TenCauThu,
                      TenDoiBong = x.TenDoiBong,
                      MaLoaiCauThu = Convert.ToInt32(x.MaLoaiCauThu),
                      TongSoBanThang = Convert.ToInt32(x.TongSoBanThang)
                  })
                  .ToList();
            return dsCauThuDTO;
        }
        #endregion

        #region DoiBongThamGiaGiaiDau
        public List<DTODoiBongThamGiaGiaiDau> DanhSachDoiBongDaDangKyGiaiDau()
        {
            List<DoiBongThamGiaGiaiDau> dsDoiBong = doiBongThamGiaGiaiDau.LoadDanhSachDoiBong();
            var dsDoiBongDTO = dsDoiBong
                .Select(x => new DTODoiBongThamGiaGiaiDau()
                {
                    MaMuaGiai = x.MaGiaiDau,
                    MaDoiBong = x.MaDoiBong,
                    TenDoiBong = x.TenDoiBong,
                    SoLuongCauThuDangKy = Convert.ToInt32(x.SoLuongCauThuDangKy),
                    SoLuongCauThuNuocNgoai = Convert.ToInt32(x.SoLuongCauThuNuocNgoaiDangKy),
                    HoanTatDangKy = Convert.ToBoolean(x.HoanTatDangKy)
                }).ToList();
            return dsDoiBongDTO;
        }
        public List<DTODoiBongThamGiaGiaiDau> DanhSachDoiBongDaDangKyGiaiDau(string _maMuaGiai)
        {
            List<DoiBongThamGiaGiaiDau> dsDoiBong = doiBongThamGiaGiaiDau.LoadDanhSachDoiBong(_maMuaGiai);
            var dsDoiBongDTO = dsDoiBong
                .Select(x => new DTODoiBongThamGiaGiaiDau()
                {
                    MaMuaGiai = x.MaGiaiDau,
                    MaDoiBong = x.MaDoiBong,
                    TenDoiBong = x.TenDoiBong,
                    SoLuongCauThuDangKy = Convert.ToInt32(x.SoLuongCauThuDangKy),
                    SoLuongCauThuNuocNgoai = Convert.ToInt32(x.SoLuongCauThuNuocNgoaiDangKy),
                    HoanTatDangKy = Convert.ToBoolean(x.HoanTatDangKy)
                }).ToList();
            return dsDoiBongDTO;
        }
        public List<DTODoiBongThamGiaGiaiDau> DanhSachDoiBongDaDangKyGiaiDau(string _maMuaGiai, string _maDoiBong)
        {
            List<DoiBongThamGiaGiaiDau> dsDoiBong = doiBongThamGiaGiaiDau.LoadDanhSachDoiBong(_maMuaGiai, _maDoiBong);
            var dsDoiBongDTO = dsDoiBong
                .Select(x => new DTODoiBongThamGiaGiaiDau()
                {
                    MaMuaGiai = x.MaGiaiDau,
                    MaDoiBong = x.MaDoiBong,
                    TenDoiBong = x.TenDoiBong,
                    SoLuongCauThuDangKy = Convert.ToInt32(x.SoLuongCauThuDangKy),
                    SoLuongCauThuNuocNgoai = Convert.ToInt32(x.SoLuongCauThuNuocNgoaiDangKy),
                    HoanTatDangKy = Convert.ToBoolean(x.HoanTatDangKy)
                }).ToList();
            return dsDoiBongDTO;
        }
        #endregion

        #region TaiKhoan
        public List<DTOTaiKhoan> DanhSachTaiKhoan()
        {
            List<TaiKhoan> dsTaiKhoan = taiKhoan.Load();
            var dsTaiKhoanDTO = dsTaiKhoan
                .Select(x => new DTOTaiKhoan()
                {
                    TenDangNhap = x.TenDangNhap,
                    MatKhau = x.MatKhau,
                    MaLoaiTaiKhoan = Convert.ToInt32(x.MaLoaiTaiKhoan)
                }).ToList();
            return dsTaiKhoanDTO;
        }
        public DTOTaiKhoan ThongTinTaiKhoan(string tenDangNhap)
        {
            List<TaiKhoan> dsTaiKhoan = taiKhoan.Load();
            var TaiKhoanDTO = dsTaiKhoan
                .Select(x => new DTOTaiKhoan()
                {
                    TenDangNhap = x.TenDangNhap,
                    MatKhau = x.MatKhau,
                    MaLoaiTaiKhoan = Convert.ToInt32(x.MaLoaiTaiKhoan)
                })
                .Where(x => x.TenDangNhap == tenDangNhap).ToList();
            if (TaiKhoanDTO.Count == 0)
            {
                return null;
            }
            return TaiKhoanDTO[0];
        }
        #endregion
    }
}
