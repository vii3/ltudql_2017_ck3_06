﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DataAcessLayer
{
    public class IpmChiTietBanThang : IChiTietBanThang
    {
        QLGVDBDQGDataContext _context;
        public IpmChiTietBanThang()
        {
            _context = new QLGVDBDQGDataContext();
        }
        public List<ChiTietBanThang> LoadChiTietBanThang(string _maMuaGiai)
        {
            try
            {             
                var _selectBanThang =
                    from _tranDau in _context.GetTable<ChiTietBanThang>()
                    where _tranDau.MaMuaGiai == _maMuaGiai
                    select _tranDau;
                List<ChiTietBanThang> _danhSachBanThang = new List<ChiTietBanThang>();

                foreach (var _banThang in _selectBanThang)
                {
                    _danhSachBanThang.Add(_banThang);
                }

                return _danhSachBanThang;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<ChiTietBanThang> LoadChiTietBanThang(string _maMuaGiai, string _maVongDau)
        {
            try
            {
                var _selectBanThang =
                    from _tranDau in _context.GetTable<ChiTietBanThang>()
                    where _tranDau.MaMuaGiai == _maMuaGiai &&
                    _tranDau.MaVongDau == _maVongDau
                    select _tranDau;
                List<ChiTietBanThang> _danhSachBanThang = new List<ChiTietBanThang>();

                foreach (var _banThang in _selectBanThang)
                {
                    _danhSachBanThang.Add(_banThang);
                }

                return _danhSachBanThang;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<ChiTietBanThang> LoadChiTietBanThang(string _maMuaGiai, string _maVongDau, string _maTranDau)
        {
            try
            {
                QLGVDBDQGDataContext _context = new QLGVDBDQGDataContext();


                var _selectBanThang =
                    from _tranDau in _context.GetTable<ChiTietBanThang>()
                    where _tranDau.MaMuaGiai == _maMuaGiai &&
                    _tranDau.MaVongDau == _maVongDau &&
                    _tranDau.MaTranDau == _maTranDau
                    select _tranDau;
                List<ChiTietBanThang> _danhSachBanThang = new List<ChiTietBanThang>();

                foreach (var _banThang in _selectBanThang)
                {
                    _danhSachBanThang.Add(_banThang);
                }

                return _danhSachBanThang;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int InsertChiTietBanThang(DTOChiTietBanThang _newBanThang)
        {
            try
            {
                ChiTietBanThang _banThang = new ChiTietBanThang();

                // Kiểm tra tồn tại CẦU THỦ trong TRẬN ĐẤU đang xét hay không
                // Nếu tồn tại thì tiến hành Thêm thông tin BÀN THẮNG
                // Nếu không tồn tại -> return -1
                var _kiemTra =
                    from _ktBanThang in _context.ChiTietBanThangs
                    where _ktBanThang.MaMuaGiai == _newBanThang.MaMuaGiai &&
                    _ktBanThang.MaVongDau == _newBanThang.MaVongDau &&
                    _ktBanThang.MaTranDau == _newBanThang.MaTranDau &&
                    _ktBanThang.MaCauThu == _newBanThang.MaCauThu &&
                    _ktBanThang.LoaiBanThang == -1
                    select _ktBanThang;
                if (_kiemTra.Count() > 0)
                {
                    ChiTietBanThang _bt = new ChiTietBanThang();
                    _bt.MaMuaGiai = _newBanThang.MaMuaGiai;
                    _bt.MaVongDau = _newBanThang.MaVongDau;
                    _bt.MaTranDau = _newBanThang.MaTranDau;
                    _bt.MaCauThu = _newBanThang.MaCauThu;
                    _bt.MaDoiBong = _newBanThang.MaDoiBong;
                    _bt.LoaiBanThang = _newBanThang.LoaiBanThang;
                    _bt.ThoiDiem = _newBanThang.ThoiDiem;
                    try
                    {
                        _context.ChiTietBanThangs.InsertOnSubmit(_bt);
                        _context.SubmitChanges();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    return 1;
                }
                return -1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int DeleteChiTietBanThang(DTOChiTietBanThang _banThang)
        {
            try
            {
                var _deleteBanThang =
                    from _bt in _context.ChiTietBanThangs
                    where _bt.MaMuaGiai == _banThang.MaMuaGiai &&
                    _bt.MaVongDau == _banThang.MaVongDau &&
                    _bt.MaTranDau == _banThang.MaTranDau &&
                    _bt.MaTranDau == _banThang.MaTranDau &&
                    _bt.MaCauThu == _banThang.MaCauThu &&
                    _bt.LoaiBanThang == _banThang.LoaiBanThang &&
                    _bt.LoaiBanThang != -1 &&
                    _bt.ThoiDiem == _banThang.ThoiDiem
                    select _bt;


                if (_deleteBanThang.Count() == 0)
                {
                    // Không tìm thấy BÀN THẮNG cần xóa
                    return -1;
                }
                //var _danhSachBanThang = _deleteBanThang.ToList();
                foreach (var _kq in _deleteBanThang)
                {
                    // Thực hiện xóa nếu tìm thấy
                    _context.ChiTietBanThangs.DeleteOnSubmit(_kq);
                }
                try
                {
                    _context.SubmitChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int UpdateChiTietBanThang(DTOChiTietBanThang _banThang, DTOChiTietBanThang _newBanThang)
        {
            try
            {
                var _updateBanThang =
                    from _bt in _context.ChiTietBanThangs
                    where _bt.MaMuaGiai == _banThang.MaMuaGiai &&
                    _bt.MaVongDau == _banThang.MaVongDau &&
                    _bt.MaTranDau == _banThang.MaTranDau &&
                    _bt.MaCauThu == _banThang.MaCauThu &&
                    _bt.LoaiBanThang == _banThang.LoaiBanThang &&
                    _bt.ThoiDiem == _banThang.ThoiDiem
                    select _bt;
                //var _danhSachBanThang = _updateBanThang.ToList();
                foreach(var _kq in _updateBanThang)
                {
                    _kq.LoaiBanThang = _newBanThang.LoaiBanThang;
                    _kq.ThoiDiem = _newBanThang.ThoiDiem;
                }
                try
                {
                    _context.SubmitChanges();
                }
                catch(Exception ex)
                {
                    throw ex;
                }
                return 1;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
