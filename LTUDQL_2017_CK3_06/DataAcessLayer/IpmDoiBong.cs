﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcessLayer
{
    public class IpmDoiBong:IDoiBong
    {
        QLGVDBDQGDataContext _context;

        public IpmDoiBong()
        {
            _context = new QLGVDBDQGDataContext();
        }
        public List<DoiBong> LoadDanhSachDoiBong()
        {
            try
            {
                // Lấy danh sách ĐỘI BÓNG từ DataBase
                var _selectQuery =
                    from _db in _context.GetTable<DoiBong>()
                    where _db.DaXoa == false
                    select _db;

                // Đưa danh sách ĐỘI BÓNG vào List<DoiBong> để thự hiện return List<DoiBong>
                List<DoiBong> _danhSachDoiBong = new System.Collections.Generic.List<DoiBong>();
                foreach (var _db in _selectQuery)
                {
                    _danhSachDoiBong.Add(_db);
                }
                return _danhSachDoiBong;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void InsertDoiBong(DTODoiBong _doiBong)
        {
            try
            {
                // Thêm ĐỘI BÓNG mới vào DataBase
                DoiBong _db = new DoiBong();
                _db.MaDoiBong = _doiBong.MaDoiBong;
                _db.TenDoiBong = _doiBong.TenDoiBong;
                _db.SanNha = _doiBong.SanNha;
                _db.GhiChu = false;
                _db.DaXoa = false;

                // Thực hiện thêm ĐỘI BÓNG
                _context.DoiBongs.InsertOnSubmit(_db);
                _context.SubmitChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void InsertDoiBong(string _maDoiBong, string _tenDoiBong, string _sanNha)
        {
            try
            {
                // Thêm ĐỘI BÓNG mới vào DataBase
                // Khởi tạo ĐỘI BÓNG với các thuộc tính: Mã ĐỘI BÓNG, Tên ĐỘI BÓNG, Tên sân nhà, Ghi chú nếu có
                DoiBong _db = new DoiBong();
                _db.MaDoiBong = _maDoiBong;
                _db.TenDoiBong = _tenDoiBong;
                _db.SanNha = _sanNha;
                _db.GhiChu = false;
                _db.DaXoa = false;

                // Thực hiện thêm ĐỘI BÓNG
                _context.DoiBongs.InsertOnSubmit(_db);
                _context.SubmitChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int DeleteDoiBong(string _maDoiBong, int _option)
        {
            // option = 11: XÓA VĨNH VIỄN ĐỘI BÓNG
            // option != 11: TẠM XÓA ĐỘI BÓNG - UPDATE ĐÃ XÓA = TRUE
            try
            {
                // Xóa ĐỘI BÓNG khỏi DataBase dựa trên tham số Mã ĐỘI BÓNG
                QLGVDBDQGDataContext _context = new QLGVDBDQGDataContext();

                // Tìm kiếm
                var _deleteDoiBong =
                    from _db in _context.DoiBongs
                    where _db.MaDoiBong == _maDoiBong &&
                    _db.DaXoa == false
                    select _db;


                if (_deleteDoiBong.Count() == 0)
                {
                    // Không tìm thấy ĐỘI BÓNG với mã số đã cho
                    return -1;
                }
                //var _doiBongs = _deleteQuery.ToList();
                foreach (var _db in _deleteDoiBong)
                {
                    if(_option == 11)
                    {
                        _context.DoiBongs.DeleteOnSubmit(_db);
                    }
                    else
                    {
                        _db.DaXoa = true;
                    }
                }
                try
                {
                    _context.SubmitChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int UpdateDoiBong(string _maDoiBong, DTODoiBong _newDoiBong)
        {
            try
            {
                var _updateDoiBong =
                    from _db in _context.DoiBongs
                    where _db.MaDoiBong == _maDoiBong &&
                    _db.DaXoa == false
                    select _db;
                //List<DoiBong> _danhSachUpdate = _updateDoiBong.ToList();
                foreach (var _db in _updateDoiBong)
                {
                    _db.TenDoiBong = _newDoiBong.TenDoiBong;
                    _db.SanNha = _newDoiBong.SanNha;
                }
                try
                {
                    _context.SubmitChanges();
                }
                catch(Exception ex)
                {
                    throw ex;
                }
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int UpdateDoiBong(string _maDoiBong, int _option, string _newThuocTinh)
        {
            // Option = 1: Thay đổi Tên ĐỘI BÓNG
            // Option = 2: Thay đổi Tên SÂN NHÀ
            // _newThuocTinh: Thông tin cần thay đổi
            try
            {
                QLGVDBDQGDataContext _context = new QLGVDBDQGDataContext();
                var _updateDoiBong =
                    from _db in _context.DoiBongs
                    where _db.MaDoiBong == _maDoiBong &&
                    _db.DaXoa == false
                    select _db;
                //List<DoiBong> _danhSachUpdate = _updateDoiBong.ToList();
                foreach(var _db in _updateDoiBong)
                {
                    if(_option == 1)
                    {
                        _db.TenDoiBong = _newThuocTinh;
                    }
                    else if(_option == 2)
                    {
                        _db.SanNha = _newThuocTinh;
                    }
                    else
                    {
                        return -1;
                    }
                }
                try
                {
                    _context.SubmitChanges();
                }
                catch(Exception ex)
                {
                    throw ex;
                }
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int UpdateDoiBong(string _maDoiBong, bool _ghiChu)
        {
            try
            {
                var _updateDoiBong =
                    from _db in _context.DoiBongs
                    where _db.MaDoiBong == _maDoiBong &&
                    _db.DaXoa == false
                    select _db;
                //List<DoiBong> _danhSachUpdate = _updateDoiBong.ToList();
                foreach (var _db in _updateDoiBong)
                {
                    _db.GhiChu = _ghiChu;
                }
                try
                {
                    _context.SubmitChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
