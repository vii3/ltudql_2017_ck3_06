﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcessLayer
{
    public class IpmCauThu:ICauThu
    {
        QLGVDBDQGDataContext _context;
        public IpmCauThu()
        {
            _context = new QLGVDBDQGDataContext();
        }
        public List<CauThu> LoadDanhSachCauThu()
        {
            try
            {

                // Lấy danh sách cầu thủ từ DataBase
                // Đưa danh sách vào List<CauThu>
                var _selectDanhSachCauThu =
                    from _ct in _context.GetTable<CauThu>()
                    where _ct.DaXoa == false
                    select _ct;
                List<CauThu> _danhSachCauThu = new List<CauThu>();

                foreach (var ct in _selectDanhSachCauThu)
                {
                    _danhSachCauThu.Add(ct);
                }

                return _danhSachCauThu;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<CauThu> LoadDanhSachCauThu(string _maDoiBong)
        {
            try
            {
                var _selectDanhSachCauThu =
                    from _ct in _context.GetTable<CauThu>()
                    where _ct.MaDoiBong == _maDoiBong &&
                    _ct.DaXoa == false
                    select _ct;
                List<CauThu> _danhSachCauThu = new List<CauThu>();

                foreach (var ct in _selectDanhSachCauThu)
                {
                    _danhSachCauThu.Add(ct);
                }

                return _danhSachCauThu;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertCauThu(DTOCauThu _newCauThu)
        {
            try
            {
                CauThu _ct = new CauThu();
                // Khởi tạo thông tin cầu thủ
                // Thông tin được từ BusinessLayer
                _ct.MaCauThu = _newCauThu.MaCauThu;
                _ct.HoTenCauThu = _newCauThu.HoTenCauThu;
                _ct.NgaySinh = _newCauThu.NgaySinh;
                _ct.MaLoaiCauThu = _newCauThu.MaLoaiCauThu;
                _ct.MaDoiBong = _newCauThu.MaDoiBong;
                _ct.TongSoBanThang = 0;
                _ct.DaXoa = false;
                try
                {
                    _context.CauThus.InsertOnSubmit(_ct);
                    _context.SubmitChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertCauThu(string _maCauThu, string _tenCauThu, DateTime _ngaySinh, int _maLoaiCauThu, string _maDoiBong)
        {
            try
            {
                CauThu _ct = new CauThu();
                _ct.MaCauThu = _maCauThu;
                _ct.HoTenCauThu = _tenCauThu;
                _ct.NgaySinh = _ngaySinh;
                _ct.MaLoaiCauThu = _maLoaiCauThu;
                _ct.MaDoiBong = _maDoiBong;
                _ct.TongSoBanThang = 0;
                _ct.DaXoa = false;
                try
                {
                    _context.CauThus.InsertOnSubmit(_ct);
                    _context.SubmitChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int DeleteCauThu(string maCT, int option)
        {
            // Xóa CẦU THỦ dựa vào MÃ CẦU THỦ
            // option 1: XÓA VĨNH VIỄN
            // option != 1: Tạm xóa - UPDATE thuộc tính ĐÃ XÓA = TRUE
            try
            {
                // Tìm kiếm cầu thủ với Mã cầu thủ được cung cấp từ BusinessLayer
                var _deleteCauThu =
                    from _ct in _context.CauThus
                    where _ct.MaCauThu == maCT &&
                    _ct.DaXoa == false
                    select _ct;

                if (_deleteCauThu.Count() == 0)
                {
                    //Không tìm ra cầu thủ
                    return -1;
                }
                var _cauThus = _deleteCauThu.ToList();

                if (option == 11)
                {
                    // Option = 1. Xóa vĩnh viễn
                    foreach (var _cauThu in _cauThus)
                    {
                        _context.CauThus.DeleteOnSubmit(_cauThu);
                        _context.SubmitChanges();
                    }
                    return 0;
                }
                else
                {
                    foreach (var _cauThu in _cauThus)
                    {
                        // Option != 1
                        // UPDATE thuộc tính ĐÃ XÓA = TRUE

                        _cauThu.DaXoa = true;
                        try
                        {
                            _context.SubmitChanges();
                        }
                        catch(Exception ex)
                        {
                            throw ex;
                        }
                    }
                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int UpdateCauThu(string maCT, DTOCauThu _newCauThu)
        {
            // Update cầu thủ với toàn bộ thuộc tính mới
            try
            {
                var _selectCauThu =
                    from _ct in _context.CauThus
                    where _ct.MaCauThu == maCT &&
                    _ct.DaXoa == false
                    select _ct;

                if (_selectCauThu.Count() == 0)
                {
                    return -1;
                }
                var _danhSachCauThu = _selectCauThu.ToList();
                foreach (var _ct in _danhSachCauThu)
                {
                    _ct.HoTenCauThu = _newCauThu.HoTenCauThu;
                    _ct.NgaySinh = _newCauThu.NgaySinh;
                    _ct.MaLoaiCauThu = _newCauThu.MaLoaiCauThu;
                    _ct.MaDoiBong = _newCauThu.MaDoiBong;
                }

                try
                {
                    _context.SubmitChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int UpdateCauThu(string _maCauThu, int _option, string _newThuocTinh)
        {
            // Update các thuộc tính kiểu string C# (char, varchar, nvarchar Sql server)
            // Option 1: Update Họ tên CẦU THỦ
            // Option 2: Update Mã ĐỘI BÓNG

            try
            {
                var _selectCauThu =
                    from _ct in _context.CauThus
                    where _ct.MaCauThu == _maCauThu &&
                    _ct.DaXoa == false
                    select _ct;

                if (_selectCauThu.Count() == 0)
                {
                    return -1;
                }

                foreach (var _ct in _selectCauThu)
                {
                    if (_option == 1)
                    {
                        _ct.HoTenCauThu = _newThuocTinh;
                    }
                    else if (_option == 2)
                    {
                        _ct.MaDoiBong = _newThuocTinh;
                    }
                    else
                    {
                        return -1;
                    }
                }

                try
                {
                    _context.SubmitChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int UpdateCauThu(string _maCauThu, int _newMaLoaiCauThu)
        {
            try
            {
                var _findCT =
                    from _ct in _context.CauThus
                    where _ct.MaCauThu == _maCauThu &&
                    _ct.DaXoa == false
                    select _ct;

                if (_findCT.Count() == 0)
                {
                    return -1;
                }
                foreach (var x in _findCT)
                {
                    x.MaLoaiCauThu = _newMaLoaiCauThu;
                }

                try
                {
                    _context.SubmitChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int UpdateCauThu(string _maCauThu, DateTime _newNgaySinh)
        {
            // Update Cầu thủ với Ngày sinh mới
            try
            {
                var _findCT =
                    from _ct in _context.CauThus
                    where _ct.MaCauThu == _maCauThu &&
                    _ct.DaXoa == false
                    select _ct;

                if (_findCT.Count() == 0)
                {
                    return -1;
                }
                foreach (var x in _findCT)
                {
                    x.NgaySinh = _newNgaySinh;
                }

                try
                {
                    _context.SubmitChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
