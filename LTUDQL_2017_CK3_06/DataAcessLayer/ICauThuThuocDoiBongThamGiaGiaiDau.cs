﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DataAcessLayer
{
    public interface ICauThuThuocDoiBongThamGiaGiaiDau
    {
        List<CauThuThuocDoiBongThamGiaGiaiDau> LoadCauThuThuocDoiBongThamGiaGiaiDau(string _maMuaGiai);
        List<CauThuThuocDoiBongThamGiaGiaiDau> LoadCauThuThuocDoiBongThamGiaGiaiDau(string _maMuaGiai, string _maDoiBong);
        int InsertCauThuThuocDoiBongThamGiaGiaiDau(DTOCauThuThuocDoiBongThamGiaGiaiDau _newCauThu);
        int DeleteCauThuThuocDoiBongThamGiaGiaiDau(DTOCauThuThuocDoiBongThamGiaGiaiDau _cauThu);
        // Truyền mã cầu thủ = "" hoặc null và truyền Mã đội bóng để xóa toàn bộ danh sách cầu thủ thuộc đội bóng
        // Truyền mã cầu thủ và truyền mã đội bóng = "" hoặc null để xóa một cầu thủ
    }
}
