﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DataAcessLayer
{
    public class IpmCauThuThuocDoiBongThamGiaGiaiDau : ICauThuThuocDoiBongThamGiaGiaiDau
    {
        QLGVDBDQGDataContext _context;
        public IpmCauThuThuocDoiBongThamGiaGiaiDau()
        {
            _context = new QLGVDBDQGDataContext();
        }
        public List<CauThuThuocDoiBongThamGiaGiaiDau> LoadCauThuThuocDoiBongThamGiaGiaiDau(string _maMuaGiai)
        {
            try
            {
                var _select =
                    from _ct in _context.GetTable<CauThuThuocDoiBongThamGiaGiaiDau>()
                    where _ct.MaGiaiDau == _maMuaGiai
                    select _ct;
                List<CauThuThuocDoiBongThamGiaGiaiDau> _listCauThu = new List<CauThuThuocDoiBongThamGiaGiaiDau>();
                foreach (var _cauThu in _select)
                {
                    _listCauThu.Add(_cauThu);
                }
                return _listCauThu;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<CauThuThuocDoiBongThamGiaGiaiDau> LoadCauThuThuocDoiBongThamGiaGiaiDau(string _maMuaGiai, string _maDoiBong)
        {
            try
            {
                var _select =
                    from _ct in _context.GetTable<CauThuThuocDoiBongThamGiaGiaiDau>()
                    where _ct.MaGiaiDau == _maMuaGiai &&
                    _ct.MaDoiBong == _maDoiBong
                    select _ct;
                List<CauThuThuocDoiBongThamGiaGiaiDau> _listCauThu = new List<CauThuThuocDoiBongThamGiaGiaiDau>();
                foreach (var _cauThu in _select)
                {
                    _listCauThu.Add(_cauThu);
                }
                return _listCauThu;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int InsertCauThuThuocDoiBongThamGiaGiaiDau(DTOCauThuThuocDoiBongThamGiaGiaiDau _newCauThu)
        {
            try
            {
                CauThuThuocDoiBongThamGiaGiaiDau _ct = new CauThuThuocDoiBongThamGiaGiaiDau();

                _ct.MaGiaiDau = _newCauThu.MaMuaGiai;
                _ct.TenCauThu = _newCauThu.TenCauThu;
                _ct.MaCauThu = _newCauThu.MaCauThu;
                _ct.MaDoiBong = _newCauThu.MaDoiBong;
                _ct.TenDoiBong = _newCauThu.TenDoiBong;
                _ct.MaLoaiCauThu = _newCauThu.MaLoaiCauThu;
                _ct.TongSoBanThang = 0;

                _context.CauThuThuocDoiBongThamGiaGiaiDaus.InsertOnSubmit(_ct);
                _context.SubmitChanges();
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int DeleteCauThuThuocDoiBongThamGiaGiaiDau(DTOCauThuThuocDoiBongThamGiaGiaiDau _cauThu)
        {
            try
            {
                if (_cauThu.MaDoiBong == "" || _cauThu.MaDoiBong == null)
                {
                    var _select =
                    from ct in _context.CauThuThuocDoiBongThamGiaGiaiDaus
                    where ct.MaCauThu == _cauThu.MaCauThu &&
                    ct.MaGiaiDau == _cauThu.MaMuaGiai
                    select ct;
                    if (_select.Count() == 0)
                    {
                        return -1;
                    }
                    foreach (var ct in _select)
                    {
                        _context.CauThuThuocDoiBongThamGiaGiaiDaus.DeleteOnSubmit(ct);
                    }
                    _context.SubmitChanges();
                    return 1;
                }
                else
                {
                    var _select =
                    from ct in _context.CauThuThuocDoiBongThamGiaGiaiDaus
                    where ct.MaDoiBong == _cauThu.MaDoiBong &&
                    ct.MaGiaiDau == _cauThu.MaMuaGiai
                    select ct;
                    if (_select.Count() == 0)
                    {
                        return -1;
                    }
                    foreach (var ct in _select)
                    {
                        _context.CauThuThuocDoiBongThamGiaGiaiDaus.DeleteOnSubmit(ct);
                    }
                    _context.SubmitChanges();
                    return 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
