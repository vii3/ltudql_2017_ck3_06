﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcessLayer
{
    public class IpmLoaiCauThu : ILoaiCauThu
    {
        QLGVDBDQGDataContext _context;
        public IpmLoaiCauThu()
        {
            _context = new QLGVDBDQGDataContext();
        }
        public List<LoaiCauThu> LoadDanhSachLoaiCauThu()
        {
            try
            {
                // Lấy danh sách LOẠI CẦU THỦ từ DataBase
                var _selectQuery =
                    from _db in _context.GetTable<LoaiCauThu>()
                    select _db;

                // Đưa danh sách ĐỘI BÓNG vào List<DoiBong> để thự hiện return List<DoiBong>
                List<LoaiCauThu> _danhSachLoaiCauThu = new System.Collections.Generic.List<LoaiCauThu>();
                foreach (var _lct in _selectQuery)
                {
                    _danhSachLoaiCauThu.Add(_lct);
                }
                return _danhSachLoaiCauThu;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
