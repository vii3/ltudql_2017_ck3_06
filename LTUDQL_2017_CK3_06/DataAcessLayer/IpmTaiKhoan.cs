﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DataAcessLayer
{
    public class IpmTaiKhoan : ITaiKhoan
    {
        QLGVDBDQGDataContext _context;
        public IpmTaiKhoan()
        {
            _context = new QLGVDBDQGDataContext();
        }
        public List<TaiKhoan> Load()
        {
            try
            {
                var select =
                    from tk in _context.GetTable<TaiKhoan>()
                    select tk;
                List<TaiKhoan> dsTaiKhoan = new List<TaiKhoan>();
                foreach(var tk in select)
                {
                    dsTaiKhoan.Add(tk);
                }
                return dsTaiKhoan;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int Insert(DTOTaiKhoan _newTaiKhoan)
        {
            try
            {
                TaiKhoan tk = new TaiKhoan();
                tk.TenDangNhap = _newTaiKhoan.TenDangNhap;
                tk.MaLoaiTaiKhoan = _newTaiKhoan.MaLoaiTaiKhoan;
                tk.MatKhau = _newTaiKhoan.MatKhau;

                _context.TaiKhoans.InsertOnSubmit(tk);
                _context.SubmitChanges();
                return 1;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
