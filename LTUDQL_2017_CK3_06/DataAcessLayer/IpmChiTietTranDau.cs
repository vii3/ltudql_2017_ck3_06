﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DataAcessLayer
{
    public class IpmChiTietTranDau : IChiTietTranDau
    {
        QLGVDBDQGDataContext _context;
        public IpmChiTietTranDau()
        {
            _context = new QLGVDBDQGDataContext();
        }
        public List<ChiTietTranDau> LoadChiTietTranDau(string _maMuaGiai)
        {
            try
            {
                var _selectLichThiDau =
                    from _ct in _context.GetTable<ChiTietTranDau>()
                    where _ct.MaMuaGiai == _maMuaGiai
                    select _ct;
                List<ChiTietTranDau> _danhSachTranDau = new List<ChiTietTranDau>();

                foreach (var _tranDau in _selectLichThiDau)
                {
                    _danhSachTranDau.Add(_tranDau);
                }

                return _danhSachTranDau;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int InsertChiTietTranDau(DTOChiTietTranDau _chiTietTranDau)
        {
            try
            {                
                ChiTietTranDau _newTranDau = new ChiTietTranDau();

                _newTranDau.MaMuaGiai = _chiTietTranDau.MaMuaGiai;
                _newTranDau.MaTranDau = _chiTietTranDau.MaTranDau;
                _newTranDau.MaVongDau = _chiTietTranDau.MaVongDau;
                _newTranDau.NgayThiDau = _chiTietTranDau.NgayThiDau;
                _newTranDau.GioThiDau = _chiTietTranDau.GioThiDau;
                _newTranDau.ChuNha = _chiTietTranDau.MaChuNha;
                _newTranDau.Khach = _chiTietTranDau.MaKhach;
                _newTranDau.DaThiDau = -1;


                // Khi thêm một thông tin trận đấu thì mặc định trận đấu CHƯA DIỄN RA
                // Khi trận đấu ĐÃ hoặc ĐANG DIỄN RA thì sẽ được cập nhật tại UpdateChiTietTranDau
                //_newTranDau.DaThiDau = _chiTietTranDau.DaThiDau;

                // Không thêm thông tin số bàn thắng chủ nhà và đội khách
                // Vì các thông tin này sẽ phải được cập nhật đồng bộ với thông tin tại bảng CHI TIẾT BÀN THẮNG
                //_newTranDau.SoBanThang_ChuNha = _chiTietTranDau.SoBanThangChuNha;
                //_newTranDau.SoBanThang_Khach = _chiTietTranDau.SoBanThangKhach;

                _context.ChiTietTranDaus.InsertOnSubmit(_newTranDau);                
                _context.SubmitChanges();
                
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int DeleteChiTietTranDau(string _maMuaGiai, string _maVongDau, string _maTranDau)
        {
            try
            {
                var _deleteTranDau =
                    from _lich in _context.ChiTietTranDaus
                    where _lich.MaMuaGiai == _maMuaGiai &&
                    _lich.MaVongDau == _maVongDau &&
                    _lich.MaTranDau == _maTranDau
                    select _lich;

                if (_deleteTranDau.Count() == 0)
                {
                    // Lịch thi đấu trống
                    return -1;
                }

                var _danhSachTranDau = _deleteTranDau.ToList();
                
                foreach(var _tranDau in _deleteTranDau)
                {
                    _context.ChiTietTranDaus.DeleteOnSubmit(_tranDau);
                }
                _context.SubmitChanges();

                
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int UpdateChiTietTranDau(string _maMuaGiai, string _maVongDau, string _maTranDau, DateTime _newNgayThiDau, TimeSpan _newGioThiDau)
        {
            try
            {
                var _selectTranDau =
                    from _tranDau in _context.ChiTietTranDaus
                    where _tranDau.MaMuaGiai == _maMuaGiai &&
                        _tranDau.MaTranDau == _maTranDau && 
                        _tranDau.MaVongDau == _maVongDau
                    select _tranDau;

                var _danhSachTranDau = _selectTranDau.ToList();
                foreach(var _tran in _danhSachTranDau)
                {
                    _tran.NgayThiDau = _newNgayThiDau;
                    _tran.GioThiDau = _newGioThiDau;
                }

                try
                {
                    _context.SubmitChanges();
                }
                catch(Exception ex)
                {
                    throw ex;
                }

                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int UpdateChiTietTranDau(string _maMuaGiai, string _maVongDau, string _maTranDau, int _daThiDau)
        {
            try
            {
                var _selectLichThiDau =
                    from _tranDau in _context.ChiTietTranDaus
                    where _tranDau.MaMuaGiai == _maMuaGiai &&
                        _tranDau.MaTranDau == _maTranDau &&
                        _tranDau.MaVongDau == _maVongDau
                    select _tranDau;

                var _danhSachTranDau = _selectLichThiDau.ToList();
                foreach (var _tran in _danhSachTranDau)
                {
                    _tran.DaThiDau = _daThiDau;
                }

                try
                {
                    _context.SubmitChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
