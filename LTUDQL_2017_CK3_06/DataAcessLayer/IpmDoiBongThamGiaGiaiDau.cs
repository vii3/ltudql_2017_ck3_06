﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DataAcessLayer
{
    public class IpmDoiBongThamGiaGiaiDau : IDoiBongThamGiaGiaiDau
    {
        QLGVDBDQGDataContext _context;
        public IpmDoiBongThamGiaGiaiDau()
        {
            _context = new QLGVDBDQGDataContext();
        }
        public List<DoiBongThamGiaGiaiDau> LoadDanhSachDoiBong()
        {
            try
            {
                var _select =
                    from db in _context.GetTable<DoiBongThamGiaGiaiDau>()
                    select db;
                List<DoiBongThamGiaGiaiDau> dsDoiBong = new List<DoiBongThamGiaGiaiDau>();
                foreach (var db in _select)
                {
                    dsDoiBong.Add(db);
                }
                return dsDoiBong;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<DoiBongThamGiaGiaiDau> LoadDanhSachDoiBong(string _maMuaGiai)
        {
            try
            {
                var _select =
                    from db in _context.GetTable<DoiBongThamGiaGiaiDau>()
                    where db.MaGiaiDau == _maMuaGiai
                    select db;
                List<DoiBongThamGiaGiaiDau> dsDoiBong = new List<DoiBongThamGiaGiaiDau>();
                foreach (var db in _select)
                {
                    dsDoiBong.Add(db);
                }
                return dsDoiBong;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<DoiBongThamGiaGiaiDau> LoadDanhSachDoiBong(string _maMuaGiai, string _maDoiBong)
        {
            try
            {
                var _select =
                    from db in _context.GetTable<DoiBongThamGiaGiaiDau>()
                    where db.MaGiaiDau == _maMuaGiai &&
                    db.MaDoiBong == _maDoiBong
                    select db;
                List<DoiBongThamGiaGiaiDau> dsDoiBong = new List<DoiBongThamGiaGiaiDau>();
                foreach (var db in _select)
                {
                    dsDoiBong.Add(db);
                }
                return dsDoiBong;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int InsertDoiBong(DTODoiBongThamGiaGiaiDau _newDoiBong)
        {
            try
            {
                DoiBongThamGiaGiaiDau db = new DoiBongThamGiaGiaiDau();
                db.MaGiaiDau = _newDoiBong.MaMuaGiai;
                db.MaDoiBong = _newDoiBong.MaDoiBong;
                db.TenDoiBong = _newDoiBong.TenDoiBong;
                db.SoLuongCauThuDangKy = _newDoiBong.SoLuongCauThuDangKy;
                db.SoLuongCauThuNuocNgoaiDangKy = _newDoiBong.SoLuongCauThuNuocNgoai;
                db.HoanTatDangKy = false;

                _context.DoiBongThamGiaGiaiDaus.InsertOnSubmit(db);
                _context.SubmitChanges();

                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int UpdateDoiBong(string _maMuaGiai, string _maDoiBong, bool _hoanTatDangKy)
        {
            try
            {
                var _select =
                    from db in _context.DoiBongThamGiaGiaiDaus
                    where db.MaGiaiDau == _maMuaGiai &&
                    db.MaDoiBong == _maDoiBong
                    select db;

                var dsDoiBong = _select.ToList();
                foreach (var db in dsDoiBong)
                {
                    db.HoanTatDangKy = _hoanTatDangKy;
                }
                _context.SubmitChanges();
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int DeleteDoiBong(string _maMuaGiai, string _maDoiBong)
        {
            try
            {
                var _select
                = from db in _context.DoiBongThamGiaGiaiDaus
                  where db.MaGiaiDau == _maMuaGiai &&
                  db.MaDoiBong == _maDoiBong
                  select db;
                if (_select.Count() == 0)
                {
                    return -1;
                }
                foreach (var db in _select)
                {
                    _context.DoiBongThamGiaGiaiDaus.DeleteOnSubmit(db);
                }
                _context.SubmitChanges();
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
