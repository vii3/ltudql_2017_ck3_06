﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcessLayer
{
    public interface ISoLieuThongKeDoiBong
    {
        List<SoLieuThongKeDoiBong> LoadSoLieuThongKe();
        List<SoLieuThongKeDoiBong> LoadSoLieuThongKe(string _maMuaGiai);
        List<SoLieuThongKeDoiBong> LoadSoLieuThongKe(string _maMuaGiai, string _maVongDau);
    }
}
