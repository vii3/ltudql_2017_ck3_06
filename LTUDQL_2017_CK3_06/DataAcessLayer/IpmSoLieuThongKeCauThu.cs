﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DataAcessLayer
{
    public class IpmSoLieuThongKeCauThu : ISoLieuThongKeCauThu
    {
        QLGVDBDQGDataContext _context = new QLGVDBDQGDataContext();
        public IpmSoLieuThongKeCauThu()
        {
            _context = new QLGVDBDQGDataContext();
        }
        public List<SoLieuThongKeCauThu> LoadSoLieuThongKe()
        {
            try
            {
                var _soLieuThongKe =
                    from _soLieu in _context.GetTable<SoLieuThongKeCauThu>()
                    select _soLieu;
                List<SoLieuThongKeCauThu> _danhSachSoLieuThongKeCauThu = new List<SoLieuThongKeCauThu>();
                foreach (var _thongKe in _soLieuThongKe)
                {
                    _danhSachSoLieuThongKeCauThu.Add(_thongKe);
                }
                return _danhSachSoLieuThongKeCauThu;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<SoLieuThongKeCauThu> LoadSoLieuThongKe(string _maMuaGiai)
        {
            try
            {
                var _soLieuThongKe =
                    from _soLieu in _context.GetTable<SoLieuThongKeCauThu>()
                    where _soLieu.MaMuaGiai == _maMuaGiai
                    select _soLieu;
                List<SoLieuThongKeCauThu> _danhSachSoLieuThongKeCauThu = new List<SoLieuThongKeCauThu>();
                foreach (var _thongKe in _soLieuThongKe)
                {
                    _danhSachSoLieuThongKeCauThu.Add(_thongKe);
                }
                return _danhSachSoLieuThongKeCauThu;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<SoLieuThongKeCauThu> LoadSoLieuThongKe(string _maMuaGiai, string _maVongDau)
        {
            try
            {
                var _soLieuThongKe =
                    from _soLieu in _context.GetTable<SoLieuThongKeCauThu>()
                    where _soLieu.MaMuaGiai == _maMuaGiai &&
                    _soLieu.MaVongDau == _maVongDau
                    select _soLieu;
                List<SoLieuThongKeCauThu> _danhSachSoLieuThongKeCauThu = new List<SoLieuThongKeCauThu>();
                foreach (var _thongKe in _soLieuThongKe)
                {
                    _danhSachSoLieuThongKeCauThu.Add(_thongKe);
                }
                return _danhSachSoLieuThongKeCauThu;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int Update(DTOSoLieuThongKeCauThu cauThu)
        {
            try
            {
                var _select =
                    from ct in _context.SoLieuThongKeCauThus
                    where ct.MaMuaGiai == cauThu.MaMuaGiai
                    && ct.MaVongDau ==cauThu.MaVongDau
                    && ct.MaCauThu == cauThu.MaCauThu
                    select ct;
                foreach (var ct in _select)
                {
                    ct.SoBanThang = cauThu.SoBanThang;
                    ct.TongSoBanThang = cauThu.TongSoBanThang;
                    ct.Hang = cauThu.Hang;
                }
                _context.SubmitChanges();
                return 1;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
