﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DataAcessLayer
{
    public interface IDoiBongThamGiaGiaiDau
    {
        List<DoiBongThamGiaGiaiDau> LoadDanhSachDoiBong();
        List<DoiBongThamGiaGiaiDau> LoadDanhSachDoiBong(string _maMuaGiai);
        List<DoiBongThamGiaGiaiDau> LoadDanhSachDoiBong(string _maMuaGiai, string _maDoiBong);
        int InsertDoiBong(DTODoiBongThamGiaGiaiDau _newDoiBong);
        int UpdateDoiBong(string _maMuaGiai, string _maDoiBong, bool _hoanTatDangKy);
        int DeleteDoiBong(string _maMuaGiai, string _maDoiBong);
    }
}
