﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DataAcessLayer
{
    public interface IChiTietTranDau
    {
        // KHÔNG KHUYẾN KHÍCH THAO TÁC TRÊN CHI TIẾT TRẬN ĐẤU
        // NÊN XỬ LÝ TRÊN BUSINESS VIỆC TẠO LỊCH THI ĐẤU -> TẠO THÀNH BẢNG CHI TIẾT TRẬN ĐẤU
        List<ChiTietTranDau> LoadChiTietTranDau(string _maMuaGiai);
        // Lấy Toàn bộ thông tin TRẬN ĐẤU từ một MÙA GIẢI cụ thể

        int InsertChiTietTranDau(DTOChiTietTranDau _chiTietTranDau);
        // Insert 1 CHI TIẾT TRẬN ĐẤU - Dữ liệu được lấy từ LỊCH THI ĐẤU

        int DeleteChiTietTranDau(string _maMuaGiai, string _maVongDau, string _maTranDau);
        // KHÔNG KHUYẾN KHÍCH TRỪ CÁC TRƯỜNG HỢP ĐẶC BIỆT CỤ THỂ LIÊN QUAN TỚI THAY ĐỔI QUY ĐỊNH GIẢI ĐẤU
        // Dựa vào khóa chính của bảng CHI TIẾT TRẬN ĐẤU  bao gồm MÃ MÙA GIẢI, MÃ VÒNG ĐẤU, MÃ TRẬN ĐẤU
        // -> Xóa thông tin TRẬN ĐẤU được đang xác định

        int UpdateChiTietTranDau(string _maMuaGiai, string _maVongDau, string _maTranDau, DateTime _newNgayThiDau, TimeSpan _newGioThiDau);
        // Dựa vào khóa chính của bảng CHI TIẾT TRẬN ĐẤU  bao gồm MÃ MÙA GIẢI, MÃ VÒNG ĐẤU, MÃ TRẬN ĐẤU
        // -> Cập nhật lại thời gian thi đấu cụ thể cho các TRẬN ĐẤU nếu có thay đổi trong quá trình diễn ra giải đấu

        int UpdateChiTietTranDau(string _maMuaGiai, string _maVongDau, string _maTranDau, int _daThiDau);
        // Nếu TRẬN ĐẤU đã diễn ra hoặc đang diễn ra có thể thực hiện update các thuộc tính
        // Đã thi đấu: 1 ----- Chưa thi đấu: -1

        // Hai thuộc tính SỐ BÀN THẮNG CHỦ NHÀ và SỐ BÀN THẮNG ĐỘI KHÁCH sẽ không được phép cập nhật thủ công
    }
}
