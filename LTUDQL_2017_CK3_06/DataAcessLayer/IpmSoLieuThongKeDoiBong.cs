﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcessLayer
{
    public class IpmSoLieuThongKeDoiBong : ISoLieuThongKeDoiBong
    {
        QLGVDBDQGDataContext _context;
        public IpmSoLieuThongKeDoiBong()
        {
            _context = new QLGVDBDQGDataContext();
        }

        public List<SoLieuThongKeDoiBong> LoadSoLieuThongKe()
        {
            try
            {
                var _soLieuThongKe =
                    from _soLieu in _context.GetTable<SoLieuThongKeDoiBong>()
                    select _soLieu;
                List<SoLieuThongKeDoiBong> _danhSachSoLieuThongKeDoiBong = new List<SoLieuThongKeDoiBong>();
                foreach(var _thongKe in _soLieuThongKe)
                {
                    _danhSachSoLieuThongKeDoiBong.Add(_thongKe);
                }
                return _danhSachSoLieuThongKeDoiBong;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        public List<SoLieuThongKeDoiBong> LoadSoLieuThongKe(string _maMuaGiai)
        {
            try
            {
                var _soLieuThongKe =
                    from _soLieu in _context.GetTable<SoLieuThongKeDoiBong>()
                    where _soLieu.MaMuaGiai == _maMuaGiai
                    select _soLieu;
                List<SoLieuThongKeDoiBong> _danhSachSoLieuThongKeDoiBong = new List<SoLieuThongKeDoiBong>();
                foreach (var _thongKe in _soLieuThongKe)
                {
                    _danhSachSoLieuThongKeDoiBong.Add(_thongKe);
                }
                return _danhSachSoLieuThongKeDoiBong;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<SoLieuThongKeDoiBong> LoadSoLieuThongKe(string _maMuaGiai, string _maVongDau)
        {
            try
            {
                var _soLieuThongKe =
                    from _soLieu in _context.GetTable<SoLieuThongKeDoiBong>()
                    where _soLieu.MaMuaGiai == _maMuaGiai &&
                    _soLieu.MaVongDau == _maVongDau
                    select _soLieu;
                List<SoLieuThongKeDoiBong> _danhSachSoLieuThongKeDoiBong = new List<SoLieuThongKeDoiBong>();
                foreach (var _thongKe in _soLieuThongKe)
                {
                    _danhSachSoLieuThongKeDoiBong.Add(_thongKe);
                }
                return _danhSachSoLieuThongKeDoiBong;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
