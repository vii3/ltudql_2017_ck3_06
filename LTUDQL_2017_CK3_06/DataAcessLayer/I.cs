﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;


namespace DataAcessLayer
{
    public interface IKetQua
    {
        List<KetQua> LoadKetQuaTranDau();
        int InsertKetQuaTranDau(DTOKetQua _newKetQua);
        int DeleteKetQuaTranDau(DTOKetQua _tranDau);
        int UpdateKetQuaTrauDau(DTOKetQua _ketQua, string _newCauThu, string _newLoaiBanThang, TimeSpan _newThoiDiem);
    }
}
