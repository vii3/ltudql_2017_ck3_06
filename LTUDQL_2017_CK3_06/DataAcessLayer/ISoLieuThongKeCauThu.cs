﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DataAcessLayer
{
    public interface ISoLieuThongKeCauThu
    {
        List<SoLieuThongKeCauThu> LoadSoLieuThongKe();
        List<SoLieuThongKeCauThu> LoadSoLieuThongKe(string _maMuaGiai);
        List<SoLieuThongKeCauThu> LoadSoLieuThongKe(string _maMuaGiai, string _maVongDau);
        int Update(DTOSoLieuThongKeCauThu cauThu);
    }
}
