﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DataAcessLayer
{
    public interface IDoiBong
    {
        List<DoiBong> LoadDanhSachDoiBong();

        void InsertDoiBong(DTODoiBong _doiBong);
        // Thêm ĐỘI BÓNG với thông tin được khởi tạo từ DTODoiBong

        
        void InsertDoiBong(string _maDoiBong, string _tenDoiBong, string _sanNha);
        // Thêm ĐỘI BÓNG mới vào DataBase
        // Khởi tạo ĐỘI BÓNG với các thuộc tính: Mã ĐỘI BÓNG, Tên ĐỘI BÓNG, Tên SÂN NHÀ, GHI CHÚ **
        // GHI CHÚ lưu thông tin ĐỘI BÓNG đã hoàn tất đăng ký hay chưa
        // ghiChu == "Hoàn tất" -> Hoàn tất
        // else -> Chưa hoàn tất

        int DeleteDoiBong(string _maDoiBong, int _option);
        // Delete ĐỘI BÓNG dựa vào mã ĐỘI BÓNG
        // option = 11: XÓA VĨNH VIỄN ĐỘI BÓNG
        // option != 11: TẠM XÓA ĐỘI BÓNG - UPDATE ĐÃ XÓA = TRUE

        int UpdateDoiBong(string _maDoiBong, DTODoiBong _newDoiBong);
        // Update thông tin ĐỘI BÓNG
        // _newDoiBong chỉ cập nhật Tên ĐỘI BÓNG và SÂN NHÀ, CÁC THUỘC TÍNH KHÁC NẾU CÓ CUNG CẤP CŨNG SẼ KHÔNG TIẾN HÀNH UPDATE

        int UpdateDoiBong(string _maDoiBong, int _option, string _newThuocTinh);
        // Update thông tin ĐỘI BÓNG
        // Option = 1: Thay đổi Tên ĐỘI BÓNG
        // Option = 2: Thay đổi Tên SÂN NHÀ
        // _newThuocTinh: Thông tin cần thay đổi
        int UpdateDoiBong(string _maDoiBong, bool _ghiChu);
        // Update thông tin ĐỘI BÓNG
        // Update lại việc ĐĂNG KÝ ĐỘI BÓNG
        // GHI CHÚ = TRUE - HOÀN TẤT ĐĂNG KÝ
        // GHI CHÚ = FALSE - CHƯA HOÀN TẤT ĐĂNG KÝ
    }
}
