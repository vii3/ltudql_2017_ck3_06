﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;


namespace DataAcessLayer
{
    public interface IChiTietBanThang
    {
        List<ChiTietBanThang> LoadChiTietBanThang(string _maMuaGiai);
        // Lấy toàn bộ thông tin các BÀN THẮNG trong một MÙA GIẢI cụ thể

        List<ChiTietBanThang> LoadChiTietBanThang(string _maMuaGiai, string _maVongDau);
        // Lấy toàn bộ thông tin các BÀN THẮNG trong một VÒNG ĐẤU của một MÙA GIẢI cụ thể

        List<ChiTietBanThang> LoadChiTietBanThang(string _maMuaGiai, string _maVongDau, string _maTranDau);
        // Lấy toàn bộ thông tin các BÀN THẮNG tại một TRẬN ĐẤU cụ thể

        int InsertChiTietBanThang(DTOChiTietBanThang _newBanThang);
        // Kiểm tra tồn tại CẦU THỦ trong TRẬN ĐẤU đang xét hay không
        // Nếu tồn tại thì tiến hành UPDATE lại thông tin LOẠI BÀN THẮNG và THỜI ĐIỂM GHI BÀN
        // Sau khi UPDATE tiến hành INSERT lại thông tin CẦU THỦ với LOẠI BÀN THẮNG = -1 và THỜI ĐIỂM = 0:0:0
        // Nếu không tồn tại -> return -1

        int DeleteChiTietBanThang(DTOChiTietBanThang _banThang);
        int UpdateChiTietBanThang(DTOChiTietBanThang _banThang, DTOChiTietBanThang _newBanThang);
    }
}
