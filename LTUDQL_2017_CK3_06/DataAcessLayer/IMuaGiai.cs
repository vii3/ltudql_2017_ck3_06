﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DataAcessLayer
{
    public interface IMuaGiai
    {
        List<MuaGiai> LoaiDanhSachMuaGiai();
        int InsertMuaGiai(DTOMuaGiai _newMuaGiai);
        int UpdateMuaGiai(string _maMuaGiai, DTOMuaGiai _newThongTin);
    }
}
