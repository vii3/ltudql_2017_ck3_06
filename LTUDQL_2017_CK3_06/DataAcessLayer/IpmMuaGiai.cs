﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DataAcessLayer
{
    public class IpmMuaGiai : IMuaGiai
    {
        QLGVDBDQGDataContext _context;
        public IpmMuaGiai()
        {
            _context = new QLGVDBDQGDataContext();
        }
        public List<MuaGiai> LoaiDanhSachMuaGiai()
        {
            try
            {
                // Lấy danh sách MÙA GIẢI từ DataBase
                var _selectQuery =
                    from _db in _context.GetTable<MuaGiai>()
                    select _db;

                // Đưa danh sách ĐỘI BÓNG vào List<DoiBong> để thự hiện return List<DoiBong>
                List<MuaGiai> _danhSachMuaGiai = new System.Collections.Generic.List<MuaGiai>();
                foreach (var _mg in _selectQuery)
                {
                    _danhSachMuaGiai.Add(_mg);
                }
                return _danhSachMuaGiai;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int InsertMuaGiai(DTOMuaGiai _newMuaGiai)
        {
            try
            {
                MuaGiai _mg = new MuaGiai();
                _mg.MaMuaGiai = _newMuaGiai.MaMuaGiai;
                _mg.TenMuaGiai = _newMuaGiai.TenMuaGiai;
                _mg.SoLuongDoiThamDuToiDa = _newMuaGiai.SoLuongDoiThamDuToiDa;
                _mg.SoLuongDoiDaDangKy = 0;
                _mg.HoanTatDangKy = false;

                _context.MuaGiais.InsertOnSubmit(_mg);
                _context.SubmitChanges();

                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int UpdateMuaGiai(string _maMuaGiai, DTOMuaGiai _newThongTin)
        {
            try
            {
                var _select =
                    from _mg in _context.MuaGiais
                    where _mg.MaMuaGiai == _maMuaGiai
                    select _mg;
                foreach (var _mg in _select)
                {
                    _mg.TenMuaGiai = _newThongTin.TenMuaGiai;
                    _mg.SoLuongDoiThamDuToiDa = _newThongTin.SoLuongDoiThamDuToiDa;
                    _mg.HoanTatDangKy = _newThongTin.HoanTatDangKy;
                }
                _context.SubmitChanges();
                //_context.

                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
