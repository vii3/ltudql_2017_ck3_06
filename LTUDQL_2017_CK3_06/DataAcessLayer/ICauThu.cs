﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DataAcessLayer
{
    public interface ICauThu
    {
        List<CauThu> LoadDanhSachCauThu();
        // Load danh sách toàn bộ CẦU THỦ

        List<CauThu> LoadDanhSachCauThu(string _maDoiBong);
        void InsertCauThu(DTOCauThu _newCauThu);
        // Insert CẦU THỦ dựa trên DTOCauThu

        void InsertCauThu(string _maCauThu, string _tenCauThu, DateTime _ngaySinh, int _maLoaiCauThu, string _maDoiBong);
        // Insert CẦU THỦ dựa trên mã CẦU THỦ, họ tên CẦU THỦ, ngày sinh, loại CẦU THỦ, mã đội bóng (Tất cả thuộc tính)
        
        int DeleteCauThu(string _maCauThu, int _option);
        // Xóa CẦU THỦ dựa vào Mã CẦU THỦ
        // option 11: XÓA VĨNH VIỄN CẦU THỦ
        // option != 11: TẠM XÓA CẦU THỦ - UPDATE thuộc tính ĐÃ XÓA = TRUE

        int UpdateCauThu(string _maCauThu, DTOCauThu _newCauThu);
        // Update dựa trên DTOCauThu
        
        int UpdateCauThu(string _maCauThu, int _option, string _newThuocTinh);
        // Update các thuộc tính kiểu string C# (char, varchar, nvarchar Sql server)
        // Option 1: Update Họ tên CẦU THỦ
        // Option 2: Update Mã ĐỘI BÓNG
        // Option != 1 or Option != 2 return -1

        int UpdateCauThu(string _maCauThu, int _newLoaiCauThu);
        // Update Loại cầu thủ
        // Mã loại cầu thủ
        // 1: Việt Nam
        // 2: Nước ngoài
      
        int UpdateCauThu(string _maCauThu, DateTime _newNgaySinh);
        // Update Ngày sinh
    }
}
