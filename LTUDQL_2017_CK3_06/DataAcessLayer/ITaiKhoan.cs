﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DataAcessLayer
{
    public interface ITaiKhoan
    {
        List<TaiKhoan> Load();
        int Insert(DTOTaiKhoan _newTaiKhoan);
    }
}
